cx_data = xlsread('Z:\Data\goura\FigureUpdates\RawData_Excels\L4_RawData_24-01-2020.xlsx','Sheet2');
cx_thisColmap = colormap(jet);%colormap(gray);
cx_thisColmap(1,:) = [0 0 0];
cx_postSel = [7 5 6]
cx_preSequ = [3 1 2]
cx_ageSequ = [7 9 14 28]
% cx_ageSequ = [9 14 28]
cx_structList = {'AD','AIS','soma','Spine','Shaft','Glia','Som. Fil.'}
figure
for i=1:length(cx_ageSequ)  
    i
    %subplot(5,1,i)
    figure
    cx_thisSel = find(cx_data(:,1)==cx_ageSequ(i));
    cx_thismx = cx_data(cx_thisSel,cx_postSel);
    cx_thismx(:,end+1) = 1-sum(cx_thismx,2);
    cx_thismx_show = (cx_thismx(cx_preSequ,:));
    cx_thismx_show(isinf(cx_thismx_show))=-5;
    cx_Clim = [-2 0];
    cx_thismx_show = cx_thismx(cx_preSequ,:);
    cx_Clim=[0 1];
    imagesc(cx_thismx_show);hold on;daspect([1 1 1]);
    set(gca,'CLim',cx_Clim);
%     set(gca,'ColorScale','log')
    colormap(cx_thisColmap);colorbar
    set(gca,'YTick',[1:length(cx_preSequ)],'YTickLabel',cx_structList(cx_data(cx_thisSel(cx_preSequ),3)),'XTick',[1:length(cx_postSel)+1],'XTickLabel',{cx_structList{cx_postSel-4},'rest'});
    title(sprintf('P%d, n= %d,%d,%d syn',cx_ageSequ(i),cx_data(cx_thisSel,4)));
end
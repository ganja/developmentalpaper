% PLOTTING CONNECTOMES - 06-02-2020

data = []; % load data rom excel tables

%% Sorting by syn counts
[x1,y1] = find(data==1); % for 1 syn BLUE
[x2,y2] = find(data==2); % for 2 syn LIGHT BLUE
[x3,y3] = find(data==3); % for 3 syn CYAN
[x4,y4] = find(data==4); % for 4 syn BET CYAN GREEN
[x5,y5] = find(data==5); % 5 syn GREEN
[x7,y7] = find(data>5 & data<10); % 5 - 10 syn GREEN YELLOW
[x10,y10] = find(data>=10 & data < 50); %10 to 50 YELLO
[x50,y50] = find(data>=50 & data < 100); % 50 to 100 ORANGE
[x100,y100] = find(data>=100); % >100 syn RED

load('Z:\Data\goura\FigureUpdates\Connectomes\AG_Colormap.mat');

figure;
scatter(x1,y1,'o','filled','MarkerEdgeColor',AG_cmap(1,:),'MarkerFaceColor',AG_cmap(1,:),'SizeData',8);
hold on
scatter(x2,y2,'o','filled','MarkerEdgeColor',AG_cmap(2,:),'MarkerFaceColor',AG_cmap(2,:),'SizeData',8);
scatter(x3,y3,'o','filled','MarkerEdgeColor',AG_cmap(3,:),'MarkerFaceColor',AG_cmap(3,:),'SizeData',8);
scatter(x4,y4,'o','filled','MarkerEdgeColor',AG_cmap(4,:),'MarkerFaceColor',AG_cmap(4,:),'SizeData',8);
scatter(x5,y5,'o','filled','MarkerEdgeColor',AG_cmap(5,:),'MarkerFaceColor',AG_cmap(5,:),'SizeData',8);
scatter(x7,y7,'o','filled','MarkerEdgeColor',AG_cmap(6,:),'MarkerFaceColor',AG_cmap(6,:),'SizeData',8);
scatter(x10,y10,'o','filled','MarkerEdgeColor',AG_cmap(7,:),'MarkerFaceColor',AG_cmap(7,:),'SizeData',8);
scatter(x50,y50,'o','filled','MarkerEdgeColor',AG_cmap(8,:),'MarkerFaceColor',AG_cmap(8,:),'SizeData',8);
scatter(x100,y100,'o','filled','MarkerEdgeColor',AG_cmap(9,:),'MarkerFaceColor',AG_cmap(9,:),'SizeData',8);
set(gca,'TickDir','out')
set(gca,'Color','k')
set(gca,'YAxisLocation','right')
set(gca,'Ylim',[0 (length(data)+1)]);
set(gca,'Xlim',[0.5 (numel(data(:,1))+0.5)]);
camroll(-90)
ylabel('Postsynaptic targets')
xlabel('Presynaptic axons')
title('P7 L4 - Conectome')
%% size data 10
figure;
scatter(x1,y1,'o','filled','MarkerEdgeColor',AG_cmap(1,:),'MarkerFaceColor',AG_cmap(1,:),'SizeData',10);
hold on
scatter(x2,y2,'o','filled','MarkerEdgeColor',AG_cmap(2,:),'MarkerFaceColor',AG_cmap(2,:),'SizeData',10);
scatter(x3,y3,'o','filled','MarkerEdgeColor',AG_cmap(3,:),'MarkerFaceColor',AG_cmap(3,:),'SizeData',10);
scatter(x4,y4,'o','filled','MarkerEdgeColor',AG_cmap(4,:),'MarkerFaceColor',AG_cmap(4,:),'SizeData',10);
scatter(x5,y5,'o','filled','MarkerEdgeColor',AG_cmap(5,:),'MarkerFaceColor',AG_cmap(5,:),'SizeData',10);
scatter(x7,y7,'o','filled','MarkerEdgeColor',AG_cmap(6,:),'MarkerFaceColor',AG_cmap(6,:),'SizeData',10);
scatter(x10,y10,'o','filled','MarkerEdgeColor',AG_cmap(7,:),'MarkerFaceColor',AG_cmap(7,:),'SizeData',10);
scatter(x50,y50,'o','filled','MarkerEdgeColor',AG_cmap(8,:),'MarkerFaceColor',AG_cmap(8,:),'SizeData',10);
scatter(x100,y100,'o','filled','MarkerEdgeColor',AG_cmap(9,:),'MarkerFaceColor',AG_cmap(9,:),'SizeData',10);
set(gca,'TickDir','out')
set(gca,'Color','k')
camroll(-90)
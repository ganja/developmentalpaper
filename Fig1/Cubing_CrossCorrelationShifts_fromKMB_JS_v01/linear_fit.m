function out=linear_fit(coeff,X,Y)
 a = coeff(1);
 b = coeff(2);
 
 Y_fun = a*X +b;
 DIFF = Y_fun - Y; 
 SQ_DIFF = DIFF.^2;

 out = sum(SQ_DIFF);
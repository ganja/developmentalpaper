function annotateCCs(C,ttemp)

list=dir([C.matdir '*.tif']);
mkdir([C.matdir 'analysis']);
for i=1:length(list)
    fn=list(i).name;
    fn2=regexp(fn,'_','split');
    m=str2num(fn2{3}(2:end));
    n=str2num(fn2{4}(2:end));
    p=str2num(fn2{5}(2:end));
    di=str2num(fn2{6}(4:end));
    targetstring=sprintf('%f %f %f %f %f',ttemp.alldeltaResult{di}(m,n,p,1)-ttemp.alldeltaErr{di}(m,n,p,1),ttemp.alldeltaResult{di}(m,n,p,2)-ttemp.alldeltaErr{di}(m,n,p,2),ttemp.alldeltaErr{di}(m,n,p,1),ttemp.alldeltaErr{di}(m,n,p,2),ttemp.weight{di}(m,n,p));
    copyfile([C.matdir fn],[C.matdir 'analysis' filesep fn(1:end-4) targetstring '.tif']);
end

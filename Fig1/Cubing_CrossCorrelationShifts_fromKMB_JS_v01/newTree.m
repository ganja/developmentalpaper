%function newTree()
limit=10;
prelim=zeros(C.M,C.N,1,2);
for m=1:C.M
    for n=1:C.N
        prelim(m,n,1,:)=permute(C.dims.*([m n]-1),[1 4 3 2]);
    end
end

xycoords=cat(4,repmat(prelim,[1 1 C.P 1])+fix(ttemp.shiftsN)+1, 1000*repmat(permute(1:C.P,[1 3 2]),[C.M C.N 1]));
xycoords=reshape(xycoords,C.M*C.N*C.P,3);
rev=repmat((1:C.M*C.N*C.P)',[1, 3]);
rev=reshape(rev,[C.M,C.N,C.P,3]);
rev=rev(:,:,:,1);
eye3=eye(3);
di=[C.M,C.N,C.P];
edgez=cell(1,2);
for dire=1:3
    target=([di-eye3(dire,:)]);
    tttt=[];
    for wf=0:1
        ttt=[];
        for dirf=1:3
            
            x=permute(1:target(dirf),[1 4 3 2]);
            if (dire==dirf)
                x=x+wf;
            end
            x=(x-(dirf>1))*prod(di(1:dirf-1));
            p=1:4;
            p(dirf)=4;
            p(4)=dirf;
            xx=permute(x,p);
            ttt=cat(4,ttt,repmat(xx,target-(eye3(dirf,:).*(target-1))));
        end
        tttt=cat(5,tttt,ttt);
    end
    
    t5=reshape(squeeze(sum(tttt,4)),[],2);
    errs=reshape(sqrt(sum(abs(ttemp.alldeltaErrW{dire}).^2,4)),prod(target),1);
    edgez{1}=[edgez{1};t5(errs<limit,:)];
    edgez{2}=[edgez{2};t5(errs>limit,:)];
end
addpath KLEE
skel={};
skel{1}{1}.nodes=xycoords;
skel{1}{1}.edges=edgez{1};
skel{2}{1}.nodes=xycoords;
skel{2}{1}.edges=edgez{2};

KLEEskeleton_exportToAmira_many_v4(skel,[C.matdir 'amira'],'document_shifts');


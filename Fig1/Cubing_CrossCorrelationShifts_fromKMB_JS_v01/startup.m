function [C D]=startup()
addpath('stackSpecific');
[C D]=st_caller();

disp(C.cubingName);

C.knumberstr=[C.samplestr '_' C.cubingName];
C.cubefn = strcat(C.datestr,'_',C.knumberstr);
% create folder for the shifts.mat and other files generated in measuring the
% shifts
C.rootf=fullfile(C.machineroot,C.projecttype);

%create folder for the shifs.mat and other files generated in measuring the
%shifts
C.matdir=fullfile(C.rootf,'results',C.cubefn,'mat',filesep);
C.cubemaindir=fullfile(C.rootf,'results',C.cubefn,'cubes',filesep);
C.cubedir=fullfile(C.rootf,'results',C.cubefn,'cubes','color',filesep);
C.tifdir=fullfile(C.rootf,'results',C.cubefn,'newtifs',filesep);

if C.defaultOrigin
    C.origin=fullfile(C.rootf,'tifs',filesep);
end

% create all output folders (to ensure file access is not denied)
% create mat folder
mkdir(C.matdir);
if isunix() && (strcmp(C.parallel_mode, 'scheduler'))
    system(['chmod 777 ' C.matdir]);
end
% create cube folder
if isempty(C.outputToDo) || ~isempty(strfind(C.outputToDo,'cubes')) || ~isempty(strfind(C.outputToDo,'high'))
    mkdir(C.cubedir);
end
% create tif folder
if isempty(C.outputToDo) || ~isempty(strfind(C.outputToDo,'tifs'))
    mkdir(C.tifdir);
end

% define cube size
C.cubesize=128;

% make file list
if C.tifNamingSchemePiezoFEI
    [all_fns,D.imageModifier]=makeFileListWrapper(C);
else
    if strcmp(C.imdatestr(1:4),'auto')
        fn_pre=regexp(C.makeFileListFun(C,C.origin,C.sourceEnding,true),'auto','split');
        list=dir([fn_pre{1} '*' C.sourceEnding]);
        C.imdatestr=list(1).name(1:10);
    end
    
    all_fns=C.makeFileListFun(C,C.origin,C.sourceEnding,false);
end

% exclude damaged slices
for i=1:length(C.exclude)
    x=C.exclude(i);
    if C.exludeKeepCounting
        numOfSpacings=size(all_fns,3)-diff(D.C.allslice_arr(2:3))-1;
        f=@(a)cat(3,a(:,:,1:x-1,:),a(:,:,x-1:end-1-numOfSpacings,:),cell(size(a(:,:,1:numOfSpacings,:))));
        
    else
        f=@(a)cat(3,a(:,:,1:x-1-(i-1),:),a(:,:,x+1-(i-1):end,:),cell(size(a(:,:,end,:))));
        
    end
    all_fns=f(all_fns);
    D.imageModifier=f(D.imageModifier);
    % D.preShiftsLongitudinal=f(D.preShiftsLongitudinal);
    
end



% further changes to all_fns
D.all_fns=D.all_fns_Modifier(all_fns);
D.imageModifier=D.all_fns_Modifier(D.imageModifier);
D.preShiftsLongitudinal=D.all_fns_Modifier(D.preShiftsLongitudinal);
if ~iscell(C.weightarray) && strcmp(C.weightarray,'auto')
    C.weightarray=cell(3,1);
    C.weightarray{1}=zeros(C.N,C.M-1)-Inf;
    C.weightarray{2}=zeros(C.N-1,C.M)-Inf;
    C.weightarray{3}=zeros(C.N,C.M)-Inf;
end
if isfield(C,'secondaryModifier')
    D.all_fns=C.secondaryModifier(D.all_fns);
    D.imageModifier=C.secondaryModifier(D.imageModifier);
    D.preShiftsLongitudinal=C.secondaryModifier(D.preShiftsLongitudinal);
    for i=1:3
        C.weightarray{i}=permute(C.secondaryModifier(permute(C.weightarray{i},[2 1 3])),[2 1 3]);
        if iscell(C.usePreShiftsTransversal) &&length(C.usePreShiftsTransversal)>=i
            C.usePreShiftsTransversal{i}=C.secondaryModifier(C.usePreShiftsTransversal{i});
        end
    end
end

C.M=size(D.all_fns,1);
C.N=size(D.all_fns,2);
C.P=floor(size(D.all_fns,3)/C.slPth)*C.slPth;




% number of independent jobs for shift detection
C.procs=C.P/C.slPth;
% this is a possibility to only run a subset of the jobs
if nargin(C.joblistF)==1
    C.joblist=C.joblistF(C.procs);
else
    C.joblist=C.joblistF(C, all_fns, D.all_fns);
end
if (size(D.all_fns,3)==C.P)
    D.all_fns{1,1,C.P+1}=[];
    D.imageModifier{1,1,C.P+1}=[];
end
for i=1:prod(size(D.all_fns))
    if ~isempty(D.all_fns{i})
        C.dims=size(readImage(D.all_fns{i},D.imageModifier{i})');
        break;
    end
end
if ~isfield(C,'update_start') 
    C.update_start=1;
end



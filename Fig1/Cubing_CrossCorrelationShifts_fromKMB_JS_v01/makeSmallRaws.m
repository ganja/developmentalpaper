for m=1:size(D.all_fns,1)
    for n=1:size(D.all_fns,2)
        for p=1:size(D.all_fns,3)
            if ~isempty( D.all_fns{m,n,p})
                temp=imread(D.all_fns{m,n,p});
                temp2=imresize(temp,0.125);
                fid=fopen([D.all_fns{m,n,p}(1:end-4) '_small.raw'],'w+');
                fwrite(fid,temp2);
                fclose(fid);
                disp([m n p]);
            end
        end
    end
end

                
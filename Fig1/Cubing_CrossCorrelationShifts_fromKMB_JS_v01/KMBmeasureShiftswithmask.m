function KMBmeasureShiftswithmask(C,currentThread,all_fns)
if exist([C.matdir 'preshifts.mat'],'file')
    load([C.matdir 'preshifts.mat']);
else
    preShiftXYZ=zeros([C.M,C.N,1,2]);
end

if ~isfield(C,'stoponCC')
%     if isempty(find(C.joblist==currentThread,1))
%         return;
%     end
    honorstoptag=false;
else
    honorstoptag=true;
end

%kmbwaiter=KMBwait(currentThread,2,C,'measure',true);
overallProcNumberStart=(currentThread-1)*C.slPth;
preShift=zeros(C.M,C.N,C.P,2);
debugOptimizeFFT=true;
cache=cell(C.M,C.N,C.slPth+1);
data=cell(C.M,C.N,C.slPth+1);
contrast=zeros(C.M,C.N,C.slPth+1,2);
for i=it([[1;1;1],[C.M; C.N; C.slPth+1]])
    if size(at(all_fns,i),1)>0
        cache{i(1),i(2),i(3)}=imread(at(all_fns,i));
        cacheTemp=cast(at(cache,i),'single');
        contrast(i(1),i(2),i(3),:)=[mean(cacheTemp(:)),std(cacheTemp(:))];
        data{i(1),i(2),i(3)}=imresize(cache{i(1),i(2),i(3)},1/8);
    end
end
%kmbwaiter.closure();
% if C.readRaw
%
%     for i=it([[1;1;1],[C.M; C.N; C.slPth+1]])
%         if size(at(all_fns,i),1)>0
%             dirtemp=at(all_fns2,i);
%             dirind=strfind(dirtemp,filesep);
%             mkdir(dirtemp(1:dirind(end)));
%             fid=fopen(at(all_fns2,i),'w+');
%             fwrite(fid,cache{i(1),i(2),i(3)});
%             fclose(fid);
%         end
%     end
%
% end
delta=cell(3,1);
weights=cell(3,1);
area=cell(3,1);
eye3=eye(3);
aya3=[1 0 1; 0 1 1; 0 0 1];
for dimIt=1:3
    if dimIt<3 && exist('ttemp','var')
        preShiftXYZ=calculatePreShift(C,dimIt,ttemp);
    end
    delta{dimIt}=zeros([[C.M C.N C.slPth+1]-aya3(dimIt,:) 2]);
    weights{dimIt}=zeros([C.M C.N C.slPth+1]-aya3(dimIt,:));
    area{dimIt}=zeros([C.M C.N C.slPth+1]-aya3(dimIt,:));
    if debugOptimizeFFT
        fftw('planner','exhaustive');
        t1=fft2(single(ones(C.xcorr_size(dimIt,:))));
        ifft2(t1);
    end
    shifts=[];
    for i=it([[1;1;1],[C.M; C.N; C.slPth+1]-aya3(dimIt,:)'])
        if honorstoptag
            if ~isempty(find([i'+[0 0 overallProcNumberStart] dimIt]~=C.stoponCC,1))
                continue;
            end
        end
        
        shifts(i(1),i(2),i(3),1,1) = 0;
        shifts(i(1),i(2),i(3),1,2) = 0;
        shifts(i(1),i(2),i(3),1,3) = -1;
        shifts(i(1),i(2),i(3),1,4) = 0;
        thisweight=1E-7;
        if size(at(all_fns,i),1)>0&&size(at(all_fns,i+eye3(:,dimIt)),1)>0
            xcorr_sizeT=C.xcorr_size(dimIt,:);
            crop=0;
            CdimsFLR=fliplr(C.dims);
            j=i+eye3(:,dimIt);
            if dimIt<3
                if dimIt==2
                    CdimsFLR=fliplr(CdimsFLR);
                    xcorr_sizeT=fliplr(xcorr_sizeT);
                end;
                dC1=[[CdimsFLR(1)/2-xcorr_sizeT(1)/2+1 CdimsFLR(1)/2+xcorr_sizeT(1)/2];...
                    [1+crop xcorr_sizeT(2)+crop]];
                dC2=[[CdimsFLR(1)/2-xcorr_sizeT(1)/2+1 CdimsFLR(1)/2+xcorr_sizeT(1)/2];...
                    [CdimsFLR(2)-xcorr_sizeT(2)+1-crop CdimsFLR(2)-crop]];
                if dimIt==2
                    dC1=flipud(dC1);
                    dC2=flipud(dC2);
                end
                dC2=dC2+repmat(flipud(squeeze(preShiftXYZ(i(1),i(2),1,:))),[1 2]);
                dC1=dC1-repmat(flipud(squeeze(preShiftXYZ(i(1),i(2),1,:))),[1 2]);
                %include mask to ignore blood vessels
                mask1=create_mask(
                
                im1=cache{j(1),j(2),j(3)}(dC1(1,1):dC1(1,2),dC1(2,1):dC1(2,2));
                im2=cache{i(1),i(2),i(3)}(dC2(1,1):dC2(1,2),dC2(2,1):dC2(2,2));
                
                
                shifts(i(1),i(2),i(3),1,:) = reshape(forMH_AlignCube_measureshifts(C,...
                    im1,im2,dimIt,honorstoptag),[1 1 1 1 4]); %calculate  shift
                shifts(i(1),i(2),i(3),1,:)=shifts(i(1),i(2),i(3),1,:)+reshape(2*[squeeze(preShiftXYZ(i(1),i(2),1,:)); 0; 0]',[1 1 1 1 4]); %include preshift
            else
                tileShiftArray=[0,0];
                if isfield(C,'subtiled')
                    if C.subtiled
                        tileShiftArray=[];
                        limitgen=@(xx,yy)floor((floor(C.dims(xx)/(xcorr_sizeT(yy)))-1)/2);
                        for tsai=-limitgen(1,2):limitgen(1,2)
                            for tsaj=-limitgen(2,1):limitgen(2,1)
                                tileShiftArray=[tileShiftArray; tsai*xcorr_sizeT(2) tsaj*xcorr_sizeT(1)];
                            end
                            
                        end
                    end
                end
                for tsak=1:size(tileShiftArray,1)
                    dC=zeros(2,2,2);
                    dC=dC+imresize(reshape(CdimsFLR,[1,1,2]),[2,2])/2;
                    reshape(xcorr_sizeT,[1,1,2]);
                    dC=dC+imresize(cat(2,-ans/2+1,ans/2),[2,2]);
                    reshape(preShift(i(1),i(2),overallProcNumberStart+i(3),:),[1,1,2]);
                    dC=dC+imresize(cat(1,-ans/2,ans/2),[2,2]);
                    dC=dC+repmat(reshape(fliplr(tileShiftArray(tsak,:)),[1 1 2]),[2 2 1]);
                    shifts(i(1),i(2),i(3),tsak,:) = reshape(forMH_AlignCube_measureshifts(C,...
                        cache{j(1),j(2),j(3)}(dC(2,1,1):dC(2,2,1),dC(2,1,2):dC(2,2,2)),...
                        cache{i(1),i(2),i(3)}(dC(1,1,1):dC(1,2,1),dC(1,1,2):dC(1,2,2)),dimIt,honorstoptag),[1 1 1 1 4]);
                    shifts(i(1),i(2),i(3),tsak,1)=shifts(i(1),i(2),i(3),tsak,1)-preShift(i(1),i(2),overallProcNumberStart+i(3),2);
                    shifts(i(1),i(2),i(3),tsak,2)=shifts(i(1),i(2),i(3),tsak,2)-preShift(i(1),i(2),overallProcNumberStart+i(3),1);
                end
            end
            thisweight=1;
        end
        delta{dimIt}=sat(delta{dimIt},[i;1], mean(shifts(i(1),i(2),i(3),:,1)));
        delta{dimIt}=sat(delta{dimIt},[i;2], mean(shifts(i(1),i(2),i(3),:,2)));
        weights{dimIt}=sat(weights{dimIt},i, thisweight);
        area{dimIt}=sat(area{dimIt},i, mean(shifts(i(1),i(2),i(3),:,3)));
    end
end
if ~honorstoptag
save(strcat(C.matdir,C.knumberstr,'_delta_',sprintf('%u',currentThread),'.mat'),'delta','area','weights','contrast','shifts');
save([C.matdir 'rapid' sprintf('%u',currentThread) '.mat'],'data');
end

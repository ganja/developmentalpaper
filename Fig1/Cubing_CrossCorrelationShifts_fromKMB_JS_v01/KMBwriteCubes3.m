function KMBwriteCubes3(all_fns,currentThread2,C,ttemp,imageModifier)
ixx=it([C.inA;[currentThread2,currentThread2]]);
fileReaderThreads=1;
fileWriterThreads=1;
for ix2=1:size(ixx,2)
    currentThread=(currentThread2-1)*size(ixx,2)+ix2;
    
    %kmbw=KMBwait(currentThread,fileReaderThreads,C,'montagesMade',true);
    xyzCs=ixx(:,ix2);
    thismontage=cell(2,1);
    writeB=ones([C.maxMagMultXY,1],'uint8');
    thismontage{1}=zeros(C.cubesize*[C.maxMagMultXY,1],'uint8');
    
    for count = (xyzCs(3)-1)*C.cubesize+1:min(xyzCs(3)*C.cubesize,C.P)
        LSSweightsum=(1:C.M*C.N)';%ttemp.weightsum((count-1)*C.M*C.N+1:count*C.M*C.N);
        for i=1:size(LSSweightsum,1)
            LSSweightsum(i,2)=i;
        end
        LSSweightsum=sortrows(LSSweightsum);
        countT=count-(xyzCs(3)-1)*C.cubesize;
        for imgcount2 = 1:C.M*C.N
            imgcount=LSSweightsum(end+1-imgcount2,2);
            thisimg = (count-1)*C.M*C.N+imgcount;
            xyIts = [ mod(imgcount-1,C.M) floor((imgcount-1)/C.M)]+1;
            thisshifts = ttemp.shiftsN(xyIts(1),xyIts(2),count,:);
            xycoords = C.dims.*(xyIts-1)+squeeze(fix(thisshifts))'+1;
            xycoordsT=xycoords+C.dims-1;
            
            
            if size(at(all_fns,[xyIts(1);xyIts(2);count]),1)>0
                
                %                 if C.readRaw
                %                     fid=fopen(at(all_fns,[xyIts(1);xyIts(2);count]),'r');
                %                     cache=reshape(fread(fid,inf,'*uint8'),fliplr(C.dims))';
                %                     fclose(fid);
                %                 else
                cache=readImage(at(all_fns,[xyIts(1);xyIts(2);count]),at(imageModifier,[xyIts(1);xyIts(2);count]))';
                %                 end
                %                 if C.adaptContrast~=1
                %                     cache=cast(cast(cache,'single')*(1+(C.adaptContrast-1)*(count/C.P))-128*(count/C.P),'uint8');
                %                 end
                thismontage{1}(xycoords(1):xycoordsT(1), xycoords(2):xycoordsT(2),countT)=cache;
            end
            
            
            
            
        end
    end
end

filestr=strcat(num2str((xyzCs(1))),'_',num2str((xyzCs(2))),'_',num2str((xyzCs(3))));
if C.saveResliceAndHistogramWhileCubing
    for i=1:128:size(thismontage{1},1)
        thisreslice.x{ceil(i/128)}=thismontage{1}(i,:,:);
    end
    for i=1:128:size(thismontage{1},2)
        thisreslice.y{ceil(i/128)}=thismontage{1}(:,i,:);
    end
    thishist=histc(thismontage{1}(:),0:255);
    
    save([C.matdir 'thisreslice' filestr '.mat'], 'thisreslice', 'thishist');
end
%kmbw.closure();
%kmbw=KMBwait(currentThread,fileWriterThreads,C,'cubesMade',true);

c8d=[C.maxMagMultXY';1];

KMBwriteCubeItsMaker(C,xyzCs,1,thismontage,writeB,c8d,true)

%kmbw.closure;

end
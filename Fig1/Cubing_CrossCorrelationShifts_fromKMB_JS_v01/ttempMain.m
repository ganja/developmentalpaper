function ttemp=ttempMain(C,all_fns)
% Make ttemp

%C.allweights_tempReset=true;
col=readShiftMats(C);
lm=C.ttempIterations;
if C.naive
    ttemp.shifts=zeros(C.M,C.N,C.P,2);
else
    for i=1:lm
        i
        tws_collect{i} = [] ;
        ttemp=forMH_AlignCube_LSsolution_002(C,col);
        for j=1:3
            if isempty(ttemp.alldeltaErrW{j})
                local_maximum(j,:)=[-1 0];
                continue;
            end
            tws=sum(abs(ttemp.alldeltaErrW{j}),4);
            tws2=sum(abs(ttemp.alldeltaErr{j}),4);
            tws_collect{i} = [tws_collect{i}; tws(:)];
            [local_maximum(j,1), local_maximum(j,2)]=max(tws(:));
        end
        [global_maximum global_maximum(2)]=max(local_maximum(:,1));
        disp([global_maximum(1) local_maximum(global_maximum(2),2) global_maximum(2) i local_maximum(global_maximum(2),1) col.weight{global_maximum(2)}(local_maximum(global_maximum(2),2))]);
        if i<lm
            col.weight{global_maximum(2)}(local_maximum(global_maximum(2),2))=1E-7;
        end
    end
   ttemp.tws_collect = tws_collect; 
end
ttemp.minis=[Inf Inf];
ttemp.maxis=[-Inf -Inf];
'second part'
%ttemp.shifts=ttemp.shifts+[(1:C.procs*C.slPth)'*1/2 (1:C.procs*C.slPth)'*1/2]
for count = 1:C.P
    for m=1:C.M
        for n=1:C.N
            xycoords = C.dims.*([m n]-1)+squeeze(fix(ttemp.shifts(m,n,count,:)))'+1;
            if ~isempty(at(all_fns,[m;n;count]))
                ttemp.minis=min([ttemp.minis;xycoords]);
                ttemp.maxis=max([ttemp.maxis;xycoords+C.dims-1]);
            end
            
        end
    end
end
ttemp.weight=col.weight;
ttemp.shiftsN=cat(4,ttemp.shifts(:,:,:,1)-ttemp. minis(1), ttemp.shifts(:,:,:,2)-ttemp. minis(2))+1;

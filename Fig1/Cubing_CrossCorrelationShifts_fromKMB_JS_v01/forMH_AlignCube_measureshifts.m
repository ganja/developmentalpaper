function [shifts] = forMH_AlignCube_measureshifts(C,im1,im2, direction,i_extern)
xcorr_sizeT=permute(C.xcorr_size{direction}(i_extern(1),i_extern(2),:),[1,3,2]);
crop=0;
C.dims=fliplr(C.dims);
if direction<3
    if direction==2
        im1=im1';
        im2=im2';
        C.dims=fliplr(C.dims);
        xcorr_sizeT=fliplr(xcorr_sizeT);
    end
    imm1=im1;
    imm2=im2;
    
    padlength = xcorr_sizeT(2)/2;
    im1 = padarray(im1,[0 padlength],mean(mean(im1)));
    im2 = padarray(im2,[0 padlength],mean(mean(im2)));
else
    imm1=im1;
    imm2=im2;
    
    padlength = xcorr_sizeT(2)/2;
    im1 = padarray(im1,[padlength padlength],mean(mean(im1)));
    im2 = padarray(im2,[padlength padlength],mean(mean(im2)));
    
end




T = fft2(single(im2)).*conj(fft2(single(im1)));
C0 = fftshift(ifft2(T));

C0s=size(C0,2);
% if direction<3
% minC=min(C0(:));
% for i=1+18:C0s-18
% C0(:,i)=(C0(:,i)-minC)*C0s*0.75/(C0s*0.75-abs(i-C0s/2))+minC;
% end
% end
C1 = C0;
%CC=C0;
for count = 1:5
    C1 = (C1.^2);
    C1 = (C1./(max(max(C1,[],1))).*255);
end

C1(C1<=254) = 0;
if false
    figure
    imagesc(C1);
    figure;
    imagesc(C0);
    figure;
    imshow(im1);
    figure;
    imshow(im2);
end


stats = regionprops(bwlabel(C1), 'Centroid','Area');
[q,w] = max([stats.Area]);
stats(w).Centroid = round(stats(w).Centroid);
real_xcorr_size=size(C1);
coarseshifts(1) = stats(w).Centroid(2)-(real_xcorr_size(1)/2)-1;
coarseshifts(2) = stats(w).Centroid(1)-(real_xcorr_size(2)/2)-1;

if direction<3
    coarseshifts(2)=coarseshifts(2)-xcorr_sizeT(2);
end
swC2=stats(w).Centroid(2);
swC1=stats(w).Centroid(1);

C0 = padarray(C0,[128 128],mean(mean(C0)),'both');
C0 = C0(stats(w).Centroid(2):stats(w).Centroid(2)+256-1,...
    stats(w).Centroid(1):stats(w).Centroid(1)+256-1);
C1 = (imresize(C0, 4));
for count = 1:5
    C1 = (C1.^2);
    C1 = (C1./(max(max(C1,[],1))).*255);
end
C1(C1<=254) = 0;
stats = regionprops(bwlabel(C1), 'Centroid','Area');
[q,w] = max([stats.Area]);
shifts(1)=coarseshifts(1)+(stats(w).Centroid(2)-514.5)/4;
shifts(2)=coarseshifts(2)+(stats(w).Centroid(1)-514.5)/4;
if direction==1 && i_extern(1)==1 && i_extern(2)==2
    shifts(1)=0;
    shifts(2)=-xcorr_sizeT(2)
end

if ~strcmp(C.document_ccs,'none') && detectWeight(C,i_extern,direction)
    hoarseshifts=round(shifts);
    F=fspecial('gaussian',24,8);
    if direction<3
        hoarseshifts(2)=hoarseshifts(2)+xcorr_sizeT(2);
    end
    
    if strcmp(C.document_ccs,'full')||strcmp(C.document_ccs,'iid')
        a1=zeros(size(imm1)+abs(hoarseshifts));
        a1(1+max(hoarseshifts(1),0):max(hoarseshifts(1),0)+size(imm1,1),1+max(hoarseshifts(2),0):max(hoarseshifts(2),0)+size(imm1,2))=imm1;
        a2=zeros(size(a1));
        a2(1-min(hoarseshifts(1),0):-min(hoarseshifts(1),0)+size(imm1,1),1-min(hoarseshifts(2),0):-min(hoarseshifts(2),0)+size(imm1,2))=imm2;
        imm1=imfilter(imm1,F);
        imm2=imfilter(imm2,F);
        aa1=imm1(1-min(0,hoarseshifts(1)):end-max(hoarseshifts(1),0),1-min(0,hoarseshifts(2)):end-max(hoarseshifts(2),0));
        aa2=imm2(1+max(hoarseshifts(1),0):end+min(hoarseshifts(1),0),1+max(hoarseshifts(2),0):end+min(hoarseshifts(2),0));
        iid=feval(@(x)mean(mean(abs(x(21:end-20,21:end-20)))),double(aa1)-double(aa2));
        
        
    else
        a1=im1;
        a2=im2;
    end
    t_function=@(x1)x1;
    if direction==2
        t_function=@(x1)x1';
    end
    if iid>5.5 ||strcmp(C.document_ccs,'full')
        imwrite(uint8(t_function(a1)),sprintf('%sdocument_ccs_m%u_n%u_dir%u_p%u_second.tif',C.matdir,i_extern(1),i_extern(2),direction,i_extern(3)),'tif');
        imwrite(uint8(t_function(a2)),sprintf('%sdocument_ccs_m%u_n%u_dir%u_p%u_first.tif',C.matdir,i_extern(1),i_extern(2),direction,i_extern(3)),'tif');
    end
end
if direction<3
    shifts(2) = shifts(2)-2*crop;
end
if direction~=2
    shifts=fliplr(shifts);
end
shifts(3)=q;
shifts(4)=w;
% Kevin M. Boergens
% kevin.boergens@brain.mpg.de
% 2014

function OXALIS_writeConfFile(C,ox_boundary_xyz,ox_scale_xyz,ox_resolutions)
% Oxalis JSON: create settings file
fname = strcat(C.cubemaindir,'settings.json');
fid=fopen(fname,'w');
fprintf(fid,'{\n');
fprintf(fid,'  \"name\": \"%s\",\n',C.cubefn);
fprintf(fid,'  \"priority\": 0,\n');
fprintf(fid,'  \"scale\" : [%f, %f, %f]\n',ox_scale_xyz(1),ox_scale_xyz(2),ox_scale_xyz(3));
fprintf(fid,'}\n');
fclose(fid);
% Oxalis JSON: create layer file
fname = strcat(C.cubedir,'layer.json');
fid=fopen(fname,'w');
fprintf(fid,'{\n');
fprintf(fid,'  \"typ\" : \"color\",\n');
fprintf(fid,'  \"class\": \"uint8\"\n');
fprintf(fid,'}\n');
fclose(fid);
% Oxalis JSON: create section file
fname = strcat(C.cubedir,'section.json');
fid=fopen(fname,'w');
fprintf(fid,'{\n');
fprintf(fid,'  \"bbox\": [\n');
fprintf(fid,'    [0,%d],\n', ox_boundary_xyz(1));
fprintf(fid,'    [0,%d],\n', ox_boundary_xyz(2));
fprintf(fid,'    [0,%d]\n', ox_boundary_xyz(3));
fprintf(fid,'  ],\n');
fprintf(fid,'  \"resolutions\": ');
if ox_resolutions == 0
    fprintf(fid,'[]\n');
else
    if ox_resolutions == 1
        fprintf(fid,'[1]\n');
    else
        fprintf(fid,'[1');
        for magItI=1:ox_resolutions-1
            magIt=2^magItI;
            fprintf(fid,',\n    %d',magIt);
        end
        fprintf(fid,']\n');
    end
end
fprintf(fid,'}\n');
fclose(fid);
end
% Kevin M. Boergens
% kevin.boergens@brain.mpg.de
% 2014

for mags=[3 13 23 5 15 25 7 17 27]
    disp(mags);
    folder=[C.cubedir filesep]
    actfolder=sprintf('%smag%u',folder,mags);
    flags=0;
    cubes=zeros([128 128 256]);
    for x=0:99
        xf=sprintf('%s%sx%0.4d',actfolder,filesep,x);
        if exist(xf,'dir') || x==xx+1
            for y=0:99
                yf=sprintf('%s%sy%0.4d',xf,filesep,y);
                if exist(yf,'dir') || y==yy+1
                    for z=0:99
                        zf=sprintf('%s%sz%0.4d',yf,filesep,z);
                        if exist(zf,'dir')
                            filename=dir([zf filesep '*.raw']);
                            ff0=[zf filesep filename.name];
                            fid = fopen( ff0 );
                            kl_cube = fread( fid, 'uint8' );
                            fclose(fid);
                            kl_cube = reshape( kl_cube, [128 128 128] );
                            cubes(:,:,mod(z,2)*128+1:mod(z,2)*128+128)=kl_cube;
                            flags=1;
                            xx=x;
                            yy=y;
                            zz=z;
                        end
                        if mod(z,2)==1
                            if flags
                                hactfolder=sprintf('%smag%u',folder,mags+10);
                                zf=sprintf('%s%sx%0.4d%sy%0.4d%sz%0.4d',hactfolder,filesep,floor(x),filesep,floor(y),filesep,floor(z/2));
                                mkdir(zf);
                                ff0=strrep(ff0,sprintf('mag%u',mags),sprintf('mag%u',mags+10));
                                ff0=strrep(ff0,sprintf('x%0.4d',xx),sprintf('x%0.4d',floor(x)));
                                ff0=strrep(ff0,sprintf('y%0.4d',yy),sprintf('y%0.4d',floor(y)));
                                ff0=strrep(ff0,sprintf('z%0.4d',zz),sprintf('z%0.4d',floor(z/2)));
                                
                                fid = fopen( ff0, 'w+' );
                                %cubes=imresize(cubes,0.5);
                                cubes=permute(cubes,[3 2 1]);
                                cubes=imresize(cubes,[128 128]);
                                cubes=permute(cubes,[3 2 1]);
                                fwrite( fid, reshape( cast(cubes,'uint8'), 1, [] ) );
                                fclose( fid );
                            end
                            flags=0;
                            cubes=zeros([128 128 256]);
                        end
                    end
                end
            end
        end
    end
end

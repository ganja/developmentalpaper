%% KMB_just_cube_js

clear D

my_slice_range = [700:799];
my_nslice = length(my_slice_range)

% version only for cubing prealined tiles
for i=1:my_nslice % iteartion over all images
    %pfad zu bildern fur image J alinierte und gestichte bilder
    D.all_fns{1,1,i}=sprintf('Y:\\Data\\straehlej\\stacks\\st030_aligned\\000007\\Stitched Image_%0.6u.tif',my_slice_range(i));
end

%all_fns muss so gebaut sein dass es die lage der bilder wiederspiegelt
mod(my_nslice,8)
D.all_fns{1,1,end+2}=[]; % muss immer ein vielfaches von 8+1 sein (siehe zahl an bildern)

% array von kaputten bildern
errors=[];
for i=1:size(errors,1)
    D.all_fns{errors(i,1),errors(i,2),errors(i,3)}=D.all_fns{errors(i,1),errors(i,2),errors(i,3)-1};
end

%% Check for missing files
tic
disp('Check for missing files')
filecheck(D.all_fns,false,1);
toc
%% Measureshifts
% C ist fue alles was klein genug ist um an fast alle functionen gefahrlos
% uebergeben zu werden
% D immer nur da wo es absolut noetig ist
C.slPth=8; %slPth slices per thread
C.M=size(D.all_fns,1);
C.N=size(D.all_fns,2);
C.P=size(D.all_fns,3)-1;

% oberer rand der bilder immer gestaucht, daher array and functionshandeln,
% die angewendet werden sobald ads bild eingeladen wird
% D.imageModifier=repmat({@(x)[imresize(x(20:49,1:end),[36,size(x,2)]);imresize(x(50:89,1:end),[44,size(x,2)]); x(90:end,1:end)]},[C.M,C.N,C.P+1]);
% fur uebehaupt keine funtion nimm die ananyme identitaetsfunstion 
D.imageModifier=repmat({@(x) x},[C.M,C.N,C.P+1]);
%upperpart=repmat({@(x)[imresize(x(20:49,1:end),[36,size(x,2)]);imresize(x(50:89,1:end),[44,size(x,2)]); x(90:end,1:end)]},[C.M,3,C.P+1]);
%lowerpart=repmat({@(x)[imresize(x(20:49,1:end),[36,size(x,2)]);imresize(x(50:89,1:end),[44,size(x,2)]); x(90:end,1:end)]},[C.M,8,C.P+1]);
%D.imageModifier= [upperpart; lowerpart];
% LOngitudinal shifts sind fue versaetze in Z Richtung (zwischen ebenen,
% siehe x,y motor probleme)
D.preShiftsLongitudinal=repmat(0,[C.M C.N C.P+1 2]); % hier dann man groessere versaetze voreinstellen dann kann es vorher kompensiert werden
C.procs=C.P/C.slPth;
C.joblist=1:C.procs;  % hier wird eingestellt wenn man nur ein SUBSET machen will!!!
C.subsetCC=[]; % non longer actuell
% cat um sachen zusammenzukleben die in der 3 dimension sind
C.xcorr_size={repmat(cat(3,1024,384-32),C.M-1,C.N),repmat(cat(3,160,1768),C.M,C.N-1),repmat(cat(3,1768,2048),C.M,C.N)}; % hier muessen die eigenen werte eingefuegt werden. Bei Z wird der ganze bild ausschnitt genommen 
% da hier FFT gemacht macht wird sollten man am besten die groesse so
% waehlen dass moeglichst viele 2 in der primfactorzerlegung der matrix
% groesse liegen (groesse 2 hoch irgendwas)

% das ist fuer shifts (in x und y, Treppen) innerhalb einer ebene sehr
% haeufig
D.preShiftsTransversal={zeros(C.M-1,C.N,C.P+1,2)/2,zeros(C.M,C.N-1,C.P+1,2)/2};

% dient dazu den functionsaufruf kuerzer zu machen...
preShiftsTransversalSplitter=@(ii)cellfun(@(x)x(:,:,(ii-1)*C.slPth+1:ii*C.slPth,:),D.preShiftsTransversal,'UniformOutput',false);

% wenn man viele FFTs berechnen muss ist es effizient MATLAB zu bitten sich
% vorher sich darauf vorzubereiten das dauert ca. 1 minute. Fuer ne
% schnelle rechnung nervt das und es macht sinn das zu ueberspringen. Fuer
% viele berechnungen sollte das angemacht werden.
C.debugOptimizeFFT=false;
C.document_ccs='none'; % tested none and full; wenn es full ist wird fuer alle contacte berechnet auf dem ganzen datensatz ist das viel zu viel, daher nur anmachen fue die C.procs die schwierig sind. Sonst austellen mit 'none'
C.matdir='Y:\Data\straehlej\stacks\cubed\000007\mat\'; % hier werden die Shifts rein gespeichert
funn=D.imageModifier{1};

C.dims=fliplr(size(funn(imread(D.all_fns{1})))); % very mean; hier wird einfach nur die dimension des processierten bildes bestimmt und geflippt wegen der art wie matlab figure id handelt.
mkdir(C.matdir)
C.knumberstr='fish2';
C.weightarray='auto'; % das ist eine funktion um groessere Epoxybereiche auszuschliessen, nur relevant fuer epoxy Datensaetze
if ~iscell(C.weightarray) && strcmp(C.weightarray,'auto') % hier werden weights verteilt sodass kanten die an epoxy grezen und chargen weniger gewichtet werden weil oft bledsinn rauskommt.
    C.weightarray=cell(3,1);
    C.weightarray{1}=zeros(C.N,C.M-1)-Inf;
    C.weightarray{2}=zeros(C.N-1,C.M)-Inf;
    C.weightarray{3}=zeros(C.N,C.M)-Inf;
end
C.ccsDims=1:3; % die dinemsionen wovon die overlap bilder rausgeschrieben werden.
C.precache = false; % ob bei measure shifts alle bilder zuerst eingeladen werden oder nach und nach. Er ist etwas schneller wenn sie nach und nach geladen werden, also precache am besten immer false
% hiermit koennte man setzten dass er fuer den ganzen Datensatz nur die ein
% spezielles bild ausgeschrieben wird (dazu muss es oben auf full gestellt
% werden siehe: C.document_ccs )
aya3=[1 0 1; 0 1 1; 0 0 1];
C.whichCCStodo = arrayfun(@(dimIt)[[1;1;1],[C.M; C.N; C.slPth+1]-aya3(dimIt,:)'],1:3,'UniformOutput',false);

%% Make ttemp
C.naive=true; % sonst false.... CAVE wenn hier true drin steht werden alle shits die im block 'Measureshifts' berechtnet wurden werden nicht verwendet.... ! bei false schon. 
% true ist fuer den fall dass man man schnell was checken oder cuben
% moechte das schon aligned ist.
C.ttempIterations=3000;
C.zFactor=3;
ttemp=ttempMain(C,D.all_fns);

%% prep cubes
C.cubesize=128;
C.maxMagHigh=8;
C.scaleDownZ=true;
C.cubefn='2016-03-05_test'
C.pixelSize=[12 12 28];
C.cubedir='Y:\Data\straehlej\stacks\cubed\000007\';
C.cubemaindir='Y:\Data\straehlej\stacks\cubed\000007\';
C.maxMagMultXY=ceil((ttemp.maxis-ttemp.minis+1)/1024)*8;
multA=C.cubesize*C.maxMagMultXY; %defines the size of chunks processed from one subsubthread
C.maxMagMultXYHigh=[1 1];
multB=C.cubesize*C.maxMagMultXY.*C.maxMagMultXYHigh;  %same thing for high writer
C.montagewh=ceil((ttemp.maxis-ttemp.minis+1)./(multB)).*multB;
C.outA=[1 1;1 1;1 ceil(C.P/(C.cubesize))];
C.outB=[1 1;1 1;1 ceil(C.P/(C.cubesize*C.maxMagHigh))];
C.inA=[[1;1], C.montagewh'./multA'];
for magItI=0:9
    magIt=2^magItI;
    montagedepth= C.outA(3,2)*C.cubesize;
    if magIt==1
        Lmontagewh=C.montagewh;
     else
        Lmontagewh= C.montagewh/magIt;
    end
    if C.scaleDownZ
        zFactor=magIt;
    else
        zFactor=1;
    end
    KNOSSOS_writeKconfFile(C,strcat(C.cubefn,'_mag',num2str(magIt)),[Lmontagewh,ceil(montagedepth/zFactor)],C.pixelSize*magIt./[1 1 zFactor],magIt);
end

% write Oxalis conf files
OXALIS_writeConfFile(C,[C.montagewh,ceil(C.outA(3,2)*C.cubesize)],C.pixelSize,10);

C.inB=[[1;1], C.montagewh'./multB'];
ix1=it(C.outA);
ix2=it(C.outB);

%% Normal cube writer
C.invertCubes=false;
C.makedir=true;
C.saveResliceAndHistogramWhileCubing=false;
D.imageModifier2=repmat({@(x)x},[C.M,C.N,C.P]);

KMBparforHelper('parfor',@(thII)KMBwriteCubes3(D.all_fns,thII,C,ttemp,D.imageModifier2),size(ix1,2));

%% High cubes
makeSuperCubesReal(C);
%%
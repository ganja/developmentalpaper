function preShiftXYZ=calculatePreShift(C,dimIt,ttemp)
aya3=[1 0 1; 0 1 1; 0 0 1];

eye3=eye(3);
sis=[C.M; C.N; C.slPth+1]-aya3(dimIt,:)';
    
        preShiftXYZ=round(median(ttemp.delta_col{dimIt},3)/2)+permute(repmat(feval(@(x)x(1:2),eye3(dimIt,:)*feval(@(x)x(dimIt),fliplr(C.xcorr_size(dimIt,:)))/2)',[1 sis(1) sis(2) 1]),[2 3 4 1]);
 
function col=readShiftMats(C)
col.delta=cell(3,1);
col.weight=cell(3,1);
col.area=cell(3,1);
 col.contrast=[];
   
if ~C.naive
    for i=1:C.P
        folder=[C.matdir sprintf('%0.5d',floor((i-1)/100)) filesep];
        
        load(strcat(folder,C.knumberstr,'_delta_',sprintf('%u',i),'.mat'));
%        contrastT=load(strcat(folder,C.knumberstr,'_delta_',sprintf('%u',i),'.mat'),'contrast');
        for dimIt=1:3
            col.area{dimIt}=cat(3,col.area{dimIt},area{dimIt});
            col.delta{dimIt}=cat(3,col.delta{dimIt},delta{dimIt});
            col.weight{dimIt}=cat(3,col.weight{dimIt},weights{dimIt});
            
        end
%        col.contrast=cat(3,col.contrast,contrastT.contrast);
    end
    col.delta{3}=col.delta{3}(:,:,1:C.P-1,:);
    col.weight{3}=col.weight{3}(:,:,1:C.P-1);
    col.area{3}=col.area{3}(:,:,1:C.P-1);
else
    col.weight{1}=ones(C.M-1,C.N,C.P);
    col.weight{2}=ones(C.M,C.N-1,C.P);
    col.weight{3}=ones(C.M,C.N,C.P-1);
    for dimIt=1:3
        col.delta{dimIt}=zeros([size(col.weight{dimIt}),2]);
    end
    
end
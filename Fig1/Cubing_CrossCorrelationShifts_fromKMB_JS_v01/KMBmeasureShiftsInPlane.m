function shifts=KMBmeasureShiftsInPlane(dimIt,CdimsFLR,xcorr_sizeT,preShiftsTransversal,cache,j,i,C,overallProcNumberStart,all_fns,imageModifier)
crop=0;
if dimIt==2
    CdimsFLR=fliplr(CdimsFLR);
    xcorr_sizeT=fliplr(xcorr_sizeT);
end;
dC1=[[round(CdimsFLR(1)/2)-xcorr_sizeT(1)/2+1 round(CdimsFLR(1)/2)+xcorr_sizeT(1)/2];...
    [1+crop xcorr_sizeT(2)+crop]];
dC2=[[round(CdimsFLR(1)/2)-xcorr_sizeT(1)/2+1 round(CdimsFLR(1)/2)+xcorr_sizeT(1)/2];...
    [CdimsFLR(2)-xcorr_sizeT(2)+1-crop CdimsFLR(2)-crop]];
if dimIt==2
    dC1=flipud(dC1);
    dC2=flipud(dC2);
end
dC2=dC2+repmat(flipud(squeeze(preShiftsTransversal{dimIt}(i(1),i(2),i(3),:))),[1 2]);
dC1=dC1-repmat(flipud(squeeze(preShiftsTransversal{dimIt}(i(1),i(2),i(3),:))),[1 2]);

if isempty(cache{j(1),j(2),j(3)})
    cache{j(1),j(2),j(3)}=readImage(at(all_fns,j),at(imageModifier,j));
end
if isempty(cache{i(1),i(2),i(3)})
    cache{i(1),i(2),i(3)}=readImage(at(all_fns,i),at(imageModifier,i));
end
shifts(i(1),i(2),i(3),1,:) = reshape(forMH_AlignCube_measureshifts(C,...
    cache{j(1),j(2),j(3)}(dC1(1,1):dC1(1,2),dC1(2,1):dC1(2,2)),...
    cache{i(1),i(2),i(3)}(dC2(1,1):dC2(1,2),dC2(2,1):dC2(2,2)),dimIt,i+[0;0;overallProcNumberStart]),[1 1 1 1 4]);
shifts(i(1),i(2),i(3),1,:)=shifts(i(1),i(2),i(3),1,:)+reshape(2*[squeeze(preShiftsTransversal{dimIt}(i(1),i(2),1,:)); 0; 0]',[1 1 1 1 4]); %include preshift

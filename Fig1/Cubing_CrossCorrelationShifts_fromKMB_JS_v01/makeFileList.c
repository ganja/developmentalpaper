

/* *************************************************************************************************** */
/* makeFileList.c   creates cubing file list                                                           */
/* Copyright 2013 Max Planck Institute of Neurobiology, Martinsried                                    */
/* Version 0.04   Martin Zauser                                                                        */
/* *************************************************************************************************** */

/* makeFileList */
#include "mex.h"
#include "stdio.h"
#include "time.h"
#include "string.h"
#define VERSION       "0.04"
#define VERSION_DATE  "09.12.2013"


/* communication variables */
#define MESSAGE_BUFFER_SIZE   200
unsigned char szMessageBuffer[MESSAGE_BUFFER_SIZE];


/* prototypes */
void mexFunction (int nlhs , mxArray *plhs[], int nrhs , const mxArray *prhs[]);

void mexFunction (int nlhs , mxArray *plhs[], int nrhs , const mxArray *prhs[]) {


    /* declare variables */
    #define MAX_LENGTH_FILENAME  200
    #define MAX_LENGTH_PATH      500
    #define MAX_LENGTH_LINE     5000
    unsigned char szHomePath[MAX_LENGTH_PATH + 1];
    unsigned char szPathAndFilename[MAX_LENGTH_FILENAME + MAX_LENGTH_PATH + 1];
    unsigned char szFilename[MAX_LENGTH_FILENAME + 1];
    unsigned char szPath[MAX_LENGTH_PATH + 1];
    unsigned char szLine[MAX_LENGTH_LINE + 1];
    unsigned char szBuffer[MAX_LENGTH_FILENAME + MAX_LENGTH_PATH + MAX_LENGTH_LINE + 1];


    mxArray *mxGetFileOutput[2];
    mxArray *mxGetFileInput[2];
    /* general variables */
    char c;
    long int i, j, k;
    int iDimensions[3]; /* three dimensions: M*M_tile, N*N_tile and P */
    long int iM, iN, iP, iM_tile, iN_tile;
    double d;
    unsigned char *p;
    time_t time_start;
    long int iLine;
    int iLength;
    int iBeginOfFilename;
    int iRotation;
    int iImageCount[5];
    int iCountUnderlines;


    /* file variables */
    FILE* file;

    /* MATLAB variables */
    mxArray *fileListStruct;
    const char *fileListElements[] = { "all_fns", "rotations" };
    #define NUM_OF_FILE_LIST_ELEMENTS     (sizeof (fileListElements) / sizeof (const char *))
    mxArray *mxArrayFilenames;
    mxArray *mxArrayRotations;
    mwIndex mw_Index_index;

    /* send welcome message */
    printf("This is Cubing-MakeFileList version %s, Copyright 2013 MPI of Neurobiology, Martinsried.\n", VERSION);
 
    /* get five input parameters: M, N, P, M_tile, N_tile, homePath, filename */

    /* check number of input arguments */
    if (nrhs > 7) {
        mexErrMsgIdAndTxt("Braintracing:makeFileList:nrhs", "Only five parameters allowed: "
                          "M[integer], N[integer], P[integer], M_tile[integer], N_tile[integer], path[character], filename[character].");
        return;
    }
    if (nrhs < 7) {
        mexErrMsgIdAndTxt("Braintracing:makeFileList:nrhs", "Five parameters expected: "
                          "M[integer], N[integer], P[integer], M_tile[integer], N_tile[integer], path[character], filename[character].");
        return;
    }

    /* check type of input arguments */
    if ((!mxIsClass(prhs[0], "double")) || (!mxIsClass(prhs[1], "double")) || (!mxIsClass(prhs[2], "double")) ||
        (!mxIsClass(prhs[3], "double")) || (!mxIsClass(prhs[4], "double"))) { 
        mexErrMsgIdAndTxt("Braintracing:makeFileList:notDouble", "Type of input parameters 'M', 'N', 'P', 'M_tile' and 'N_tile' must be integer or double.");
        return;
    }
    if ((!mxIsClass(prhs[5], "char")) || (!mxIsClass(prhs[6], "char"))) {
        mexErrMsgIdAndTxt("Braintracing:makeFileList:notString", "Type of input parameters 'homePath' and 'filename' must be string.");
        return;
    }

    /* get parameters M, N, P, M_tile, N_tile */
    iM = (int)mxGetScalar(prhs[0]);
    iN = (int)mxGetScalar(prhs[1]);
    iP = (int)mxGetScalar(prhs[2]);
    iM_tile = (int)mxGetScalar(prhs[3]);
    iN_tile = (int)mxGetScalar(prhs[4]);

    /* get home path */
    if (mxGetString(prhs[5], szHomePath, MAX_LENGTH_PATH)) {
        mexErrMsgIdAndTxt("Braintracing:makeFileList:FilenameTooLong", "Input parameter 'homePath' too long.");
        return;
    }
    /* get configuration file name */
    if (mxGetString(prhs[6], szPathAndFilename, MAX_LENGTH_PATH + MAX_LENGTH_FILENAME)) {
        mexErrMsgIdAndTxt("Braintracing:makeFileList:FilenameTooLong", "Input parameter 'filename' too long.");
        return;
    }

    /* check number of output arguments */
    if (nlhs != 1) {
        mexErrMsgIdAndTxt("Braintracing:makeFileList:nlhs", "Output required. Proper use of function is "
                          "'MyVar = makeFileList(M, N, P, homePath, filename)'");
        return;
    }

    /* start time */
    time_start = time(NULL);


    /* open file */
    if (!(file = fopen (szPathAndFilename, "r"))) {
        sprintf(szMessageBuffer, "Cannot open file %s.\n", szPathAndFilename);
        mexErrMsgIdAndTxt("Braintracing:makeFileList:OpenFile", szMessageBuffer);
        return;
    }


    /* ************** */
    /* make file list */
    /* ************** */
    
    /* prepare matlab structures */
    fileListStruct = mxCreateStructMatrix (1, 1, NUM_OF_FILE_LIST_ELEMENTS, fileListElements);

    /* ###################################################################### */
    /* #########              CREATE MATLAB CELL            ################# */
    /* ###################################################################### */
    /* initialize dimensions */
    iDimensions[0] = iM * iM_tile;
    iDimensions[1] = iN * iN_tile;
    iDimensions[2] = iP;
    mxArrayFilenames = mxCreateCellArray (3, iDimensions);
    mxArrayRotations = mxCreateCellArray (3, iDimensions);
    mxSetFieldByNumber (fileListStruct, 0, 0, mxArrayFilenames);
    mxSetFieldByNumber (fileListStruct, 0, 1, mxArrayRotations);

    /* read line */
    iLine = 0;

    /* read first character */
    c = fgetc (file);

    /* read all lines */
    while (iLine < (iM * iM_tile * iN * iN_tile * iP)) {
        /* check for end of file */
        if (c == EOF) {
            sprintf(szMessageBuffer, "Error in line %d: Unexpected end of file. Need %d lines (images) in total.\n",
                (iLine + 1), (iM * iM_tile * iN * iN_tile * iP));
            mexErrMsgIdAndTxt("Braintracing:makeFileList:ReadLine", szMessageBuffer);
            return;
        }
        /* read text line */
        i = 0;
        while ((c != '\x0A') && (c != '\x0D') && (c != EOF)) {
            /* store character */
            szLine[i++] = c;
            if (i > MAX_LENGTH_LINE) {
                sprintf(szMessageBuffer, "Error in line %d: line too long. Only %d characters allowed.\n", (iLine + 1), MAX_LENGTH_LINE);
                mexErrMsgIdAndTxt("Braintracing:makeFileList:ReadLine", szMessageBuffer);
                return;
            }
            /* read next character */
            c = fgetc (file);
        }
        
        /* store length and add trailing zero */
        iLength = i;
        szLine[i] = 0;

        /* EVALUATE VALUES */
        /* get columns and rows */
        /* iImageCount[0] = zLayer */
        /* iImageCount[1] = motor_row */
        /* iImageCount[2] = motor_column */
        /* iImageCount[3] = row    */
        /* iImageCount[4] = column */
        i = 0;
        for (k = 0; k < 5; k++) {
            j = 0;
            while ((i < strlen(szLine)) && (szLine[i] != ' '))
                szBuffer[j++] = szLine[i++];
            /* add trailing zero */
            szBuffer[j] = 0;
            /* store value */
            iImageCount[k] = atoi(szBuffer);
            /* get next character */
            i++;
            /* remove spaces */
            while ((i < strlen(szLine)) && (szLine[i] == ' '))
                i++;
        }
        /* store begin of filename */
        iBeginOfFilename = i;
        
        /* get rotation */
        iRotation = 0;
        if (iLength > 0) {
            j = iLength - 1;
            while ((j > 0) && (szLine[j] != '_'))
                j--;
            if (j > 0) {
                j--;
                if ((szLine[j] == 'r') || (szLine[j] == 'r'))
                    iRotation = 1;
            }

        }

        /* check for correct dimensions */
        if (((iImageCount[1] * iM + iImageCount[3]) >= iM * iM_tile) || (iImageCount[1] >= iM_tile) || (iImageCount[3] >= iM)) {
            sprintf(szMessageBuffer, "Error in line %d: row number is %d (+1). Only %d rows allowed (parameter M).\n",
                        (iLine + 1), (iImageCount[1] * iM + iImageCount[3]), iM * iM_tile);
            mexErrMsgIdAndTxt("Braintracing:makeFileList:ReadLine", szMessageBuffer);
            return;
        }
        if (((iImageCount[2] * iN + iImageCount[4]) >= iN * iN_tile) || (iImageCount[2] >= iN_tile) || (iImageCount[4] >= iN)) {
            sprintf(szMessageBuffer, "Error in line %d: column number is %d (+1). Only %d columns allowed (parameter N).\n",
                        (iLine + 1), (iImageCount[2] * iN + iImageCount[4]), iN * iN_tile);
            mexErrMsgIdAndTxt("Braintracing:makeFileList:ReadLine", szMessageBuffer);
            return;
        }
        if (iImageCount[0] >= (iP)) {
            sprintf(szMessageBuffer, "Error in line %d: layer number is %d (+1). Only %d layers allowed (parameter P).\n",
                        (iLine + 1), iImageCount[0], iP);
            mexErrMsgIdAndTxt("Braintracing:makeFileList:ReadLine", szMessageBuffer);
            return;
        }

        /* calculate index */
        mw_Index_index = iImageCount[1] * iM + iImageCount[3] + 
                         (iImageCount[2] * iN + iImageCount[4]) * iM * iM_tile + 
                         iImageCount[0] * iM * iM_tile * iN * iN_tile;

        /* DONOTUSE parameter */
        if (strncmp(&szLine[iBeginOfFilename], "DONOTUSE", strlen("DONOTUSE")) == 0) {
            /* set only rotation value but NO entry in mxArrayFilenames */
            mxSetCell (mxArrayRotations, mw_Index_index, mxCreateDoubleScalar(0)); /* set rotation value as 0 */
        } else {

            /* concatenate string */
            sprintf(szBuffer, "%s%s", szHomePath, &szLine[iBeginOfFilename]);

            /* replace backslash by ordinary slash */
            for (i = 0; i < strlen(szBuffer); i++)
                if (szBuffer[i] == '\\')
                    szBuffer[i] = '/';

            mxSetCell (mxArrayFilenames, mw_Index_index, mxCreateString (szBuffer));
            mxSetCell (mxArrayRotations, mw_Index_index, mxCreateDoubleScalar(iRotation));

        }

        /* ignore white spaces */
        while (((c == '\x0A') || (c == '\x0D') || (c == ' ')) && (c != EOF))
            c = fgetc (file);

        /* increase line number */
        iLine++;
    }

    /* close file */
    fclose (file);

    /* print processing time */
    printf("Processing time for %d lines: %d seconds.\n", iLine, time(NULL) - time_start);

    /* print success message */
    printf("Image file list from file %s successfully created.\n", szPathAndFilename);

    /* return cell */
    plhs[0] = fileListStruct;
    return;
}

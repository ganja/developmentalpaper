%% Measure shifts and stop at certain Cross Correlations

'tic'
tic
display('Measure shifts and stop at certain Cross Correlations')
KMBparforHelper('for',@(ii)KMBmeasureShifts(C,ii,all_fns(:,:,(ii-1)*C.slPth+1:ii*C.slPth+1),D.document_css),C.procs);

% listprob=updater(C.procs,C.slPth,C.stoponCC(3));
% for ii=listprob
%     KMBmeasureShifts(C,ii,all_fns(:,:,(ii-1)*C.slPth+1:ii*C.slPth+1));
% end
'toc'
toc

%% Find problems

'tic'
tic
%disp('Plot problematic parts!')
%plot(overtempD)
%figure;


plot([overtempA overtempB overtempC ]);
legend
%alls=[overtempA overtempB overtempC];
%[rows cols]=find((overtempC<1240) & (overtempC>400))

'toc'
toc
% %% Measure inter image differences
% C.maxMagMultXY=ceil((C.maxis-C.minis+1)/1024)*8;
% multA=C.cubesize*C.maxMagMultXY; %defines the size of chunks processed from one subsubthread
% C.maxMagMultXYHigh=[1 1];
% multB=C.cubesize*C.maxMagMultXY.*C.maxMagMultXYHigh;  %same thing for high writer
% C.montagewh=ceil((C.maxis-C.minis+1)./(multB)).*multB;
% C.outA=[1 1;1 1;1 ceil(C.P/(C.cubesize))];
% C.inA=[[1;1], C.montagewh'./multA'];
% ix1=it(C.outA);
% KMBparforHelper(false,@(thII)KMBcalcIID(all_fns,thII,C,ttemp),size(ix1,2));
%% High cubes

'tic'
tic
disp('High cubes')

% jm = findResource('scheduler', 'configuration', jobmanagername);
% job = createJob(jm, 'configuration', jobmanagername);
% for thII=1:size(ix2,2)
%
%     inputargs = {thII,C};
%     createTask(job, @KMBwriteCubesHigh, 0, inputargs, 'configuration', jobmanagername);
% end
% submit(job);
KMBparforHelper(D.parallel_mode,@(thII)KMBwriteCubesHigh(thII,C),size(ix2,2));

toc
%% Super high cubes
tic
'tic'
disp('Super high cubes')
makeSuperCubesReal;
'toc'
toc
%% Rapid cubes
tic
'tic'
disp('Rapid cubes')
C.originRapid=[C.matdir 'rapid'];
for i=1:size(ix1,2)
    KMBwriteCubesRapid(all_fns,i,C,ttemp,true,false);
end
'toc'
toc
%% Naive isotropic mag 16 cubes
tic
'tic'
disp('Naive isotropic mag 16 cubes')
C.naive=true;
ttempMain
C.originRapid=[C.matdir 'rapid'];
for i=1:size(ix1,2)
    KMBwriteCubesRapid(all_fns,i,C,ttemp,true,true);
end
%makeSuperCubes();
'toc'
toc
%% Reslices
tic
'tic'

disp('Reslices')
resliceWriter();
'toc'
toc

%% Clean up cubes
doesnotexist();
tic
'tic'
disp('Clean up cubes')
jm = findResource('scheduler', 'configuration',jobmanagername);
job = createJob(jm, 'configuration', jobmanagername);

inputargs = {};
createTask(job, @()system('rm -r /zdata/kevin/cortex/cubes/2012-11-23_ex144_st08x2/mag16') , 0, inputargs, 'configuration', jobmanagername);
submit(job);
'toc'
toc
%% Clean up jobs
tic
'tic'
disp('Clean up jobs')
killAllJobs(1,true);
'toc'
toc
%%
load([C.matdir 'iids.mat'])
excl=[];
plotar=[];
legendar=cell(0);
if C.calcIID
    for im=1:C.M
        for in=1:C.N
            plotar=[plotar, squeeze(iids(im,in,:))];
            legendar=[legendar sprintf('m%u n%u',im,in)];
        end
    end
end
plot(plotar)
legend(legendar)
%%
%% Find problems
whichDeltasToShow=[1:12];
plot(overtempC(:,whichDeltasToShow));
legend(overfake(whichDeltasToShow));
plot([overtempB ])



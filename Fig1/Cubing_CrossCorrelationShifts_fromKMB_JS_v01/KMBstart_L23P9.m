clear D

my_slice_range = [1:1882]; % Muss immer so gebaut sein dass D.all_fns = 8+1
my_nslice = length(my_slice_range)

% % version only for cubing prealined tiles
% for i=1:my_nslice % iteartion over all images
%     %pfad zu bildern fur image J alinierte und gestichte bilder
%     D.all_fns{1,1,i}=sprintf('Y:\\Data\\straehlej\\stacks\\st030_aligned\\000007\\Stitched Image_%0.6u.tif',my_slice_range(i));
% end

% version for cubing and alining
my_y_range = [0,1]
for i=1:my_nslice % iteartion over all images
    %pfad zu bildern fur image J alinierte und gestichte bilder
    for y = 1:length(my_y_range)       
%     D.all_fns{1,y,i}=sprintf('Y:\\Data\\straehlej\\stacks\\st030Top\\st030\\%0.6u\\%0.6u\\2016-03-03_st030_%0.6u_000_%0.3u_000_n_00.tif',floor(my_slice_range(i)/100),my_slice_range(i),my_slice_range(i),my_y_range(y));
%     D.all_fns{1,y,i}=sprintf('Z:\\Data\\goura\\Stacks\\P14_L4_08-12-2016\\st132Top\\st132\\%0.6u\\%0.6u\\2016-12-08_st132_%0.6u_000_%0.3u_000_n_00.tif',floor(my_slice_range(i)/100),my_slice_range(i),my_slice_range(i),my_y_range(y));
    D.all_fns{1,y,i}=sprintf(('Z:\\Data\\goura\\Stacks\\P7_L4_24-05-2017\\st025\\%0.6u\\%0.6u\\2017-06-02_st025_%0.6u_000_%0.3u_000_n_00.tif',floor(my_slice_range(i)/100),my_slice_range(i),my_slice_range(i),my_y_range(y)) & sprintf('Z:\\Data\\goura\\Stacks\\P7_L4_24-05-2017\\st025\\%0.6u\\%0.6u\\2017-06-02_st025_%0.6u_000_%0.3u_000_n_00.tif',floor(my_slice_range(i)/100),my_slice_range(i),my_slice_range(i),my_y_range(y));
    end
end



%% array von kaputten bildern
errors=[1, 1, find(my_slice_range == 170);
        1, 2, find(my_slice_range == 170);
        1, 1, find(my_slice_range == 267);
        1, 2, find(my_slice_range == 267);
        1, 1, find(my_slice_range == 298);
        1, 2, find(my_slice_range == 298);
        1, 1, find(my_slice_range == 299);
        1, 2, find(my_slice_range == 299);
        1, 1, find(my_slice_range == 343);
        1, 2, find(my_slice_range == 343);
        1, 1, find(my_slice_range == 358);
        1, 2, find(my_slice_range == 358);
        1, 1, find(my_slice_range == 361);
        1, 2, find(my_slice_range == 361);
        1, 1, find(my_slice_range == 483);
        1, 2, find(my_slice_range == 483);
        1, 1, find(my_slice_range == 751);
        1, 2, find(my_slice_range == 751);
        1, 1, find(my_slice_range == 793);
        1, 2, find(my_slice_range == 793);
        1, 1, find(my_slice_range == 796);
        1, 2, find(my_slice_range == 796);
        1, 1, find(my_slice_range == 876);
        1, 2, find(my_slice_range == 876);
        1, 1, find(my_slice_range == 936);
        1, 2, find(my_slice_range == 936);
        1, 1, find(my_slice_range == 940);
        1, 2, find(my_slice_range == 940);
        1, 1, find(my_slice_range == 1064);
        1, 2, find(my_slice_range == 1064);
        1, 1, find(my_slice_range == 1159);
        1, 2, find(my_slice_range == 1159);
        1, 1, find(my_slice_range == 1215);
        1, 2, find(my_slice_range == 1215);
        1, 1, find(my_slice_range == 1246);
        1, 2, find(my_slice_range == 1246);
        1, 1, find(my_slice_range == 1255);
        1, 2, find(my_slice_range == 1255);
        1, 1, find(my_slice_range == 1297);
        1, 2, find(my_slice_range == 1297);
        1, 1, find(my_slice_range == 1324);
        1, 2, find(my_slice_range == 1324);
        1, 1, find(my_slice_range == 1416);
        1, 2, find(my_slice_range == 1416);
        1, 1, find(my_slice_range == 1558);
        1, 2, find(my_slice_range == 1558);
        1, 1, find(my_slice_range == 1600);
        1, 2, find(my_slice_range == 1600);
        1, 1, find(my_slice_range == 1619);
        1, 2, find(my_slice_range == 1619);
        1, 1, find(my_slice_range == 1716);
        1, 2, find(my_slice_range == 1716)];
for i=1:size(errors,1)
    D.all_fns{errors(i,1),errors(i,2),errors(i,3)}=D.all_fns{errors(i,1),errors(i,2),errors(i,3)-1};
end



%% Check for missing files
tic
disp('Check for missing files')
filecheck(D.all_fns,false,1);
toc
%% Measureshifts
% C ist fue alles was klein genug ist um an fast alle functionen gefahrlos
% uebergeben zu werden
% D immer nur da wo es absolut noetig ist
C.slPth=8; %slPth slices per thread
C.M=size(D.all_fns,1);
C.N=size(D.all_fns,2);
C.P=size(D.all_fns,3)-1;


% oberer rand der bilder immer gestaucht, daher array and functionshandeln,
% die angewendet werden sobald ads bild eingeladen wird
% D.imageModifier=repmat({@(x)[imresize(x(20:49,1:end),[36,size(x,2)]);imresize(x(50:89,1:end),[44,size(x,2)]); x(90:end,1:end)]},[C.M,C.N,C.P+1]);
% fur uebehaupt keine funtion nimm die ananyme identitaetsfunstion 
D.imageModifier=repmat({@(x) x},[C.M,C.N,C.P+1]);



%upperpart=repmat({@(x)[imresize(x(20:49,1:end),[36,size(x,2)]);imresize(x(50:89,1:end),[44,size(x,2)]); x(90:end,1:end)]},[C.M,3,C.P+1]);
%lowerpart=repmat({@(x)[imresize(x(20:49,1:end),[36,size(x,2)]);imresize(x(50:89,1:end),[44,size(x,2)]); x(90:end,1:end)]},[C.M,8,C.P+1]);
%D.imageModifier= [upperpart; lowerpart];


% LOngitudinal shifts sind fue versaetze in Z Richtung (zwischen ebenen,
% siehe x,y motor probleme)
D.preShiftsLongitudinal=zeros([C.M C.N C.P+1 2]); % hier dann man groessere versaetze voreinstellen dann kann es vorher kompensiert werden
C.procs=C.P/C.slPth;
C.joblist=1:C.procs;  % hier wird eingestellt wenn man nur ein SUBSET machen will!!!
C.subsetCC=[]; % non longer actuell

C.xcorr_size{1}=repmat(cat(3,6000, 320),C.M-1,C.N); %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
C.xcorr_size{2}=repmat(cat(3,320, 6144),C.M,C.N-1);
C.xcorr_size{3}=repmat(cat(3,2048, 4096),C.M,C.N);
% hier muessen die eigenen werte eingefuegt werden. Bei Z wird der ganze bild ausschnitt genommen 
% da hier FFT gemacht macht wird sollten man am besten die groesse so
% waehlen dass moeglichst viele 2 in der primfactorzerlegung der matrix
% groesse liegen (groesse 2 hoch irgendwas)

% das ist fuer shifts (in x und y, Treppen) innerhalb einer ebene sehr
% haeufig
D.preShiftsTransversal={zeros(C.M-1,C.N,C.P+1,2)/2,zeros(C.M,C.N-1,C.P+1,2)/2};

% dient dazu den functionsaufruf kuerzer zu machen...
preShiftsTransversalSplitter=@(ii)cellfun(@(x)x(:,:,(ii-1)*C.slPth+1:ii*C.slPth,:),D.preShiftsTransversal,'UniformOutput',false);

% wenn man viele FFTs berechnen muss ist es effizient MATLAB zu bitten sich
% vorher sich darauf vorzubereiten das dauert ca. 1 minute. Fuer ne
% schnelle rechnung nervt das und es macht sinn das zu ueberspringen. Fuer
% viele berechnungen sollte das angemacht werden.
C.debugOptimizeFFT=false;
C.document_ccs= 'none'%'none'; % tested none and full; wenn es full ist wird fuer alle contacte berechnet auf dem ganzen datensatz ist das viel zu viel, daher nur anmachen fue die C.procs die schwierig sind. Sonst austellen mit 'none'
C.matdir='Z:\Data\goura\Stacks\P7_L4_24-05-2017\Cubing_st025\'; % hier werden die Shifts rein gespeichert
funn=D.imageModifier{1};

C.dims=fliplr(size(funn(imread(D.all_fns{1})))); % very mean; hier wird einfach nur die dimension des processierten bildes bestimmt und geflippt wegen der art wie matlab figure id handelt.
mkdir(C.matdir)
C.knumberstr='P7_L4_st025';
C.weightarray='auto'; % das ist eine funktion um groessere Epoxybereiche auszuschliessen, nur relevant fuer epoxy Datensaetze
if ~iscell(C.weightarray) && strcmp(C.weightarray,'auto') % hier werden weights verteilt sodass kanten die an epoxy grezen und chargen weniger gewichtet werden weil oft bledsinn rauskommt.
    C.weightarray=cell(3,1);
    C.weightarray{1}=zeros(C.N,C.M-1)-Inf;
    C.weightarray{2}=zeros(C.N-1,C.M)-Inf;
    C.weightarray{3}=zeros(C.N,C.M)-Inf;
end
C.ccsDims=1:3; % die dinemsionen wovon die overlap bilder rausgeschrieben werden.
C.precache = false; % ob bei measure shifts alle bilder zuerst eingeladen werden oder nach und nach. Er ist etwas schneller wenn sie nach und nach geladen werden, also precache am besten immer false
% hiermit koennte man setzten dass er fuer den ganzen Datensatz nur die ein
% spezielles bild ausgeschrieben wird (dazu muss es oben auf full gestellt
% werden siehe: C.document_ccs )
aya3=[1 0 1; 0 1 1; 0 0 1];
C.whichCCStodo = arrayfun(@(dimIt)[[1;1;1],[C.M; C.N; C.slPth+1]-aya3(dimIt,:)'],1:3,'UniformOutput',false);

% hier werden immer nur die Shift fuer die anteile berechnet.... und weil
% das ding so lang wurde oben funtcionen wie preShiftsTransversalSplitter
% (siehe oben)
myfunc = @(ii)KMBmeasureShifts(C,ii,D.all_fns(:,:,(ii-1)*C.slPth+1:ii*C.slPth+1),D.imageModifier(:,:,(ii-1)*C.slPth+1:ii*C.slPth+1),D.preShiftsLongitudinal(:,:,(ii-1)*C.slPth+1:ii*C.slPth,:),preShiftsTransversalSplitter(ii));

% das hiernur ausf�hren wenn man wirklich shifts measuren will
KMBparforHelper('parfor',myfunc,C.procs);
% par for generiert unterschiedliche indices (je nachdem was gebraucht
% wird) my func schneidet dann entscprechende auschnitte der shifts aus

%% Make ttemp
C.naive=false; % sonst false.... CAVE wenn hier true drin steht werden alle shits die im block 'Measureshifts' berechtnet wurden werden nicht verwendet.... ! bei false schon. 
% true ist fuer den fall dass man man schnell was checken oder cuben
% moechte das schon aligned ist.

C.ttempIterations=1; % hin und wieder gehen sachen schief , daher wurde der weight des gr��ten fehlers iterativ herabgesetzt

C.zFactor=3;
ttemp=ttempMain(C,D.all_fns);
%% search for problematic deltas
h = figure;
mkdir iterations
for i = C.ttempIterations
    hist(ttemp.tws_collect{i});
    set(gca,'yscale','log');
    saveas(h, sprintf('iterations\\%0.4u.png',i));
end
xlabel('pixels')
ylabel('number')

% 
% topbottomproblems = ttemp.alldeltaErr{2}(:,:,:,:);
% topbottomproblems = topbottomproblems(:);
% [C,I] = max(abs(topbottomproblems));
% [x1 x2 x3 x4] = ind2sub([3, 7, 400, 2],I)

% hist(abs(topbottomproblems))

%% Low memory tif writer
C.cubesize=128;
C.small=false;
C.calcIID=false;
C.writeRawsWithTifs=false;
C.outtifresizefactor=1;
C.tifdir='Z:\Data\goura\Stacks\P7_L4_24-05-2017\Cubing_st025\stitched\';
mkdir(C.tifdir);
C.maxMagMultXY=ceil((ttemp.maxis-ttemp.minis+1)/1024)*8;
multA=C.cubesize*C.maxMagMultXY; %defines the size of chunks processed from one subsubthread
C.maxMagMultXYHigh=[1 1];
multB=C.cubesize*C.maxMagMultXY.*C.maxMagMultXYHigh;  %same thing for high writer
C.montagewh=ceil((ttemp.maxis-ttemp.minis+1)./(multB)).*multB;
C.outA=[1 1;1 1;1 ceil(C.P/(C.cubesize))];
C.inA=[[1;1], C.montagewh'./multA'];
ix1=it(C.outA);
KMBwriteTifs(D.all_fns,ix1,C,ttemp,false,D.imageModifier);
%% prep cubes
C.cubesize=128;
C.maxMagHigh=8;
C.scaleDownZ=true;
C.cubefn='P7_L4_AG_27-05-2017_v1'
C.pixelSize=[11.24 11.24 30];
C.cubedir='Z:\Data\goura\Stacks\P7_L4_24-05-2017\Cubes\';
C.cubemaindir='Z:\Data\goura\Stacks\P7_L4_24-05-2017\Cubes\';
C.maxMagMultXY=ceil((ttemp.maxis-ttemp.minis+1)/1024)*8;
multA=C.cubesize*C.maxMagMultXY; %defines the size of chunks processed from one subsubthread
C.maxMagMultXYHigh=[1 1];
multB=C.cubesize*C.maxMagMultXY.*C.maxMagMultXYHigh;  %same thing for high writer
C.montagewh=ceil((ttemp.maxis-ttemp.minis+1)./(multB)).*multB;
C.outA=[1 1;1 1;1 ceil(C.P/(C.cubesize))];
C.outB=[1 1;1 1;1 ceil(C.P/(C.cubesize*C.maxMagHigh))];
C.inA=[[1;1], C.montagewh'./multA'];
for magItI=0:9
    magIt=2^magItI;
    montagedepth= C.outA(3,2)*C.cubesize;
    if magIt==1
        Lmontagewh=C.montagewh;
        
    else
        Lmontagewh= C.montagewh/magIt;
    end
    if C.scaleDownZ
        zFactor=magIt;
    else
        zFactor=1;
    end
    
    KNOSSOS_writeKconfFile(C,strcat(C.cubefn,'_mag',num2str(magIt)),[Lmontagewh,ceil(montagedepth/zFactor)],C.pixelSize*magIt./[1 1 zFactor],magIt);
end
% for i=1:3
%     for j=1:4
%         magnum=i*2+1+(j-1)*10;
%         magstr=sprintf('%u',magnum);
%         KNOSSOS_writeKconfFile(strcat(C.cubedir,filesep,magstr,filesep),strcat(C.cubefn,'_mag',magstr),[Lmontagewh/(2^(i-1)),montagedepth*8/(2^(j-1))],[1 1 1],1);
%     end
% end

% write Oxalis conf files
OXALIS_writeConfFile(C,[C.montagewh,ceil(C.outA(3,2)*C.cubesize)],C.pixelSize,10);

C.inB=[[1;1], C.montagewh'./multB'];
ix1=it(C.outA);
ix2=it(C.outB);

%% Normal cube writer

C.invertCubes=false;
C.makedir=true;
C.saveResliceAndHistogramWhileCubing=false;
D.imageModifier2=repmat({@(x)x},[C.M,C.N,C.P]);

KMBparforHelper('parfor',@(thII)KMBwriteCubes3(D.all_fns,thII,C,ttemp,D.imageModifier2),size(ix1,2));

%% High cubes
makeSuperCubesReal(C);

function [kn_lineSetNameList,kn_SkelDataNameList] = KLEEskeleton_exportToAmira_many_v4(kl_skels,kl_outputpath,kl_uniqueLabel,kl_colormode,kl_bbox,...
    kl_setVisibility,kl_LineWidth,kl_vxscaleFactor)

    if nargin<4
        kl_colormode=1; %1: each skeletlet one color; %2: each cluster one color; 
    end
    if nargin<5
        kl_bbox=[];
    end
    if nargin<6
        kl_setVisibility=0; % 32766 is off
    end
    if size(kl_colormode,2)==3
        if size(kl_colormode,1)==1
            kl_colorGlobal = kl_colormode;
            kl_colormode=3;
        else
            kl_colorGlobal = kl_colormode;
            kl_colormode=4;
        end
    end
    if nargin<7
        kl_LineWidth=1;
    end
    if nargin<8
        kl_vxscaleFactor=[1 1 1];
    end
    if ~exist(kl_outputpath,'dir')
        system(sprintf('mkdir %s',kl_outputpath));
    end
    
    kl_outputfname = fullfile(kl_outputpath,sprintf('%s_amiraNetwork.hx',kl_uniqueLabel));

    for kl_clustc=1:size(kl_skels,2)
        kl_skelc=1;
        %for kl_skelc=1:size(kl_skels{kl_clustc},2)
            kl_thisskelfname = strrep(kl_outputfname,'.hx',sprintf('clust%d_skel%d.am',kl_clustc,kl_skelc));
            if ~isempty(kl_bbox)
                for kl_skcc=1:size(kl_skels{kl_clustc},2)
                    kl_skels{kl_clustc}{kl_skcc} = ...
                        KLEEskeleton_getSkeletonInBBox(kl_skels{kl_clustc}{kl_skcc},kl_bbox);
                end
            end
%              kl_clustc
            if ~isempty(kl_skels{kl_clustc})
                kn_writesucces(kl_clustc) = KLEEskeleton_exportToAmira_many(kl_skels{kl_clustc},...
                    kl_thisskelfname,kl_vxscaleFactor);
            end
                %end
    end
    
    kl_hx_fname = kl_outputfname;

    fid = fopen(kl_hx_fname,'w');
    fprintf(fid,'# Amira Script\n');
 
    kn_lineSetNameList_intern={};
    fprintf(fid,'set hideNewModules 0\n');
    kl_totalc=0;
    for kl_clustc=1:size(kl_skels,2)
        kl_skelc=1;
%        for kl_skelc=1:size(kl_skels{kl_clustc},2)
        if ~isempty(kl_skels{kl_clustc}) && kn_writesucces(kl_clustc)>0
            kl_totalc = kl_totalc+1;
            kl_thisskelfname = strrep(kl_outputfname,'.hx',sprintf('clust%d_skel%d.am',kl_clustc,kl_skelc));
            kl_thisskelfname = sprintf('%s_amiraNetworkclust%d_skel%d.am',kl_uniqueLabel,kl_clustc,kl_skelc)
            %if kl_forWin~=1
                kl_thisskelfname = strrep(kl_thisskelfname,'\','/');
            %end
            kl_thisskellabel = sprintf('%s_clust%d_skel%d',kl_uniqueLabel,kl_clustc,kl_skelc);
            kl_thisLineSetViewname = sprintf('LineSetView_%s_%d_%d',kl_uniqueLabel,kl_clustc,kl_skelc);
            if kl_colormode==1
                kl_thisColor = mh_getColor(kl_totalc);
            elseif kl_colormode==2 
                kl_thisColor = mh_getColor(kl_clustc);
            elseif kl_colormode==3 
                kl_thisColor = kl_colorGlobal;
            elseif kl_colormode==4
                kl_thisColor = kl_colorGlobal(kl_clustc,:);
            end
            fprintf(fid,'[ load ${SCRIPTDIR}/%s ] setLabel %s\n',kl_thisskelfname,kl_thisskellabel);
            fprintf(fid,'%s fire\n',kl_thisskellabel);
            fprintf(fid,'create HxDisplayLineSet {%s}\n',kl_thisLineSetViewname);
            fprintf(fid,'%s data connect %s\n',kl_thisLineSetViewname,kl_thisskellabel);
            if kl_setVisibility>0                
%                 fprintf(fid,'%s shape setIndex 0 0\n',kl_thisLineSetViewname);
                fprintf(fid,'%s setViewerMask %d\n',kl_thisLineSetViewname,kl_setVisibility);
            end
            fprintf(fid,'%s fire\n',kl_thisLineSetViewname);
            fprintf(fid,'%s setLineWidth %d\n\n',kl_thisLineSetViewname,kl_LineWidth);
            fprintf(fid,'%s setLineColor %.2f %.2f %.2f\n\n',kl_thisLineSetViewname,kl_thisColor);
            
            kn_lineSetNameList_intern{kl_totalc} = kl_thisLineSetViewname;
            kn_SkelDataNameList_intern{kl_totalc} = kl_thisskellabel;
        end
 %       end
    end
    
    fclose(fid);

    fprintf('amira file successfully written.\n');

    if nargout>0
        kn_lineSetNameList = kn_lineSetNameList_intern;
        kn_SkelDataNameList = kn_SkelDataNameList_intern;
    end


end
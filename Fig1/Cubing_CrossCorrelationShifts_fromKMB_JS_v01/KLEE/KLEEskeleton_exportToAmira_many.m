function kn_return = KLEEskeleton_exportToAmira_many(kl_skels,kl_outputfname,kl_vxscaleFactor)

    if nargin<3
        kl_vxscaleFactor=[1 1 1];
    end

    fid = fopen(kl_outputfname,'w');
    
    kl_allNodes = [];
    kl_nodeCumOffset=repmat(0,[1 size(kl_skels,2)+1]);
    for kl_skelc=1:size(kl_skels,2)
        if isfield(kl_skels{kl_skelc},'nodes')
            kl_allNodes = [kl_allNodes;...
                kl_skels{kl_skelc}.nodes];
            kl_nodeCumOffset(kl_skelc+1) = kl_nodeCumOffset(kl_skelc) + size(kl_skels{kl_skelc}.nodes,1);
        end
    end
    if ~isempty(kl_allNodes)
        kl_allEdges = [];
        for kl_skelc=1:size(kl_skels,2)
            if isfield(kl_skels{kl_skelc},'edges')
                kl_allEdges = [kl_allEdges;...
                    (kl_skels{kl_skelc}.edges+kl_nodeCumOffset(kl_skelc))];
            end
        end
        
        kl_nNodes = size(kl_allNodes,1);
        kl_nEdges = size(kl_allEdges,1);
        fprintf(fid,'# AmiraMesh 3D ASCII 2.0\n\n\n');
        fprintf(fid,'define Lines %d\n',kl_nEdges*3);
        fprintf(fid,'nVertices %d\n',kl_nNodes);
        fprintf(fid,'Parameters {\n\tContentType \"HxLineSet\"\n}');
        
        fprintf(fid,'Lines { int LineIdx } @1\n');
        fprintf(fid,'Vertices { float[3] Coordinates } @2\n');
        
        fprintf(fid,'@1\n');
        
        
        kl_edgesToWrite = kl_allEdges;
        kl_edgesToWrite(:,3) = 0;
        kl_edgesToWrite = kl_edgesToWrite'-1;
        
        fprintf(fid,'%d\n',kl_edgesToWrite(:));
        
        fprintf(fid,'\n@2\n');
        fprintf(fid,'%.2f %.2f %.2f\n',(kl_allNodes(:,[1 2 3]).*repmat(kl_vxscaleFactor,[size(kl_allNodes,1),1]))');
        
        
        
        fprintf('amira file successfully written.\n');
        if nargout>0
            kn_return=1;
        end
    else
        fprintf('amira file NOT written (empty nodes).\n');
        if nargout>0
            kn_return=0;
        end
    end

    fclose(fid);
end
function KNOSSOS_exportSkeletonToAmira_ColoredPerEdge(kn_skel,kn_path,kn_label,kn_collist,kn_linewidth,resolution)

    if nargin<5
        kn_linewidth=1;
    end
    if size(kn_collist,2)==3
        kn_cols_unique = unique(kn_collist,'rows');
        kn_outputskels={};
        for i=1:size(kn_cols_unique,1)
            kn_outputskels{i}{1}.nodes = kn_skel.nodes;
            kn_thisedgesel = sum(kn_collist==repmat(kn_cols_unique(i,:),[size(kn_collist,1),1]),2)>2;
            kn_outputskels{i}{1}.edges = kn_skel.edges(kn_thisedgesel,:);
        end
        KLEEskeleton_exportToAmira_many_v4(kn_outputskels,kn_path,kn_label,kn_cols_unique,[],0,kn_linewidth);
    else
%         kn_colors = mh_getColor_patterned(kn_collist);
        kn_cols_unique = unique(kn_collist);
        kn_outputskels={};
        for i=1:length(kn_cols_unique)
            kn_outputskels{i}{1}.nodes = kn_skel.nodes;
            kn_thisedgesel = kn_collist == kn_cols_unique(i);
            kn_outputskels{i}{1}.edges = kn_skel.edges(kn_thisedgesel,:);
        end
        KLEEskeleton_exportToAmira_many_v4(kn_outputskels,kn_path,kn_label,mh_getColor_patterned(kn_cols_unique),[],0,kn_linewidth);

    end








end
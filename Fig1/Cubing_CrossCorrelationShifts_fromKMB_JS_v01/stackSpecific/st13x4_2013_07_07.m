function D=st13x4_2013_07_07()

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end

D.C.samplestr='ex373';
D.C.slPth=8;

D.C.datestr='2013-07-07';
D.exclude=[];
D.C.imdatestr='2013-07-07';
D.C.useEveryNthSlice=1;
% If true, all weights and shifts are reset to zero before LS is started
D.C.naive=false;

%allows to make changes to all_fns after makeFileListFun is done;
%Example: just use image 3,4 from all slices
%It's not possible to modify the third component
%D.all_fns_Modifier=@(x)x(3,4,:);
D.all_fns_Modifier=@(x)x;

D.C.xcorr_size=[1024 512;384 2048;1536 2048]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
D.C.M=1;    %Number of tiles per row
D.C.N=1;   %Number of tiles per column
D.makeFileListFun=@KMBmakeFileList;
D.C.projecttype='cortex';

D.C.document_ccs=false;

% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
%
D.C.machineroot='Z:/';


D.parallel_mode='parfor';

D.C.subfoldersForSourceTifs='two';

D.C.allslice_arr = [
    136 1 128 1 D.C.N 1 D.C.M
    ]; %Be careful, here the order is Z-direction, then N, then M!

D.C.P=ceil(sum(D.C.allslice_arr(:,3)-D.C.allslice_arr(:,2)+1)/D.C.slPth)*D.C.slPth;


%D.C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
%D.C.joblistF=@(x)updater(x,D.C.slPth,[134]);
D.C.joblistF=@(x)1:x;

D.C.stackN='st13x4';

D.C.makedir=true;   %Creates directories for cubes or not
%make array for epoxy inclusion
D.C.weightarray=cell(3,1);
x=Inf;
o=-Inf;
D.C.weightarray{1}=zeros(D.C.N,D.C.M-1)+o;
D.C.weightarray{2}=zeros(D.C.N-1,D.C.M)+o;
D.C.weightarray{3}=zeros(D.C.N,D.C.M)+o;

D.C.pixelSize=[11.28 11.28 28];

D.C.usePreShiftsTransversal=false;
% (:,:,:,1) determins y-shifts. Keep in mind that a value of e.g. 1000
% means that the images are shifted by 1000 pixels relative to each other,
% so this would access each of them with indices shifted by 500
D.C.preShiftsLongitudinal=zeros(D.C.M,D.C.N,D.C.P,2);

%FEI forgot to invert images, don't change
D.C.invertCubes=false;

%if defaultOrigin is false, D.C.origin has to be set:
%D.C.origin='Y:\st005Top';
D.defaultOrigin=true;

%FEI stuff von Kevin! 
D.C.tifNamingSchemePiezoFEI=false;

D.parallel_mode_cubes='for';
D.C.saveResliceAndHistogramWhileCubing=false;

%is applied to every source tif image directly after reading
D.imageModifier=repmat({@(x)x},[D.C.M,D.C.N,D.C.P+1]);
%is applied to imageModifier cell arry
D.imageModifierModifier=D.all_fns_Modifier;

%changes orientation of every second column
D.C.motorSnake=false;

%makes sense to set to false if only very few crosscorrelations are
%calculated
D.C.debugOptimizeFFT=false;

%the highmag cubes are scaled down at the same rate in all three dimensions
D.C.scaleDownZ=true;

D.C.saveDataForSuperRapidIteration=false;

end
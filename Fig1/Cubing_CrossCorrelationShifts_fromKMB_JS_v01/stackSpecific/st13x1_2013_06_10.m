function D=st13x1_2013_06_10()

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end
D.C.samplestr='ex324';
D.C.slPth=8;
D.C.stackN='st13x1';
D.C.datestr='2013-06-10';
D.defaultOrigin=true;
%if defaultOrigin is false, D.C.origin has to be set:
%D.C.origin='Y:\st005Top';
D.exclude=[];
D.C.imdatestr='2013-06-09';

% If true, all weights and shifts are reset to zero before LS is started
D.C.naive=false;
D.C.useEveryNthSlice=1;

%allows to make changes to all_fns after makeFileListFun is done;
%Example: just use image 3,4 from all slices
%It's not possible to modify the third component
%D.all_fns_Modifier=@(x)x(3,4,:);
D.imageModifierModifier=@(x)cat(3,reshape([x(6,4,1:2:end-1),x(2,1,2:2:end)],[1,1,floor(size(x,3)/2)*2]),{[]});

D.all_fns_Modifier=@(x)cellfun(@(x)strrep(x,'st130_','st129_'),D.imageModifierModifier(x),'UniformOutput',false);

D.C.xcorr_size=[1024 512;384 2048; 1024 2048]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
D.C.M=7;    %Number of tiles per row
D.C.N=4;   %Number of tiles per column
D.makeFileListFun=@KMBmakeFileList;
D.C.projecttype='cortex';

D.C.document_ccs=false;

% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
% 
D.C.machineroot='Y:';

% folder for output tifs (if they are created), not original tifs

D.parallel_mode='parfor';
D.parallel_mode_cubes='for';

D.C.allslice_arr = [
    130 1 128 1 D.C.N 1 D.C.M
    ]; %Be careful, here the order is Z-direction, then N, then M!

D.C.P=ceil(sum(D.C.allslice_arr(:,3)-D.C.allslice_arr(:,2)+1)/D.C.slPth)*D.C.slPth;


%D.C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
%D.C.joblistF=@(x)updater(x,D.C.slPth,[134]);
D.C.joblistF=@(x)1:x;

D.C.makedir=true;   %Creates directories for cubes or not
%make array for epoxy inclusion
D.C.weightarray=cell(3,1);
x=Inf;
o=-Inf;
D.C.weightarray{1}=zeros(D.C.N,D.C.M-1)+o;
D.C.weightarray{2}=zeros(D.C.N-1,D.C.M)+o;
D.C.weightarray{3}=zeros(D.C.N,D.C.M)+o;


D.C.pixelSize=[11.28 11.28 28];
D.C.usePreShifts=false;
D.C.saveResliceAndHistogramWhileCubing=false;
D.C.subfoldersForSourceTifs='two';
D.C.tifNamingSchemePiezoFEI=true;
D.C.invertCubes=false;
%is applied to every source tif image directly after reading
D.imageModifier=repmat(permute({@(x)x(1:end-1,1:end-1),@(x)rot90(x(1:end-1,1:end-1),2)},[1 3 2]),[D.C.M,D.C.N,D.C.P+1]);
%changes orientation of every second column
D.C.motorSnake=false;
D.C.usePreShiftsTransversal=false;
D.C.preShiftsLongitudinal=repmat(permute([-1022,1022;0,0],[3 4 2 1]),[D.C.M,D.C.N,D.C.P/2,1]);
%makes sense to set to false if only very few crosscorrelations are
%calculated
D.C.debugOptimizeFFT=true;

end
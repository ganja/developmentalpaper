function [C, D]=st18x0_2013_11_30()
% version 0.05  2013-11-13

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end
% cuts one row/column if image has odd number of pixels in x or y direction
    function res=cut_img(x)
        res=x;
        if mod(size(x,1),2) && mod(size(x,2),2)      % if number of rows is odd then ...
            res=[x ones(size(x,1),1)*mean(x(:)); ones(1,size(x,2)+1)*mean(x(:))]; % ... add row and column
        end
        
    end


C.samplestr='ex526';
C.slPth=8;

% name of cubing from Google Drive document
C.cubingName=feval(@(x)x{1},regexp(mfilename(),'_','split'));
% date the cubing was done
C.datestr=feval(@(x)x(length(C.cubingName)+2:end),strrep(mfilename(),'_','-'));

% date the images where taken, can be set to 'auto' to enable autodetection
C.imdatestr='auto';


C.useEveryNthSlice=1;
% If true, all weights and shifts are reset to zero before LS is started
C.naive=true;
C.M=6;   %   Number of tiles per row
C.N=14;   %  Number of tiles per column


%C.xcorr_size=[1024 512;384 2048; 1536 2048]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
C.xcorr_size{1}=repmat(cat(3,2048, 256),C.M-1,C.N); %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
C.xcorr_size{2}=repmat(cat(3,192, 3072),C.M,C.N-1);
C.xcorr_size{3}=repmat(cat(3,2048, 3072),C.M,C.N);

C.whichDimsShiftCalc=1:3;

C.maxMagHigh=512;


C.projecttype='cortex';

C.document_ccs='none';

% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% C.joblistF=@(x)setdiff(1:x,b);
%

C.machineroot='/home/boergens/cubing_data/boergens/';

%ending of initial files
C.sourceEnding='.tif';
C.parallel_mode='for';
C.parallel_mode_cubes='for';

C.subfoldersForSourceTifs='two';

C.allslice_arr = [
    180 1 350  1 C.N 1 C.M
    ]; %Be careful, here the order is Z-direction, then N, then M!

C.P=ceil(sum(C.allslice_arr(:,3)-C.allslice_arr(:,2)+1)/C.slPth)*C.slPth;

%allows to make changes to all_fns after makeFileListFun is done;
%Example: just use image 3,4 from all slices
%It's not possible to modify the third component
%D.all_fns_Modifier=@(x)x(3,4,:);
%D.all_fns_Modifier=@(x)flipdim(flipdim(x,1),2);
D.all_fns_Modifier=@(x)x;
%D.all_fns_Modifier=@(x)sort_img(x,C.sourceEnding,C.slPth,C.M,C.N,true);

% functions and modifier
C.makeFileListFun=@KMBmakeFileList;
% show_filenames shows the file numbers in x as doubles
% helpful function for debugging


%C.joblistF=@(x)updater(x,C.slPth,D.exclude);
%C.joblistF=@(x)updater(x,C.slPth,[134]);
C.joblistF=@(x)1:x;

C.makedir=true;   %Creates directories for cubes or not
%make array for epoxy inclusion
C.weightarray=cell(3,1);
x=Inf;
o=-Inf;
C.weightarray{1}=zeros(C.N,C.M-1)+o;
C.weightarray{2}=zeros(C.N-1,C.M)+o;
C.weightarray{3}=zeros(C.N,C.M)+o;


C.pixelSize=[11.24 11.24 28];

C.usePreShiftsTransversal=false;

% (:,:,:,1) determins y-shifts. Keep in mind that a value of e.g. 1000
% means that the images are shifted by 1000 pixels relative to each other,
% so this would access each of them with indices shifted by 500
D.preShiftsLongitudinal=zeros(C.M,C.N,C.P,2);

%invert ("255-x") output cubes
C.invertCubes=false;

%if defaultOrigin is false, C.origin has to be set:
%C.origin='Y:\st005Top';
%D.defaultOrigin=true;
%C.origin='F:\martin\cortex17x3';
C.defaultOrigin=true;

%FEI stuff von Kevin!
C.tifNamingSchemePiezoFEI=true;


C.saveResliceAndHistogramWhileCubing=false;

%is applied to every source tif image directly after reading
%D.imageModifier=repmat(feval(@(x)[x rot90(x,2);rot90(x,2) x;x rot90(x,2)],repmat([{@(x)x}; {@(x)rot90(x,2)}],C.M/6,C.N/2)),[1 1 C.P+1]);
%D.imageModifier=repmat(feval(@(x)[x rot90(x,2);x rot90(x,2);x rot90(x,2)], ...
%    repmat([{@(x)cut_img(x)}; {@(x)rot90(cut_img(x),2)}],C.M/6,C.N/2)),[1 1 C.P+1]);
C.imageModifierBase={@(x)x,@(x)rot90(rot90(x))};

%changes orientation of every second column
C.motorSnake=false;

%makes sense to set to false if only very few crosscorrelations are
%calculated
C.debugOptimizeFFT=true;

%the highmag cubes are scaled down at the same rate in all three dimensions
C.scaleDownZ=true;


%save data for super fast iterations
C.saveDataForSuperRapidIteration=true;

%slices to exclude
C.exclude=[];

%if false, slices are silently excluded, if true, spacers are introduces
%and numbering is shifted
C.exludeKeepCounting=false;

%if not empty, only certain cross correlations are computed
%no mat files with CC results are saved
%per line: direction x y z
%C.subsetCC=[1 7 6 300;1 7 5 301];
C.subsetCC=[];

%are inter image differences be calculated during low memory tif writer
C.calcIID=false;

C.maxMagHigh=8;

%if empty, manual cell mode in KMBstart, otherwise include strings 'cubes','high' or 'tifs'
C.outputToDo='';

C.outtifresizefactor=1;
%weight adjustment for z deltas
C.zFactor=1;
C.ttempIterations=1;
C.writeRawsWithTifs=[];
C.small=true;

end

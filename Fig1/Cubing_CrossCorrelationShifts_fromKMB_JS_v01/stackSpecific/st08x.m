function D=st08x()
    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end

D.C.slPth=8;
D.C.stackN='st08x';
D.C.datestr='2012-09-29';
D.C.origin=['/zdata/kevin/cortex/tifs/st08x/'];
D.exclude=[13 95 100 115 237 243 261 375 437];
D.C.M=3;
D.C.N=3;
D.C.xcorrmax=2048;
D.C.xcorr_size=[2048 256;256 2048;D.C.xcorrmax D.C.xcorrmax];
D.C.allslice_arr = [
    80 1 142 1 3 1 3;
    81 1 310 1 3 1 3
    ];
%D.C.joblistF=@(x)1:x;
D.C.joblistF=@(x)updater(x,D.C.slPth,[134]);
D.C.makedir=true;

end
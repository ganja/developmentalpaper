function D=st08x2()


    function al2=rem3262085(al1)
        al1{3,1,620*8+5}=al1{3,1,620*8+4};
        al1{3,2,620*8+5}=al1{3,2,620*8+4};
        al1{3,3,620*8+5}=al1{3,3,620*8+4};
        al1{3,3,5908}=al1{3,3,5907};
        al2=al1;
    end

D.C.slPth=8;
D.C.stackN='st08x2';
D.C.samplestr='ex144';
D.C.datestr='2012-11-23';
D.C.origin=['/zdata/kevin/cortex/tifs/st083Charm/'];
D.exclude=[];
D.C.xcorrmax=2048;
D.C.xcorr_size=[1024 128;128 1024;D.C.xcorrmax D.C.xcorrmax];
D.C.pixelSize=[12 12 28];
% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
% 
D.exclude=[1133 1062 7563 7585];


D.C.allslice_arr = {
    83 1 7777 1 3 1 3 '2012-11-07'
   
    };
%D.C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
D.C.joblistF=@(x)[updater(x,D.C.slPth,[D.exclude 5908]),621];
%D.C.joblistF=@(x)1:x;
%D.C.joblistF=@(x)621;
D.all_fns_Modifier=@rem3262085;
D.C.makedir=true;
D.C.subtiled=false;
D.C.M=3;
D.C.N=3;
D.makeFileListFun=@KMBmakeFileListLong;
end
function [C, D]=st17x4_2013_11_21()

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end

C.samplestr='exR286';
C.slPth=8;

% name of cubing from Google Drive document
%C.cubingName=feval(@(x)x{1},regexp(mfilename(),'_','split'));
C.cubingName='st17x4';
% date the cubing was done
C.datestr='2013-11-21';
%C.datestr=feval(@(x)x(length(D.C.cubingName)+2:end),strrep(mfilename(),'_','-'));

% date the images where taken
C.imdatestr='2013-10-29';

C.useEveryNthSlice=1;
% If true, all weights and shifts are reset to zero before LS is started
C.naive=false;

C.M=7;    %Number of tiles per row
C.N=11;   %Number of tiles per column


%C.xcorr_size=[1024 512;384 2048; 1536 2048]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
C.xcorr_size{1}=repmat(cat(3,1024, 512),C.M-1,C.N); %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
C.xcorr_size{2}=repmat(cat(3,384, 2048),C.M,C.N-1);
C.xcorr_size{3}=repmat(cat(3,1536, 2048),C.M,C.N);

C.whichDimsShiftCalc=1:3;

C.makeFileListFun=@KMBmakeFileList;
C.projecttype='entorhinal';

C.document_ccs=false;

% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
%
%C.machineroot='Y:/entorhinall/st171Top/st171/';
%C.machineroot='~/';
C.machineroot='/home/boergens/cubing_data/mzauser/';

C.parallel_mode='parfor';
C.parallel_mode_cubes='for';

C.subfoldersForSourceTifs='two';

C.allslice_arr = [
    171 1 250 1 C.N 1 C.M
    ]; %Be careful, here the order is Z-direction, then N, then M!

C.P=ceil(sum(C.allslice_arr(:,3)-C.allslice_arr(:,2)+1)/C.slPth)*C.slPth;

%allows to make changes to all_fns after makeFileListFun is done;
%Example: just use image 3,4 from all slices
%It's not possible to modify the third component
%D.all_fns_Modifier=@(x)x(3,4,:);
D.all_fns_Modifier=@(x)flipdim(flipdim(x,1),2);
% D.all_fns_Modifier=@(x)x;


%C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
%C.joblistF=@(x)updater(x,D.C.slPth,[134]);
C.joblistF=@(x)1:x;

C.makedir=true;   %Creates directories for cubes or not
%make array for epoxy inclusion
C.weightarray=cell(3,1);
x=Inf;
o=-Inf;
C.weightarray{1}=zeros(C.N,C.M-1)+o;
C.weightarray{2}=zeros(C.N-1,C.M)+o;
C.weightarray{3}=zeros(C.N,C.M)+o;
% C.weightarray{1}=[
%     x x x x x x;
%     x x x o o o;
%     x x o o o o;
%     x x o o o o;
%     x o o o o o;
%     x o o o o o;
%     x x o o o o;
%     x x x x x o;
%     x x x x x x;
%     x x x x x x;
%     x x x x x x];
% C.weightarray{2}=[
%     x x x x x o x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o x;
%     x x o o o o x;
%     x x x o o o x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];
% C.weightarray{3}=[
%     x x x x x x x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x x o o o o;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];


C.pixelSize=[36.62 36.62 1600];

C.usePreShiftsTransversal=false;
% (:,:,:,1) determins y-shifts. Keep in mind that a value of e.g. 1000
% means that the images are shifted by 1000 pixels relative to each other,
% so this would access each of them with indices shifted by 500
D.preShiftsLongitudinal=zeros(C.M,C.N,C.P,2);

%invert ("255-x") output cubes
C.invertCubes=false;

%if defaultOrigin is false, C.origin has to be set:
%C.origin='Y:\st005Top';
C.defaultOrigin=true;
%C.defaultOrigin=false;
%C.origin='/home/boergens/entorhinal';

%FEI stuff von Kevin!
C.tifNamingSchemePiezoFEI=false;


C.saveResliceAndHistogramWhileCubing=false;

%is applied to every source tif image directly after reading
D.imageModifier=repmat({@(x)x},[C.M,C.N,C.P+1]);
%is applied to imageModifier cell arry
D.imageModifierModifier=D.all_fns_Modifier;

%changes orientation of every second column
C.motorSnake=true;

%makes sense to set to false if only very few crosscorrelations are
%calculated
C.debugOptimizeFFT=true;

%the highmag cubes are scaled down at the same rate in all three dimensions
C.scaleDownZ=false;

%save data for super fast iterations
C.saveDataForSuperRapidIteration=false;

%ending of initial files
C.sourceEnding='.tif';


%slices to exclude
C.exclude=[];

%if false, slices are silently excluded, if true, spacers are introduces
%and numbering is shifted
C.exludeKeepCounting=false;

%if not empty, only certain cross correlations are computed
%no mat files with CC results are saved
%per line: direction x y z
%D.C.subsetCC=[1 7 6 300;1 7 5 301];
C.subsetCC=[];

%are inter image differences be calculated during low memory tif writer
C.calcIID=false;

C.maxMagHigh=8;

%weight adjustment for z deltas
C.zFactor=1;
C.ttempIterations=1;
C.writeRawsWithTifs=[];
C.small=false;

%if empty, manual cell mode in KMBstart, otherwise include strings 'cubes','high' or 'tifs'
C.outputToDo='cubes,high';

end

function D=st16x8_2013_09_28()
% piezo stack after major fix
    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end
    function y=localImageModifier()
        y=cell(D.C.M,D.C.N,D.C.P+1);
        for im=1:D.C.M
            for in=1:D.C.N
                for ip=1:D.C.P+1
                    if mod(im+ip,2)==0
                        y{im,in,ip}=@(ix)ix(1:end-1,1:end-1);
                    else
                        y{im,in,ip}=@(ix)rot90(ix(1:end-1,1:end-1),2);
                    end
                end
            end
        end
    end
    function y=localallfnsmodifier(afns)
        for i=1:size(afns,3);
            if mod(i,2)==0
                y(:,:,i)=rot90(afns(:,:,i),2);
            else
                y(:,:,i)=afns(:,:,i);
            end
        end
    end
D.C.samplestr='ex455';
D.C.slPth=4;

% name of cubing from Google Drive document
D.C.cubingName=feval(@(x)x{1},regexp(mfilename(),'_','split'));
% date the cubing was done
D.C.datestr=feval(@(x)x(length(D.C.cubingName)+2:end),strrep(mfilename(),'_','-'));

% date the images where taken, can be set to auto to enable autodetection
D.C.imdatestr='auto';

D.C.useEveryNthSlice=1;
% If true, all weights and shifts are reset to zero before LS is started
D.C.naive=true;

D.C.xcorr_size=[1920 256;192 2048; 1536 2048]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction

D.C.M=11;    %Number of tiles per row
D.C.N=14;   %Number of tiles per column
D.makeFileListFun=@KMBmakeFileList;
D.C.projecttype='cortex';

D.C.document_ccs=true;

% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
%
D.C.machineroot='Y:/';


D.parallel_mode='for';
D.parallel_mode_cubes='for';

D.C.subfoldersForSourceTifs='two';

D.C.allslice_arr = [
    168 1 1 1 D.C.N 1 D.C.M
    ]; %Be careful, here the order is Z-direction, then N, then M!

D.C.P=ceil(sum(D.C.allslice_arr(:,3)-D.C.allslice_arr(:,2)+1)/D.C.slPth)*D.C.slPth;

%allows to make changes to all_fns after makeFileListFun is done;
%Example: just use image 3,4 from all slices
%It's not possible to modify the third component
%D.all_fns_Modifier=@(x)x(3,4,:);
%D.all_fns_Modifier=@(x)flipdim(flipdim(x,1),2);
D.all_fns_Modifier=@(x)feval(@(x2)x2,localallfnsmodifier(x));


%D.C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
%D.C.joblistF=@(x)updater(x,D.C.slPth,[134]);
D.C.joblistF=@(x)1:x;

D.C.makedir=true;   %Creates directories for cubes or not
%make array for epoxy inclusion
D.C.weightarray=cell(3,1);
x=Inf;
o=-Inf;
D.C.weightarray{1}=zeros(D.C.N,D.C.M-1)+o;
D.C.weightarray{2}=zeros(D.C.N-1,D.C.M)+o;
D.C.weightarray{3}=zeros(D.C.N,D.C.M)+o;
% D.C.weightarray{1}=[
%     x x x x x x;
%     x x x o o o;
%     x x o o o o;
%     x x o o o o;
%     x o o o o o;
%     x o o o o o;
%     x x o o o o;
%     x x x x x o;
%     x x x x x x;
%     x x x x x x;
%     x x x x x x];
% D.C.weightarray{2}=[
%     x x x x x o x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o x;
%     x x o o o o x;
%     x x x o o o x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];
% D.C.weightarray{3}=[
%     x x x x x x x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x x o o o o;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];


D.C.pixelSize=[11.24 11.24 28];


%this value can either be logical or a cell array
%if it is true, the preshifts are calculated from the last cubing run.
%if it is false, no transversal preshifts are used
%if it is a cell matrix, the explicit shifts are used
D.C.usePreShiftsTransversal=cell(1,2);
D.C.usePreShiftsTransversal{1}=cat(4,-ones(D.C.M,D.C.N)*50,zeros(D.C.M,D.C.N));
D.C.usePreShiftsTransversal{2}=zeros(D.C.M,D.C.N,1,2);

% (:,:,:,1) determins y-shifts. Keep in mind that a value of e.g. 1000
% means that the images are shifted by 1000 pixels relative to each other,
% so this would access each of them with indices shifted by 500
D.C.preShiftsLongitudinal=zeros(D.C.M,D.C.N,D.C.P,2);

%invert ("255-x") output cubes
D.C.invertCubes=false;

%if defaultOrigin is false, D.C.origin has to be set:
%D.C.origin='Y:\st005Top';
D.defaultOrigin=true;

%FEI stuff von Kevin! 
D.C.tifNamingSchemePiezoFEI=true;


D.C.saveResliceAndHistogramWhileCubing=false;

%is applied to every source tif image directly after reading
D.imageModifier=localImageModifier();
%is applied to imageModifier cell arry
D.imageModifierModifier=D.all_fns_Modifier;

%changes orientation of every second column, also for piezos
D.C.motorSnake=true;

%makes sense to set to false if only very few crosscorrelations are
%calculated
D.C.debugOptimizeFFT=true;

%the highmag cubes are scaled down at the same rate in all three dimensions
D.C.scaleDownZ=true;

%save data for super fast iterations
D.C.saveDataForSuperRapidIteration=false;

%ending of initial files
D.sourceEnding='.tif';

%slices to exclude
D.exclude=[];

%if false, slices are silently excluded, if true, spacers are introduces
%and numbering is shifted
D.exludeKeepCounting=false;

%if not empty, only certain cross correlations are computed
%no mat files with CC results are saved
%per line: direction x y z
%D.C.subsetCC=[1 7 6 300;1 7 5 301];
D.C.subsetCC=[];

%are inter image differences be calculated during low memory tif writer
D.C.calcIID=true;

%if empty, manual cell mode in KMBstart, otherwise include strings 'cubes','high' or 'tifs' 
D.outputToDo='tifs'; 


end
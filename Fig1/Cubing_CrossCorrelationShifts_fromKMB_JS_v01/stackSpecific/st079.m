function D=st079()

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end

D.C.samplestr='ex145';
D.C.slPth=3;
D.C.stackN='079';
D.C.datestr='2012-09-23';
D.C.origin=['P:\paul\matlab\cubing\'];
%D.exclude=[113 312 380-1 513 672 688-1 877-1 1090 1093 1149 1153 1276 1306 1568 2146 2506 2502 2601 2739 2812 2903 2952 3362 3369];
D.C.xcorrmax=2048;
D.C.xcorr_size=[512 256;256 512;D.C.xcorrmax D.C.xcorrmax]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
D.C.M=3;
D.C.N=3;
% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
% 


D.C.allslice_arr = [
    79 1 3 1 3 1 3
    ];
%D.C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
D.C.joblistF=@(x)updater(x,D.C.slPth,[134]);
%D.C.joblistF=@(x)1:x;

D.C.makedir=true;
end
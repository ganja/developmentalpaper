function [C, D]=st17x9_2013_11_11()
% version 0.05  2013-11-13

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end
% cuts one row/column if image has odd number of pixels in x or y direction
    function res=cut_img(x)
        res=x;
        if mod(size(x,1),2) && mod(size(x,2),2)      % if number of rows is odd then ...
            res=[x ones(size(x,1),1)*mean(x(:)); ones(1,size(x,2)+1)*mean(x(:))]; % ... add row and column
        end
        
    end

% extract numbers from filenames and show it as decimals (e.g. 127.08)
    function a=show_filenames(x,len_ext)
        a=zeros(size(x,1), size(x,2));
        for i=1:size(x,1)
            for j=1:size(x,2)
                s = x(i,j);
                a(i,j) = str2double(s{1}(end-9-len_ext:end-3-len_ext)) + ...
                    str2double(s{1}(end-1-len_ext:end-0-len_ext)) * 0.01;
            end
        end
    end
%
% sort images
% sorts the array/cell according a special scheme
%    parameter: x:    array/cell to sort
%               ext:  file extension (e.g. '.tif')
%               show: if true (and x contains filenames) numbering scheme
%                       of the filenames will be displayed
    function res=sort_img(x,ext,DslPth,M,N,show)
        % define block size and number of blocks
        num_of_blocks_row = 2;
        num_of_blocks_col = 3;
        
        block_size_row = N/num_of_blocks_row; %number of tiles per piezo row
        block_size_col = M/num_of_blocks_col;  %number of tiles per piezo column
        % calculate auxiliary variables
        block_size = block_size_row * block_size_col;
        % default result matrix/cell is a copy of the input matrix/cell
        res = x;
        % check for correct dimensions
        if (size(x,2) ~= block_size_row * num_of_blocks_row) || ...
                (size(x,1) ~= block_size_col * num_of_blocks_col)
            disp('ERROR: incorrect dimensions of image Matrix. Must be 28 x 18.')
            return
        end
        % create matrix with numbers for file name and demo matrix
        num_filename_default = zeros(block_size_row * num_of_blocks_row, block_size_col * num_of_blocks_col, 2);
        num_filename = zeros(block_size_row * num_of_blocks_row, block_size_col * num_of_blocks_col, 2);
        if show
            res_show_default = zeros(block_size_row * num_of_blocks_row, block_size_col * num_of_blocks_col);
            res_show = zeros(block_size_row * num_of_blocks_row, block_size_col * num_of_blocks_col);
        end
        % fill file name array and demo array with values
        block_counter = 1;
        for block_col=num_of_blocks_col:-1:1
            for block_row=1:num_of_blocks_row
                
                for row=1:block_size_row
                    for col=1:block_size_col
                        % fill array with numbers used for file naming
                        row_index = row + (block_row - 1) * block_size_row;
                        col_index = col + (block_col - 1) * block_size_col;
                        if mod(block_row,2)==0
                            col_filename = block_size_col-col + (block_counter - 1) * block_size;
                            
                        else
                            col_filename = col - 1 + (block_counter - 1) * block_size;
                            
                        end
                        if mod(block_row+col,2)==0
                            row_filename = row - 1;
                        else
                            row_filename = block_size_row-row;
                        end
                        num_filename_default(row_index, col_index, 1) = row_filename;
                        num_filename_default(row_index, col_index, 2) = col_filename;
                        % create demo array with double values aaa.bb0c
                        %    aaa = row_number (0,1,2,3,4,5,84,85, ...)
                        %    bb  = column number (0,1,2, ... 13)
                        %    c   = block_number (1,2,3,4,5,6)
                        if show
                            res_show_default(row_index, col_index) = ...
                                col_filename + ...
                                row_filename/100 + ...
                                block_counter/10000;
                        end
                    end
                end
                block_counter = block_counter + 1;
            end
        end
        % sort elements
        % define order of the six sections
        %   +---+---+---+
        %   | 5 | 3 | 1 |
        %   +---+---+---+
        %   | 2 | 4 | 6 |
        %   +---+---+---+
        % offset values have to multiplied by block size
        %  offset[[x-offset(block1) y-offset(block1)] [x-offset(block2) ...
        %  block no.1     2     3     4     5     6
        %         offset = [[1 2];[0 2];[0 1];[1 1];[0 0];[1 0]];
        %         % ... and start sorting
        %         for k=1:size(x,3)
        %             for block_row=1:num_of_blocks_row
        %                 for block_col=1:num_of_blocks_col
        %                     % calculate block number (1,2,3,4,5,6)
        %                     n = (block_row - 1) * num_of_blocks_col + block_col;
        %                     for row=1:block_size_row
        %                         for col=1:block_size_col
        %                             % calculate new indices
        %                             if (mod(col,2) == 0)
        %                                 % odd column numbers (2,4,6 ...): reverse row
        %                                 row_index_new = (block_size_row - row + 1) + (offset(n,1) * block_size_row);
        %                             else
        %                                 row_index_new = row + (offset(n,1) * block_size_row);
        %                             end
        %                             col_index_new = col + (offset(n,2) * block_size_col);
        %                             % calculate old indices
        %                             row_index_old = (block_row - 1) * block_size_row + row;
        %                             col_index_old = (block_col - 1) * block_size_col + col;
        %                             % set new order
        %                             res(col_index_new, row_index_new,k) = x(col_index_old,row_index_old,k);
        %                             % set new order for file names and demo (only one times)
        %                             if (k == 1)
        %                                 num_filename(row_index_new, col_index_new,1) = num_filename_default(row_index_old,col_index_old,1);
        %                                 num_filename(row_index_new, col_index_new,2) = num_filename_default(row_index_old,col_index_old,2);
        %                                 if show
        %                                     res_show(row_index_new, col_index_new) = res_show_default(row_index_old,col_index_old);
        %                                 end
        %                             end
        %                         end
        %                     end
        %                 end
        %             end
        %         end
        num_filename=num_filename_default;
        % create new filenames
        % please note: the order of the condition is important
        %   because x could be a matrix (then it is no cell and of course doesn't
        %   contain filenames); if x is a cell then it can contain for
        %   example function handles (which are of course no filenames and
        %   also do not consist of characters. Only if x is a cell AND contains
        %   characters it will be considered containing filenames
        if iscell(x) && ischar(x{1,1,1})
            % x is a cell with characters that means x contains filenames
            % set offset for first file
            offset_num_filename_col = (C.allslice_arr(1,2)-1)*M*N;
            for k=1:(size(x,3))
                for i=1:size(x,2)
                    for j=1:size(x,1)
                        s = x(j,i,k);
                        if isempty(s{1})
                            continue
                        end
                        % create filename: #######ccccccc_rrxxxx
                        % ####### = use existing part of filename
                        % ccccccc = column
                        % rr      = row
                        % xxxx    = extension
                        num_filename_col = num_filename(i, j, 2) + (k-1)*size(x,1)*size(x,2)+ offset_num_filename_col;
                        num_filename_row = num_filename(i, j, 1);
                        res(j,i,k) = cellstr(strcat(s{1}(1:end-10-length(ext)), ...
                            sprintf('%07d', num_filename_col), '_', ...
                            sprintf('%02d', num_filename_row), ext));
                    end
                end
            end
            % show result of sorting
            if show
                disp(res_show)
            end
        end
    end


C.samplestr='ex526';
C.slPth=4;

% name of cubing from Google Drive document
C.cubingName=feval(@(x)x{1},regexp(mfilename(),'_','split'));
% date the cubing was done
C.datestr=feval(@(x)x(length(C.cubingName)+2:end),strrep(mfilename(),'_','-'));

% date the images where taken, can be set to 'auto' to enable autodetection
C.imdatestr='2013-11-11';


C.useEveryNthSlice=1;
% If true, all weights and shifts are reset to zero before LS is started
C.naive=false;
C.M=1;   %   Number of tiles per row
C.N=1;   %  Number of tiles per column


%C.xcorr_size=[1024 512;384 2048; 1536 2048]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
C.xcorr_size{1}=repmat(cat(3,2048, 256),C.M-1,C.N); %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
C.xcorr_size{2}=repmat(cat(3,192, 3072),C.M,C.N-1);
C.xcorr_size{3}=repmat(cat(3,2048, 3072),C.M,C.N);
C.xcorr_size{1}([6 12],:,2)=3072;
C.xcorr_size{2}(:,14,1)=2048;

C.whichDimsShiftCalc=1:3;

C.maxMagHigh=512;


C.projecttype='cortex';

C.document_ccs='full';

% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% C.joblistF=@(x)setdiff(1:x,b);
%

C.machineroot='~/';

%ending of initial files
C.sourceEnding='.tif';
C.parallel_mode='for';
C.parallel_mode_cubes='for';

C.subfoldersForSourceTifs='two';

C.allslice_arr = [
    179 1 30   1 C.N 1 C.M
    ]; %Be careful, here the order is Z-direction, then N, then M!

C.P=ceil(sum(C.allslice_arr(:,3)-C.allslice_arr(:,2)+1)/C.slPth)*C.slPth;

%allows to make changes to all_fns after makeFileListFun is done;
%Example: just use image 3,4 from all slices
%It's not possible to modify the third component
%D.all_fns_Modifier=@(x)x(3,4,:);
%D.all_fns_Modifier=@(x)flipdim(flipdim(x,1),2);
D.all_fns_Modifier=@(x)x;
%D.all_fns_Modifier=@(x)sort_img(x,C.sourceEnding,C.slPth,C.M,C.N,true);

% functions and modifier
C.makeFileListFun=@KMBmakeFileList;
% show_filenames shows the file numbers in x as doubles
% helpful function for debugging
C.show_fnsF=@(x)show_filenames(x,length(C.sourceEnding));


%C.joblistF=@(x)updater(x,C.slPth,D.exclude);
%C.joblistF=@(x)updater(x,C.slPth,[134]);
C.joblistF=@(x)1:x;

C.makedir=true;   %Creates directories for cubes or not
%make array for epoxy inclusion
C.weightarray=cell(3,1);
x=Inf;
o=-Inf;
C.weightarray{1}=zeros(C.N,C.M-1)+o;
C.weightarray{2}=zeros(C.N-1,C.M)+o;
C.weightarray{3}=zeros(C.N,C.M)+o;
% C.weightarray{1}=[
%     x x x x x x;
%     x x x o o o;
%     x x o o o o;
%     x x o o o o;
%     x o o o o o;
%     x o o o o o;
%     x x o o o o;
%     x x x x x o;
%     x x x x x x;
%     x x x x x x;
%     x x x x x x];
% C.weightarray{2}=[
%     x x x x x o x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o x;
%     x x o o o o x;
%     x x x o o o x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];
% C.weightarray{3}=[
%     x x x x x x x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x x o o o o;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];


C.pixelSize=[11.24 11.24 28];

% create specific preshifts
%c.usePreShiftsTransversal=cell(1,2);
%preshift_x_top = zeros(17,1);
%preshift_x_bottom = zeros(17,1);
%preshift_y_top =  zeros(17,1);
%preshift_y_bottom =  zeros(17,1);
%
%for i=1:18-1
%    for j=1:28
%        if j<=14
%            C.usePreShiftsTransversal{1}(i,j,1,1) = preshift_x_top(i);
%            C.usePreShiftsTransversal{1}(i,j,1,2) = preshift_y_top(i);
%        else
%            C.usePreShiftsTransversal{1}(i,j,1,1) = preshift_x_bottom(i);
%            C.usePreShiftsTransversal{1}(i,j,1,2) = preshift_y_bottom(i);
%        end
%        
%    end
%end
%
%preshift_x_default = 0;
%preshift_y_default = 0;
%preshift_y_edge = zeros(18,1);
%for i=1:18
%    for j=1:28-1
%        C.usePreShiftsTransversal{2}(i,j,1,1) = preshift_x_default;
%        if mod(j,14)==0
%            C.usePreShiftsTransversal{2}(i,j,1,2) = preshift_y_edge(i);%+C.xcorr_size(2,1);
%        else
%            C.usePreShiftsTransversal{2}(i,j,1,2) = preshift_y_default;
%        end
%    end
%end
C.usePreShiftsTransversal=false;

% (:,:,:,1) determins y-shifts. Keep in mind that a value of e.g. 1000
% means that the images are shifted by 1000 pixels relative to each other,
% so this would access each of them with indices shifted by 500
D.preShiftsLongitudinal=zeros(C.M,C.N,C.P,2);

%invert ("255-x") output cubes
C.invertCubes=false;

%if defaultOrigin is false, C.origin has to be set:
%C.origin='Y:\st005Top';
%D.defaultOrigin=true;
%C.origin='F:\martin\cortex17x3';
C.defaultOrigin=true;

%FEI stuff von Kevin!
C.tifNamingSchemePiezoFEI=false;


C.saveResliceAndHistogramWhileCubing=false;

%is applied to every source tif image directly after reading
%D.imageModifier=repmat(feval(@(x)[x rot90(x,2);rot90(x,2) x;x rot90(x,2)],repmat([{@(x)x}; {@(x)rot90(x,2)}],C.M/6,C.N/2)),[1 1 C.P+1]);
%D.imageModifier=repmat(feval(@(x)[x rot90(x,2);x rot90(x,2);x rot90(x,2)], ...
%    repmat([{@(x)cut_img(x)}; {@(x)rot90(cut_img(x),2)}],C.M/6,C.N/2)),[1 1 C.P+1]);
D.imageModifier=repmat({@(x)x},[C.M,C.N,C.P+1]);
%is applied to imageModifier cell arry
D.imageModifierModifier=@(x)x;

%changes orientation of every second column
C.motorSnake=false;

%makes sense to set to false if only very few crosscorrelations are
%calculated
C.debugOptimizeFFT=true;

%the highmag cubes are scaled down at the same rate in all three dimensions
C.scaleDownZ=true;


%save data for super fast iterations
C.saveDataForSuperRapidIteration=true;

%slices to exclude
C.exclude=[];

%if false, slices are silently excluded, if true, spacers are introduces
%and numbering is shifted
C.exludeKeepCounting=false;

%if not empty, only certain cross correlations are computed
%no mat files with CC results are saved
%per line: direction x y z
%C.subsetCC=[1 7 6 300;1 7 5 301];
C.subsetCC=[];

%are inter image differences be calculated during low memory tif writer
C.calcIID=false;

C.maxMagHigh=8;

%if empty, manual cell mode in KMBstart, otherwise include strings 'cubes','high' or 'tifs'
C.outputToDo='';

C.outtifresizefactor=1;
%weight adjustment for z deltas
C.zFactor=1;
C.ttempIterations=1;
C.writeRawsWithTifs=[];
C.small=false;

end

function D=st17x3_2013_11_01()

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end

D.C.samplestr='exR286';
D.C.slPth=8;

% name of cubing from Google Drive document
D.C.cubingName='st17x3';
D.C.cubingName=feval(@(x)x{1},regexp(mfilename(),'_','split'));
% date the cubing was done
D.C.datestr='2013-11-01';
%D.C.datestr=feval(@(x)x(length(D.C.cubingName)+2:end),strrep(mfilename(),'_','-'));

% date the images where taken
D.C.imdatestr='2013-10-29';

D.C.useEveryNthSlice=1;
% If true, all weights and shifts are reset to zero before LS is started
D.C.naive=false;

D.C.xcorr_size=[1024 512;384 2048; 1536 2048]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction

D.C.M=7;    %Number of tiles per row
D.C.N=11;   %Number of tiles per column
D.makeFileListFun=@KMBmakeFileList;
D.C.projecttype='entorhinal';

D.C.document_ccs=false;

% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
%
D.C.machineroot='//FS/bmo/mberning/forHelene/';

D.parallel_mode='parfor';
D.parallel_mode_cubes='for';

D.C.subfoldersForSourceTifs='two';

D.C.allslice_arr = [
    171 1 100 1 D.C.N 1 D.C.M
    ]; %Be careful, here the order is Z-direction, then N, then M!

D.C.P=ceil(sum(D.C.allslice_arr(:,3)-D.C.allslice_arr(:,2)+1)/D.C.slPth)*D.C.slPth;

%allows to make changes to all_fns after makeFileListFun is done;
%Example: just use image 3,4 from all slices
%It's not possible to modify the third component
%D.all_fns_Modifier=@(x)x(3,4,:);
D.all_fns_Modifier=@(x)flipdim(flipdim(x,1),2);
% D.all_fns_Modifier=@(x)x;


%D.C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
%D.C.joblistF=@(x)updater(x,D.C.slPth,[134]);
D.C.joblistF=@(x)1:x;

D.C.makedir=true;   %Creates directories for cubes or not
%make array for epoxy inclusion
D.C.weightarray=cell(3,1);
x=Inf;
o=-Inf;
D.C.weightarray{1}=zeros(D.C.N,D.C.M-1)+o;
D.C.weightarray{2}=zeros(D.C.N-1,D.C.M)+o;
D.C.weightarray{3}=zeros(D.C.N,D.C.M)+o;
% D.C.weightarray{1}=[
%     x x x x x x;
%     x x x o o o;
%     x x o o o o;
%     x x o o o o;
%     x o o o o o;
%     x o o o o o;
%     x x o o o o;
%     x x x x x o;
%     x x x x x x;
%     x x x x x x;
%     x x x x x x];
% D.C.weightarray{2}=[
%     x x x x x o x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o x;
%     x x o o o o x;
%     x x x o o o x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];
% D.C.weightarray{3}=[
%     x x x x x x x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x x o o o o;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];


D.C.pixelSize=[36.62 36.62 1600];

D.C.usePreShiftsTransversal=false;
% (:,:,:,1) determins y-shifts. Keep in mind that a value of e.g. 1000
% means that the images are shifted by 1000 pixels relative to each other,
% so this would access each of them with indices shifted by 500
D.C.preShiftsLongitudinal=zeros(D.C.M,D.C.N,D.C.P,2);

%invert ("255-x") output cubes
D.C.invertCubes=false;

%if defaultOrigin is false, D.C.origin has to be set:
%D.C.origin='Y:\st005Top';
D.defaultOrigin=true;

%FEI stuff von Kevin!
D.C.tifNamingSchemePiezoFEI=false;


D.C.saveResliceAndHistogramWhileCubing=false;

%is applied to every source tif image directly after reading
D.imageModifier=repmat({@(x)x},[D.C.M,D.C.N,D.C.P+1]);
%is applied to imageModifier cell arry
D.imageModifierModifier=D.all_fns_Modifier;

%changes orientation of every second column
D.C.motorSnake=true;

%makes sense to set to false if only very few crosscorrelations are
%calculated
D.C.debugOptimizeFFT=true;

%the highmag cubes are scaled down at the same rate in all three dimensions
D.C.scaleDownZ=false;

%save data for super fast iterations
D.C.saveDataForSuperRapidIteration=false;

%ending of initial files
D.sourceEnding='.tif';


%slices to exclude
D.exclude=[];

%if false, slices are silently excluded, if true, spacers are introduces
%and numbering is shifted
D.exludeKeepCounting=false;

%if not empty, only certain cross correlations are computed
%no mat files with CC results are saved
%per line: direction x y z
%D.C.subsetCC=[1 7 6 300;1 7 5 301];
D.C.subsetCC=[];

%are inter image differences be calculated during low memory tif writer
D.C.calcIID=false;

D.C.maxMagHigh=1;  % ???

%if empty, manual cell mode in KMBstart, otherwise include strings 'cubes','high' or 'tifs'
D.outputToDo='';

end

function D=st141()

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end

% name of sample
D.C.samplestr='M0609';

% # of slices per thread 
D.C.slPth=8;

% name of cubing from Google Drive document
D.C.cubingName=feval(@(x)x{1},regexp(mfilename(),'_','split'));

% date the cubing was done
D.C.datestr=feval(@(x)x(length(D.C.cubingName)+2:end),strrep(mfilename(),'_','-'));

% date the images where taken
D.C.imdatestr='2013-07-24';

% ? under sampling
D.C.useEveryNthSlice=1;

D.C.naive=false;
% If true, all weights and shifts are reset to zero before LS is started

D.C.xcorr_size=[1768 512; 512 2024; 1768 2024]; 
% correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction

D.C.M=2;    % Number of tiles per row
D.C.N=3;    % Number of tiles per column

D.makeFileListFun=@KMBmakeFileList;

% name of the current project
D.C.projecttype='';

% ??
D.C.document_ccs=true;

D.C.machineroot='C:/';

D.parallel_mode='parfor';

D.C.subfoldersForSourceTifs='none';

% This array is used to read out the data from the location specified in D.C.origin.
D.C.allslice_arr = [141 1 8 1 D.C.N 1 D.C.M]; 
% [#stack, 1st z-layer, last z-layer, 1st x-layer, last x-layer, 1st y-layer, last y-layer]
% Be careful, here the order is Z-direction, then N, then M!

% # of z-layers
D.C.P=ceil(sum(D.C.allslice_arr(:,3)-D.C.allslice_arr(:,2)+1)/D.C.slPth)*D.C.slPth;

% allows to make changes to all_fns after makeFileListFun is done;
% Example: just use image 3,4 from all slices
% It's not possible to modify the third component
% D.all_fns_Modifier=@(x)x(3,4,:);
% D.all_fns_Modifier=@(x)flipdim(flipdim(x,1),2);
D.all_fns_Modifier=@(x)x;

%D.C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
%D.C.joblistF=@(x)updater(x,D.C.slPth,[134]);
D.C.joblistF=@(x)1:x;

D.C.cubingName='st141x1';

% if creates directories
D.C.makedir=true;   

% make array for epoxy inclusion
D.C.weightarray=cell(3,1);
x=Inf;
o=-Inf;
D.C.weightarray{1}=zeros(D.C.N,D.C.M-1)+o;
D.C.weightarray{2}=zeros(D.C.N-1,D.C.M)+o;
D.C.weightarray{3}=zeros(D.C.N,D.C.M)+o;

D.C.pixelSize=[36.62 36.62 200];

D.C.usePreShiftsTransversal=false;
% (:,:,:,1) determins y-shifts. Keep in mind that a value of e.g. 1000
% means that the images are shifted by 1000 pixels relative to each other,
% so this would access each of them with indices shifted by 500

D.C.preShiftsLongitudinal=zeros(D.C.M,D.C.N,D.C.P,2);

%invert ("255-x") output cubes
D.C.invertCubes=false;

%if defaultOrigin is false, D.C.origin has to be set:
%D.C.origin='Y:\st005Top';
D.defaultOrigin=true;

%FEI stuff von Kevin! 
D.C.tifNamingSchemePiezoFEI=false;

D.parallel_mode_cubes='for';
D.C.saveResliceAndHistogramWhileCubing=false;

%is applied to every source tif image directly after reading
D.imageModifier=repmat({@(x)x},[D.C.M, D.C.N, D.C.P+1]);

%is applied to imageModifier cell arry
D.imageModifierModifier=D.all_fns_Modifier;

%changes orientation of every second column
D.C.motorSnake=false;

%makes sense to set to false if only very few crosscorrelations are
%calculated
D.C.debugOptimizeFFT=true;

%the highmag cubes are scaled down at the same rate in all three dimensions
D.C.scaleDownZ=true;

%save data for super fast iterations
D.C.saveDataForSuperRapidIteration=false;

%ending of initial files
D.sourceEnding='.tif';

%slices to exclude
D.exclude=[];

%if false, slices are silently excluded, if true, spacers are introduces
%and numbering is shifted
D.exludeKeepCounting=false;

end
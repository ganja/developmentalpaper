function [C, D]=st18x3_2013_12_04()

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end
    function res=change_filenames(x)
        % default result matrix/cell is a copy of the input matrix/cell
        res = x;
        if iscell(x) && ischar(x{1,1,1})
            for i=1:size(x,3)
                if ~isempty(x{1,1,i})
                    % get position of slashes or backslashes
                    pos = strfind(res{1,1,i},'\');
                    if size(pos,2) == 0
                        pos = strfind(res{1,1,i},'/');
                    end
                    if size(pos,2) > 0
                        % get position of last slash/backslash
                        pos = pos(size(pos, 2));
                        % create filename
                        res{1,1,i} = strcat(res{1,1,i}(1:pos), ...
                            'BS 1 test29 1.0kv 30uapHC 13pix 1k1k 70nm thick 4usec_3VBSED_slice_', ...
                            sprintf('%04d', (i - 1)), '.tif');
                    end
                end
            end
        end
    end
%C.samplestr='exR286';
C.samplestr='BS_1_test29';
C.slPth=8;

% name of cubing from Google Drive document
%C.cubingName=feval(@(x)x{1},regexp(mfilename(),'_','split'));
C.cubingName='st18x3';
% date the cubing was done
C.datestr='2013-12-04';
%C.datestr=feval(@(x)x(length(D.C.cubingName)+2:end),strrep(mfilename(),'_','-'));

% date the images where taken
C.imdatestr='2013-11-27';

C.useEveryNthSlice=1;
% If true, all weights and shifts are reset to zero before LS is started
C.naive=false;

C.M=1;   %Number of tiles per row
C.N=1;   %Number of tiles per column


%C.xcorr_size=[1024 512;384 2048; 1536 2048]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
C.xcorr_size{1}=repmat(cat(3,1024, 1024),C.M-1,C.N); %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
C.xcorr_size{2}=repmat(cat(3,1024, 1024),C.M,C.N-1);
C.xcorr_size{3}=repmat(cat(3,1024, 1024),C.M,C.N);

C.whichDimsShiftCalc=1:3;

C.makeFileListFun=@KMBmakeFileList;
C.projecttype='other';

% 'none', 'full' or 'iid'
C.document_ccs='full';

% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
%
%C.machineroot='Y:/entorhinall/st171Top/st171/';
%C.machineroot='~/';
%C.machineroot='/home/boergens/cubing_data/mzauser/st183/';
C.machineroot='C:\martin\';

C.parallel_mode='parfor';
C.parallel_mode_cubes='for';

C.subfoldersForSourceTifs='none';

C.allslice_arr = [
    183 1 52 1 C.N 1 C.M
    ]; %Be careful, here the order is Z-direction, then N, then M!

C.P=ceil(sum(C.allslice_arr(:,3)-C.allslice_arr(:,2)+1)/C.slPth)*C.slPth;

%allows to make changes to all_fns after makeFileListFun is done;
%Example: just use image 3,4 from all slices
%It's not possible to modify the third component
%D.all_fns_Modifier=@(x)x(3,4,:);
%D.all_fns_Modifier=@(x)flipdim(flipdim(x,1),2);
%D.all_fns_Modifier=@(x)x;
D.all_fns_Modifier=@(x)change_filenames(x);


%C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
%C.joblistF=@(x)updater(x,D.C.slPth,[134]);
C.joblistF=@(x)1:x;

C.makedir=true;   %Creates directories for cubes or not
%make array for epoxy inclusion
C.weightarray=cell(3,1);
x=Inf;
o=-Inf;
C.weightarray{1}=zeros(C.N,C.M-1)+o;
C.weightarray{2}=zeros(C.N-1,C.M)+o;
C.weightarray{3}=zeros(C.N,C.M)+o;
% C.weightarray{1}=[
%     x x x x x x;
%     x x x o o o;
%     x x o o o o;
%     x x o o o o;
%     x o o o o o;
%     x o o o o o;
%     x x o o o o;
%     x x x x x o;
%     x x x x x x;
%     x x x x x x;
%     x x x x x x];
% C.weightarray{2}=[
%     x x x x x o x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o x;
%     x x o o o o x;
%     x x x o o o x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];
% C.weightarray{3}=[
%     x x x x x x x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x x o o o o;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];


C.pixelSize=[13 13 70];

C.usePreShiftsTransversal=false;
% (:,:,:,1) determins y-shifts. Keep in mind that a value of e.g. 1000
% means that the images are shifted by 1000 pixels relative to each other,
% so this would access each of them with indices shifted by 500
D.preShiftsLongitudinal=zeros(C.M,C.N,C.P,2);

%invert ("255-x") output cubes
C.invertCubes=false;

%if defaultOrigin is false, C.origin has to be set:
%C.origin='Y:\st005Top';
%C.defaultOrigin=true;
C.defaultOrigin=false;
%C.origin='/home/boergens/entorhinal';
C.origin='C:/martin';

%FEI stuff von Kevin!
C.tifNamingSchemePiezoFEI=false;


C.saveResliceAndHistogramWhileCubing=false;

%is applied to every source tif image directly after reading
D.imageModifier=repmat({@(x)x},[C.M,C.N,C.P+1]);
%is applied to imageModifier cell arry
D.imageModifierModifier=D.all_fns_Modifier;

%changes orientation of every second column
C.motorSnake=true;

%makes sense to set to false if only very few crosscorrelations are
%calculated
C.debugOptimizeFFT=true;

%the highmag cubes are scaled down at the same rate in all three dimensions
C.scaleDownZ=true;

%save data for super fast iterations
C.saveDataForSuperRapidIteration=false;

%ending of initial files
C.sourceEnding='.tif';


%slices to exclude
C.exclude=[];

%if false, slices are silently excluded, if true, spacers are introduces
%and numbering is shifted
C.exludeKeepCounting=false;

%if not empty, only certain cross correlations are computed
%no mat files with CC results are saved
%per line: direction x y z
%D.C.subsetCC=[1 7 6 300;1 7 5 301];
C.subsetCC=[];

%are inter image differences be calculated during low memory tif writer
C.calcIID=true;

C.maxMagHigh=8;

C.outtifresizefactor=1;
%weight adjustment for z deltas
C.zFactor=1;
C.ttempIterations=1;
C.writeRawsWithTifs=[];
C.small=false;

%if empty, manual cell mode in KMBstart, otherwise include strings 'cubes','high' or 'tifs'
C.outputToDo='';

end

function [C, D]=st18x1b_2013_12_03()
% version 0.05  2013-11-13

    function todo=updater(C,old, new)
        todo=[];
        diff=find(1-min(min(strcmp(old,new),[],2),[],1));
        for i=1:length(diff)
            if ~isempty(new{1,1,diff(i)})
                if mod(diff(i)-1,C.slPth)==0
                    todo=[todo max(ceil(diff(i)/C.slPth)-1,1) ceil(diff(i)/C.slPth)];
                else
                    todo=[todo ceil(diff(i)/C.slPth)];
                end
            end
        end
        todo=unique(todo);
    end
% cuts one row/column if image has odd number of pixels in x or y direction
    function res=cut_img(x)
        res=x;
        if mod(size(x,1),2) && mod(size(x,2),2)      % if number of rows is odd then ...
            res=[x ones(size(x,1),1)*mean(x(:)); ones(1,size(x,2)+1)*mean(x(:))]; % ... add row and column
        end
        
    end

    function y=repair(x)
        x(:,:,1,:)=x(:,:,2,:);
        x(:,:,49,:)=x(:,:,48,:);
        x(:,:,50,:)=x(:,:,51,:);
        x(:,:,94,:)=x(:,:,93,:);
        x(:,:,186,:)=x(:,:,187,:);
        x(:,:,124,:)=x(:,:,123,:);
        x(:,:,205,:)=x(:,:,206,:);
        x(:,:,197,:)=x(:,:,198,:);
        y={};
        for iii=1:size(x,3);
            if iscell(x(1))
                
                if ischar(x{1,1,1})
                    if ~isempty(x{1,1,iii})&&strcmp(x{1,1,iii}(114),'n');
                        y=cat(3,y,x(:,:,iii));
                    end
                else
                    if isequal(x{1,1,iii},C.imageModifierBase{1})
                        y=cat(3,y,x(:,:,iii));
                    end
                end
            end
        end
        if isempty(y)
            y=x;
        end
    end

C.samplestr='ex536';
C.slPth=8;
 
% name of cubing from Google Drive document
C.cubingName=feval(@(x)x{1},regexp(mfilename(),'_','split'));
% date the cubing was done
C.datestr=feval(@(x)x(length(C.cubingName)+2:end),strrep(mfilename(),'_','-'));

C.useEveryNthSlice=1;
% If true, all weights and shifts are reset to zero before LS is started
C.naive=false;
C.M=6;   %   Number of tiles per row
C.N=14;   %Piplica   Number of tiles per column


%C.xcorr_size=[1024 512;384 2048; 1536 2048]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
C.xcorr_size{1}=repmat(cat(3,2048, 256),C.M-1,C.N); %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
C.xcorr_size{2}=repmat(cat(3,192, 3072),C.M,C.N-1);
C.xcorr_size{3}=repmat(cat(3,2048, 3072),C.M,C.N);

C.whichDimsShiftCalc=1:3;

C.maxMagHigh=512;

C.projecttype='cortex';

%none, full, iid, normal
C.document_ccs='iid';

% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% C.joblistF=@(x)setdiff(1:x,b);
%

C.machineroot='/home/boergens/cubing_data/boergens/';

%ending of initial files
C.sourceEnding='.tif';
C.parallel_mode='parfor';
C.parallel_mode_cubes='for';


C.allslice_arr = [
    181 1 350  1 C.N 1 C.M
    ]; %Be careful, here the order is Z-direction, then N, then M!

C.P=ceil(sum(C.allslice_arr(:,3)-C.allslice_arr(:,2)+1)/C.slPth)*C.slPth;

%allows to make changes to all_fns after makeFileListFun is done;
%Example: just use image 3,4 from all slices
%It's not possible to modify the third component
%D.all_fns_Modifier=@(x)x(3,4,:);
%D.all_fns_Modifier=@(x)flipdim(flipdim(x,1),2);
D.all_fns_Modifier=@repair;
%D.all_fns_Modifier=@(x)sort_img(x,C.sourceEnding,C.slPth,C.M,C.N,true);



%C.joblistF=@(x)updater(x,C.slPth,D.exclude);
%C.joblistF=@(x)updater(x,C.slPth,[134]);
C.joblistF=@(x)1:x;

C.makedir=true;   %Creates directories for cubes or not
%make array for epoxy inclusion
C.weightarray='auto';

C.pixelSize=[11.24 11.24 28];

C.usePreShiftsTransversal=false;

% (:,:,:,1) determins y-shifts. Keep in mind that a value of e.g. 1000
% means that the images are shifted by 1000 pixels relative to each other,
% so this would access each of them with indices shifted by 500
D.preShiftsLongitudinal=zeros(C.M,C.N,C.P,2);

%invert ("255-x") output cubes
C.invertCubes=false;

%if defaultOrigin is false, C.origin has to be set:
%C.origin='Y:\st005Top';
%D.defaultOrigin=true;
%C.origin='F:\martin\cortex17x3';
C.defaultOrigin=true;

%FEI stuff von Kevin!
C.tifNamingSchemePiezoFEI=true;


C.saveResliceAndHistogramWhileCubing=false;

%is applied to every source tif image directly after reading
%D.imageModifier=repmat(feval(@(x)[x rot90(x,2);rot90(x,2) x;x rot90(x,2)],repmat([{@(x)x}; {@(x)rot90(x,2)}],C.M/6,C.N/2)),[1 1 C.P+1]);
%D.imageModifier=repmat(feval(@(x)[x rot90(x,2);x rot90(x,2);x rot90(x,2)], ...
%    repmat([{@(x)cut_img(x)}; {@(x)rot90(cut_img(x),2)}],C.M/6,C.N/2)),[1 1 C.P+1]);
C.imageModifierBase={@(x)cut_img(x),@(x)rot90(rot90(cut_img(x)))};


%makes sense to set to false if only very few crosscorrelations are
%calculated
C.debugOptimizeFFT=true;

%the highmag cubes are scaled down at the same rate in all three dimensions
C.scaleDownZ=true;


%save data for super fast iterations
C.saveDataForSuperRapidIteration=false;

%slices to exclude
C.exclude=[8 17 18 70 90 91 95 99 140 141 142 216 250 264 311 330 343 344];

%if false, slices are silently excluded, if true, spacers are introduces
%and numbering is shifted
C.exludeKeepCounting=false;

%if not empty, only certain cross correlations are computed
%no mat files with CC results are saved
%per line: direction x y z
%C.subsetCC=[1 7 6 300;1 7 5 301];
C.subsetCC=[];

%are inter image differences be calculated during low memory tif writer
C.calcIID=false;

C.maxMagHigh=8;

%if empty, manual cell mode in KMBstart, otherwise include strings 'cubes','high' or 'tifs'
C.outputToDo='';

C.outtifresizefactor=1;
%weight adjustment for z deltas
C.zFactor=1;
C.ttempIterations=1;
C.writeRawsWithTifs=[];
C.small=true;

end

function D=st005()

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end

D.C.samplestr='R276';
D.C.slPth=8;
D.C.stackN='005interm';
D.C.datestr='2013-06-05';
D.C.origin='D:\st005Top';
D.exclude=[];
D.C.imdatestr='2013-05-30';

% If true, all weights and shifts are reset to zero before LS is started
D.C.naive=false;
D.C.xcorrmax=1024;

%allows to make changes to all_fns after makeFileListFun is done;
%Example: just use image 3,4 from all slices
%It's not possible to modify the third component
%D.all_fns_Modifier=@(x)x(3,4,:);
D.all_fns_Modifier=@(x)flipdim(flipdim(x,1),2);

D.C.xcorr_size=[1024 512;384 2048;D.C.xcorrmax D.C.xcorrmax]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
D.C.M=7;    %Number of tiles per row
D.C.N=11;   %Number of tiles per column
D.makeFileListFun=@KMBmakeFileList;
D.C.projecttype='entorhinal';

D.document_ccs=false;

% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
%
D.C.machineroot='D:/';

D.parallel_mode='pool';

D.C.subfoldersForSourceTifs='none';

D.C.allslice_arr = [
    5 1 115 1 D.C.N 1 D.C.M
    ]; %Be careful, here the order is Z-direction, then N, then M!
%D.C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
%D.C.joblistF=@(x)updater(x,D.C.slPth,[134]);
D.C.joblistF=@(x)1:x;

D.C.makedir=true;   %Creates directories for cubes or not
%make array for epoxy inclusion
D.C.weightarray=cell(3,1);
x=Inf;
o=-Inf;
D.C.weightarray{1}=[
    x x x x x x;
    x x x o o o;
    x x o o o o;
    x x o o o o;
    x o o o o o;
    x o o o o o;
    x x o o o o;
    x x x x x o;
    x x x x x x;
    x x x x x x;
    x x x x x x];
D.C.weightarray{2}=[
    x x x x x o x;
    x x x o o o o;
    x x x o o o o;
    x x o o o o o;
    x x o o o o x;
    x x o o o o x;
    x x x o o o x;
    x x x x x x x;
    x x x x x x x;
    x x x x x x x];
D.C.weightarray{3}=[
    x x x x x x x;
    x x x o o o o;
    x x x o o o o;
    x x o o o o o;
    x x o o o o o;
    x x o o o o o;
    x x x o o o o;
    x x x x x x x;
    x x x x x x x;
    x x x x x x x;
    x x x x x x x];

% D.C.weightarray{1}=zeros(D.C.N,D.C.M-1)+o;
% D.C.weightarray{2}=zeros(D.C.N-1,D.C.M)+o;
% D.C.weightarray{3}=zeros(D.C.N,D.C.M)+o;

D.C.pixelSize=[36.62 36.62 200];

D.C.usePreShifts=false;

%FEI forgot to invert images, don't change
D.C.invertCubes=false;

%if defaultOrigin is false, D.C.origin has to be set:
%D.C.origin='Y:\st005Top';
D.defaultOrigin=false;

%FEI Rotz von Kevin!
D.C.tifNamingSchemePiezoFEI=false;

D.parallel_mode_cubes='for';
D.C.saveResliceAndHistogramWhileCubing=false;

end
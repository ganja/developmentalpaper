function D=st07x2()

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end

D.C.slPth=8;
D.C.stackN='07x2';
D.C.datestr='2012-11-04';
D.C.origin=['/zdata/kevin/cortex/tifs/st07x2/'];
D.exclude=[];
D.C.xcorrmax=128;
D.C.xcorr_size=[1024 128;128 1024;D.C.xcorrmax D.C.xcorrmax];

% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
% 
D.exclude=[113 312 380-1 513 672 688-1 877-1 1090 1093 1149 1153 1276 1306 1568 2146 2506 2502 2601 2739 2812 2903 2952 3362 3369];


D.C.allslice_arr = [
    76 1 113 1 3 1 3;
    77 2 2235 1 3 1 3;
    78 1 1010 1 3 1 3;
    79 1 65 1 3 1 3
    ];
%D.C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
%D.C.joblistF=@(x)updater(x,D.C.slPth,[134]);
D.C.joblistF=@(x)1:x;
D.all_fns_Modifier=@(x)x(2,2,1001:1129);
D.C.makedir=true;
D.C.subtiled=true;
D.C.M=3;
D.C.N=3;
end
function D=st17x2_2013_10_30()
    % version 0.02  2013-10-31

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end
    % extract numbers from filenames and show it as decimals (e.g. 127.08)
    function a=show_filenames(x,len_ext)
        a=zeros(size(x,1), size(x,2));
        for i=1:size(x,1)
            for j=1:size(x,2)
                s = x(i,j);
                a(i,j) = str2double(s{1}(end-9-len_ext:end-3-len_ext)) + ...
                         str2double(s{1}(end-1-len_ext:end-0-len_ext)) * 0.01;
            end
        end
    end
    %
    % sort images
    % sorts the array/cell according a special scheme
    %    parameter: x:    array/cell to sort
    %               ext:  file extension (e.g. '.tif')
    %               show: if true (and x contains filenames) numbering scheme
    %                       of the filenames will be displayed
    function res=sort_img(x,ext,show)
        % define block size and number of blocks
        block_size_row = 14;
        block_size_col = 6;
        num_of_blocks_row = 2;
        num_of_blocks_col = 3;
        % calculate auxiliary variables
        block_size = block_size_row * block_size_col;
        % default result matrix/cell is a copy of the input matrix/cell
        res = x;
        % check for correct dimensions
        if (size(x,1) ~= block_size_row * num_of_blocks_row) || ...
           (size(x,2) ~= block_size_col * num_of_blocks_col)
            disp('ERROR: incorrect dimensions of image Matrix. Must be 28 x 18.')
            return
        end
        % create matrix with numbers for file name and demo matrix
        num_filename_default = zeros(block_size_row * num_of_blocks_row, block_size_col * num_of_blocks_col, 2);
        num_filename = zeros(block_size_row * num_of_blocks_row, block_size_col * num_of_blocks_col, 2);
        if show
            res_show_default = zeros(block_size_row * num_of_blocks_row, block_size_col * num_of_blocks_col);
            res_show = zeros(block_size_row * num_of_blocks_row, block_size_col * num_of_blocks_col);
        end
        % fill file name array and demo array with values
        block_counter = 1;
        for block_row=1:num_of_blocks_row
            for block_col=1:num_of_blocks_col
                for row=1:block_size_row
                    for col=1:block_size_col
                        % fill array with numbers used for file naming
                        row_index = row + (block_row - 1) * block_size_row;
                        col_index = col + (block_col - 1) * block_size_col;
                        row_filename = row - 1;
                        col_filename = col - 1 + (block_counter - 1) * block_size;
                        num_filename_default(row_index, col_index, 1) = row_filename;
                        num_filename_default(row_index, col_index, 2) = col_filename;
                        % create demo array with double values aaa.bb0c
                        %    aaa = row_number (0,1,2,3,4,5,84,85, ...)
                        %    bb  = column number (0,1,2, ... 13)
                        %    c   = block_number (1,2,3,4,5,6)
                        if show
                            res_show_default(row_index, col_index) = ...
                                col_filename + ...
                                row_filename/100 + ...
                                block_counter/10000;
                        end
                    end
                end
                block_counter = block_counter + 1;
            end
        end
        % sort elements
        % define order of the six sections
        %   +---+---+---+
        %   | 5 | 3 | 1 |
        %   +---+---+---+
        %   | 2 | 4 | 6 |
        %   +---+---+---+
        % offset values have to multiplied by block size
        %  offset[[x-offset(block1) y-offset(block1)] [x-offset(block2) ...
        %  block no.1     2     3     4     5     6
        offset = [[0 2];[1 0];[0 1];[1 1];[0 0];[1 2]];
        % ... and start sorting
        for k=1:size(x,3)
            for block_row=1:num_of_blocks_row
                for block_col=1:num_of_blocks_col
                    % calculate block number (1,2,3,4,5,6)
                    n = (block_row - 1) * num_of_blocks_col + block_col;
                    for row=1:block_size_row
                        for col=1:block_size_col
                            % calculate new indices
                            if (mod(col,2) == 0)
                                % odd column numbers (2,4,6 ...): reverse row
                                row_index_new = (block_size_row - row + 1) + (offset(n,1) * block_size_row);
                            else
                                row_index_new = row + (offset(n,1) * block_size_row);
                            end
                            col_index_new = col + (offset(n,2) * block_size_col);
                            % calculate old indices
                            row_index_old = (block_row - 1) * block_size_row + row;
                            col_index_old = (block_col - 1) * block_size_col + col;
                            % set new order
                            res(row_index_new, col_index_new, k) = x(row_index_old,col_index_old,k);
                            % set new order for file names and demo (only one times)
                            if (k == 1)
                                num_filename(row_index_new, col_index_new,1) = num_filename_default(row_index_old,col_index_old,1);
                                num_filename(row_index_new, col_index_new,2) = num_filename_default(row_index_old,col_index_old,2);
                                if show
                                    res_show(row_index_new, col_index_new) = res_show_default(row_index_old,col_index_old);
                                end
                            end
                        end
                    end
                end
            end
        end
        % create new filenames
        % please note: the order of the condition is important
        %   because x could be a matrix (then it is no cell and of course doesn't
        %   contain filenames); if x is a cell then it can contain for
        %   example function handles (which are of course no filenames and
        %   also do not consist of characters. Only if x is a cell AND contains
        %   characters it will be considered containing filenames
        if iscell(x) && ischar(x{1,1,1})
            % x is a cell with characters that means x contains filenames
            for i=1:size(x,1)
                for j=1:size(x,2)
                    s = x(i,j);
                    % create filename: #######sssssss_rrxxxx
                    % ####### = use existing part of filename
                    % ccccccc = column
                    % rr      = row
                    % xxxx    = extension
                    res(i,j,1) = cellstr(strcat(s{1}(1:end-10-length(ext)), ...
                             sprintf('%07d', num_filename(i, j, 2)), '_', ...
                             sprintf('%02d', num_filename(i, j, 1)), ext));
                end
            end
            % show result of sorting
            if show
                disp(res_show)
            end
        end
    end


D.C.samplestr='ex000';
D.C.slPth=4;

% name of cubing from Google Drive document
D.C.cubingName=feval(@(x)x{1},regexp(mfilename(),'_','split'));
% date the cubing was done
D.C.datestr=feval(@(x)x(length(D.C.cubingName)+2:end),strrep(mfilename(),'_','-'));

% date the images where taken, can be set to 'auto' to enable autodetection
D.C.imdatestr='2013-10-30';


D.C.useEveryNthSlice=1;
% If true, all weights and shifts are reset to zero before LS is started
D.C.naive=false;

D.C.xcorr_size=[1024 512;384 2048; 1536 2048]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction

D.C.maxMagHigh=512

D.C.M=28;   %   Number of tiles per row
D.C.N=18;   %  Number of tiles per column

D.C.projecttype='cortex_piezo';

D.C.document_ccs=false;

% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
%

D.C.machineroot='F:/martin/';

%ending of initial files
D.sourceEnding='.tif';

D.parallel_mode='parfor';
D.parallel_mode_cubes='for';

D.C.subfoldersForSourceTifs='none';

D.C.allslice_arr = [
     172 1 1   1 D.C.N 1 D.C.M
    ]; %Be careful, here the order is Z-direction, then N, then M!

D.C.P=ceil(sum(D.C.allslice_arr(:,3)-D.C.allslice_arr(:,2)+1)/D.C.slPth)*D.C.slPth;

%allows to make changes to all_fns after makeFileListFun is done;
%Example: just use image 3,4 from all slices
%It's not possible to modify the third component
%D.all_fns_Modifier=@(x)x(3,4,:);
%D.all_fns_Modifier=@(x)flipdim(flipdim(x,1),2);
%D.all_fns_Modifier=@(x)x;
D.all_fns_Modifier=@(x)sort_img(x,D.sourceEnding,true);

% functions and modifier
D.makeFileListFun=@KMBmakeFileList;
% show_filenames shows the file numbers in x as doubles
% helpful function for debugging
D.show_fnsF=@(x)show_filenames(x,length(D.sourceEnding));


%D.C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
%D.C.joblistF=@(x)updater(x,D.C.slPth,[134]);
D.C.joblistF=@(x)1:x;

D.C.makedir=true;   %Creates directories for cubes or not
%make array for epoxy inclusion
D.C.weightarray=cell(3,1);
x=Inf;
o=-Inf;
D.C.weightarray{1}=zeros(D.C.N,D.C.M-1)+o;
D.C.weightarray{2}=zeros(D.C.N-1,D.C.M)+o;
D.C.weightarray{3}=zeros(D.C.N,D.C.M)+o;
% D.C.weightarray{1}=[
%     x x x x x x;
%     x x x o o o;
%     x x o o o o;
%     x x o o o o;
%     x o o o o o;
%     x o o o o o;
%     x x o o o o;
%     x x x x x o;
%     x x x x x x;
%     x x x x x x;
%     x x x x x x];
% D.C.weightarray{2}=[
%     x x x x x o x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o x;
%     x x o o o o x;
%     x x x o o o x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];
% D.C.weightarray{3}=[
%     x x x x x x x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x x o o o o;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];


D.C.pixelSize=[36.62 36.62 200];

D.C.usePreShiftsTransversal=false;
% (:,:,:,1) determins y-shifts. Keep in mind that a value of e.g. 1000
% means that the images are shifted by 1000 pixels relative to each other,
% so this would access each of them with indices shifted by 500
D.C.preShiftsLongitudinal=zeros(D.C.M,D.C.N,D.C.P,2);

%invert ("255-x") output cubes
D.C.invertCubes=false;

%if defaultOrigin is false, D.C.origin has to be set:
%D.C.origin='Y:\st005Top';
D.defaultOrigin=true;

%FEI stuff von Kevin! 
D.C.tifNamingSchemePiezoFEI=true;


D.C.saveResliceAndHistogramWhileCubing=false;

%is applied to every source tif image directly after reading
D.imageModifier=repmat([{@(x)x} {@(x)rot90(x,2)}],[D.C.M,D.C.N/2,D.C.P+1]);
%is applied to imageModifier cell arry
%D.imageModifierModifier=D.all_fns_Modifier;
D.imageModifierModifier=[D.C.M,D.C.N,D.C.P+1];

%changes orientation of every second column
D.C.motorSnake=false;

%makes sense to set to false if only very few crosscorrelations are
%calculated
D.C.debugOptimizeFFT=true;

%the highmag cubes are scaled down at the same rate in all three dimensions
D.C.scaleDownZ=true;

%save data for super fast iterations
D.C.saveDataForSuperRapidIteration=false;

%slices to exclude
D.exclude=[];

%if false, slices are silently excluded, if true, spacers are introduces
%and numbering is shifted
D.exludeKeepCounting=false;

%if not empty, only certain cross correlations are computed
%no mat files with CC results are saved
%per line: direction x y z
%D.C.subsetCC=[1 7 6 300;1 7 5 301];
D.C.subsetCC=[];

%are inter image differences be calculated during low memory tif writer
D.C.calcIID=false;

%if empty, manual cell mode in KMBstart, otherwise include strings 'cubes','high' or 'tifs'
D.outputToDo=''; 

end

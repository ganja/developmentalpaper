function D=st116()

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end

D.C.samplestr='ex144';
D.C.slPth=3;
D.C.stackN='116';
D.C.datestr='2012-11-07';
D.C.origin=['/nfs/bmo/boergens/cortex/tifs/OverviewStacks/stack116Top/st116Top'];
D.C.xcorrmax=4096;
D.C.xcorr_size=[2048 512;512 2048;D.C.xcorrmax D.C.xcorrmax]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
D.C.M=3;
D.C.N=3;
% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
% 


D.C.allslice_arr = [
    116 1 3 1 3 1 3
    ];
%D.C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
D.C.joblistF=@(x)updater(x,D.C.slPth,[134]);
%D.C.joblistF=@(x)1:x;

D.C.makedir=true;
end
function D=st003()

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end

D.C.samplestr='';
D.C.slPth=8;
D.C.stackN='005interm';
D.C.datestr='2013-05-30';
D.C.origin='D:\st005Top';
D.exclude=[];
D.C.imdatestr='2013-05-30';

%D.exclude=[113 312 380-1 513 672 688-1 877-1 1090 1093 1149 1153 1276 1306 1568 2146 2506 2502 2601 2739 2812 2903 2952 3362 3369];
D.C.xcorrmax=1024;

D.C.xcorr_size=[1024 512;384 1024;D.C.xcorrmax D.C.xcorrmax]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
D.C.M=7;
D.C.N=11;
D.makeFileListFun=@KMBmakeFileList;
D.C.projecttype='entorhinal';
% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
% 


D.C.allslice_arr = [
    5 1 115 1 7 1 11
    ];
%D.C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
%D.C.joblistF=@(x)updater(x,D.C.slPth,[134]);
D.C.joblistF=@(x)1:x;

D.C.makedir=true;
end
function [C, D]=RK_rcb_st0070()

    function v=updater(x,DslPth,doThese)
        v=[];
        for i=1:x
            ii=DslPth*(i-1)+1:DslPth*i+1;
            if ~isempty(intersect(ii,doThese))
                v(length(v)+1)=i;
            end
        end
    end
    function res=change_filenames(x)
        % default result matrix/cell is a copy of the input matrix/cell
        res = x;
        if iscell(x) && ischar(x{1,1,1})
            for iii=1:size(x,3)
                for i=1:size(x,1)
                    for ii=1:size(x,2)
                        if ~isempty(x{i,ii,iii})
                            % get position of slashes or backslashes
                            pos = strfind(res{i,ii,iii},'\');
                            if size(pos,2) == 0
                                pos = strfind(res{i,ii,iii},'/');
                            end
                            if size(pos,2) > 0
                                % get position of last slash/backslash
                                pos = pos(size(pos, 2));
                                % create filename
                                num=regexp(res{i,ii,iii}(pos:end),'(\d*)','match');
                                res{i,ii,iii} = strcat(res{i,ii,iii}(1:pos), ...
                                    '2016-03-01_st023_', ...
                                    sprintf('%07d', str2double(num{3})), '.tif');
                            end
                        end
                    end
                end
                %res(:,:,iii)=rot90(res(:,:,iii),2);
            end
        end
    end
%C.samplestr='exR286';
C.samplestr='ex4';
C.slPth=8;

% name of cubing from Google Drive document
%C.cubingName=feval(@(x)x{1},regexp(mfilename(),'_','split'));
C.cubingName='RN_CC_S1BF_L4';
% date the cubing was done
C.datestr='2016-03-01';
%C.datestr=feval(@(x)x(length(D.C.cubingName)+2:end),strrep(mfilename(),'_','-'));

% date the images where taken
C.imdatestr='2016';

C.useEveryNthSlice=1;
% If true, all weights and shifts are reset to zero before LS is started
C.naive=false;

C.M=1;   %Number of tiles per row
C.N=2;   %Number of tiles per column


%C.xcorr_size=[1024 512;384 2048; 1536 2048]; %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
C.xcorr_size{1}=repmat(cat(3,6000, 256),C.M-1,C.N); %correlation in dir 1 and dir 2 must be bigger than half the overlap in that direction
C.xcorr_size{2}=repmat(cat(3,256, 6000),C.M,C.N-1);
C.xcorr_size{3}=repmat(cat(3,2048, 2048),C.M,C.N);

C.whichDimsShiftCalc=1:3;

C.makeFileListFun=@KMBmakeFileList;
C.projecttype='other';

% 'none', 'full' or 'iid'
C.document_ccs='full';

% Repair crash
% a=dir([C.matdir 'rapid*.mat']);
% for i=1:length(a)
%     b(i)=str2num(a(i).name(6:end-4));
% end
% D.C.joblistF=@(x)setdiff(1:x,b);
%
%C.machineroot='Y:/entorhinall/st171Top/st171/';
%C.machineroot='~/';
%C.machineroot='/home/boergens/cubing_data/mzauser/st183/';

%Hier kommts raus
C.machineroot='E:\Dcontents\CubingAll';

C.parallel_mode='for';
C.parallel_mode_cubes='for';

C.subfoldersForSourceTifs='three';

C.allslice_arr = [
    1 0 3988 1 C.N 1 C.M
    ]; %Be careful, here the order is Z-direction, then N, then M!

C.P=3992

%allows to make changes to all_fns after makeFileListFun is done;
%Example: just use image 3,4 from all slices
%It's not possible to modify the third component
%D.all_fns_Modifier=@(x)x(3,4,:);
%D.all_fns_Modifier=@(x)flipdim(flipdim(x,1),2);
%D.all_fns_Modifier=@(x)x;
D.all_fns_Modifier=@(x)change_filenames(x);


%C.joblistF=@(x)updater(x,D.C.slPth,D.exclude);
%C.joblistF=@(x)updater(x,D.C.slPth,[134]);
C.joblistF=@(x)1:x;

C.makedir=true;   %Creates directories for cubes or not
%make array for epoxy inclusion
C.weightarray=cell(3,1);
x=Inf;
o=-Inf;
C.weightarray{1}=zeros(C.N,C.M-1)+o;
C.weightarray{2}=zeros(C.N-1,C.M)+o;
C.weightarray{3}=zeros(C.N,C.M)+o;
% C.weightarray{1}=[
%     x x x x x x;
%     x x x o o o;
%     x x o o o o;
%     x x o o o o;
%     x o o o o o;
%     x o o o o o;
%     x x o o o o;
%     x x x x x o;
%     x x x x x x;
%     x x x x x x;
%     x x x x x x];
% C.weightarray{2}=[
%     x x x x x o x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o x;
%     x x o o o o x;
%     x x x o o o x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];
% C.weightarray{3}=[
%     x x x x x x x;
%     x x x o o o o;
%     x x x o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x o o o o o;
%     x x x o o o o;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x;
%     x x x x x x x];


C.pixelSize=[12 12 30];

C.usePreShiftsTransversal=false;
% (:,:,:,1) determins y-shifts. Keep in mind that a value of e.g. 1000
% means that the images are shifted by 1000 pixels relative to each other,
% so this would access each of them with indices shifted by 500
D.preShiftsLongitudinal=zeros(C.M,C.N,C.P,2);

%invert ("255-x") output cubes
C.invertCubes=false;

%if defaultOrigin is false, C.origin has to be set:
%C.origin='Y:\st005Top';
%C.defaultOrigin=true;
C.defaultOrigin=false;

%Hier kommts nei
C.origin='E:\ratL4stackFlat';

%FEI stuff von Kevin!
C.tifNamingSchemePiezoFEI=false;


C.saveResliceAndHistogramWhileCubing=false;

%is applied to every source tif image directly after reading
D.imageModifier=repmat({@(x)x(10:end,:)},[C.M,C.N,C.P+1]);
%is applied to imageModifier cell arry
D.imageModifierModifier=D.all_fns_Modifier;

%changes orientation of every second column
C.motorSnake=true;

%makes sense to set to false if only very few crosscorrelations are
%calculated
C.debugOptimizeFFT=true;

%the highmag cubes are scaled down at the same rate in all three dimensions
C.scaleDownZ=true;

%save data for super fast iterations
C.saveDataForSuperRapidIteration=false;

%ending of initial files
C.sourceEnding='.tif';


%slices to exclude
C.exclude=[];

%if false, slices are silently excluded, if true, spacers are introduces
%and numbering is shifted
C.exludeKeepCounting=false;

%if not empty, only certain cross correlations are computed
%no mat files with CC results are saved
%per line: direction x y z
%D.C.subsetCC=[1 7 6 300;1 7 5 301];
C.subsetCC=[];

%are inter image differences be calculated during low memory tif writer
C.calcIID=true;

C.maxMagHigh=8;

C.outtifresizefactor=1;
%weight adjustment for z deltas
C.zFactor=1;
C.ttempIterations=1;
C.writeRawsWithTifs=[];
C.small=false;

%if empty, manual cell mode in KMBstart, otherwise include strings 'cubes','high' or 'tifs'
C.outputToDo='';

end

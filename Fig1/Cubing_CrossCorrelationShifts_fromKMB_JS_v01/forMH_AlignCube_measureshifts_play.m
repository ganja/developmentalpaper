function [shifts] = forMH_AlignCube_measureshifts_play(C,im1,im2, direction,stoptag)
xcorr_sizeT=C.xcorr_size(direction,:);
crop=0;
C.dims=fliplr(C.dims);
%------------------------------------------------------------------------
%prepare images, normalize 

im1=normalizeimage(im1);
im2=normalizeimage(im2);

if direction<3
    if direction==2
        im1=im1';
        im2=im2';
        C.dims=fliplr(C.dims);
        xcorr_sizeT=fliplr(xcorr_sizeT);
    end
    
    padlength = xcorr_sizeT(2)/2;
    im1 = padarray(im1,[0 padlength],mean(mean(im1)));
    im2 = padarray(im2,[0 padlength],mean(mean(im2)));
else
    padlength = xcorr_sizeT(2)/2;
    im1 = padarray(im1,[padlength padlength],mean(mean(im1)));
    im2 = padarray(im2,[padlength padlength],mean(mean(im2)));
    
end

%--------------------------------------------------------------------
%calculate correlation matrix
T = fft2(single(im2)).*conj(fft2(single(im1)));
C0 = fftshift(ifft2(T));
C0=C0/max(max(C0))*255;
C0(C0<0)=0;

%--------------------------------------------------------------------
%Coarse shifts
C1 = C0;

for count = 1:5
    C1 = (C1.^2);
    C1 = (C1./(max(max(C1,[],1))).*255);
end

C1(C1<=254) = 0;

stats = regionprops(bwlabel(C1), 'Centroid','Area');
[q,w] = max([stats.Area]);

stats(w).Centroid = round(stats(w).Centroid);
real_xcorr_size=size(C1);
coarseshifts(1) = stats(w).Centroid(2)-(real_xcorr_size(1)/2)-1;
coarseshifts(2) = stats(w).Centroid(1)-(real_xcorr_size(2)/2)-1;
if direction<3
    coarseshifts(2)=coarseshifts(2)-xcorr_sizeT(2);
end
%-----------------------------------------------------------------------
%Fine tuning
C0 = padarray(C0,[128 128],mean(mean(C0)),'both');
C0 = C0(stats(w).Centroid(2):stats(w).Centroid(2)+256-1,...
    stats(w).Centroid(1):stats(w).Centroid(1)+256-1);
C1 = (imresize(C0, 4));

for count = 1:5
    C1 = (C1.^2);
    C1 = (C1./(max(max(C1,[],1))).*255);
end
C1(C1<=254) = 0;

stats = regionprops(bwlabel(C1), 'Centroid','Area');
[q,w] = max([stats.Area]);
shifts(1)=coarseshifts(1)+(stats(w).Centroid(2)-514.5)/4;
shifts(2)=coarseshifts(2)+(stats(w).Centroid(1)-514.5)/4;
if direction<3
    shifts(2) = shifts(2)-2*crop;
end

%-------------------------------------------------------------------------
%Correct for direction and give back values
if direction~=2
    shifts=fliplr(shifts);
end
shifts(3)=q;
shifts(4)=w;
% for i=1:size(ix1,2)
%     tr{i}=load([C.matdir sprintf('thisreslice1_1_%u.mat',i)]);
% end
for xy=['x' 'y']
    for j=1:size(tr{1}.thisreslice.(xy),2)
        trxy.(xy){j}=[];
        for i=1:size(ix1,2)
            trxy.(xy){j}=cat(3,trxy.(xy){j},tr{i}.thisreslice.(xy){j});
     
        end
    end
end
for xy=['x' 'y']
    resfolder=[C.rootf 'newtifs' filesep C.cubefn filesep 'reslice' xy filesep];
    mkdir(resfolder);
    for j=1:size(tr{1}.thisreslice.(xy),2)
        imwrite(squeeze(trxy.(xy){j}), [resfolder sprintf('reslice%s%u.tif',xy,j)],'tif');
    end
end
function KMBmeasureShifts(C,currentThread,all_fns,imageModifier,preShiftsLongitudinal,preShiftsTransversal)
overallProcNumberStart=(currentThread-1)*C.slPth;
if isempty(find(C.joblist==currentThread,1))
    return;
end
if size(C.subsetCC,1)>0 && isempty(intersect(C.subsetCC(:,4),overallProcNumberStart:overallProcNumberStart+C.slPth))
    return;
end






currentThread
%kmbwaiter=KMBwait(currentThread,2,C,'measure',true);
cache=cell(C.M,C.N,C.slPth+1);
data=cell(C.M,C.N,C.slPth+1);
contrast=zeros(C.M,C.N,C.slPth+1,2);
if isempty(C.subsetCC)
    for i=it([[1;1;1],[C.M; C.N; C.slPth+1]]) %works with shrinked images
        if size(at(all_fns,i),1)>0
            if C.precache
                cache{i(1),i(2),i(3)}=readImage(at(all_fns,i),at(imageModifier,i));
                
                cacheTemp=cast(at(cache,i),'single');
                contrast(i(1),i(2),i(3),:)=[mean(cacheTemp(:)),std(cacheTemp(:))];
            end
            
        end
    end
end
%kmbwaiter.closure();
% if C.readRaw
%
%     for i=it([[1;1;1],[C.M; C.N; C.slPth+1]])
%         if size(at(all_fns,i),1)>0
%             dirtemp=at(all_fns2,i);
%             dirind=strfind(dirtemp,filesep);
%             mkdir(dirtemp(1:dirind(end)));
%             fid=fopen(at(all_fns2,i),'w+');
%             fwrite(fid,cache{i(1),i(2),i(3)});
%             fclose(fid);
%         end
%     end
%
% end
delta=cell(3,1);
weights=cell(3,1);
area=cell(3,1);
eye3=eye(3);
aya3=[1 0 1; 0 1 1; 0 0 1]; %is just used to make the cell structure of the
%delta weights and area cells have the shape necessary for each direction,
%i.e. for dir x I have 2x3xslPth values

for dimIt=C.ccsDims
    delta{dimIt}=zeros([[C.M C.N C.slPth+1]-aya3(dimIt,:) 2]);
    weights{dimIt}=zeros([C.M C.N C.slPth+1]-aya3(dimIt,:));
    area{dimIt}=zeros([C.M C.N C.slPth+1]-aya3(dimIt,:));
    if C.debugOptimizeFFT&&isempty(C.subsetCC)
        xcorr_temp=reshape(C.xcorr_size{dimIt},numel(C.xcorr_size{dimIt})/2,2);
        xcorr_temp=unique(xcorr_temp,'rows');
        for i=1:size(xcorr_temp,1)
            fftw('planner','exhaustive');
            t1=fft2(single(ones(xcorr_temp(i,:))));
            ifft2(t1);
        end
    end
    shifts=[];
    for i=it(C.whichCCStodo{dimIt})
        if size(C.subsetCC,1)>0 && ~isempty(find([dimIt i'+[0 0 overallProcNumberStart]]~=C.subsetCC,1))
            continue;
        end
        shifts(i(1),i(2),i(3),1,1) = 0;
        shifts(i(1),i(2),i(3),1,2) = 0;
        shifts(i(1),i(2),i(3),1,3) = -1;
        shifts(i(1),i(2),i(3),1,4) = 0;
        thisweight=1E-7;
        if size(at(all_fns,i),1)>0&&size(at(all_fns,i+eye3(:,dimIt)),1)>0
            xcorr_sizeT=permute(C.xcorr_size{dimIt}(i(1),i(2),:),[1,3,2]);
            CdimsFLR=fliplr(C.dims);
            j=i+eye3(:,dimIt);
            if dimIt<3
                shifts=KMBmeasureShiftsInPlane(dimIt,CdimsFLR,xcorr_sizeT,preShiftsTransversal,cache,j,i,C,overallProcNumberStart, all_fns, imageModifier);
            else
                tileShiftArray=[0,0];
                if isfield(C,'subtiled')
                    if C.subtiled
                        tileShiftArray=[];
                        limitgen=@(xx,yy)floor((floor(C.dims(xx)/(xcorr_sizeT(yy)))-1)/2);
                        for tsai=-limitgen(1,2):limitgen(1,2)
                            for tsaj=-limitgen(2,1):limitgen(2,1)
                                tileShiftArray=[tileShiftArray; tsai*xcorr_sizeT(2) tsaj*xcorr_sizeT(1)];
                            end
                            
                        end
                    end
                end
                for tsak=1:size(tileShiftArray,1)
                    cache{j(1),j(2),j(3)}=readImage(at(all_fns,j),at(imageModifier,j));
                    cache{i(1),i(2),i(3)}=readImage(at(all_fns,i),at(imageModifier,i));
                    dC=zeros(2,2,2);
                    dC=dC+round(imresize(reshape(CdimsFLR,[1,1,2]),[2,2])/2); % center of image
                    reshape(xcorr_sizeT,[1,1,2]);
                    dC=dC+imresize(cat(2,-ans/2+1,ans/2),[2,2]); %add correlation size
                    reshape(preShiftsLongitudinal(i(1),i(2),i(3),:),[1,1,2]);
                    dC=dC+imresize(cat(1,-ans/2,ans/2),[2,2]); % add preshifts
                    dC=dC+repmat(reshape(fliplr(tileShiftArray(tsak,:)),[1 1 2]),[2 2 1]);
                    shifts(i(1),i(2),i(3),tsak,:) = reshape(forMH_AlignCube_measureshifts(C,...
                        cache{j(1),j(2),j(3)}(dC(2,1,1):dC(2,2,1),dC(2,1,2):dC(2,2,2)),...
                        cache{i(1),i(2),i(3)}(dC(1,1,1):dC(1,2,1),dC(1,1,2):dC(1,2,2)),dimIt,i+[0;0;overallProcNumberStart]),[1 1 1 1 4]);
                    shifts(i(1),i(2),i(3),tsak,1)=shifts(i(1),i(2),i(3),tsak,1)-preShiftsLongitudinal(i(1),i(2),i(3),2);
                    shifts(i(1),i(2),i(3),tsak,2)=shifts(i(1),i(2),i(3),tsak,2)-preShiftsLongitudinal(i(1),i(2),i(3),1);
                end
            end
            thisweight=1;
        end
        delta{dimIt}=sat(delta{dimIt},[i;1], mean(shifts(i(1),i(2),i(3),:,1)));
        delta{dimIt}=sat(delta{dimIt},[i;2], mean(shifts(i(1),i(2),i(3),:,2)));
        weights{dimIt}=sat(weights{dimIt},i, thisweight);
        area{dimIt}=sat(area{dimIt},i, mean(shifts(i(1),i(2),i(3),:,3)));
    end
end
if size(C.subsetCC,1)==0
    area_split={};
    delta_split={};
    weights_split={};
    shifts_split={};
    contrast_split={};
    for j=1:C.slPth
        contrast_split{j}=contrast(:,:,j,:);
        shifts_split{j}=shifts(:,:,j,:,:);
        for i=1:3
            
            area_split{j}{i}=area{i}(:,:,j,:);
            delta_split{j}{i}=delta{i}(:,:,j,:);
            weights_split{j}{i}=weights{i}(:,:,j,:);
        end
    end
    for j=1:C.slPth
        folder=[C.matdir sprintf('%0.5d',floor(((currentThread-1)*C.slPth+j-1)/100)) filesep];
        if ~exist(folder,'dir');
            mkdir (folder);
        end
        area=area_split{j};
        delta=delta_split{j};
        weights=weights_split{j};
        contrast=contrast_split{j};
        shifts=shifts_split{j};
        save([folder C.knumberstr sprintf('_delta_%u.mat',(currentThread-1)*C.slPth+j)],'area','delta','weights','contrast','shifts');
        
    end
end
end

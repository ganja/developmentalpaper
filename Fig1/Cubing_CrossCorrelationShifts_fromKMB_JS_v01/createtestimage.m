clf;
clear all;

direction=2;
shiftx=5;
shifty=3;
overlap=30;

sun=imread('sun.jpg','JPEG');
sun=sun(:,:,1);
[m,n]=size(sun);

C=struct('xcorr_size',[0 0;0 0;0 0],'dims',0);
switch direction
    case 1
        sun1=sun(round(m/4):round(3*m/4),round(n/2):round(n/2)+overlap-1);
        sun2=sun(round(m/4)+shifty:round(3*m/4)+shifty,round(n/2)+shiftx:round(n/2)+overlap-1+shiftx);
        
        C.xcorr_size(direction,:)=2*round(size(sun1)/2);
        C.xcorr_size(direction,2)=overlap;
        
        subplot(1,2,1)
        imshow(sun1);
        subplot(1,2,2)
        imshow(sun2);        
    case 2
        sun1=sun(round(m/2):round(m/2)+overlap-1,round(n/4):round(3*n/4));
        sun2=sun(round(m/2)+shifty:round(m/2)+overlap-1+shifty,round(n/4)+shiftx:round(3*n/4)+shiftx);
        
        C.xcorr_size(direction,:)=fliplr(2*round(size(sun1)/2));
        C.xcorr_size(direction,1)=overlap;
        
        subplot(2,1,1)
        imshow(sun1);
        subplot(2,1,2)
        imshow(sun2);
    case 3
        sun1=sun(round(m/4):round(3*m/4),round(n/4):round(3*n/4));
        sun2=sun(round(m/4)+shifty:round(3*m/4)+shifty,round(n/4)+shiftx:round(3*n/4)+shiftx);
        
        C.xcorr_size(direction,:)=2*round(size(sun1)/2);
        
        subplot(2,1,1)
        imshow(sun1);
        subplot(2,1,2)
        imshow(sun2);
end

sun1n=normalizeimage(sun1);
sun2n=normalizeimage(sun2);



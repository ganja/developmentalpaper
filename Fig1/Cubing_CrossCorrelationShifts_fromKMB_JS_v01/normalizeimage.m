function imnorm = normalizeimage(im)
%normalizeimage: normalize image to mean 0, stddeviation 1 

[m,n]=size(im);

meanimage=mean(mean(im));
sigimage=sqrt(var(reshape(single(im),m*n,1)));

imnorm=(single(im)-meanimage)/sigimage;

end


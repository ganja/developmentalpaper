function y=at(array,iter)
n=size(iter,1);
switch n
    case 1
        y=array{iter(1)};
    case 2
        y=array{iter(1),iter(2)};
    case 3
        y=array{iter(1),iter(2),iter(3)};
    case 4
        y=array{iter(1),iter(2),iter(3),iter(4)};
    case 5
        y=array{iter(1),iter(2),iter(3),iter(4),iter(5)};
end

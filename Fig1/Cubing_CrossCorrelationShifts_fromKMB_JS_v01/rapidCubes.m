for i=1:ceil(C.P/(C.cubesize))
    fn=(all_fns(:,:,(i-1)*C.cubesize+1:i*C.cubesize));
    for fni=it([[1;1;1],[C.M; C.N; C.cubesize]])
        if size(fni{i(1),i(2),i(3)},1)>0
            data{it(1),it(2),it(3)}=imresize(imload(fni{i(1),i(2),i(3)}),1/8);
        end
    end
    save([C.cubedir filesep 'rapid' sprintf('%u',i) '.mat'],'data');
end



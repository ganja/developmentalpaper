function y=returnDeltas(delta_lin, project)
fake=cell(3,1);
for m=1:project.M-1
    for n=1:project.N
        for p=1:project.P
            fake{1}{m,n,p}={m,n,p,1};
        end
    end
end
for m=1:project.M
    for n=1:project.N-1
        for p=1:project.P
             fake{2}{m,n,p}={m,n,p,2};
        end
    end
end
for m=1:project.M
    for n=1:project.N
        for p=1:project.P
             fake{3}{m,n,p}={m,n,p,3};
        end
    end
end
fake_lin=[reshape(fake{1},[],1);reshape(fake{2},[],1);reshape(fake{3},[],1)];
for i=1:length(fake_lin)
    thisone=fake_lin{i};
    y{thisone{4}}(thisone{1},thisone{2},thisone{3},:)=delta_lin(i,:);
end

function KMBwriteTifs(all_fns,ix1,C,ttemp,onlyfirst,imageModifier)
lastimage=[];
iids=[];
for currentThread2=1:size(ix1,2)
    ixx=it([C.inA;[currentThread2,currentThread2]]);
    for ix2=1:size(ixx,2)
        currentThread=(currentThread2-1)*size(ixx,2)+ix2;
        %kmbw=KMBwait(currentThread,fileReaderThreads,C,'montagesMade',true);
        xyzCs=ixx(:,ix2);
        thismontage=cell(2,size(ixx,2));
        writeB=ones([C.maxMagMultXY,1],'uint8');
        if onlyfirst
            theend=(xyzCs(3)-1)*C.cubesize+1;
        else
            theend=min(xyzCs(3)*C.cubesize,C.P);
        end
        for count = (xyzCs(3)-1)*C.cubesize+1:theend
            LSSweightsum=ttemp.weightsum((count-1)*C.M*C.N+1:count*C.M*C.N);
            for i=1:size(LSSweightsum,1)
                LSSweightsum(i,2)=i;
            end
            LSSweightsum=sortrows(LSSweightsum);
            countT=count-(xyzCs(3)-1)*C.cubesize;
            thismontage{1,countT}=zeros(C.cubesize*[C.maxMagMultXY],'uint8');
            iid_position_store=cell(C.M,C.N);
            for imgcount2 = 1:C.M*C.N
                imgcount=LSSweightsum(end+1-imgcount2,2);
                thisimg = (count-1)*C.M*C.N+imgcount;
                xyIts = [ mod(imgcount-1,C.M) floor((imgcount-1)/C.M)]+1;
                thisshifts = ttemp.shiftsN(thisimg,:);
                xycoords = C.dims.*(xyIts-1)+fix(thisshifts)+1;
                xycoordsT=xycoords+C.dims-1;
                if size(at(all_fns,[xyIts(1);xyIts(2);count]),1)>0
                    cache=readImage(at(all_fns,[xyIts(1);xyIts(2);count]),at(imageModifier,[xyIts(1);xyIts(2);count]))';
                    thismontage{1,countT}(xycoords(1):xycoordsT(1), xycoords(2):xycoordsT(2))=cache;
                    iid_position_store{xyIts(1), xyIts(2)}=[xycoords(1),xycoordsT(1), xycoords(2),xycoordsT(2)];
                end
            end
            if onlyfirst
                imwrite(thismontage{1,countT}',[C.tifdir  sprintf('reslicez%u.tif',currentThread2)],'tif');
            else
                if C.calcIID
                    if ~isempty(lastimage)
                        for im=1:C.M
                            for in=1:C.N
                                if isempty(iid_position_store{im,in})
                                    iids_temp(im,in)=0;
                                    continue;
                                end
                                iid_im=single(feval(@(z)cat(3,z(thismontage{1,countT}),z(lastimage)),@(y)feval(@(x)y(x(1):x(2), x(3):x(4)),iid_position_store{im,in})));
                                iids_temp(im,in)=sqrt(sum(sum((diff(iid_im,1,3).*(prod(iid_im,3)>0)).^2))/length(find(prod(iid_im,3)>0)));
                            end
                        end
                        iids=cat(3,iids,iids_temp);
                    end
                    lastimage=thismontage{1,countT};
                end
                if size(thismontage{1,countT},1)*size(thismontage{1,countT},2) > (2^31-1)
                    imwrite(thismontage{1,countT}(1:size(thismontage{1,countT},1)/2,:),[C.tifdir  sprintf('%0.5u_1.tif',count)],'tif');
                    imwrite(thismontage{1,countT}(size(thismontage{1,countT},1)/2+1:end,:),[C.tifdir  sprintf('%0.5u_2.tif',count)],'tif');
                else
                    imwrite(thismontage{1,countT}',[C.tifdir  sprintf('%0.5u.tif',count)],'tif');
                end
            end
            thismontage{1,countT}=[];
        end
    end
end
if C.calcIID
    
    save([C.matdir 'iids.mat']);
end
end
function ttemp = forMH_AlignCube_LSsolution_002(C,col)
alldeltas_temp=cell(3,1);
allweights_temp=cell(3,1);
allareas=cell(3,1);
for dimIt=1:3
    alldeltas_temp{dimIt}=reshape(col.delta{dimIt},[],2);
    allweights_temp{dimIt}=col.weight{dimIt}(:);
    if dimIt==3
        allweights_temp{dimIt}=allweights_temp{dimIt}*C.zFactor;
    end
    allareas{dimIt}=col.area{dimIt}(:);
end
alldeltas=[alldeltas_temp{1};alldeltas_temp{2};alldeltas_temp{3}];
allweights=[allweights_temp{1};allweights_temp{2};allweights_temp{3}];

M=C.M;
N=C.N;
P=C.procs*C.slPth;

temp = diag(ones(M*N,1),1);
A = -eye(M*N,M*N)+temp(1:M*N,1:M*N);
A(end,:) = [];
A(M:M:end,:) = [];
temp = diag(ones(M*N,1),M);
B = -eye(M*N,M*N)+temp(1:M*N,1:M*N);
B(end-M+1:end,:) = [];
I = speye(P,P);
D_A = kron(I,A);
D_B = kron(I,B);
D = [D_A;D_B];
temp = diag(sparse(ones(M*N*P,1)),M*N);
E = -sparse(1:M*N*P,1:M*N*P,1)+temp(1:M*N*P,1:M*N*P);
E(end-M*N+1:end,:)=[];
F = [D;E];


W_vec = allweights';
W_vec(C.M*C.N*(P-1)+C.M*(C.N-1)*P+(C.M-1)*C.N*P+1)=1;
W_temp = speye(length(W_vec));
W = spdiags((W_vec)',0,W_temp);

% FIX offset
ff=zeros(1,C.M*C.N*P);
ff(1,ceil(C.M/2)+C.M*ceil(C.N/2))=1;
F=cat(1,F,ff); % F is the main matrix
alldeltas=cat(1,alldeltas,zeros(1,2));

%calculate optimal shifts
shifts=zeros(C.M*C.N*P,2);
Wad1=W*alldeltas(:,1);
Wad2=W*alldeltas(:,2);
shifts(:,2) = (W*F)\Wad2;
shifts(:,1) = (W*F)\Wad1;

R.alldeltaErrW=(W*F)*shifts-[Wad1 Wad2];
R.alldeltaErr=F*shifts-alldeltas;
R.alldeltaResult=F*shifts;

for i=fieldnames(R)'
    counter=1;
    for j=1:3
        k=[C.M C.N C.P]-feval(@(x)x(j,:),eye(3));
        ttemp.(i{1}){j}=reshape(R.(i{1})(counter:counter+prod(k)-1,:),k(1),k(2),k(3),[]);
        counter=counter+prod(k);
    end
end
for i=1:2
    ttemp.shifts(:,:,:,i)=reshape(shifts(:,i),C.M,C.N,C.P);
end

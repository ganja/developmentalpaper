function filecheck(all_fns,smallcheck,update_start)
l=0;
for k=update_start:size(all_fns,3)
    for i=1:size(all_fns,1)
        for j=1:size(all_fns,2)
            fn=all_fns{i,j,k};
            if ~isempty(fn)
                if mod(l,1000)==0
                    disp(l);
                end
                l=l+1;
                
                if ~exist(fn,'file')
                    disp([fn ' does not exist']);
                    return;
                end
                if ~smallcheck
                    continue;
                end
                fn=[fn(1:end-4) '_small.raw'];
                if ~exist(fn,'file')
                    disp([fn ' does not exist']);
                    return;
                end
            end
        end
    end
end
function all_fns=KMBmakeFileList(C,prefix,suffix,breakafterfirst)


all_fns = cell(C.M,C.N,C.P+1);
Pidx = 0;
for count = 1:size(C.allslice_arr,1)
    thisst=C.allslice_arr(count,1);
    thisimg=(C.allslice_arr(count,2)-1)*...
        (C.allslice_arr(count,5)-C.allslice_arr(count,4)+1)...
        *(C.allslice_arr(count,7)-C.allslice_arr(count,6)+1);
    for count2 = C.allslice_arr(count,2):C.allslice_arr(count,3)
        Pidx = Pidx + 1;
        for Midx = C.allslice_arr(count,6):C.allslice_arr(count,7)
            for Nidx = C.allslice_arr(count,4):C.allslice_arr(count,5)
                Naa=(1-C.allslice_arr(count,6)+C.allslice_arr(count,7))*(1-C.allslice_arr(count,4)+C.allslice_arr(count,5));
                thisst_str=sprintf('st%0.3d',thisst);
                if C.tifNamingSchemePiezoFEI
                    thisimg_str=sprintf('%0.7d_%0.2d',floor(thisimg/Naa)*Naa+Midx-1,Nidx-1);
                else
                    thisimg_str=sprintf('%0.3d',thisimg);
                end
                    
                thisdiv_str=sprintf('%0.6d',floor(thisimg/Naa));
                thisdiv_str2=sprintf('%0.6d',floor(thisimg/Naa/100));
                stackdir =  fullfile(prefix);
                switch C.subfoldersForSourceTifs
                    case 'none'
                        stackdir =  fullfile(prefix, [thisst_str 'Top'], thisst_str);
                    case 'one'
                        stackdir =  fullfile(prefix, [thisst_str 'Top'], thisst_str, thisdiv_str);
                    case 'two'
                        stackdir =  fullfile(prefix, [thisst_str 'Top'], thisst_str, thisdiv_str2, thisdiv_str);
                    case 'three'
                        stackdir =  fullfile(prefix);
                    otherwise
                        disp('Impossibru');
                        return;
                end
                all_fns{Midx,Nidx,Pidx} = fullfile(stackdir,strcat(C.imdatestr,'_',thisst_str,'_',thisimg_str,suffix));
                if breakafterfirst
                    all_fns=all_fns{Midx,Nidx,Pidx};
                    return;
                end
                thisimg=thisimg+1;
            end
            if C.motorSnake&&mod(Midx,2)==0
                 all_fns(Midx,:,Pidx)=fliplr(all_fns(Midx,:,Pidx));
            end
        end
    end
end
all_fns_sub = all_fns(:,:,1:C.useEveryNthSlice:Pidx);
all_fns = cell(C.M,C.N,ceil(Pidx/(C.useEveryNthSlice*C.slPth))*C.slPth+1);
all_fns(:,:,1:size(all_fns_sub,3))=all_fns_sub;

%% reordering the images (max 9 images)
choice='No';
while strcmp(choice,'Yes')
    count=1;
    for i=1:C.N
        for j=1:C.M
        subplot(C.N,C.M,count)
            imshow(imread((all_fns{j,i,1})));
            pos{count}=[j,i];
            count=count+1;
        end
    end
    choice=questdlg('reordering','reordering images?','Yes','No','No');
    if strcmp(choice,'Yes')
        reply=input('new order:','s');
        if ~isempty(reply)
            neworder=str2mat(reply);
            all_fns_old=all_fns;
            all_fns=cell(size(all_fns_old));
            for j=1:count2/C.useEveryNthSlice
                for i=1:length(neworder)
                    r=str2num(neworder(1,i));
                    all_fns(pos{r}(1),pos{r}(2),j)=...
                        all_fns_old(pos{i}(1),pos{i}(2),j);
                end            
            end
        end
    end
    close all
end

function im3=KMBtext(im, hash, xIt, yIt,LSshifts, thisimg,this_slicecount,M,N)
%% Create the text mask
% Make an image the same size and put text in it

hf = figure('color','white','units','normalized','position',[.1 .1 .8 .8]);
image(ones(512));
set(gca,'units','pixels','position',[5 5 size(im,2)-1 size(im,1)-1],'visible','off')

% Text at arbitrary position
if xIt>1
    text('units','pixels','position',[100 500],'fontsize',45,'string',strcat(num2str(LSshifts(thisimg,1)-LSshifts((this_slicecount-1)*M*N+(yIt-1)*M+xIt-1,1)),'  |  ',...
        num2str(LSshifts(thisimg,2)-LSshifts((this_slicecount-1)*M*N+(yIt-1)*M+xIt-1,2))));
end
if yIt>1
    text('units','pixels','position',[100 450],'fontsize',45,'string',strcat(num2str(LSshifts(thisimg,1)-LSshifts((this_slicecount-1)*M*N+(yIt-2)*M+xIt,1)),'  |  ',...
        num2str(LSshifts(thisimg,2)-LSshifts((this_slicecount-1)*M*N+(yIt-2)*M+xIt,2))));
end
if this_slicecount>1
    text('units','pixels','position',[100 400],'fontsize',45,'string',strcat(num2str(LSshifts(thisimg,1)-LSshifts((this_slicecount-2)*M*N+(yIt-1)*M+xIt,1)),'  |  ',...
        num2str(LSshifts(thisimg,2)-LSshifts((this_slicecount-2)*M*N+(yIt-1)*M+xIt,2)))) ;
end
if size(hash,1)>0
for i=1:6
    if size(hash(i),1)==0
        hash(i)=java.lang.String;
    end
end
text('units','pixels','position',[100 350],'fontsize',45,'string', hash(1).toCharArray()');
text('units','pixels','position',[100 300],'fontsize',45,'string',hash(2).toCharArray()');
text('units','pixels','position',[100 250],'fontsize',45,'string',hash(3).toCharArray()');
text('units','pixels','position',[100 200],'fontsize',45,'string',hash(4).toCharArray()');
text('units','pixels','position',[100 150],'fontsize',45,'string',hash(5).toCharArray()');
text('units','pixels','position',[100 100],'fontsize',45,'string',hash(6).toCharArray()');
text('units','pixels','position',[100 50],'fontsize',45,'string',strcat(num2str(xIt),'  |  ',num2str(yIt)));
end

% Capture the text image
% Note that the size will have changed by about 1 pixel
tim = getframe(gca);
close(hf)

% Extract the cdata
tim2 = tim.cdata;

% Make a mask with the negative of the text
tmask = tim2(:,:,3)==0;
im2=im(101:100+size(tmask,1),101:100+size(tmask,2));

% Place white text
% Replace mask pixels with UINT8 max
im2(tmask) = uint8(0);
im(101:100+size(tmask,1),101:100+size(tmask,2))=im2;
im3=im;
function KMBwriteCubesHigh(currentThread2,C)
if C.scaleDownZ
    magAtWhichScalingShouldBegin=8;
else
    magAtWhichScalingShouldBegin=1;
end

for xyzCs=it([C.inB;[currentThread2,currentThread2]])
    thismontage=cell(1);
    relativeV=[C.inA./C.inB;[1,1]].*ceil((C.outA./C.outB)/8).*[1,1;1,1;1,8/magAtWhichScalingShouldBegin];
    thismontage{2}=zeros(C.cubesize/2*[C.maxMagMultXY,1].*relativeV(:,2)','uint8');
    
    for ix2FF=it(relativeV);
        ix2GG=ix2FF+relativeV(:,2).*(xyzCs-1);
        filestr=strcat(num2str(ix2GG(1)),'_',num2str(ix2GG(2)),'_',num2str(ix2GG(3)));
        filename123=fullfile(C.cubedir,strcat(filestr,'.raw'));
        disp(filename123);
        Mdims=C.cubesize*[C.maxMagMultXY,1]/2;
        if exist(filename123,'file')==0
            disp('not found');
        else
            fid=fopen(filename123,'r');
            mitF=Mdims.*(ix2FF'-1)+1;
            mitL=Mdims.*(ix2FF');
            thismontage{2}(mitF(1):mitL(1),mitF(2):mitL(2),mitF(3):mitL(3))=reshape(fread(fid,inf,'*uint8'),Mdims);
            if C.scaleDownZ
                thismontage{2}=thismontage{2}(:,:,1:2:end-1)/2+thismontage{2}(:,:,2:2:end)/2;
            end
            
            fclose(fid);
        end
    end
    
    for magItI=1:log2(C.maxMagHigh)
        magIt=2^magItI;
        c8d=1/magIt*[C.maxMagMultXY';1].*relativeV(:,2);
        KMBwriteCubeItsMaker(C,xyzCs,magIt,thismontage,0,c8d,false)
        thismontage{log2(magIt)+2}=imresize(thismontage{log2(magIt)+1},0.5);
        if C.scaleDownZ
            thismontage{log2(magIt)+2}=thismontage{log2(magIt)+2}(:,:,1:2:end-1)/2+thismontage{log2(magIt)+2}(:,:,2:2:end)/2;
        end
        
    end
end


function KMBparforHelper(parld,func,procs)
%KMBPARFORHELPER Abstraction layer for different modes of exection
%   parld sets whether it is parallelized or not
%
    function submittoJM
        jm = findResource('scheduler', 'configuration', jobmanagername);
        job = createJob(jm, 'configuration', jobmanagername);
        for ii=1:C.procs
            
            inputargs = {ii};
            createTask(job, func, 0, inputargs, 'configuration', jobmanagername);
        end
        submit(job);
    end
switch parld
    case 'parfor'
        if matlabpool('size')==0
            matlabpool;
            'matlabpool'
        end
        threads=matlabpool('size');
        parfor thI=1-threads:0
            for th2I=1:ceil(procs/threads)
                ii=th2I*threads+thI;
                if ii<=procs
                    func(ii);
                end
                
            end
        end
    case 'for'
        for ii=1:procs
            func(ii);
        end
    case 'scheduler_local'
        jobmanagername='local';
        submittoJM();
    case 'scheduler'
        submittoJM();
    otherwise
        disp('Bad cop, no donut');
        
end
end




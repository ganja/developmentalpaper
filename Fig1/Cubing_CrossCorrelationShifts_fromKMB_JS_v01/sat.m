function y=sat(array,iter,value)
n=size(iter,1);
y=array;
switch n
    case 1
        y(iter(1))=value;
    case 2
        y(iter(1),iter(2))=value;
    case 3
        y(iter(1),iter(2),iter(3))=value;
    case 4
        y(iter(1),iter(2),iter(3),iter(4))=value;
    case 5
        y(iter(1),iter(2),iter(3),iter(4),iter(5))=value;
end

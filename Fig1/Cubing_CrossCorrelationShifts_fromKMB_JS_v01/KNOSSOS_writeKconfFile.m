function KNOSSOS_writeKconfFile(C,kl_filePrefix,kl_boundary_xyz,kl_scale_xyz,kl_mag)
if C.scaleDownZ
    kl_parname=[C.cubedir,num2str(kl_mag),filesep];
else
    kl_parname=[C.cubedir,num2str(kl_mag),'_',num2str(kl_mag),'_1',filesep];
end
mkdir(kl_parname);
%system(['chmod 777 ' kl_parname]);
kl_fname = strcat(kl_parname,'knossos.conf');
fid=fopen(kl_fname,'w');

fprintf(fid,'experiment name \"%s\";\n',kl_filePrefix);
fprintf(fid,'boundary x %d;\n',kl_boundary_xyz(1));
fprintf(fid,'boundary y %d;\n',kl_boundary_xyz(2));
fprintf(fid,'boundary z %d;\n',kl_boundary_xyz(3));
fprintf(fid,'scale x %.2f;\n',kl_scale_xyz(1));
fprintf(fid,'scale y %.2f;\n',kl_scale_xyz(2));
fprintf(fid,'scale z %.2f;\n',kl_scale_xyz(3));

fprintf(fid,'magnification %d;\n',kl_mag);


fclose(fid);




end
function use=detectWeight(C,i,dimIt)
weightarray=makeWA(C);
use=false;
if weightarray{dimIt}(i(2),i(1))>0
    if i(3)>weightarray{dimIt}(i(2),i(1))
        use=true;
    end
else
    if i(3)<-weightarray{dimIt}(i(2),i(1))
        use=true;
    end
end
end

function KMBwriteTifs(all_fns,ix1,C,ttemp,onlyfirst,imageModifier)
lastimage=[];
iids=[];
if C.small
    factor=8;
else
    factor=1;
end

dims=C.dims/factor;
maxMagMultXY=C.maxMagMultXY/factor;
for currentThread2=1:size(ix1,2)
    ixx=it([C.inA;[currentThread2,currentThread2]]);
    for ix2=1:size(ixx,2)
        
        currentThread=(currentThread2-1)*size(ixx,2)+ix2;
        %kmbw=KMBwait(currentThread,fileReaderThreads,C,'montagesMade',true);
        xyzCs=ixx(:,ix2);
        thismontage=cell(2,size(ixx,2));
        writeB=ones([maxMagMultXY,1],'uint8');
        if onlyfirst
            theend=(xyzCs(3)-1)*C.cubesize+1;
        else
            theend=min(xyzCs(3)*C.cubesize,C.P);
        end
        for count = (xyzCs(3)-1)*C.cubesize+1:theend
            count
            countT=count-(xyzCs(3)-1)*C.cubesize;
            thismontage{1,countT}=zeros(C.cubesize*[maxMagMultXY],'uint8');
            iid_position_store=cell(C.M,C.N);
            for imgcount = [C.M*C.N:-1:1]
                xyIts = [ mod(imgcount-1,C.M) floor((imgcount-1)/C.M)]+1;
                thisshifts = ttemp.shiftsN(xyIts(1),xyIts(2),count,:)/factor;
                xycoords = dims.*(xyIts-1)+squeeze(fix(thisshifts))'+1;
                xycoordsT=xycoords+dims-1;
                if size(at(all_fns,[xyIts(1);xyIts(2);count]),1)>0
                    if C.small
                        fid=fopen([all_fns{xyIts(1),xyIts(2),count}(1:end-4) '_small.raw'],'r');
                        tempdata=fread(fid,'uint8');
                        fclose(fid);
                        cache=reshape(uint8(tempdata),dims);
                        
                    else
                        cache=readImage(at(all_fns,[xyIts(1);xyIts(2);count]),at(imageModifier,[xyIts(1);xyIts(2);count]))';
                    end
                    thismontage{1,countT}(xycoords(1):xycoordsT(1), xycoords(2):xycoordsT(2))=cache;
                    iid_position_store{xyIts(1), xyIts(2)}=[xycoords(1),xycoordsT(1), xycoords(2),xycoordsT(2)];
                end
            end
            if onlyfirst
                imwrite(thismontage{1,countT}',[C.tifdir  sprintf('reslicez%u.tif',currentThread2)],'tif');
            else
                if C.calcIID
                    if ~isempty(lastimage)
                        for im=1:C.M
                            for in=1:C.N
                                if isempty(iid_position_store{im,in})
                                    iids_temp(im,in)=0;
                                    continue;
                                end
                                iid_im=single(feval(@(z)cat(3,z(thismontage{1,countT}),z(lastimage)),@(y)feval(@(x)y(x(1):x(2), x(3):x(4)),iid_position_store{im,in})));
                                iids_temp(im,in)=sqrt(sum(sum((diff(iid_im,1,3).*(prod(iid_im,3)>0)).^2))/length(find(prod(iid_im,3)>0)));
                            end
                        end
                        iids=cat(3,iids,iids_temp);
                    end
                    lastimage=thismontage{1,countT};
                end
                if numel(setdiff(C.writeRawsWithTifs,count))<numel(C.writeRawsWithTifs)
                    fid=fopen([C.tifdir  sprintf('%0.5u.raw',count)],'w+');
                    fwrite(fid,thismontage{1,countT}');
                    fclose(fid);
                end
                if C.small
                    imwrite(imresize(thismontage{1,countT}',C.outtifresizefactor),[C.tifdir  sprintf('%0.5u_small.tif',count)],'tif');
                else
                    imwrite(imresize(thismontage{1,countT}',C.outtifresizefactor),[C.tifdir  sprintf('%0.5u.tif',count)],'tif');
                end
                
                
            end
            thismontage{1,countT}=[];
        end
    end
end
if C.calcIID
    
    save([C.matdir 'iids.mat']);
end
end
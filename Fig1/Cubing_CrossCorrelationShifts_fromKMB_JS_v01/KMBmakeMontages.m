
C:\Users\Kevin\Documents\MATLAB\Cubing>@git.exe %*
function KMBmakeMontages(C,all_fns,hashmap,reverse,firstSlice,lastSlice,ttemp)


for count = firstSlice:lastSlice
    thismontage=zeros(fliplr(C.montagehw),'uint8');
    thismontage=thismontage+255;
    this_slicecount = count;
    LSSweightsum=ttemp.weightsum((this_slicecount-1)*C.M*C.N+1:this_slicecount*C.M*C.N);
    for i=1:size(LSSweightsum,1)
        LSSweightsum(i,2)=i;
    end
    LSSweightsum=sortrows(LSSweightsum);
    for imgcount2 = 1:C.M*C.N
        if reverse
            imgcount=LSSweightsum(end+1-imgcount2,2);
        else
            imgcount=LSSweightsum(imgcount2,2);
        end
        thisimg = (this_slicecount-1)*C.M*C.N+imgcount;
        xIt = mod(imgcount-1,C.M)+1;
        yIt = floor((imgcount-1)/C.M)+1;
        thisshifts = ttemp.shiftsN(thisimg,:);
        xcoord = C.dims(2)*mod(imgcount-1,C.M)+fix(thisshifts(2))+1;
        ycoord = C.dims(1)*floor((imgcount-1)/C.M)+fix(thisshifts(1))+1;
        if size(at(all_fns,[xIt;yIt;count]),1)>0
            at(all_fns,[xIt;yIt;count])
            im = (KMBreadTif(at(all_fns,[xIt;yIt;count])));
            im=KMBtext(im, hashmap.get(at(all_fns,[xIt;yIt;count])), xIt, yIt,ttemp.shifts, thisimg,this_slicecount,C.M,C.N);
            thismontage(xcoord+C.writeCrop:xcoord+C.dims(2)-1-C.writeCrop,ycoord+C.writeCrop:ycoord+C.dims(1)-1-C.writeCrop) =im(1+C.writeCrop:end-C.writeCrop,1+C.writeCrop:end-C.writeCrop)';
            
        end
        
    end
    if reverse
        imwrite(thismontage',strcat('D:\\testb',num2str(100+count),'.tif'));
    else
        imwrite(thismontage',strcat('D:\\testa',num2str(100+count),'.tif'));
    end
end


C:\Users\Kevin\Documents\MATLAB\Cubing>@set ErrorLevel=%ErrorLevel%

C:\Users\Kevin\Documents\MATLAB\Cubing>@rem Restore the original console codepage.

C:\Users\Kevin\Documents\MATLAB\Cubing>@chcp %cp_oem% > nul < nul

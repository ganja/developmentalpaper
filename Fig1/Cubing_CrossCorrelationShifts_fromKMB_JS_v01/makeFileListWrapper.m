function [all_fns, imageModifier]=makeFileListWrapper(C)
temp=makeFileList(C.piezoM,C.piezoN,C.allslice_arr(1,3),C.M/C.piezoM,C.N/C.piezoN,C.origin(1:end-1),[C.origin sprintf('st%uTop',C.allslice_arr(1,1)) filesep sprintf('st%u',C.allslice_arr(1,1)) 'Config/imageFiles.txt']);
all_fns=temp.all_fns;
all_fns=all_fns(:,:,C.allslice_arr(1,2):end);

newsize=ceil(size(all_fns,3)/C.slPth)*C.slPth;
all_fns{1,1,newsize}=[];
rotation_mat=cell2mat(temp.rotations);

imageModifier=reshape(C.imageModifierBase(rotation_mat(:)+1),[C.M,C.N,size(rotation_mat,3)]);
imageModifier=imageModifier(:,:,C.allslice_arr(1,2):end);
imageModifier{1,1,newsize}=[];
end
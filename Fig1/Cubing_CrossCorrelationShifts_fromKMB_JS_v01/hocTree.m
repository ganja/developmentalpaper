function [ output_args ] = hocTree(C,ttemp)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

addpath('../auxiliaryMethods/skeletonClass');
skel=skeleton()
[skel,mt]=skel.addTree('MyTree');
lookup=[];

dire=@(x)(x(:,:,:,1))^2+x(:,:,:,1)^2;
for count=1:C.P
    count
    for n=1:C.N
        for m=1:C.M
            xycoords=C.dims.*([m n]-1)+squeeze(fix(ttemp.shiftsN(m,n,count,:)))'+1;
            if count+n+m==3
                skel=skel.addFirstNode(mt,xycoords(1),xycoords(2),count*1000);
                lookup(1,1,1)=1;
                
                continue
            end
            ce=find([m>1,n>1,count>1],1);
            for k=1:2
                for j=eye(3)
                    if  min([m,n,count]-j')==0
                        continue;
                    end
                    ti=lookup(m-j(1),n-j(2),count-j(3));
                    tv=skel.nodes{mt}(ti,:);
                    diameter=dire(ttemp.alldeltaErrW{find(j)}(m-j(1),n-j(2),count-j(3),:));
                    if find(j)==ce&&k==1
                        skel=skel.addNode(mt,tv(1),tv(2),tv(3),ti,diameter);
                        skel=skel.addNode(mt,xycoords(1),xycoords(2),count*1000,size(skel.nodes{mt},1),diameter);
                        lookup(m,n,count)=size(skel.nodes{mt},1);
                    end
                    if find(j)~=ce&&k==2
                             
                        skel=skel.addNode(mt,xycoords(1),xycoords(2),count*1000,lookup(m,n,count),diameter);
                        skel=skel.addNode(mt,tv(1),tv(2),tv(3),size(skel.nodes{mt},1),diameter);
                        
                        
                    end
                    
                end
            end
        end
    end
end
skel.writeHoc('~/test.hoc');



end


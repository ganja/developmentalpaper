% Kevin M. Boergens
% kevin.boergens@brain.mpg.de
% 2014

function makeSuperCubesReal(C)
for mags=[1 2 4 8 16 32 64 128 256]
    folder=C.cubedir;
    if C.scaleDownZ||mags<2
        actfolder=sprintf('%s%u',folder,mags);
    else
        actfolder=sprintf('%s%u_%u_%u',folder,mags,mags,1);
    end
    flags=0;
    cubes=zeros([256 256 128+C.scaleDownZ*128]);
    for metax=0:2:198
        disp(sprintf('mag %u - x folder %u',mags,metax));
        xf=sprintf('%s%sx%0.4d',actfolder,filesep,metax);
        xf2=sprintf('%s%sx%0.4d',actfolder,filesep,metax+1);
        if exist(xf,'dir') ||exist(xf2,'dir')
            for metay=0:2:198
                xf=sprintf('%s%sx%0.4d',actfolder,filesep,metax);
                xf2=sprintf('%s%sx%0.4d',actfolder,filesep,metax+1);
                yf=sprintf('%s%sy%0.4d',xf,filesep,metay);
                yf2=sprintf('%s%sy%0.4d',xf,filesep,metay+1);
                yf3=sprintf('%s%sy%0.4d',xf2,filesep,metay);
                yf4=sprintf('%s%sy%0.4d',xf2,filesep,metay+1);
                if exist(yf,'dir') ||exist(yf2,'dir')||exist(yf3,'dir')||exist(yf4,'dir')
                    for metaz=0:1+C.scaleDownZ:98
                        for x=metax:metax+1
                            for y=metay:metay+1
                                for z=metaz:metaz+C.scaleDownZ
                                    xf=sprintf('%s%sx%0.4d',actfolder,filesep,x);
                                    yf=sprintf('%s%sy%0.4d',xf,filesep,y);
                                    zf=sprintf('%s%sz%0.4d',yf,filesep,z);
                                    if exist(zf,'dir')
                                        filename=dir([zf filesep '*.raw']);
                                        ff0=[zf filesep filename.name];
                                        fid = fopen( ff0 );
                                        kl_cube = fread( fid,inf,'*uint8' );
                                        fclose(fid);
                                        kl_cube = reshape( kl_cube, [128 128 128] );
                                        cubes(mod(x,2)*128+1:mod(x,2)*128+128,mod(y,2)*128+1:mod(y,2)*128+128,mod(z,1+C.scaleDownZ)*128+1:mod(z,1+C.scaleDownZ)*128+128)=kl_cube;
                                        flags=1;
                                        xx=x;
                                        yy=y;
                                        zz=z;
                                    end
                                    if mod(x,2)==1 && mod(y,2)==1 && (mod(z,2)==1|| ~C.scaleDownZ)
                                        if flags
                                            if C.scaleDownZ
                                                hactfolder=sprintf('%s%u',folder,mags*2);
                                            else
                                                hactfolder=sprintf('%s%u_%u_%u',folder,mags*2,mags*2,1);
                                            end
                                            
                                            zf=sprintf('%s%sx%0.4d%sy%0.4d%sz%0.4d',hactfolder,filesep,floor(x/2),filesep,floor(y/2),filesep,floor(z/(1+C.scaleDownZ)));
                                            mkdir(zf);
                                            ff1=[zf filesep filename.name];
                                            ff1=strrep(ff1,sprintf('x%0.4d',xx),sprintf('x%0.4d',floor(x/2)));
                                            ff1=strrep(ff1,sprintf('y%0.4d',yy),sprintf('y%0.4d',floor(y/2)));
                                            if C.scaleDownZ
                                                ff1=strrep(ff1,sprintf('z%0.4d',zz),sprintf('z%0.4d',floor(z/2)));
                                            end
                                            ff1=strrep(ff1,sprintf('mag%u',mags),sprintf('mag%u',mags*2));
                                            
                                            fid = fopen( ff1, 'w+' );
                                            cubes=imresize(cubes,0.5);
                                            if C.scaleDownZ
                                                cubes=permute(cubes,[3 2 1]);
                                                cubes=imresize(cubes,[128 128]);
                                                cubes=permute(cubes,[3 2 1]);
                                            end
                                            fwrite( fid, reshape( cast(cubes,'uint8'), 1, [] ) );
                                            fclose( fid );
                                        end
                                        flags=0;
                                        cubes=zeros([256 256 128+128*C.scaleDownZ]);
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

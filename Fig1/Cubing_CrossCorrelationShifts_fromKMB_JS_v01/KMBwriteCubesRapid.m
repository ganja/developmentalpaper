function KMBwriteCubesRapid(all_fns,currentThread2,C,ttemp,mag3,superfast)
ixx=it([C.inA;[currentThread2,currentThread2]]);

for ix2=1:size(ixx,2)
    currentThread=(currentThread2-1)*size(ixx,2)+ix2;
    data=[];
    for i=1:16
        fn=[C.originRapid  sprintf('%u',(currentThread-1)*16+i) '.mat'];
        if (exist(fn,'file')~=0)
            zz=load(fn);
            data=cat(3,data,zz.data(:,:,1:8));
        end
        
    end
    
    xyzCs=ixx(:,ix2);
    thismontage=cell(2,1);
    thismontage{2}=zeros(C.cubesize*[C.maxMagMultXY,8]/8,'uint8');
    
    for count = (xyzCs(3)-1)*C.cubesize+1:min(xyzCs(3)*C.cubesize,C.P)
        LSSweightsum=ttemp.weightsum((count-1)*C.M*C.N+1:count*C.M*C.N);
        for i=1:size(LSSweightsum,1)
            LSSweightsum(i,2)=i;
        end
        LSSweightsum=sortrows(LSSweightsum);
        countT=count-(xyzCs(3)-1)*C.cubesize;
        for imgcount2 = 1:C.M*C.N
            imgcount=LSSweightsum(end+1-imgcount2,2);
            thisimg = (count-1)*C.M*C.N+imgcount;
            xyIts = [ mod(imgcount-1,C.M) floor((imgcount-1)/C.M)]+1;
            thisshifts = ttemp.shiftsN(thisimg,:);
            xycoords = C.dims.*(xyIts-1)+fix(thisshifts)+1;
            xycoordsT=xycoords+C.dims-1;
            
            
            if size(at(all_fns,[xyIts(1);xyIts(2);count]),1)>0
                cache=data{xyIts(1),xyIts(2),countT}';
                xycoordsT=floor(xycoordsT/8);
                xycoords=floor((xycoords-1)/8)+1;
                thismontage{2}(xycoords(1):xycoordsT(1), xycoords(2):xycoordsT(2),countT)=cache;
            end
            
            
            
            
        end
    end
end
if (~mag3)
    thismontage2=zeros(C.cubesize*[C.maxMagMultXY,1]/8,'uint8');
    for i=1:8
        thismontage2=thismontage2+thismontage{2}(:,:,i:8:end)/8;
    end
    save([C.cubedir filesep 'rapidFin' sprintf('%u',currentThread) '.mat'],'thismontage2');
else
    relativeV=[1,1;1,1;1 8];
    c8d=1/8*[C.maxMagMultXY';1].*relativeV(:,2);
        if ~superfast
            KMBwriteCubeItsMaker(C,xyzCs,3,thismontage,0,c8d,false);
        end
        % mag 5
        thisfact=2;
        relativeV=[1,1/thisfact;1,1/thisfact;1 8];
        thismontage{3}=zeros(ceil(size(thismontage{2})./[C.cubesize*8*thisfact C.cubesize*8*thisfact 1]).*[C.cubesize*8 C.cubesize*8 1],'uint8');
        tmt=imresize(thismontage{2},1/thisfact);
        thismontage{3}(1:size(tmt,1),1:size(tmt,2),:)=tmt;
        c8d=ceil(1/8*[C.maxMagMultXY';1].*relativeV(:,2));
        KMBwriteCubeItsMaker(C,xyzCs,5,thismontage,0,c8d,false);
        %mag7
       
        thisfact=4;
        relativeV=[1,1/thisfact;1,1/thisfact;1 8];
        thismontage{3}=zeros(ceil(size(thismontage{2})./[C.cubesize*8*thisfact C.cubesize*8*thisfact 1]).*[C.cubesize*8 C.cubesize*8 1],'uint8');
        tmt=imresize(thismontage{2},1/thisfact);
        thismontage{3}(1:size(tmt,1),1:size(tmt,2),:)=tmt;
        c8d=ceil(1/8*[C.maxMagMultXY';1].*relativeV(:,2));
        KMBwriteCubeItsMaker(C,xyzCs,7,thismontage,0,c8d,false);
   
    
end
end
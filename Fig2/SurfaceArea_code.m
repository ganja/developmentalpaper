skel = skeleton('Z:\Data\goura\Analysis\Shrew\SpineVolume\Shrew_spineVolume.nml');
SomaTree = find(cellfun(@(x)any(strfind(x, 'ex')==1),skel.names));
somaNames = skel.names(SomaTree);
Area = zeros(length(SomaTree),1);
Volume = zeros(length(SomaTree),1);
for i = 1:length(SomaTree)
TreeNodes = skel.getNodes(SomaTree(i));
TreeNodes_um = TreeNodes.*(skel.scale/1000);

x = TreeNodes_um(:,1);
y = TreeNodes_um(:,2);
z = TreeNodes_um(:,3);

shp = alphaShape(x,y,z);
Area(i) = surfaceArea(shp);
Volume(i) = volume(shp);
end
% xlin = linspace(min(x),max(x),33);
% ylin = linspace(min(y),max(y),33);
% [X,Y] = meshgrid(xlin,ylin);
% Z = griddata(x,y,z,X,Y,'cubic');
% 
% figure;
% plot(shp)

%% getting synapses as separate for AMIRA
treeIndices = find(cellfun(@(x)any(strfind(x, 'SomaSurface001')==1),skel.names));
namesCorrect = skel.names(treeIndices);
for i=1:length(treeIndices)
    skel.write(namesCorrect{i},treeIndices(i))
end

%% Catenate surface areas and bootstrp
MeanAreas = cat(1,mean(Area_P7Soma), mean(Area_P9Soma),mean(Area_P14Soma),mean(Area_P28Soma));
g1 = ones(size(Area_P7Soma));
g2 = 2*ones(size(Area_P9Soma));
g3 = 3*ones(size(Area_P14Soma));
g4 = 4*ones(size(Area_P28Soma));
G = cat(1,g1,g2,g3,g4);
Data = cat(1,Area_P7Soma,Area_P9Soma,Area_P14Soma,Area_P28Soma);

% boot strp
Area_P7btstrp = bootstrp(10,@mean,Area_P7Soma); SE_7 = std(Area_P7btstrp);
Area_P9btstrp = bootstrp(10,@mean,Area_P9Soma); SE_9 = std(Area_P9btstrp);
Area_P14btstrp = bootstrp(10,@mean,Area_P14Soma); SE_14 = std(Area_P14btstrp);
Area_P28btstrp = bootstrp(10,@mean,Area_P28Soma); SE_28 = std(Area_P28btstrp);
BtstrpArea = cat(2,Area_P7btstrp,Area_P9btstrp,Area_P14btstrp,Area_P28btstrp);
SE = cat(1,SE_7,SE_9,SE_14,SE_28);
plus = MeanAreas+SE;
minus = MeanAreas-SE;

%% For synaptic inputs on soma
MeanSynapses = cat(1,mean(Syn_P7Soma), mean(Syn_P9Soma),mean(Syn_P14Soma),mean(Syn_P28Soma));
g1 = ones(size(Syn_P7Soma));
g2 = 2*ones(size(Syn_P9Soma));
g3 = 3*ones(size(Syn_P14Soma));
g4 = 4*ones(size(Syn_P28Soma));
G_syn = cat(1,g1,g2,g3,g4);
Data_Syn = cat(1,Syn_P7Soma,Syn_P9Soma,Syn_P14Soma,Syn_P28Soma);

Syn_P7btstrp = bootstrp(10,@mean,Syn_P7Soma); SE_syn_7 = std(Syn_P7btstrp);
Syn_P9btstrp = bootstrp(10,@mean,Syn_P9Soma); SE_syn_9 = std(Syn_P9btstrp);
Syn_P14btstrp = bootstrp(10,@mean,Syn_P14Soma); SE_syn_14 = std(Syn_P14btstrp);
Syn_P28btstrp = bootstrp(10,@mean,Syn_P28Soma); SE_syn_28 = std(Syn_P28btstrp);
BtstrpSyn = cat(2,Syn_P7btstrp,Syn_P9btstrp,Syn_P14btstrp,Syn_P28btstrp);
SE_syn = cat(1,SE_syn_7,SE_syn_9,SE_syn_14,SE_syn_28);
SynPlus = MeanSynapses+SE_syn;
SynMinus = MeanSynapses-SE_syn;


%%
figure;
% yyaxis left
boxplot(Data_Syn,G_syn,'positions',[7 9 14 28],'Colors','k','Widths',1.8)
hold on
plot([7 9 14 28],MeanSynapses,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot([7 9 14 28],SynPlus,'--k')% lplus err line
hold on
plot([7 9 14 28],SynMinus,'--k')% lplus err line
hold on
set(gca,'Ylim',[0 100])
ylabel('equivalent diamteter (mm)')
hold on
scatter(ones(numel(Syn_P7Soma),1)*7+rand(numel(Syn_P7Soma),1)*.75-.45, Syn_P7Soma,'x','MarkerEdgeColor',[0.25 0.25 0.25],'SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(Syn_P9Soma),1)*9+rand(numel(Syn_P9Soma),1)*.75-.45, Syn_P9Soma,'x','MarkerEdgeColor',[0.25 0.25 0.25],'SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(Syn_P14Soma),1)*14+rand(numel(Syn_P14Soma),1)*.75-.45, Syn_P14Soma,'x','MarkerEdgeColor',[0.25 0.25 0.25],'SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(Syn_P28Soma),1)*28+rand(numel(Syn_P28Soma),1)*.75-.45, Syn_P28Soma,'x','MarkerEdgeColor',[0.25 0.25 0.25],'SizeData',200, 'LineWidth',0.0005);
hold on
% yyaxis right
% boxplot(Data,G,'positions',[7 9 14 28],'Colors','k','Widths',1.8)
% hold on
% plot([7 9 14 28],MeanAreas,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
% hold on
% plot([7 9 14 28],plus,'--k')% lplus err line
% hold on
% plot([7 9 14 28],minus,'--k')% lplus err line
% hold on
% set(gca,'Ylim',[0 1500])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
% ylabel('Somatic surface area (um^2)')
xlabel('Postnatal Age (days)')
% hold on
% scatter(ones(numel(Area_P7Soma),1)*7+rand(numel(Area_P7Soma),1)*.75-.45, Area_P7Soma,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200, 'LineWidth',0.0005);
% hold on
% scatter(ones(numel(Area_P9Soma),1)*9+rand(numel(Area_P9Soma),1)*.75-.45, Area_P9Soma,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200, 'LineWidth',0.0005);
% hold on
% scatter(ones(numel(Area_P14Soma),1)*14+rand(numel(Area_P14Soma),1)*.75-.45, Area_P14Soma,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200, 'LineWidth',0.0005);
% hold on
% scatter(ones(numel(Area_P28Soma),1)*28+rand(numel(Area_P28Soma),1)*.75-.45, Area_P28Soma,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200, 'LineWidth',0.0005);
% hold on
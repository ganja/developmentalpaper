% skel = skeleton('C:\Users\goura\Downloads\P7_L4 (5).nml'); %P7_L4
% skel = skeleton('C:\Users\goura\Downloads\P9_L4_ApicalsAISAxons (2).nml'); %P9-L4a
% skel = skeleton('C:\Users\goura\Downloads\P9_L4_n2 (1).nml');%P9_L4b
% skel = skeleton('C:\Users\goura\Downloads\P14n1_2019 (4).nml');%P14-L4a
% skel = skeleton('C:\Users\goura\Downloads\P14L4_n2_updatedWithADs (3).nml');%P14-L4b
% skel = skeleton('C:\Users\goura\Downloads\Tracin_new.nml');%P14-L4b part2
% skel = skeleton('C:\Users\goura\Downloads\P28_AG_30-01-2019 (1).nml'); %P28-L4
%L2/3
% skel = skeleton('Z:\Data\goura\FigureUpdates\L23\NML_05-02-2020\P9_23_05-02-2020.nml');
% skel = skeleton('Z:\Data\goura\FigureUpdates\L23\NML_05-02-2020\P14_L23_05-02-2020.nml'); %P14 L2/3
skel = skeleton('Z:\Data\goura\FigureUpdates\L23\NML_05-02-2020\P28_L23_05-02-2020.nml'); %P28 L2/3

%% for soma axons
treeIndices = find(cellfun(@(x)any(strfind(x, 'IS0')==1),skel.names));
Names = skel.names(treeIndices);
AxCount = numel(treeIndices);

AllSyn = skel.getNodesWithComment('syn',treeIndices,'partial'); %first get all syn
ADComments = skel.getNodesWithComment('AD',treeIndices,'partial');%for AD syn
AISComments = skel.getNodesWithComment('AIS',treeIndices,'partial');
SomaComments = skel.getNodesWithComment('Soma',treeIndices,'partial');%for Soma syn
SpineComments = skel.getNodesWithComment('spine',treeIndices,'partial');
ShaftComments = skel.getNodesWithComment('shaft',treeIndices,'partial');
GliaComments = skel.getNodesWithComment('glia',treeIndices,'partial');
SomFilComments = skel.getNodesWithComment('Som. fil.',treeIndices,'partial');

for i = 1:AxCount
ADSyn{i} = intersect(AllSyn{i},ADComments{i});
AISSyn{i} = intersect(AllSyn{i},AISComments{i});
SomaSyn{i}= intersect(AllSyn{i},SomaComments{i});
SpineSyn{i} = intersect(AllSyn{i},SpineComments{i});
ShaftSyn{i} = intersect(AllSyn{i},ShaftComments{i});
GliaSyn{i} = intersect(AllSyn{i},GliaComments{i});
SomFilSyn{i} = intersect(AllSyn{i},SomFilComments{i});
end

ADSyn = ADSyn'; 
AISSyn = AISSyn';
SomaSyn = SomaSyn';
SpineSyn = SpineSyn';
ShaftSyn = ShaftSyn';
GliaSyn = GliaSyn';
SomFilSyn = SomFilSyn';

ADSyn_num = zeros(size(treeIndices));ADfrac = zeros(size(treeIndices));
AISSyn_num = zeros(size(treeIndices));AISfrac = zeros(size(treeIndices));
SMSyn_num = zeros(size(treeIndices));SMfrac = zeros(size(treeIndices));
SpineSyn_num = zeros(size(treeIndices));Spinefrac = zeros(size(treeIndices));
ShaftSyn_num = zeros(size(treeIndices));Shaftfrac = zeros(size(treeIndices));
GliaSyn_num = zeros(size(treeIndices));Gliafrac = zeros(size(treeIndices));
SomFilSyn_num = zeros(size(treeIndices));SomFilfrac = zeros(size(treeIndices));

TotalSyn = zeros(size(treeIndices));
PathLengths = zeros(size(treeIndices));
for i = 1:AxCount
    TotalSyn(i) = numel(AllSyn{i});
    ADSyn_num(i) = numel(ADSyn{i});
    AISSyn_num(i) = numel(AISSyn{i});
    SMSyn_num(i)= numel(SomaSyn{i});
    SpineSyn_num(i)= numel(SpineSyn{i});
    ShaftSyn_num(i)= numel(ShaftSyn{i});
    GliaSyn_num(i)= numel(GliaSyn{i});
    SomFilSyn_num(i)= numel(SomFilSyn{i});
    PathLengths(i) = skel.pathLength(treeIndices(i))/1000;
end

SynData = cat(2,PathLengths,TotalSyn,ADSyn_num,AISSyn_num,SMSyn_num,SpineSyn_num,ShaftSyn_num,GliaSyn_num,SomFilSyn_num);

%% for innervations

T = TotalSyn-1;
AIS = AISSyn_num-1;


ADfrac = ADSyn_num./T;
AISfrac = AIS./T;
SMfrac = SMSyn_num./T;
Spinefrac = SpineSyn_num./T;
Shaftfrac = ShaftSyn_num./T;
Gliafrac = GliaSyn_num./T;
SomFilfrac = SomFilSyn_num./T;

FracInn = cat(2,ADfrac,AISfrac,SMfrac,Spinefrac,Shaftfrac,Gliafrac,SomFilfrac);

%% for bulk innervations
ADBulk = (sum(ADSyn_num))/(sum(TotalSyn)-AxCount);
AISBulk = (sum(AISSyn_num)-AxCount)/(sum(TotalSyn)-AxCount);
SomaBulk = (sum(SMSyn_num))/(sum(TotalSyn)-AxCount);
SpineBulk = (sum(SpineSyn_num))/(sum(TotalSyn)-AxCount);
ShaftBulk = (sum(ShaftSyn_num))/(sum(TotalSyn)-AxCount);
GliaBulk = (sum(GliaSyn_num))/(sum(TotalSyn)-AxCount);
SomFilBulk = (sum(SomFilSyn_num))/(sum(TotalSyn)-AxCount);

BulkInn = cat(2,ADBulk,AISBulk,SomaBulk,SpineBulk,ShaftBulk,GliaBulk,SomFilBulk);

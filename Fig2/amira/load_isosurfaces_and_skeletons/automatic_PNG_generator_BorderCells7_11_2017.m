%this will make Amira snapshots of every cell within subfolders with a
%depth of 1 in the given folder

clear 

path = ('Z:\Projekte\2017\L4_CenterwholeCells_2017\new_wholeCells_7_11_2017\BorderCells');
ending = '.ply'; %.issf, .ply or .am


%start scripot
source = dir([(path) '\*' ending]);
fileID = fopen('make_nice_isosurface.txt','w');


for i = 1:size(source,1); 
    
colornumber = size(source,1);
colors = distinguishable_colors(colornumber,[1 1 1; 0 0 0]); %[1 1 1; 0 0 0] oder [1 1 1] (wei�) oder [0 0 0] (schwarz)
allColors = [colors]';


 pathFile = ([(path) '/' (source(i,1).name)]); %load files
 pathFileModified = strrep(pathFile, '\', '/');
 fprintf(fileID,'load %s\n',pathFileModified); %load files

 SurfaceName = (source(i,1).name); %load files
  fprintf(fileID,'%s makeOnePatch\n',SurfaceName); %make one patch

 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'create HxDisplaySurface "%s"\n',SurfaceNameModified); %create surface view

 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'"%s"',SurfaceNameModified); %connect to data
 fprintf(fileID,' data connect %s\n',SurfaceName); %connect to data

 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 Color1 = allColors(1,i);
 Color2 = allColors(2,i);
 Color3 = allColors(3,i);
 fprintf(fileID,'"%s"',SurfaceNameModified); %make the color
 fprintf(fileID,' colormap setDefaultColor %1.4f',Color1); %make the color
 fprintf(fileID,' %1.4f',Color2); %make the color
 fprintf(fileID,' %1.4f\n',Color3); %make the color

%  SurfaceName = (source(i,1).name); %load files
%  SurfaceNameModified = strrep(SurfaceName, ending, '');
%  fprintf(fileID,'"%s" colormap setDefaultColor 1 0 0\n',SurfaceNameModified); %make all ONE color

 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'"%s" drawStyle setNormalBinding 1\n',SurfaceNameModified);%set vertex normals

%set transparency:
%  fprintf(fileID,'"%s" drawStyle setValue 4\n',SurfaceNameModified); %make it transparent
%  fprintf(fileID,'"%s" drawStyle setAlphaMode 3\n',SurfaceNameModified);%make it transparent
%  fprintf(fileID,'"%s" baseTrans setValue 0.7\n',SurfaceNameModified);%set transparency value

 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'"%s" colorMode setIndex 0 5\n',SurfaceNameModified);%color mode constant


 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'"%s" fire\n',SurfaceNameModified); %fire

    
fprintf(fileID,'viewer 0 setCameraOrientation 0.643926 0.429079 0.633444 1.52465\n');
fprintf(fileID,'viewer 0 setCameraPosition 172339 -22280.3 116951\n');
fprintf(fileID,'viewer 0 setCameraFocalDistance 171530\n');
fprintf(fileID,'viewer 0 setCameraNearDistance 107855\n');
fprintf(fileID,'viewer 0 setCameraFarDistance 233856\n');
fprintf(fileID,'viewer 0 snapshot -tiled 2 2 Z:/Projekte/2017/L4_CenterwholeCells_2017/images/%s.png\n',(source(i,1).name));


fprintf(fileID,'remove "%s"\n',SurfaceName);
fprintf(fileID,'\n\n');

end

fclose(fileID);
disp('Amira commands have been written into make_nice_isosurface.txt')


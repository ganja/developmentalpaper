function make_nice_issf(path, ending, transparency_level, allOneColor, rangedColor)
% Create Amira command to load isosurface files into Amira
% 2015-2018, MPIBR, heiko.wissler@brain.mpg.de

%start script
source = dir([(path) '\*' ending]);
[~,order] = sort_nat({source.name}); %sorting to solve 1.hoc, 10.hoc, ...
source = source(order);

%prepare colors
colornumber = size(source,1);
if isempty(rangedColor)
    colors = distinguishable_colors(colornumber,[1 1 1; 0 0 0]); %for background [1 1 1; 0 0 0] oder [1 1 1] (wei�) oder [0 0 0] (schwarz)
    allColors = colors';
else
    if strcmp(rangedColor, 'gray') == 1 
        allColors = 0.3 + (0.99-0.3)*rand(1,colornumber); %or these two lines for gray
        allColors(3,:) = allColors(1,:);
        allColors(2,:) = allColors(1,:);
    else
        allColors = hsv2rgb(getRangedColors(rangedColor(1,1),rangedColor(1,2),rangedColor(1,3),rangedColor(1,4),rangedColor(1,5),rangedColor(1,6),colornumber)')';
    end
end

fileID = fopen('load_Amira_files.txt','w');

for i = 1:size(source,1)
    pathFile = ([(path) '/' (source(i,1).name)]); %load files
    pathFileModified = strrep(pathFile, '\', '/');
    fprintf(fileID,'load %s\n',pathFileModified); %load files
end

for i = 1:size(source,1)
    SurfaceName = (source(i,1).name); %load files
    fprintf(fileID,'%s makeOnePatch\n',SurfaceName); %make one patch
end

for i = 1:size(source,1)
    SurfaceName = (source(i,1).name); %load files
    SurfaceNameModified = strrep(SurfaceName, ending, '');
    fprintf(fileID,'create HxDisplaySurface "%s"\n',SurfaceNameModified); %create surface view
end

for i = 1:size(source,1)
    SurfaceName = (source(i,1).name); %load files
    SurfaceNameModified = strrep(SurfaceName, ending, '');
    fprintf(fileID,'"%s"',SurfaceNameModified); %connect to data
    fprintf(fileID,' data connect %s\n',SurfaceName); %connect to data
end

%make the color
if isempty(allOneColor)
    for i = 1:size(source,1)
        SurfaceName = (source(i,1).name); %load files
        SurfaceNameModified = strrep(SurfaceName, ending, '');
        Color1 = allColors(1,i);
        Color2 = allColors(2,i);
        Color3 = allColors(3,i);
        fprintf(fileID,'"%s"',SurfaceNameModified); %make the color
        fprintf(fileID,' colormap setDefaultColor %1.4f',Color1); %make the color
        fprintf(fileID,' %1.4f',Color2); %make the color
        fprintf(fileID,' %1.4f\n',Color3); %make the color
    end
else
    for i = 1:size(source,1)
        SurfaceName = (source(i,1).name); %load files
        SurfaceNameModified = strrep(SurfaceName, ending, '');
        fprintf(fileID,'"%s" colormap setDefaultColor ',SurfaceNameModified); %make all ONE color
        fprintf(fileID,'%s \n',allOneColor); %make all ONE color
        
    end
end

for i = 1:size(source,1)
    SurfaceName = (source(i,1).name); %load files
    SurfaceNameModified = strrep(SurfaceName, ending, '');
    fprintf(fileID,'"%s" drawStyle setNormalBinding 1\n',SurfaceNameModified);%set vertex normals
    % set transparency:
    if transparency_level == 0
    else
        fprintf(fileID,'"%s" drawStyle setValue 4\n',SurfaceNameModified); %make it transparent
        fprintf(fileID,'"%s" drawStyle setAlphaMode 3\n',SurfaceNameModified);%make it transparent
        fprintf(fileID,'"%s" baseTrans setValue ',SurfaceNameModified);%set transparency value
        fprintf(fileID,'%s \n',transparency_level);%set transparency value
    end
end

for i = 1:size(source,1)
    SurfaceName = (source(i,1).name); %load files
    SurfaceNameModified = strrep(SurfaceName, ending, '');
    fprintf(fileID,'"%s" colorMode setIndex 0 5\n',SurfaceNameModified);%color mode constant
end

for i = 1:size(source,1)
    SurfaceName = (source(i,1).name); %load files
    SurfaceNameModified = strrep(SurfaceName, ending, '');
    fprintf(fileID,'"%s" fire\n',SurfaceNameModified); %fire
end

%{
%% BUG FIX JS to fit old AMIRA Verions *flipped around X axis
for i = 1:size(source,1)
    SurfaceName = (source(i,1).name); %load files
     fprintf(fileID,'"%s" setTransform 1 0 0 0 0 -1 0 0 0 0 -1 0 0 178435 54915.2 1\n',SurfaceName);
end
%}

fclose(fileID);

disp('Amira commands have been written into load_Amira_files.txt')

end


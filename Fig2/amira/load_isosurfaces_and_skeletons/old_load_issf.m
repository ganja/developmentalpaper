%get code to load items into Amira (paste code in Amira console)
clear
clc

path = ('Z:\temp\test_NMLs\Amira_hoc-files'); %just set path, rest will be done automatically
ending = '.hoc'; %.issf, .ply or .am
transparency_level = '0'; %0-1, 1 = fully transparent, 0 = no transparency
allOneColor = ''; %set the color in RGB, example: 1 0.4 0.2; '' for multicolor
rangedColor = ''; %HSV range, set example below include [] or set this for multicolor: ''
%yellow/orange: [0.056,0.174,0.55,1,0.65,1]
%red: [0.77,1,0.2,1,0.9,1]
%orange: [0.08,0.12,0.7,1,0.7,1]
%magenta/red: [0.85,1,0.2,1,0.9,1]
%blue cyan green: [0.3,0.66,0.5,1,0.3,1]
%blue: [0.56,0.65,0.8,1,0.3,0.6]


%start script
source = dir([(path) '\*' ending]);
colornumber = size(source,1);
if isempty(rangedColor)
    colors = distinguishable_colors(colornumber,[1 1 1; 0 0 0]); %for background [1 1 1; 0 0 0] oder [1 1 1] (wei�) oder [0 0 0] (schwarz)
    allColors = [colors]';
else
    allColors = hsv2rgb(getRangedColors(rangedColor(1,1),rangedColor(1,2),rangedColor(1,3),rangedColor(1,4),rangedColor(1,5),rangedColor(1,6),colornumber)')';
    % allColors = 0.3 + (0.8-0.3)*rand(1,colornumber); %or these two lines for gray ranged colors
    % allColors(3,:) = allColors(1,:)
end

fileID = fopen('make_nice_isosurface.txt','w');

for i = 1:size(source,1)
    pathFile = ([(path) '/' (source(i,1).name)]); %load files
    pathFileModified = strrep(pathFile, '\', '/');
    fprintf(fileID,'load %s\n',pathFileModified); %load files
end

for i = 1:size(source,1)
    SurfaceName = (source(i,1).name); %load files
    fprintf(fileID,'%s makeOnePatch\n',SurfaceName); %make one patch
end

for i = 1:size(source,1)
    SurfaceName = (source(i,1).name); %load files
    SurfaceNameModified = strrep(SurfaceName, ending, '');
    fprintf(fileID,'create HxDisplaySurface "%s"\n',SurfaceNameModified); %create surface view
end

for i = 1:size(source,1)
    SurfaceName = (source(i,1).name); %load files
    SurfaceNameModified = strrep(SurfaceName, ending, '');
    fprintf(fileID,'"%s"',SurfaceNameModified); %connect to data
    fprintf(fileID,' data connect %s\n',SurfaceName); %connect to data
end

%make the color
if isempty(allOneColor)
    for i = 1:size(source,1)
        SurfaceName = (source(i,1).name); %load files
        SurfaceNameModified = strrep(SurfaceName, ending, '');
        Color1 = allColors(1,i);
        Color2 = allColors(2,i);
        Color3 = allColors(3,i);
        fprintf(fileID,'"%s"',SurfaceNameModified); %make the color
        fprintf(fileID,' colormap setDefaultColor %1.4f',Color1); %make the color
        fprintf(fileID,' %1.4f',Color2); %make the color
        fprintf(fileID,' %1.4f\n',Color3); %make the color
    end
else
    for i = 1:size(source,1)
        SurfaceName = (source(i,1).name); %load files
        SurfaceNameModified = strrep(SurfaceName, ending, '');
        fprintf(fileID,'"%s" colormap setDefaultColor ',SurfaceNameModified); %make all ONE color
        fprintf(fileID,'%s \n',allOneColor); %make all ONE color
        
    end
end

for i = 1:size(source,1)
    SurfaceName = (source(i,1).name); %load files
    SurfaceNameModified = strrep(SurfaceName, ending, '');
    fprintf(fileID,'"%s" drawStyle setNormalBinding 1\n',SurfaceNameModified);%set vertex normals
    % set transparency:
    if transparency_level == '0'
    else
        fprintf(fileID,'"%s" drawStyle setValue 4\n',SurfaceNameModified); %make it transparent
        fprintf(fileID,'"%s" drawStyle setAlphaMode 3\n',SurfaceNameModified);%make it transparent
        fprintf(fileID,'"%s" baseTrans setValue ',SurfaceNameModified);%set transparency value
        fprintf(fileID,'%s \n',transparency_level);%set transparency value
    end
end

for i = 1:size(source,1)
    SurfaceName = (source(i,1).name); %load files
    SurfaceNameModified = strrep(SurfaceName, ending, '');
    fprintf(fileID,'"%s" colorMode setIndex 0 5\n',SurfaceNameModified);%color mode constant
end

for i = 1:size(source,1)
    SurfaceName = (source(i,1).name); %load files
    SurfaceNameModified = strrep(SurfaceName, ending, '');
    fprintf(fileID,'"%s" fire\n',SurfaceNameModified); %fire
end

fclose(fileID);

disp('Amira commands have been written into make_nice_isosurface.txt')


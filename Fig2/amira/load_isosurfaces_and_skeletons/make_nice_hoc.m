
function make_nice_hoc(path, ending, rangedColor, tubeScaleFactor)

%start script
source = dir([(path) '\*' ending]);
[~,order] = sort_nat({source.name}); %sorting to solve 1.hoc, 10.hoc, ...
source = source(order);

%allColors =  hsv2rgb(getRangedColors(0.3,0.66,0.5,1,0.3,1,colornumber)')'; %use colors with given HSV range, 
%example for yellow/orange: 0.056,0.174,0.55,1,0.65,1,colornumber
%red: 0.77,1,0.2,1,0.9,1,colornumber
%blue/green: 0.3,0.66,0.5,1,0.3,1,colornumber
%prepare colors
colornumber = size(source,1);
if isempty(rangedColor)
    colors = distinguishable_colors(colornumber,[1 1 1; 0 0 0]); %for background [1 1 1; 0 0 0] oder [1 1 1] (wei�) oder [0 0 0] (schwarz)
    allColors = colors';
else
    if strcmp(rangedColor, 'gray') == 1 
        allColors = 0.3 + (0.99-0.3)*rand(1,colornumber); %or these two lines for gray
        allColors(3,:) = allColors(1,:);
        allColors(2,:) = allColors(1,:);
    else
        allColors = hsv2rgb(getRangedColors(rangedColor(1,1),rangedColor(1,2),rangedColor(1,3),rangedColor(1,4),rangedColor(1,5),rangedColor(1,6),colornumber)')';
    end
end


% allColors = hsv2rgb(getRangedColors(rangedColor(1,1),rangedColor(1,2),rangedColor(1,3),rangedColor(1,4),rangedColor(1,5),rangedColor(1,6),colornumber)')';


fileID = fopen('make_nice_hocs.txt','w');

for i = 1:size(source,1)
 pathFile = ([(path) '/' (source(i,1).name)]); 
 pathFileModified = strrep(pathFile, '\', '/');
 fprintf(fileID,'load %s\n',pathFileModified); %load files
end

for i = 1:size(source,1)
 SurfaceName = (source(i,1).name);
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'create HxSpatialGraphView "%s"\n',SurfaceNameModified); %create spatial graph view
end

for i = 1:size(source,1)
 SurfaceName = (source(i,1).name); 
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'"%s"',SurfaceNameModified); 
 fprintf(fileID,' data connect %s\n',SurfaceName); %connect to data
end

for i = 1:size(source,1)
 SurfaceName = (source(i,1).name); 
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'"%s" fire\n',SurfaceNameModified); %fire
end

for i = 1:size(source,1)
 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 Color1 = allColors(1,i);
 Color2 = allColors(2,i);
 Color3 = allColors(3,i);
 fprintf(fileID,'"%s"',SurfaceNameModified); %make the color
 fprintf(fileID,' segmentColor setColor 0 %1.4f',Color1); %make the color
 fprintf(fileID,' %1.4f',Color2); %make the color
 fprintf(fileID,' %1.4f\n',Color3); %make the color
end


% for i = 1:size(source,1)
%  SurfaceName = (source(i,1).name); %load files
%  SurfaceNameModified = strrep(SurfaceName, ending, '');
%  fprintf(fileID,'"%s" nodeOnOff setValue 0\n',SurfaceNameModified); %deactivate node spheres, old amira version: itemsToShow setValue 0 0
% end

% activate tubes
for i = 1:size(source,1)
 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'"%s" segmentStyle setValue 2 1\n',SurfaceNameModified);
end

%set tube thickness
for i = 1:size(source,1)
 SurfaceName = (source(i,1).name); 
 SurfaceNameModified = strrep(SurfaceName, ending, '');
%  fprintf(fileID,'"%s" tubeScaleFactor setValue 1000\n',SurfaceNameModified); %fire
 fprintf(fileID,'"%s" %s %1.0f\n', SurfaceNameModified, 'tubeScaleFactor setValue', tubeScaleFactor);
end

for i = 1:size(source,1)
 SurfaceName = (source(i,1).name); 
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'"%s" fire\n',SurfaceNameModified); %fire
end


fclose(fileID);

disp('Amira commands have been written into make_nice_hocs.txt')
end



clear

source = 'Z:\Projekte\2018\L4_show_all_movie\allProjectsOut\AmiraMesh'; %files to be inserted
ending = '.am';
addKeyframes = 'yes'; %yes or no
timeValue = 1000; %first insertion after t milliseconds


%start script
source = dir([(source) '\*' ending]);
[~,order] = sort_nat({source.name}); %sorting to solve 1.hoc, 10.hoc, ...
source = source(order);

fileID = fopen('amira_movie.txt','w');
t = timeValue;
for i=1:size(source,1)
    
    if i>0
        x = 600-i*20;
    elseif i>10
        x = 600-i*15;
    elseif i>20
        x = 600-i*10;
    elseif i>50
        x = 20;
    elseif i>100
        x = 10;
    elseif i>250
        x = 2;
    end
    
    if x<10
        x = 30;
    end
    
    t = t+x;
    format = '%s%s%s\n';
    fprintf(fileID, format,'<AnimationGroup name="', source(i).name, '">') ;
    format = '%s\n';
    fprintf(fileID, format,'<Animation name="Visibility in viewer 0">') ;
    fprintf(fileID, format,'<Keyframe timeLocked="false" value="false" time="0"/>') ;
    format = '%s%1.0f%s\n';
    fprintf(fileID, format,'<Keyframe timeLocked="false" value="true" time="', t, '"/>') ;
    format = '%s\n';
    fprintf(fileID, format,'</Animation>') ;
    fprintf(fileID, format,'</AnimationGroup>') ;
    %fprintf(fileID, format,'') ;
end



if strcmp(addKeyframes, 'yes') == 1 %add keyframes to visualize caption text
   
    format = '%s\n';
    fprintf(fileID, format,'<AnimationGroup name="Caption">');
    fprintf(fileID, format,'<Animation name="Text">');
    t = timeValue;
    for i=1:size(source,1)
        if i>0
            x = 600-i*20;
        elseif i>10
            x = 600-i*15;
        elseif i>20
            x = 600-i*10;
        elseif i>50
            x = 20;
        elseif i>100
            x = 10;
        elseif i>250
            x = 2;
        end
        if x<10
            x = 30;
        end
        t = t+x;
        
        theseHours = strsplit(source(i).name, '_'); %extract the hours of the filename
        format = '%s%s%s%1.0f%s\n';
        fprintf(fileID, format,'<Keyframe value="', theseHours{1,4}, ' hours" time="', t, '" timeLocked="false"/>');
   
    end
    format = '%s\n';
    fprintf(fileID, format,'</Animation>');
    fprintf(fileID, format,'</AnimationGroup>');
    
    
end

fclose (fileID);
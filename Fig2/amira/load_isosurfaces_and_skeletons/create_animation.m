clear
clc

AddThisTime=600;
counter=0;
countermem=0;
t=0;
intervall = 1800;


%animations header
disp ('_animations removeAll;_animations create {;<!DOCTYPE NewAnimation>');
disp ('<Settings createdFromDemoMaker="false" currentPlayMode="PLAY_MODE_ONCE" timeUpdateInterval="40" playSpeedFactor="1" currentTime="0" endTime="34000" startTime="0">');
disp (' <Options skipBreakEvents="false" skipPauseEvents="false" functionKeysActivated="true" skipGoToEvents="false" autoStartActivated="false" skipSnapshotEvents="false" snappingResolution="1000" snapshotsOutputDirectory="${AMIRA_DATADIR}"/>');
disp (' <AnimationGroup name="All">');


%show all wholeCells
for i=1:195;
%neues Intervall f�r jede Zelle   
if i==1
t=intervall; counter=counter+1;
end

if i==3
t=intervall; counter=counter+1;
end

if i==6
t=intervall; counter=counter+1;
end

if i==8
t=intervall; counter=counter+1;
end

if i==11
t=intervall; counter=counter+1;
end

if i==14
t=intervall; counter=counter+1;
end

if i==16
t=intervall; counter=counter+1;
end

if i==19
t=intervall; counter=counter+1;
end

if i==22
t=intervall; counter=counter+1;
end

if i==24
t=intervall; counter=counter+1;
end

if i==26
t=intervall; counter=counter+1;
end

if i==28
t=intervall; counter=counter+1;
end

if i==30
t=intervall; counter=counter+1;
end

if i==32
t=intervall; counter=counter+1;
end

if i==34
t=intervall; counter=counter+1;
end

if i==37
t=intervall; counter=counter+1;
end

if i==40
t=intervall; counter=counter+1;
end

if i==43
t=intervall; counter=counter+1;
end

if i==46
t=intervall; counter=counter+1;
end

if i==49
t=intervall; counter=counter+1;
end

if i==52
t=intervall; counter=counter+1;
end

if i==54
t=intervall; counter=counter+1;
end

if i==57
t=intervall; counter=counter+1;
end

if i==59
t=intervall; counter=counter+1;
end

if i==61
t=intervall; counter=counter+1;
end

if i==63
t=intervall; counter=counter+1;
end

if i==65
t=intervall; counter=counter+1;
end

if i==67
t=intervall; counter=counter+1;
end

if i==69
t=intervall; counter=counter+1;
end

if i==71
t=intervall; counter=counter+1;
end

if i==73
t=intervall; counter=counter+1;
end

if i==76
t=intervall; counter=counter+1;
end

if i==79
t=intervall; counter=counter+1;
end

if i==81
t=intervall; counter=counter+1;
end

if i==83
t=intervall; counter=counter+1;
end

if i==85
t=intervall; counter=counter+1;
end

if i==87
t=intervall; counter=counter+1;
end

if i==89
t=intervall; counter=counter+1;
end

if i==91
t=intervall; counter=counter+1;
end

if i==93
t=intervall; counter=counter+1;
end

if i==95
t=intervall; counter=counter+1;
end

if i==97
t=intervall; counter=counter+1;
end

if i==99
t=intervall; counter=counter+1;
end

if i==102
t=intervall; counter=counter+1;
end

if i==105
t=intervall; counter=counter+1;
end

if i==108
t=intervall; counter=counter+1;
end

if i==111
t=intervall; counter=counter+1;
end

if i==114
t=intervall; counter=counter+1;
end

if i==117
t=intervall; counter=counter+1;
end

if i==120
t=intervall; counter=counter+1;
end

if i==123
t=intervall; counter=counter+1;
end

if i==125
t=intervall; counter=counter+1;
end

if i==128
t=intervall; counter=counter+1;
end

if i==131
t=intervall; counter=counter+1;
end

if i==134
t=intervall; counter=counter+1;
end

if i==137
t=intervall; counter=counter+1;
end

if i==139
t=intervall; counter=counter+1;
end

if i==142
t=intervall; counter=counter+1;
end

if i==144
t=intervall; counter=counter+1;
end

if i==146
t=intervall; counter=counter+1;
end

if i==149
t=intervall; counter=counter+1;
end

if i==151
t=intervall; counter=counter+1;
end

if i==153
t=intervall; counter=counter+1;
end

if i==155
t=intervall; counter=counter+1;
end

if i==157
t=intervall; counter=counter+1;
end

if i==159
t=intervall; counter=counter+1;
end

if i==161
t=intervall; counter=counter+1;
end

if i==163
t=intervall; counter=counter+1;
end

if i==165
t=intervall; counter=counter+1;
end

if i==167
t=intervall; counter=counter+1;
end

if i==169
t=intervall; counter=counter+1;
end

if i==170
t=intervall; counter=counter+1;
end

if i==171
t=intervall; counter=counter+1;
end

if i==172
t=intervall; counter=counter+1;
end

if i==173
t=intervall; counter=counter+1;
end

if i==174
t=intervall; counter=counter+1;
end

if i==175
t=intervall; counter=counter+1;
end

if i==176
t=intervall; counter=counter+1;
end

if i==177
t=intervall; counter=counter+1;
end

if i==178
t=intervall; counter=counter+1;
end

if i==179
t=intervall; counter=counter+1;
end

if i==180
t=intervall; counter=counter+1;
end

if i==181
t=intervall; counter=counter+1;
end

if i==182
t=intervall; counter=counter+1;
end

if i==183
t=intervall; counter=counter+1;
end

if i==185
t=intervall; counter=counter+1;
end

if i==188
t=intervall; counter=counter+1;
end

if i==190
t=intervall; counter=counter+1;
end

if i==191
t=intervall; counter=counter+1;
end

if i==194
t=intervall; counter=counter+1;
end

%time intervall langsam verringern ab Zelle 4 (i==8)
if i>=6; %intervall Reduktion ab Zelle 4 beginnen
    if counter~=countermem;
        intervall=intervall+AddThisTime;
        AddThisTime=AddThisTime-50;
        countermem=counter;
    end
end

if AddThisTime<=50;
    AddThisTime=50;
end


%Eintraege fuer die .hx ausgeben:

disp([' <AnimationGroup name="wholeCellsView' num2str(i) '">;' ' <Animation name="Visibility in viewer 0">;' ' <Keyframe timeLocked="false" value="false" time="0"/>;' ' <Keyframe timeLocked="false" value="true" time="' num2str(t) '"/>;' ' </Animation>;' ' </AnimationGroup>']);

% old:
% disp(['  <AnimationGroup name="wholeCellsView' num2str(i) '">']);
% disp('   <Animation name="Visibility in viewer 0">');
% disp('    <Keyframe timeLocked="false" value="false" time="0"/>');
% disp(['    <Keyframe timeLocked="false" value="true" time="' num2str(t) '"/>']);
% disp('   </Animation>');
% disp('  </AnimationGroup>');
end



%show apicals
for i=1:63
     
t=t+AddThisTime;
AddThisTime=AddThisTime-5; %jede Zelle wird um AddThisTime(ms) schneller gezeigt

if AddThisTime<=40; %aber zwischen den Zellen liegen mindestens 40 ms
    AddThisTime=40;
end


disp([' <AnimationGroup name="apicalsView' num2str(i) '">;' ' <Animation name="Visibility in viewer 0">;' ' <Keyframe timeLocked="false" value="false" time="0"/>;' ' <Keyframe timeLocked="false" value="true" time="' num2str(t) '"/>;' ' </Animation>;' ' </AnimationGroup>']);

end





%show smooth dendrites

for i=1:111
    
t=t+AddThisTime;
AddThisTime=AddThisTime-5;

if AddThisTime<=40;
    AddThisTime=40;
end
    
disp([' <AnimationGroup name="smoothDendritesView' num2str(i) '">;' ' <Animation name="Visibility in viewer 0">;' ' <Keyframe timeLocked="false" value="false" time="0"/>;' ' <Keyframe timeLocked="false" value="true" time="' num2str(t) '"/>;' ' </Animation>;' ' </AnimationGroup>']);

end


%other dendrites:
for i=1:600
    
t=t+AddThisTime;
AddThisTime=AddThisTime-2;

if AddThisTime<=15;
    AddThisTime=15;
end
    
disp([' <AnimationGroup name="otherDendritesView' num2str(i) '">;' ' <Animation name="Visibility in viewer 0">;' ' <Keyframe timeLocked="false" value="false" time="0"/>;' ' <Keyframe timeLocked="false" value="true" time="' num2str(t) '"/>;' ' </Animation>;' ' </AnimationGroup>']);
end

%tell Amira to end animation entries here
disp ('</AnimationGroup>;</Settings>;}');


%Beispiel aus der .hx
%   <AnimationGroup name="Surface View 6">
%    <Animation name="Visibility in viewer 0">
%     <Keyframe timeLocked="false" value="false" time="0"/>
%     <Keyframe timeLocked="false" value="true" time="3500"/>
%    </Animation>
%   </AnimationGroup>
  

  
  
  
  
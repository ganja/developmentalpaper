% simplify for multiple issf:
% layer_4_cell_$i.issf: name of issf
% numTris*0.01: 1% of original faces
% maxSurf 10: layer_4_cell_1 - layer_4_cell_10

thisdir = dir('Z:\Projekte\2018\L4_bloodVessels\bloodVessel_somata_v1-files\*.am'); %folder to all isosurfaces to be simplified
fileID = fopen('Z:\Projekte\2018\L4_bloodVessels\simplify.txt','w'); %target_textfile with Amira command
simplification = '0.1'; %simplification level, 0.1 would be 10% of all isosurfaces

format = '%s%s%s%s%s%s%s \n' ;%Format has to be changed if anything in line fprintf changes
ersterTeil = 'set maxSurf 1; for {set i 1 } {$i <= $maxSurf } { incr i} {set numTris ["';
zweiterTeil = '" getNumTriangles]; set targetTris [expr $numTris*';
dritterTeil = ']; set mySimplifier [create HxSimplifier]; $mySimplifier attach ';
vierterTeil = '; $mySimplifier simplifyParameters setValue 0 $targetTris; $mySimplifier simplifyAction setValue 0 1; $mySimplifier simplifyAction send; $mySimplifier detach}';
for i = 1:size(thisdir,1)
    thisply = thisdir(i,1).name;
fprintf(fileID, format, ersterTeil, thisply, zweiterTeil, simplification, dritterTeil, thisply, vierterTeil)
    
end

fclose(fileID)
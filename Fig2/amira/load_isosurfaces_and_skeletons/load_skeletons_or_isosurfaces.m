% Load skeleton and volume data into Amira
% 2015-2018, MPIBR, heiko.wissler@brain.mpg.de


clear
clc
% 
path = 'Z:\Data\goura\FigureUpdates\Amira\SomaAxon_P7\Axon_08-10-2019\output\SomaSynapses\';
% path = 'Z:\Data\goura\FigureUpdates\Auxiliary sketches for paper\Amira_hoc-files\Amira_soma-files\ForMultipleHits\Amira_soma-files\';
% path = 'Z:\Data\goura\Chandelier_Amira\P28\Output\Amira_hoc-files\';
ThisType = 'volume'; %for .am files: skeleton or volume
% ThisType = 'skeleton'; %for .am files: skeleton or volume
% transparency_level = 0.05; %for isosurfaces: 0-1, 1 = fully transparent, 0 = no transparency
transparency_level = 0; %for isosurfaces: 0-1, 1 = fully transparent, 0 = no transparency
UseRanks = 'no'; %for AmiraMesh files: yes or no. use the colors that have been written into the 'ranks' within the .am file
tubeScaleFactor = 1.5; %for tubes: set thickness
allOneColor = ''; %set the color in RGB, example: 1 0.4 0.2; '' for multicolor
% rangedColor = [ 0.68,0.68,0.95,0.95,1,1];%for ultramrine blue
% rangedColor = [ 0.16,0.16,0.85,0.85,0.8,0.8];%for yellow
% % rangedColor = [0,0,0,0,1,1,];%for white
rangedColor = [ 0.88,0.88,0.95,0.95,1,1];%for magenta pink
% rangedColor = [0.083,0.083,1,1,1,1]; % for orange
% rangedColor = [0.889,0.889,0.946,0.946,0.793,0.793];
% rangedColor = [0,0,1,1,1,1]; %for red
% rangedColor = [0.65,0.65,0,0,0.556,0.556]; %for light grey
% rangedColor = [0,0,0,0,0,0]; %for black
%rangedColor = [0.08,0.12,0.7,1,0.7,1] ; %HSV range, set example below include [] or set this for multicolor: '' or 'gray' for gray-ranged
%yellow/orange: [0.056,0.174,0.55,1,0.65,1] [H von , H bis, S von , S bis, V cvon, V bis]
%red: [0.77,1,0.7,1,0.7,1]
% orange: [0.08,0.12,0.7,1,0.7,1]
%magenta/red: [0.85,1,0.2,1,0.9,1]
% rangedColor = [0.5,0.5,0.8,0.8,0.9,0.9]; %for cyan
% blue cyan green: [0.3,0.66,0.5,1,0.3,1]
%blue: [0.56,0.65,0.8,1,0.3,0.6]
%green: [0.25,0.4,0.8,1,0.3,0.6]

% blue|: 0.65,0.66,0.60,0.70,0.95,1
% yellow 0.1,0.1,0.80,0.80,0.95,1

%define most likely content in path folder (ply, issf, am or hoc)
thisPath = dir([path '\*.*']);
thisMatrix(1,1) = 0; %ply
thisMatrix(1,2) = 0; %issf
thisMatrix(1,3) = 0; %am
thisMatrix(1,4) = 0; %hoc
for i = 1:size(thisPath,1)
    if (contains(thisPath(i,1).name,'.ply')) == 1
        thisMatrix(1,1) = thisMatrix(1,1) + 1;
    end
    if (contains(thisPath(i,1).name,'.issf')) == 1
        thisMatrix(1,2) = thisMatrix(1,2) + 1;
    end
    if (contains(thisPath(i,1).name,'.am')) == 1
        thisMatrix(1,3) = thisMatrix(1,3) + 1;
    end
    if (contains(thisPath(i,1).name,'.hoc')) == 1
        thisMatrix(1,4) = thisMatrix(1,4) + 1;
    end
end
[maxValue, linearIndexesOfMaxes] = max(thisMatrix(:));

%write textfiles with the Amira commands
if linearIndexesOfMaxes == 1 
    disp([num2str(maxValue) ' ply files will be processed'])
    ending = '.ply';
    make_nice_issf(path, ending, transparency_level, allOneColor, rangedColor)
end
if linearIndexesOfMaxes == 2  
    disp([num2str(maxValue) ' issf files will be processed'])
    ending = '.issf';
    make_nice_issf(path, ending, transparency_level, allOneColor, rangedColor)
end
if linearIndexesOfMaxes == 3 && strcmp(ThisType, 'volume') == 1
    disp([num2str(maxValue) ' am volume files will be processed'])
    ending = '.am';
    make_nice_issf(path, ending, transparency_level, allOneColor, rangedColor)
end
if linearIndexesOfMaxes == 3 && strcmp(ThisType, 'skeleton') == 1
    disp([num2str(maxValue) ' AmiraMesh skeleton files will be processed'])
    ending = '.am';
    make_nice_AmiraMesh(path, ending, allOneColor, rangedColor, UseRanks, tubeScaleFactor)
end
if linearIndexesOfMaxes == 4
    disp([num2str(maxValue) ' hoc files will be processed'])
    ending = '.hoc';
    make_nice_hoc(path, ending, rangedColor,tubeScaleFactor)
end


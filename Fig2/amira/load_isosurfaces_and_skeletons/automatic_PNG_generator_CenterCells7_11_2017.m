%this will make Amira snapshots of every cell within subfolders with a
%depth of 1 in the given folder

clear 

path = ('Z:\Projekte\2017\L4_CenterwholeCells_2017\new_wholeCells_7_11_2017\CenterCells');
ending = '.ply'; %.issf, .ply or .am


%start scripot
source = dir([(path) '\*' ending]);
fileID = fopen('make_nice_isosurface.txt','w');

for i = 1:size(source,1); 
    

% fprintf(fileID,'remove -all\n');

% colornumber = size(source,1);
% colors = distinguishable_colors(colornumber,[1 1 1; 0 0 0]); %[1 1 1; 0 0 0] oder [1 1 1] (wei�) oder [0 0 0] (schwarz)
% allColors = [colors]';
load Z:/Projekte/2017/L4_CenterwholeCells_2017/allColors


 pathFile = ([(path) '/' (source(i,1).name)]); %load files
 pathFileModified = strrep(pathFile, '\', '/');
 fprintf(fileID,'load %s\n',pathFileModified); %load files



 SurfaceName = (source(i,1).name); %load files
  fprintf(fileID,'%s makeOnePatch\n',SurfaceName); %make one patch



 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'create HxDisplaySurface "%s"\n',SurfaceNameModified); %create surface view



 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'"%s"',SurfaceNameModified); %connect to data
 fprintf(fileID,' data connect %s\n',SurfaceName); %connect to data



 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 Color1 = allColors(1,i);
 Color2 = allColors(2,i);
 Color3 = allColors(3,i);
 fprintf(fileID,'"%s"',SurfaceNameModified); %make the color
 fprintf(fileID,' colormap setDefaultColor %1.4f',Color1); %make the color
 fprintf(fileID,' %1.4f',Color2); %make the color
 fprintf(fileID,' %1.4f\n',Color3); %make the color



%  SurfaceName = (source(i,1).name); %load files
%  SurfaceNameModified = strrep(SurfaceName, ending, '');
%  fprintf(fileID,'"%s" colormap setDefaultColor 1 0 0\n',SurfaceNameModified); %make all ONE color



 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'"%s" drawStyle setNormalBinding 1\n',SurfaceNameModified);%set vertex normals

%set transparency:
%  fprintf(fileID,'"%s" drawStyle setValue 4\n',SurfaceNameModified); %make it transparent
%  fprintf(fileID,'"%s" drawStyle setAlphaMode 3\n',SurfaceNameModified);%make it transparent
%  fprintf(fileID,'"%s" baseTrans setValue 0.7\n',SurfaceNameModified);%set transparency value



 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'"%s" colorMode setIndex 0 5\n',SurfaceNameModified);%color mode constant



 SurfaceName = (source(i,1).name); %load files
 SurfaceNameModified = strrep(SurfaceName, ending, '');
 fprintf(fileID,'"%s" fire\n',SurfaceNameModified); %fire

fprintf(fileID,'"Bounding Box" setViewerMask 16382\n');


fprintf(fileID,'load Z:/Projekte/2017/L4_CenterwholeCells_2017/Landmarks-2-sets.landmarkAscii\n');
fprintf(fileID,'create HxArbitraryCut "Clipping Plane"\n');
fprintf(fileID,'create HxArbitraryCut "Clipping Plane2"\n');
fprintf(fileID,'create HxArbitraryCut "Clipping Plane3"\n');
fprintf(fileID,'create HxArbitraryCut "Clipping Plane4"\n');
fprintf(fileID,'create HxArbitraryCut "Clipping Plane5"\n');
fprintf(fileID,'create HxArbitraryCut "Clipping Plane6"\n');

fprintf(fileID,'"Clipping Plane" data connect "Landmarks-2-sets.landmarkAscii"\n');
fprintf(fileID,'"Clipping Plane2" data connect "Landmarks-2-sets.landmarkAscii"\n');
fprintf(fileID,'"Clipping Plane3" data connect "Landmarks-2-sets.landmarkAscii"\n');
fprintf(fileID,'"Clipping Plane4" data connect "Landmarks-2-sets.landmarkAscii"\n');
fprintf(fileID,'"Clipping Plane5" data connect "Landmarks-2-sets.landmarkAscii"\n');
fprintf(fileID,'"Clipping Plane6" data connect "Landmarks-2-sets.landmarkAscii"\n');

fprintf(fileID,'ClippingPlane setPlane 6451.76 0 0 0 0 -1 -0 1 0\n');
fprintf(fileID,'ClippingPlane2 setPlane 57649.96 0 0 0 0 -1 -0 1 0\n');
fprintf(fileID,'ClippingPlane3 setPlane 0 0 8624 0 -1 0 1 0 -0\n');
fprintf(fileID,'ClippingPlane4 setPlane 0 0 90639.36 0 -1 0 1 0 -0\n');
fprintf(fileID,'ClippingPlane5 setPlane 0 6451.76 0 0 0 1 1 0 0\n');
fprintf(fileID,'ClippingPlane6 setPlane 0 90580 0 0 -0 1 1 0 0\n');


if i == 1
    a = ('SkeletonGraphView_2');
    b = ('SkeletonGraphView_3');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);

end
if i == 2
    a = ('SkeletonGraphView_40');
    b = ('SkeletonGraphView_41');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);    
end
if i == 3
    a = ('SkeletonGraphView_42');
    b = ('SkeletonGraphView_43');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 4
    a = ('SkeletonGraphView_44');
    b = ('SkeletonGraphView_45');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 5
    a = ('SkeletonGraphView_25');
    b = ('SkeletonGraphView_26');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 6
    a = ('SkeletonGraphView_27');
    b = ('SkeletonGraphView_28');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 7
    a = ('SkeletonGraphView_29');
    b = ('SkeletonGraphView_30');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 8
    a = ('SkeletonGraphView_46');
    b = ('SkeletonGraphView_47');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 9
    a = ('SkeletonGraphView_13');
    b = ('SkeletonGraphView_14');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 10
    a = ('SkeletonGraphView_15');
    b = ('SkeletonGraphView_16');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 11
    a = ('SkeletonGraphView_17');
    b = ('SkeletonGraphView_18');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 12
    a = ('SkeletonGraphView_34');
    b = ('SkeletonGraphView_35');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 13
    a = ('SkeletonGraphView_19');
    b = ('SkeletonGraphView_20');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 14
    a = ('SkeletonGraphView_21');
    b = ('SkeletonGraphView_22');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 15
    a = ('SkeletonGraphView_23');
    b = ('SkeletonGraphView_24');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 16
    a = ('SkeletonGraphView_48');
    b = ('SkeletonGraphView_49');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 17
    a = ('SkeletonGraphView_31');
    b = ('SkeletonGraphView_32');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 18
    a = ('SkeletonGraphView_50');
    b = ('SkeletonGraphView_51');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 19
    a = ('SkeletonGraphView_52');
    b = ('SkeletonGraphView_53');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 20
    a = ('SkeletonGraphView_1');
    b = ('SkeletonGraphView_33');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 21
    a = ('SkeletonGraphView_4');
        c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    
fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 22
    a = ('SkeletonGraphView_5');
    b = ('SkeletonGraphView_6');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);;
end
if i == 23
    a = ('SkeletonGraphView_7');
    b = ('SkeletonGraphView_8');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 24
    a = ('SkeletonGraphView_9');
    b = ('SkeletonGraphView_10');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 25
    a = ('SkeletonGraphView_11');
    b = ('SkeletonGraphView_12');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 26
    a = ('SkeletonGraphView_36');
    b = ('SkeletonGraphView_37');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end
if i == 27
    a = ('SkeletonGraphView_38');
    b = ('SkeletonGraphView_39');
    c = SurfaceNameModified;
    fprintf(fileID,'"%s" setViewerMask 16383\n', a);
    fprintf(fileID,'"%s" setViewerMask 16383\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', a);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', a);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', a);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', a);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', a);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', b);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', b);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', b);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', b);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', b);

fprintf(fileID,'"%s" clipGeom "Clipping Plane2" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane4" "16383"\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane5" "16383"\n', c);
fprintf(fileID,'"Clipping Plane5" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane" "16383"\n', c);
fprintf(fileID,'"Clipping Plane" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane3" "16383"\n', c);
fprintf(fileID,'"Clipping Plane3" invertClippingPlane\n', c);
fprintf(fileID,'"%s" clipGeom "Clipping Plane6" "16383"\n', c);
end


    
%load and set bbox:
% fprintf(fileID,'set hideNewModules 0\n');
% fprintf(fileID,'[ load Z:/Projekte/2017/L4_CenterwholeCells_2017/bbox ] setLabel "bbox"\n');
% fprintf(fileID,'"bbox" setIconPosition 20 298\n');
% fprintf(fileID,'"bbox" fire\n');
% fprintf(fileID,'"bbox" fire\n');
% fprintf(fileID,'"bbox" setViewerMask 16383\n');
% 
% fprintf(fileID,'set hideNewModules 1\n');
% fprintf(fileID,'create HxDataDirectory "DataDirectory_bbox"\n');
% fprintf(fileID,'"DataDirectory_bbox" setIconPosition 0 0\n');
% fprintf(fileID,'"DataDirectory_bbox" setInvisible 1\n');
% fprintf(fileID,'"DataDirectory_bbox" parentDir connect "Data"\n');
% fprintf(fileID,'"DataDirectory_bbox" attachedData connect "bbox"\n');
% fprintf(fileID,'"DataDirectory_bbox" fire\n');
% fprintf(fileID,'"DataDirectory_bbox" setViewerMask 16383\n');
% 
% fprintf(fileID,'set hideNewModules 0\n');
% fprintf(fileID,'create HxBoundingBox "Bounding Box"\n');
% fprintf(fileID,'"Bounding Box" setIconPosition -40 334\n');
% fprintf(fileID,'"Bounding Box" setVar "CustomHelp" {HxBoundingBox}\n');
% fprintf(fileID,'"Bounding Box" data connect "bbox"\n');
% fprintf(fileID,'"Bounding Box" fire\n');
% fprintf(fileID,'"Bounding Box" options setState {item 0 0 color 1 1 0.5 0 item 3 0 }\n');
% fprintf(fileID,'"Bounding Box" lineWidth setMinMax 0 1 10\n');
% fprintf(fileID,'"Bounding Box" lineWidth setValue 0 1\n');
% fprintf(fileID,'"Bounding Box" font setState {name: "Arial Unicode MS" size: 12 buttonText: Select bold: 0 italic: 0 color: 0.8 0.8 0.8}\n');
% fprintf(fileID,'"Bounding Box" fire\n');
% fprintf(fileID,'"Bounding Box" setViewerMask 16383\n');
% fprintf(fileID,'"Bounding Box" select\n');
% fprintf(fileID,'"Bounding Box" setShadowStyle 0\n');
% fprintf(fileID,'"Bounding Box" setPickable 1\n');



fprintf(fileID,'"Bounding Box" setViewerMask 16383\n');
fprintf(fileID,'viewer 0 setCameraOrientation 0.643926 0.429079 0.633444 1.52465\n');
fprintf(fileID,'viewer 0 setCameraPosition 172339 -22280.3 116951\n');
fprintf(fileID,'viewer 0 setCameraFocalDistance 171530\n');
fprintf(fileID,'viewer 0 setCameraNearDistance 107855\n');
fprintf(fileID,'viewer 0 setCameraFarDistance 233856\n');
fprintf(fileID,'viewer 0 snapshot -tiled 2 2 Z:/Projekte/2017/L4_CenterwholeCells_2017/images/%s.png\n',(source(i,1).name));



if i == 1
    fprintf(fileID,'"SkeletonGraphView_2" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_3" setViewerMask 16382\n');
end
if i == 2
    fprintf(fileID,'"SkeletonGraphView_40" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_41" setViewerMask 16382\n');
end
if i == 3
    fprintf(fileID,'"SkeletonGraphView_42" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_43" setViewerMask 16382\n');
end
if i == 4
    fprintf(fileID,'"SkeletonGraphView_44" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_45" setViewerMask 16382\n');
end
if i == 5
    fprintf(fileID,'"SkeletonGraphView_25" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_26" setViewerMask 16382\n');
end
if i == 6
    fprintf(fileID,'"SkeletonGraphView_27" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_28" setViewerMask 16382\n');
end
if i == 7
    fprintf(fileID,'"SkeletonGraphView_29" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_30" setViewerMask 16382\n');
end
if i == 8
    fprintf(fileID,'"SkeletonGraphView_46" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_47" setViewerMask 16382\n');
end
if i == 9
    fprintf(fileID,'"SkeletonGraphView_13" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_14" setViewerMask 16382\n');
end
if i == 10
    fprintf(fileID,'"SkeletonGraphView_15" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_16" setViewerMask 16382\n');
end
if i == 11
    fprintf(fileID,'"SkeletonGraphView_17" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_18" setViewerMask 16382\n');
end
if i == 12
    fprintf(fileID,'"SkeletonGraphView_34" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_35" setViewerMask 16382\n');
end
if i == 13
    fprintf(fileID,'"SkeletonGraphView_19" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_20" setViewerMask 16382\n');
end
if i == 14
    fprintf(fileID,'"SkeletonGraphView_21" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_22" setViewerMask 16382\n');
end
if i == 15
    fprintf(fileID,'"SkeletonGraphView_23" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_24" setViewerMask 16382\n');
end
if i == 16
    fprintf(fileID,'"SkeletonGraphView_48" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_49" setViewerMask 16382\n');
end
if i == 17
    fprintf(fileID,'"SkeletonGraphView_31" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_32" setViewerMask 16382\n');
end
if i == 18
    fprintf(fileID,'"SkeletonGraphView_50" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_51" setViewerMask 16382\n');
end
if i == 19
    fprintf(fileID,'"SkeletonGraphView_52" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_53" setViewerMask 16382\n');
end
if i == 20
    fprintf(fileID,'"SkeletonGraphView_1" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_33" setViewerMask 16382\n');
end
if i == 21
    fprintf(fileID,'"SkeletonGraphView_4" setViewerMask 16382\n');
end
if i == 22
    fprintf(fileID,'"SkeletonGraphView_5" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_6" setViewerMask 16382\n');
end
if i == 23
    fprintf(fileID,'"SkeletonGraphView_7" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_8" setViewerMask 16382\n');
end
if i == 24
    fprintf(fileID,'"SkeletonGraphView_9" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_10" setViewerMask 16382\n');
end
if i == 25
    fprintf(fileID,'"SkeletonGraphView_11" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_12" setViewerMask 16382\n');
end
if i == 26
    fprintf(fileID,'"SkeletonGraphView_36" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_37" setViewerMask 16382\n');
end
if i == 27
    fprintf(fileID,'"SkeletonGraphView_38" setViewerMask 16382\n');
    fprintf(fileID,'"SkeletonGraphView_39" setViewerMask 16382\n');
end

fprintf(fileID,'remove "%s"\n',SurfaceName);
fprintf(fileID,'remove "Landmarks-2-sets.landmarkAscii"\n'); 
fprintf(fileID,'\n\n');

end

fclose(fileID);
disp('Amira commands have been written into make_nice_isosurface.txt')


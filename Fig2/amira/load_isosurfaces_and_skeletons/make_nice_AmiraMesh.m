function make_nice_AmiraMesh(path, ending, allOneColor, rangedColor, UseRanks, tubeScaleFactor)
% Create Amira command to load Amira Mesh files into Amira
% 2018, MPIBR, heiko.wissler@brain.mpg.de


%start script
source = dir([(path) '\*' ending]);
[~,order] = sort_nat({source.name}); %sorting to solve 1.hoc, 10.hoc, ...
source = source(order);

%prepare colors
colornumber = size(source,1);
if isempty(rangedColor)
    colors = distinguishable_colors(colornumber,[1 1 1; 0 0 0]); %for background [1 1 1; 0 0 0] oder [1 1 1] (wei�) oder [0 0 0] (schwarz)
    allColors = colors';
else
    if strcmp(rangedColor, 'gray') == 1 
        allColors = 0.3 + (0.99-0.3)*rand(1,colornumber); %or these two lines for gray
        allColors(3,:) = allColors(1,:);
        allColors(2,:) = allColors(1,:);
    else
        allColors = hsv2rgb(getRangedColors(rangedColor(1,1),rangedColor(1,2),rangedColor(1,3),rangedColor(1,4),rangedColor(1,5),rangedColor(1,6),colornumber)')';
    end
end


fileID = fopen('load_Amira_files.txt','w');

%load files
for i = 1:size(source,1)
    pathFile = ([(path) '/' (source(i,1).name)]);
    pathFileModified = strrep(pathFile, '\', '/');
    fprintf(fileID,'load %s\n',pathFileModified);
end

%create spatial graph view
for i = 1:size(source,1)
    ThisFilename = (source(i,1).name);
    ThisFilenameModified = strrep(ThisFilename, ending, '');
    fprintf(fileID,'create HxSpatialGraphView "%s"\n',ThisFilenameModified);
end

%connect spatial graph view to data
for i = 1:size(source,1)
    ThisFilename = (source(i,1).name);
    ThisFilenameModified = strrep(ThisFilename, ending, '');
    fprintf(fileID,'"%s"',ThisFilenameModified);
    fprintf(fileID,' data connect %s\n',ThisFilename);
end

%fire
for i = 1:size(source,1)
    ThisFilename = (source(i,1).name);
    ThisFilenameModified = strrep(ThisFilename, ending, '');
    fprintf(fileID,'"%s" fire\n',ThisFilenameModified);
end

%deactivate node spheres, old amira version: itemsToShow setValue 0 0
for i = 1:size(source,1)
    ThisFilename = (source(i,1).name); %load files
    ThisFilenameModified = strrep(ThisFilename, ending, '');
    fprintf(fileID,'"%s" nodeOnOff setValue 0\n',ThisFilenameModified);
end

%deactivate lines
for i = 1:size(source,1)
    ThisFilename = (source(i,1).name); %load files
    ThisFilenameModified = strrep(ThisFilename, ending, '');
    fprintf(fileID,'"%s" segmentStyle setValue 0 0\n',ThisFilenameModified);
end

%activate tubes
for i = 1:size(source,1)
    ThisFilename = (source(i,1).name); %load files
    ThisFilenameModified = strrep(ThisFilename, ending, '');
    fprintf(fileID,'"%s" segmentStyle setValue 2 1\n',ThisFilenameModified);
end

%set tube thickness
for i = 1:size(source,1)
    ThisFilename = (source(i,1).name);
    ThisFilenameModified = strrep(ThisFilename, ending, '');
    fprintf(fileID,'"%s" %s %1.0f\n', ThisFilenameModified, 'tubeScaleFactor setValue', tubeScaleFactor);
end


if strcmp(UseRanks, 'yes') == 1 %if colors via ranks
    
    %set colors to ranks
    for i = 1:size(source,1)
        ThisFilename = (source(i,1).name); %load files
        ThisFilenameModified = strrep(ThisFilename, ending, '');
        fprintf(fileID,'"%s" segmentColoring setIndex 0 1\n',ThisFilenameModified);
    end
   
elseif strcmp(UseRanks, 'no') == 1 %if colors via constant
    
    %set colors to constant
    for i = 1:size(source,1)
        ThisFilename = (source(i,1).name); %load files
        ThisFilenameModified = strrep(ThisFilename, ending, '');
        fprintf(fileID,'"%s" segmentColoring setIndex 0 0\n',ThisFilenameModified);
    end
    
    
    %make the color
    if isempty(allOneColor)
        for i = 1:size(source,1)
            ThisFilename = (source(i,1).name); %load files
            ThisFilenameModified = strrep(ThisFilename, ending, '');
            Color1 = allColors(1,i);
            Color2 = allColors(2,i);
            Color3 = allColors(3,i);
            fprintf(fileID,'"%s"',ThisFilenameModified); %make the color
            fprintf(fileID,' segmentColor setColor 0 %1.4f',Color1); %make the color
            fprintf(fileID,' %1.4f',Color2); %make the color
            fprintf(fileID,' %1.4f\n',Color3); %make the color
        end
    else
        for i = 1:size(source,1)
            ThisFilename = (source(i,1).name); %load files
            ThisFilenameModified = strrep(ThisFilename, ending, '');
            fprintf(fileID,'"%s" segmentColor setColor 0 ',ThisFilenameModified); %make all ONE color
            fprintf(fileID,'%s \n',allOneColor); %make all ONE color
            
        end
    end
end

%fire
for i = 1:size(source,1)
    ThisFilename = (source(i,1).name);
    ThisFilenameModified = strrep(ThisFilename, ending, '');
    fprintf(fileID,'"%s" fire\n',ThisFilenameModified); %fire
end

fclose(fileID);

disp('Amira commands have been written into load_Amira_files.txt')
end



function [ allcolors ] = getRangedColors(Rfrom,Rto,Gfrom,Gto,Bfrom,Bto,n)
%GETRANGEDCOLORS give n to get RGB-list with n colors with the given range
% RGB from and to must be numbers from 0 to 1
allcolors(1,:) = Rfrom + (Rto-Rfrom)*rand(1,n);  %// Red from first number to second number
allcolors(2,:) = Gfrom + (Gto-Gfrom)*rand(1,n);  %// Blue from first number to second number
allcolors(3,:) = Bfrom + (Bto-Bfrom)*rand(1,n);   %// Green from first number to second number
% colormap( [R(:), G(:), B(:)] );  %test range
% figure(1)
% surf(peaks)
% colorbar

end


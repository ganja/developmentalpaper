%% script to create soma spheres and skeleton tracings (hoc files) to visualize them in Amira.
% input is a folder with many NMLs (each soma has to be commented "soma"
% within the tracing. Output will be .am and .hoc files and two .txt files.
% You only have to execute the content of these two .txt files in the Amira
% console to load and visualize them.
% 2016, MPIBR, heiko.wissler@brain.mpg.de

% set all variables
clear
pathToNMLs='NMLs';
output_folder='Z:\PProject\PP_tracing\Amira\SomaAndHocAll-In-One\out';
if ~exist(output_folder, 'dir')
    mkdir(output_folder);
end
x_resolution=12;
y_resolution=12;
z_resolution=30;
diameter=5000; %diameter of every sphere in nm (for somata use 5000)
%filename='soma-test.am' ;%filename of amira file you create
faces=40; %number of faces for each sphere (10 ist fast, 50 is HighRes but slow if many thousand)
tubeScaleFactor = 500;

%prepare folders:
somafolder = ([output_folder filesep 'Amira_soma-files']);
hocfolder = ([output_folder filesep 'Amira_hoc-files']);
delete ([somafolder filesep '*.am']);
delete ([hocfolder filesep '*.hoc']);

%% read out all nodes (all nodes in all NMLs are then Somata):
% tracings={};
% source=dir([pathToNMLs '\*.nml']);
% for i=1:size(source,1)
%     tracings{1,i}=parseNml([pathToNMLs '\' source(i,1).name]);
% end
% clear newtracings1
% y=1;
% for z=1:length(tracings) %z= number of NML files read by parseNml
% for x=1:length(tracings{1, z}) % x = number of trees
%     for i=1:size(tracings{1, 1}{1, x}.nodes,1) % i= number of nodes
% 
%         newtracings1{y,1}=tracings{1, 1}{1, x}.nodes(i,1);
%         newtracings1{y,2}=tracings{1, 1}{1, x}.nodes(i,2);
%         newtracings1{y,3}=tracings{1, 1}{1, x}.nodes(i,3);
%         y=y+1;
%     end
% end
% end
% 
% disp('soma coordinates (all nodes of all trees of all NMLs) have been written to "somata"')
% somata=(newtracings1); %coordinates of somata in newtracings1

%% read out all nodes (NOT all nodes in all NMLs are then Somata but only these that have the comment 'soma'):%

tracings={};
source=dir([pathToNMLs filesep '*.nml']);
for i=1:size(source,1)
    tracings{1,i}=parseNml([pathToNMLs filesep source(i,1).name]);
end
clear newtracings1
y=1;
for z=1:length(tracings) %z= number of NML files read by parseNml
for x=1:length(tracings{1, z}) % x = number of trees
    for i=1:size(tracings{1, z}{1, x}.nodesAsStruct,2) % i= number of nodes
        
    %    if tracings{1, z}{1, x}.nodesAsStruct{1, i}.comment == 'soma'
        if isequal(tracings{1, z}{1, x}.nodesAsStruct{1, i}.comment,'soma')
        newtracings1{y,1}=tracings{1, z}{1, x}.nodes(i,1);
        newtracings1{y,2}=tracings{1, z}{1, x}.nodes(i,2);
        newtracings1{y,3}=tracings{1, z}{1, x}.nodes(i,3);
        newtracings1{y,4}=source(z,1).name;
        newtracings1{y,5}=tracings{1, z}{1, x}.name;
        newtracings1{y,5}( isspace(newtracings1{y,5}) ) = [] ;
        newtracings1{y,6}=z;
        newtracings1{y,7}=x;
        
        y=y+1;
        else
        end
            
     end
        
    end
end

disp('soma coordinates (all nodes that have comment "soma") have been read')
somata=(newtracings1); %coordinates of somata in newtracings1

%% if you do not have a NML but a list of coordinates instead:
% somata = cell(1,3)
% %and now copy/paste all your coordinates into 'somata'


%% use this part to create one soma file for each soma
disp('writing Amira soma files...')
if ~exist(somafolder, 'dir')
  mkdir(somafolder);
end

for i=1:size(somata)
    somaList{i,1}=somata{i,1}*x_resolution;
    somaList{i,2}=somata{i,2}*y_resolution;
    somaList{i,3}=somata{i,3}*z_resolution;
    somaList{i,4}=diameter;
end
somaList2=cell2mat(somaList);

%color=fi_vis_getColors(size(somaList,1)); %multicolor
color=repmat([1,0,0],[size(somaList2,1) 1]); %all same color

%create Soma Isosurfaces in Folder
for i=1:size(somaList2,1)
[X,Y,Z]=sphere(faces);
abc{1}=surf2patch(X,Y,Z,'triangles');
abc{1}.vertices=abc{1}.vertices.*somaList2(i,4);
abc{1}.vertices=abc{1}.vertices+repmat(somaList2(i,1:3),[size(abc{1}.vertices,1),1]);

while true
        filename2 = ([somafolder filesep somata{i, 4} '_' somata{i, 5} '_' num2str(somata{i, 6}) '_' num2str(somata{i, 7}) '.am']);
        if exist(filename2, 'file') == 2
            else
            break
        end
    end

KLEEv4_exportSurfaceToAmira_v2(abc,filename2,color);

end

disp('.')
disp(['soma files have been written into ' somafolder])

%% MAKE HOCs:
disp('writing Amira hoc files...')
if ~exist(hocfolder, 'dir')
  mkdir(hocfolder);
end

for z=1:length(tracings) %z= number of NML files read by parseNml
for zz=1:length(tracings{1, z}) % x = number of trees
    
    hocname{1,1}=source(z,1).name; %1st part of hoc filename
    hocname{1,2}=tracings{1, z}{1, zz}.name; %2nd part of hoc filename
    hocname{1,2}( isspace(hocname{1,2}) ) = [] ;
    hocname{1,3}=z;
    hocname{1,4}=zz;
    
    while true
        filename = ([hocfolder filesep hocname{1,1} '_' hocname{1,2} '_' num2str(hocname{1,3}) '_' num2str(hocname{1,4}) '.hoc']); %complete hof filename
        if exist(filename, 'file') == 2
        else
            break
        end
  
    end
    
    EverySingleTree{1,1} = tracings{1, z}{1,zz}; %get all trees from all NMLs
        
    convertKnossosNmlToHocAll(EverySingleTree,filename,0,1,0,0,[x_resolution,y_resolution,z_resolution]); %x,x,x,x (type Konstellation 0 1 0 0 oder mit nodes 1 1 1 0) erste zahl hei�t edges werden ignoriert, zweite zahl hei�t diameter eintr�ge werden ignoriert, dritte zahl: show all nodes (geht nur wenn erste zahl auf 1 steht), vierte Zahl gl�ttet die diameter
 
    end
end        

disp(['hoc files have been written into ' hocfolder])

%% Make hocs and somata nice

%this part produces 3 lists:
% result_hoc_and_soma: filenames of soma and hoc files that get same color
% result_hoc_only: hocfiles that have no corresponding soma file
% result_soma_only: somafiles that have no corresponding hoc file
source_hoc = dir([hocfolder filesep '*.hoc']);
source_soma = dir([somafolder filesep '*.am']);

[~,order] = sort_nat({source_hoc.name});
source_hoc = source_hoc(order);
[~,order] = sort_nat({source_soma.name});
source_soma = source_soma(order);

for m = 1:size(source_hoc,1)
    HOC_correct_format{1,m} = ([source_hoc(m,1).name(1:end-4)]); %get comparable filename
    HOC_correct_format{2,m} = ([source_hoc(m,1).name]);
end
for m = 1:size(source_soma,1)
    soma_correct_format{1,m} = ([source_soma(m,1).name(1:end-3)]); %get comparable filename
    soma_correct_format{2,m} = ([source_soma(m,1).name]);
end

for n = 1:size(source_hoc,1)
    equalresult(n,1:size(source_soma,1)) = strcmp(HOC_correct_format(1,n), soma_correct_format(1,:)); %compare them and write results in equalresult
end

%create result_hoc_and_soma and result_hoc_only (as described above)
counter=1;
counter2=1;
for p = 1:size(equalresult,1)
    for o = 1:size(equalresult,2)
         if equalresult(p,o) == 1 %o = rows and p = lines of equalresult
         result_hoc_and_soma(counter,1) = HOC_correct_format(2,p); 
         result_hoc_and_soma(counter,2) = soma_correct_format(2,o);
         counter = counter+1;
         break
         elseif equalresult(p,o) == 0 &&  o == size(equalresult,2)
           result_hoc_only(counter2,1) = HOC_correct_format(2,p); 
           counter2 = counter2 + 1;
        end
    end
end

%create result_soma_only (as described above)
counter=1;
check_soma_sum=0;
for o = 1:size(equalresult,2)
    for p = 1:size(equalresult,1)
        if equalresult(p,o) == 1
            break
        end
            if exist('result_soma_only');
             check_soma = strcmp(soma_correct_format(2,o),result_soma_only(:,1));
             check_soma_sum = sum(check_soma);
            end     
             if equalresult(p,o) == 0 &&  p == size(equalresult,1) && check_soma_sum <1
              result_soma_only(counter,1) = soma_correct_format(2,o); 
              counter = counter + 1;
              check_soma_sum = 0;
             end
    end
end

%end of 3-lists-part


% make the hocs
if exist('result_hoc_and_soma') | exist ('result_hoc_only')

%getting the number of hoc files used
if exist ('result_hoc_and_soma') 
    if exist ('result_hoc_only')
        hocnumber = size(result_hoc_and_soma,1)+size(result_hoc_only,1);
    else
        hocnumber = size(result_hoc_and_soma,1);
    end
else
    hocnumber = size(result_hoc_only,1);
end

Amira_path = strrep(hocfolder,'\','/'); %Amira likes / instead of \
colors = distinguishable_colors(hocnumber,[1 1 1]); %background is [1 1 1; 0 0 0] oder [1 1 1] (wei�) oder [0 0 0] (schwarz)
A = [1:hocnumber]'; 
all = [A,colors]';

%write all Amira commands into a txt file
fileID = fopen(([hocfolder filesep 'nice_HOCs.txt']),'w');
format = '%s %s%s%s \n';

for i=1:size(source_hoc,1)
fprintf(fileID, format, 'load', Amira_path, '/', source_hoc(i,1).name); %load all .hoc files into Amira
end
fprintf(fileID,'create HxSpatialGraphView "SkeletonGraphView_%1.0f"\n',A); %set spacial graph view
format = '%s%1.0f%s %s \n'; % %s=string %1.0f just a number
if exist('result_hoc_and_soma')
    if exist('result_hoc_only')
        for i=1:size(result_hoc_and_soma,1)
          fprintf(fileID, format, '"SkeletonGraphView_', i, '" data connect', result_hoc_and_soma{i,1}); %connect spatial graph view to data (i.e. 1.hoc)
        end
    
        for j=1:size(result_hoc_only,1)
            i=i+1;
            fprintf(fileID, format, '"SkeletonGraphView_', i, '" data connect', result_hoc_only{j,1}); %connect spatial graph view to data (i.e. 1.hoc)
        end
    else
        for i=1:size(result_hoc_and_soma,1)
          fprintf(fileID, format, '"SkeletonGraphView_', i, '" data connect', result_hoc_and_soma{i,1}); %connect spatial graph view to data (i.e. 1.hoc)
        end
    end
else
    for i=1:size(result_hoc_only,1)
        fprintf(fileID, format, '"SkeletonGraphView_', i, '" data connect', result_hoc_only{i,1}); %connect spatial graph view to data (i.e. 1.hoc)
    end
end

fprintf(fileID,'"SkeletonGraphView_%1.0f" fire\n',A); %fire
fprintf(fileID,'"SkeletonGraphView_%1.0f" segmentColor setColor 0 %1.4f %1.4f %1.4f\n',all); %set tubes color
fprintf(fileID,'"SkeletonGraphView_%1.0f" itemsToShow setValue 0 0\n',A); %not show nodes
fprintf(fileID,'"SkeletonGraphView_%1.0f" segmentStyle setValue 2 1\n',A); %show as tubes not lines
fprintf(fileID,['"SkeletonGraphView_%1.0f" tubeScaleFactor setValue ' num2str(tubeScaleFactor) '\n'],A); %tube-thickness set value
fprintf(fileID,'"SkeletonGraphView_%1.0f" fire\n',A); %fire
fclose(fileID);
end


% make the somata
if exist('result_hoc_and_soma') | exist ('(result_soma_only')
%getting the number of hoc files used
if exist ('result_hoc_and_soma') 
    if exist ('result_soma_only')
        somanumber = size(result_hoc_and_soma,1)+size(result_soma_only,1);
    else
        somanumber = size(result_hoc_and_soma,1);
    end
else
    somanumber = size(result_soma_only,1);
end

Amira_path = strrep(somafolder,'\','/'); %Amira likes / instead of \
colors = distinguishable_colors(somanumber,[1 1 1]); %background is [1 1 1; 0 0 0] oder [1 1 1] (wei�) oder [0 0 0] (schwarz)
A = [1:somanumber]'; 
all = [A,colors]';

%write all Amira commands into a txt file
fileID = fopen(([somafolder filesep 'nice_SOMATA.txt']),'w');
format = '%s %s%s%s \n';

for i=1:size(source_soma,1)
fprintf(fileID, format, 'load', Amira_path, '/', source_soma(i,1).name); %load all .am files into Amira
end
fprintf(fileID,'create HxDisplaySurface "SomaSurfaceView_%1.0f"\n',A); %set spacial graph view
format = '%s%1.0f%s %s \n'; % %s=string %1.0f just a number
if exist('result_hoc_and_soma')
    if exist('result_soma_only')
        for i=1:size(result_hoc_and_soma,1)
          fprintf(fileID, format, '"SomaSurfaceView_', i, '" data connect', result_hoc_and_soma{i,2}); %connect Surface View to data (i.e. 1.am)
        end
    
        for j=1:size(result_soma_only,1)
            i=i+1;
            fprintf(fileID, format, '"SomaSurfaceView_', i, '" data connect', result_soma_only{j,1}); %connect Surface View view to data (i.e. 1.am)
        end
    else
        for i=1:size(result_hoc_and_soma,1)
          fprintf(fileID, format, '"SomaSurfaceView_', i, '" data connect', result_hoc_and_soma{i,2}); %connect Surface View view to data (i.e. 1.am)
        end
    end
else
    for i=1:size(result_soma_only,1)
        fprintf(fileID, format, '"SomaSurfaceView_', i, '" data connect', result_soma_only{i,1}); %connect Surface View view to data (i.e. 1.am)
    end
end

fprintf(fileID,'"SomaSurfaceView_%1.0f" colormap setDefaultColor %1.4f %1.4f %1.4f\n',all); %make the color
fprintf(fileID,'set maxSurf %1.0f; for {set i 1} {$i <= $maxSurf } { incr i} {"SomaSurfaceView_$i" drawStyle setNormalBinding 1}\n',A);%set vertex normals
fprintf(fileID,'set maxSurf %1.0f; for {set i 1} {$i <= $maxSurf } { incr i} {"SomaSurfaceView_$i" colorMode setIndex 0 5}\n',A);%color mode constant
fprintf(fileID,'set maxSurf %1.0f; for {set i 1} {$i <= $maxSurf } { incr i} {"SomaSurfaceView_$i" fire}\n',A); %fire
fclose(fileID);
end

disp('finished!')
disp('nice_HOCs.txt and nice_SOMATA.txt have been written into your hoc- and somafolder.')
disp('Please execute the content in the Amira console to load and visualize')

%% use this part to combine the .am somata to one bbox (compare amira description bbox in the amira folder)


% fileID = fopen('bbox.txt','w');
% format = '%s%1.0f%s%s%s \n %s \n';
% for i=1:size(result_hoc_and_soma(:,2),1);
%     fprintf(fileID, format,'"CombineLandmarks" source', i, ' connect "', cell2mat(result_hoc_and_soma(i,2)), '"', '"CombineLandmarks" fire') ;
%     
% end; 
% fclose (fileID);



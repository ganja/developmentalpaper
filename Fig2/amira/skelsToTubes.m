%%
% set all variables
% pathToNMLs='Z:\Data\goura\forSL\connBerlin\P9_L4_Apicals';
pathToNMLs='Z:\Data\goura\FigureUpdates\SomaticFilopodia_L4';
% output_folder='Z:\Data\goura\forSL\connBerlin2019\P9_L4_Apicals_out';
output_folder='Z:\Data\goura\FigureUpdates\SomaticFilopodia_L4';
if ~exist(output_folder, 'dir')
    mkdir(output_folder);
end
x_resolution=11.24;
y_resolution=11.24;
z_resolution=30;
%filename='soma-test.am' ;%filename of amira file you create
faces=40; %number of faces for each sphere (10 ist fast, 50 is HighRes but slow if many thousand)

%prepare folders:
somafolder = ([output_folder filesep 'Amira_soma-files']);
hocfolder = ([output_folder filesep 'Amira_hoc-files']);
delete ([somafolder filesep '*.am']);
delete ([hocfolder filesep '*.hoc']);

%% read out all nodes (all nodes in all NMLs are then Somata):
tracings={};
source=dir([pathToNMLs '\*.nml']);
for i=1:size(source,1)
    tracings{1,i}=parseNml([pathToNMLs '\' source(i,1).name]);
end

disp('writing Amira hoc files...')
if ~exist(hocfolder, 'dir')
  mkdir(hocfolder);
end

%%
for z=1:length(tracings) %z= number of NML files read by parseNml
    for zz=1:length(tracings{1, z}) % x = number of trees
    
        hocname{1,1}=source(z,1).name(1:end-4); %1st part of hoc filename
        hocname{1,2}=tracings{1, z}{1, zz}.name; %2nd part of hoc filename
        hocname{1,2}( isspace(hocname{1,2}) ) = [] ;
        hocname{1,3}=z;
        hocname{1,4}=zz;

        while true
            filename = ([hocfolder filesep hocname{1,1} '_' hocname{1,2} '_' num2str(hocname{1,3}) '_' num2str(hocname{1,4}) '.hoc']); %complete hof filename
            filename = strrep(filename,'?','Q');
            filename = strrep(filename,'!','Q');
            filename = strrep(filename,'/','Q');
            if exist(filename, 'file') == 2
            else
                break
            end

        end

        EverySingleTree{1,1} = tracings{1, z}{1,zz}; %get all trees from all NMLs

        convertKnossosNmlToHocAll(EverySingleTree,filename,0,1,0,0,...
            [x_resolution,y_resolution,z_resolution]); %x,x,x,x (type Konstellation 0 1 0 0 oder mit nodes 1 1 1 0) erste zahl hei�t edges werden ignoriert, zweite zahl hei�t diameter eintr�ge werden ignoriert, dritte zahl: show all nodes (geht nur wenn erste zahl auf 1 steht), vierte Zahl gl�ttet die diameter
 
    end
end      
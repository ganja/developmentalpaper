%% AD specificity box plot
% cx_data = xlsread('Z:\Data\goura\Analysis\L4Data_Updated_12.10.2018.xlsx','Sheet1');
cx_data = xlsread('Z:\Data\goura\FigureUpdates\RawData_Excels\L4_RawData_24-01-2020.xlsx','Sheet1');
Age = [7 9 14 28]

%% For AD specificity 
ToSel_1 = find(cx_data(:,1)== 7& cx_data(:,3)== 1); %use the age and seeding as filter
ADInn_7 = cx_data(ToSel_1,16); % 6 for total syn; 17 is for AD specificity; 19 for Soma specificity; 25 for syn density
ADSyn_7 = cx_data(ToSel_1,6);
ADDens_7 = cx_data(ToSel_1,24);
% ADBinInn_7 = cx_data(ToSel_1,33);

ToSel_2 = find(cx_data(:,1)== 9& cx_data(:,3)== 1); 
ADInn_9a = cx_data(ToSel_2,16); ADSyn_9a = cx_data(ToSel_2,6); ADDens_9a = cx_data(ToSel_2,24); 
% ADBinInn_9a = cx_data(ToSel_2,33);
ToSel_3 = find(cx_data(:,1)== 9.2& cx_data(:,3)== 1); 
ADInn_9b = cx_data(ToSel_3,16); ADSyn_9b = cx_data(ToSel_3,6); ADDens_9b = cx_data(ToSel_3,24); 
% ADBinInn_9b = cx_data(ToSel_3,33);

ToSel_4 = find(cx_data(:,1)== 14.1& cx_data(:,3)== 1);
ADInn_14a = cx_data(ToSel_4,16); ADSyn_14a = cx_data(ToSel_4,6); ADDens_14a = cx_data(ToSel_4,24);
% ADBinInn_14a = cx_data(ToSel_4,33);
ToSel_5 = find(cx_data(:,1)== 14.2& cx_data(:,3)== 1); %use the age and seeding as filter
ADInn_14b = cx_data(ToSel_5,16); ADSyn_14b = cx_data(ToSel_5,6); ADDens_14b = cx_data(ToSel_5,24);
% ADBinInn_14b = cx_data(ToSel_5,33);

ToSel_6 = find(cx_data(:,1)== 28& cx_data(:,3)== 1); %use the age and seeding as filter
ADInn_28 = cx_data(ToSel_6,16);  ADSyn_28 = cx_data(ToSel_6,6); ADDens_28 = cx_data(ToSel_6,24); 
% ADBinInn_28 = cx_data(ToSel_6,33);

%% Combine two datasets
ADSyn_9 = cat(1,ADSyn_9a,ADSyn_9b); ADInn_9 = cat(1,ADInn_9a,ADInn_9b); ADDens_9 = cat(1,ADDens_9a,ADDens_9b); 
% ADBinInn_9 = cat(1,ADBinInn_9a,ADBinInn_9b);
ADSyn_14 = cat(1,ADSyn_14a,ADSyn_14b); ADInn_14 = cat(1,ADInn_14a,ADInn_14b); ADDens_14 = cat(1,ADDens_14a,ADDens_14b); 
% ADBinInn_14 = cat(1,ADBinInn_14a,ADBinInn_14b);

%% Concatenate all the parameters and sort
AD7all = cat(2,ADSyn_7,ADInn_7,ADDens_7); 
AD9all = cat(2,ADSyn_9,ADInn_9,ADDens_9);
AD14all = cat(2,ADSyn_14,ADInn_14,ADDens_14);
AD28all = cat(2,ADSyn_28,ADInn_28,ADDens_28);

AD7sort = sortrows(AD7all,1);
AD9sort = sortrows(AD9all,1);
AD14sort = sortrows(AD14all,1);
AD28sort = sortrows(AD28all,1);

%% Different thresholding
%thrA = <10; thrB = 10-20; thrC = > 20syn
SelA = find(AD7sort(:,1) < 10);
SelB = find(AD7sort(:,1) > 9 & AD7sort(:,1) <20);
SelC = find(AD7sort(:,1) > 19); 
AD7_thrA_Inn = AD7sort(SelA,2); AD7_thrB_Inn = AD7sort(SelB,2); AD7_thrC_Inn = AD7sort(SelC,2);
AD7_thrA_Dens = AD7sort(SelA,3); AD7_thrB_Dens = AD7sort(SelB,3); AD7_thrC_Dens = AD7sort(SelC,3);
% AD7_thrA_BinInn = AD7sort(SelA,4); AD7_thrB_BinInn = AD7sort(SelB,4); AD7_thrC_BinInn = AD7sort(SelC,4);


SelA = find(AD9sort(:,1) < 10);
SelB = find(AD9sort(:,1) > 9 & AD9sort(:,1) <20);
SelC = find(AD9sort(:,1) > 19); 
AD9_thrA_Inn = AD9sort(SelA,2); AD9_thrB_Inn = AD9sort(SelB,2); AD9_thrC_Inn = AD9sort(SelC,2);
AD9_thrA_Dens = AD9sort(SelA,3); AD9_thrB_Dens = AD9sort(SelB,3); AD9_thrC_Dens = AD9sort(SelC,3);
% AD9_thrA_BinInn = AD9sort(SelA,4); AD9_thrB_BinInn = AD9sort(SelB,4); AD9_thrC_BinInn = AD9sort(SelC,4);

SelA = find(AD14sort(:,1) < 10);
SelB = find(AD14sort(:,1) > 9 & AD14sort(:,1) <20);
SelC = find(AD14sort(:,1) > 19); 
AD14_thrA_Inn = AD14sort(SelA,2); AD14_thrB_Inn = AD14sort(SelB,2); AD14_thrC_Inn = AD14sort(SelC,2);
AD14_thrA_Dens = AD14sort(SelA,3); AD14_thrB_Dens = AD14sort(SelB,3); AD14_thrC_Dens = AD14sort(SelC,3);
% AD14_thrA_BinInn = AD14sort(SelA,4); AD14_thrB_BinInn = AD14sort(SelB,4); AD14_thrC_BinInn = AD14sort(SelC,4);


SelA = find(AD28sort(:,1) < 10);
SelB = find(AD28sort(:,1) > 9 & AD28sort(:,1) <20);
SelC = find(AD28sort(:,1) > 19); 
AD28_thrA_Inn = AD28sort(SelA,2); AD28_thrB_Inn = AD28sort(SelB,2); AD28_thrC_Inn = AD28sort(SelC,2);
AD28_thrA_Dens = AD28sort(SelA,3); AD28_thrB_Dens = AD28sort(SelB,3); AD28_thrC_Dens = AD28sort(SelC,3);
% AD28_thrA_BinInn = AD28sort(SelA,4); AD28_thrB_BinInn = AD28sort(SelB,4); AD28_thrC_BinInn = AD28sort(SelC,4);

%% Bulk and Cumulative data for plotting along with Bootstrapping
% getting general parameters
% AD_BulkInn = []; AD_BulkDens = [];

% Bootstrapping to get error means
ADInn_7_bstrp = bootstrp(10,@mean,ADInn_7); SE_ADInn_7 = std(ADInn_7_bstrp);
ADInn_9_bstrp = bootstrp(10,@mean,ADInn_9); SE_ADInn_9 = std(ADInn_9_bstrp); 
ADInn_14_bstrp = bootstrp(10,@mean,ADInn_14); SE_ADInn_14 = std(ADInn_14_bstrp); 
ADInn_28_bstrp = bootstrp(10,@mean,ADInn_28); SE_ADInn_28 = std(ADInn_28_bstrp);
BootStrpData_ADInn = cat(2,ADInn_7_bstrp,ADInn_9_bstrp,ADInn_14_bstrp,ADInn_28_bstrp);

ADDens_7_bstrp = bootstrp(10,@mean,ADDens_7); SE_ADDens_7 = std(ADDens_7_bstrp);
ADDens_9_bstrp = bootstrp(10,@mean,ADDens_9); SE_ADDens_9 = std(ADDens_9_bstrp); 
ADDens_14_bstrp = bootstrp(10,@mean,ADDens_14); SE_ADDens_14 = std(ADDens_14_bstrp); 
ADDens_28_bstrp = bootstrp(10,@mean,ADDens_28); SE_ADDens_28 = std(ADDens_28_bstrp);
BootStrpData_ADDens = cat(2,ADDens_7_bstrp,ADDens_9_bstrp,ADDens_14_bstrp,ADDens_28_bstrp);

% Concatenate lines
SE_Inn = cat(1,SE_ADInn_7,SE_ADInn_9,SE_ADInn_14,SE_ADInn_28);
SE_Dens = cat(1,SE_ADDens_7,SE_ADDens_9,SE_ADDens_14,SE_ADDens_28);

% Concatenate data for box plots
InnData = cat(1,ADInn_7, ADInn_9, ADInn_14, ADInn_28);
DensData = cat(1,ADDens_7, ADDens_9, ADDens_14, ADDens_28);
% BinInnData = cat(1,ADBinInn_7, ADBinInn_9, ADBinInn_14, ADBinInn_28);

% Grouping
g1 = ones(size(ADInn_7));
g2 = 2*ones(size(ADInn_9));
g3 = 3*ones(size(ADInn_14));
g4 = 4*ones(size(ADInn_28));
Group = cat(1,g1,g2,g3,g4);

%% Plotting
%error lines
SE_Inn_plus = AD_BulkInn + SE_Inn;
SE_Inn_minus = AD_BulkInn - SE_Inn;

SE_Dens_plus = AD_BulkDens + SE_Dens;
SE_Dens_minus = AD_BulkDens - SE_Dens;

% Innervation fractions
figure;
boxplot(InnData,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('AD innervation fraction)')
xlabel('Postnatal Age')
hold on
plot(Age,AD_BulkInn,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot(Age,SE_Inn_plus,'--k')% lplus err line
hold on
plot(Age,SE_Inn_minus,'--k')% minus err line
hold on
scatter(ones(numel(AD7_thrA_Inn),1)*7+rand(numel(AD7_thrA_Inn),1)*.75-.45, AD7_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',200);
hold on
scatter(ones(numel(AD7_thrB_Inn),1)*7+rand(numel(AD7_thrB_Inn),1)*.75-.45, AD7_thrB_Inn,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(AD7_thrC_Inn),1)*7+rand(numel(AD7_thrC_Inn),1)*.75-.45, AD7_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',200);
hold on
scatter(ones(numel(AD9_thrA_Inn),1)*9+rand(numel(AD9_thrA_Inn),1)*.75-.45, AD9_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',200);
hold on
scatter(ones(numel(AD9_thrB_Inn),1)*9+rand(numel(AD9_thrB_Inn),1)*.75-.45, AD9_thrB_Inn,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(AD9_thrC_Inn),1)*9+rand(numel(AD9_thrC_Inn),1)*.75-.45, AD9_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',200);
hold on
scatter(ones(numel(AD14_thrA_Inn),1)*14+rand(numel(AD14_thrA_Inn),1)*.75-.45, AD14_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',200);
hold on
scatter(ones(numel(AD14_thrB_Inn),1)*14+rand(numel(AD14_thrB_Inn),1)*.75-.45, AD14_thrB_Inn,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(AD14_thrC_Inn),1)*14+rand(numel(AD14_thrC_Inn),1)*.75-.45, AD14_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',200);
hold on
scatter(ones(numel(AD28_thrA_Inn),1)*28+rand(numel(AD28_thrA_Inn),1)*.75-.45, AD28_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',200);
hold on
scatter(ones(numel(AD28_thrB_Inn),1)*28+rand(numel(AD28_thrB_Inn),1)*.75-.45, AD28_thrB_Inn,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(AD28_thrC_Inn),1)*28+rand(numel(AD28_thrC_Inn),1)*.75-.45, AD28_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',200);
hold on
title('L4 Innervation fractions - AD axons')


% Syn desnities
figure;
boxplot(DensData,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 0.6])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Synapse density (syn/um axonal path length)')
xlabel('Postnatal Age')
hold on
plot(Age,AD_BulkDens,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot(Age,SE_Dens_plus,'--k')% lplus err line
hold on
plot(Age,SE_Dens_minus,'--k')% minus err line
hold on
scatter(ones(numel(AD7_thrA_Dens),1)*7+rand(numel(AD7_thrA_Dens),1)*.75-.45, AD7_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',200);
hold on
scatter(ones(numel(AD7_thrB_Dens),1)*7+rand(numel(AD7_thrB_Dens),1)*.75-.45, AD7_thrB_Dens,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(AD7_thrC_Dens),1)*7+rand(numel(AD7_thrC_Dens),1)*.75-.45, AD7_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',200);
hold on
scatter(ones(numel(AD9_thrA_Dens),1)*9+rand(numel(AD9_thrA_Dens),1)*.75-.45, AD9_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',200);
hold on
scatter(ones(numel(AD9_thrB_Dens),1)*9+rand(numel(AD9_thrB_Dens),1)*.75-.45, AD9_thrB_Dens,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(AD9_thrC_Dens),1)*9+rand(numel(AD9_thrC_Dens),1)*.75-.45, AD9_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',200);
hold on
scatter(ones(numel(AD14_thrA_Dens),1)*14+rand(numel(AD14_thrA_Dens),1)*.75-.45, AD14_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',200);
hold on
scatter(ones(numel(AD14_thrB_Dens),1)*14+rand(numel(AD14_thrB_Dens),1)*.75-.45, AD14_thrB_Dens,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(AD14_thrC_Dens),1)*14+rand(numel(AD14_thrC_Dens),1)*.75-.45, AD14_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',200);
hold on
scatter(ones(numel(AD28_thrA_Dens),1)*28+rand(numel(AD28_thrA_Dens),1)*.75-.45, AD28_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',200);
hold on
scatter(ones(numel(AD28_thrB_Dens),1)*28+rand(numel(AD28_thrB_Dens),1)*.75-.45, AD28_thrB_Dens,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(AD28_thrC_Dens),1)*28+rand(numel(AD28_thrC_Dens),1)*.75-.45, AD28_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',200);
hold on
title('L4 Synapse densities - AD axons')

%% Scatter plots - Innervation fractions vs Density
p1 = polyfit(ADDens_7,ADInn_7,1);
f1 = polyval(p1,ADDens_7);
p2 = polyfit(ADDens_9,ADInn_9,1);
f2 = polyval(p2,ADDens_9);
p3 = polyfit(ADDens_14,ADInn_14,1);
f3 = polyval(p3,ADDens_14);
p4 = polyfit(ADDens_28,ADInn_28,1);
f4 = polyval(p4,ADDens_28);


%for P7 and P9
figure;
% scatter(AD7_thrA_Dens,AD7_thrA_Inn,'x','MarkerEdgeColor','b','SizeData',20)
% hold on
% scatter(AD7_thrB_Dens,AD7_thrB_Inn,'x','MarkerEdgeColor','b','SizeData',70)
% hold on
scatter(AD7_thrC_Dens,AD7_thrC_Inn,'x','MarkerEdgeColor','b','SizeData',150)
hold on
% scatter(AD9_thrA_Dens,AD9_thrA_Inn,'x','MarkerEdgeColor','r','SizeData',20)
% hold on
% scatter(AD9_thrB_Dens,AD9_thrB_Inn,'x','MarkerEdgeColor','r','SizeData',70)
% hold on
scatter(AD9_thrC_Dens,AD9_thrC_Inn,'x','MarkerEdgeColor','r','SizeData',150)
hold on
% plot(ADDens_7,f1,'-','Color','b')
% hold on
% plot(ADDens_9,f2,'-','Color','r')
% hold on
box off
set(gca,'TickDir','out')
daspect([1 1 1])
set(gca,'Xlim',[0 1])
set(gca,'Ylim',[0 1])
set(gca,'XTick',[0 0.2 0.4 0.6 0.8 1])
set(gca,'YTick',[0 0.2 0.4 0.6 0.8 1])
xlabel('Synapse density (syn/um axonal path length')
ylabel('AD innervation fraction')
title('L4 - Innervation fraction vs Syn density (AD Axons)')

% for all ages
figure;
scatter(AD7_thrA_Dens,AD7_thrA_Inn,'x','MarkerEdgeColor','b','SizeData',20)
hold on
scatter(AD7_thrB_Dens,AD7_thrB_Inn,'x','MarkerEdgeColor','b','SizeData',70)
hold on
scatter(AD7_thrC_Dens,AD7_thrC_Inn,'x','MarkerEdgeColor','b','SizeData',150)
hold on
scatter(AD9_thrA_Dens,AD9_thrA_Inn,'x','MarkerEdgeColor','r','SizeData',20)
hold on
scatter(AD9_thrB_Dens,AD9_thrB_Inn,'x','MarkerEdgeColor','r','SizeData',70)
hold on
scatter(AD9_thrC_Dens,AD9_thrC_Inn,'x','MarkerEdgeColor','r','SizeData',150)
hold on
scatter(AD14_thrA_Dens,AD14_thrA_Inn,'x','MarkerEdgeColor','g','SizeData',20)
hold on
scatter(AD14_thrB_Dens,AD14_thrB_Inn,'x','MarkerEdgeColor','g','SizeData',70)
hold on
scatter(AD14_thrC_Dens,AD14_thrC_Inn,'x','MarkerEdgeColor','g','SizeData',150)
hold on
scatter(AD28_thrA_Dens,AD28_thrA_Inn,'x','MarkerEdgeColor','m','SizeData',20)
hold on
scatter(AD28_thrB_Dens,AD28_thrB_Inn,'x','MarkerEdgeColor','m','SizeData',70)
hold on
scatter(AD28_thrC_Dens,AD28_thrC_Inn,'x','MarkerEdgeColor','m','SizeData',150)
hold on
plot(ADDens_7,f1,'-','Color','b')
hold on
plot(ADDens_9,f2,'-','Color','r')
hold on
plot(ADDens_14,f3,'-','Color','k')
hold on
plot(ADDens_28,f4,'-','Color','m')
hold on
box off
set(gca,'TickDir','out')
daspect([1 1 1])
set(gca,'Xlim',[0 1])
set(gca,'Ylim',[0 1])
set(gca,'XTick',[0 0.2 0.4 0.6 0.8 1])
set(gca,'YTick',[0 0.2 0.4 0.6 0.8 1])
xlabel('Synapse density (syn/um axonal path length')
ylabel('AD innervation fraction')
title('L4 - Innervation fraction vs Syn density (AD Axons)')

% get r-sq values
lm = fitlm(ADDens_7,ADInn_7)
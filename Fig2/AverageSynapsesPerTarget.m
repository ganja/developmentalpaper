%L4 - Avg syn per target for AD and Soma Axons
%SOMA AXONS

% load('Z:\Data\goura\FigureUpdates\AvgSynPerTarget\Data_31-10-2019\L4_SomaAxons_AvgSynPerTarget.mat');dont
% use!!!!

cx_data = xlsread('Z:\Data\goura\FigureUpdates\RawData_Excels\L4_RawData_24-01-2020.xlsx','Sheet3');
ToSel_1 = find(cx_data(:,1)== 7& cx_data(:,3)== 3); %use the age and seeding as filter
P7_AvgSynPerSoma = cx_data(ToSel_1,7);
ToSel_2 = find(cx_data(:,1)== 9.1& cx_data(:,3)== 3); %use the age and seeding as filter
P9a_AvgSynPerSoma = cx_data(ToSel_2,7);
ToSel_3 = find(cx_data(:,1)== 9.2& cx_data(:,3)== 3); %use the age and seeding as filter
P9b_AvgSynPerSoma = cx_data(ToSel_3,7);
ToSel_4 = find(cx_data(:,1)== 14.1& cx_data(:,3)== 3); %use the age and seeding as filter
P14a_AvgSynPerSoma = cx_data(ToSel_4,7);
ToSel_5 = find(cx_data(:,1)== 14.2& cx_data(:,3)== 3); %use the age and seeding as filter
P14b_AvgSynPerSoma = cx_data(ToSel_5,7);
ToSel_6 = find(cx_data(:,1)== 28& cx_data(:,3)== 3); %use the age and seeding as filter
P28_AvgSynPerSoma = cx_data(ToSel_6,7);


P9_AvgSynPerSoma = cat(1,P9a_AvgSynPerSoma,P9b_AvgSynPerSoma);
P14_AvgSynPerSoma = cat(1,P14a_AvgSynPerSoma,P14b_AvgSynPerSoma);
%%
Data = cat(1, P7_AvgSynPerSoma, P9_AvgSynPerSoma, P14_AvgSynPerSoma, P28_AvgSynPerSoma);
g1 = ones(size(P7_AvgSynPerSoma));g2 = 2*ones(size(P9_AvgSynPerSoma));g3 = 3*ones(size(P14_AvgSynPerSoma));g4 = 4*ones(size(P28_AvgSynPerSoma));
G = cat(1,g1,g2,g3,g4);
Mean = [mean(P7_AvgSynPerSoma); mean(P9_AvgSynPerSoma); mean(P14_AvgSynPerSoma); mean(P28_AvgSynPerSoma)];
Err_SEM = [(std(P7_AvgSynPerSoma)/sqrt(numel(P7_AvgSynPerSoma)));
(std(P9_AvgSynPerSoma)/sqrt(numel(P9_AvgSynPerSoma)));
(std(P14_AvgSynPerSoma)/sqrt(numel(P14_AvgSynPerSoma)));
(std(P28_AvgSynPerSoma)/sqrt(numel(P28_AvgSynPerSoma)))];
Age = [7 9 14 28];

figure
boxplot(Data,G,'Positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 5])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Avg # syn / soma')
xlabel('Postnatal Age (days)')
hold on
plot(Age,Mean,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot(Age,(Mean+Err_SEM),'--k')% lplus err line
hold on
plot(Age,(Mean-Err_SEM),'--k')% minus err line
hold on
scatter(ones(numel(P7_AvgSynPerSoma),1)*7+rand(numel(P7_AvgSynPerSoma),1)*.75-.45, P7_AvgSynPerSoma,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(P9_AvgSynPerSoma),1)*9+rand(numel(P9_AvgSynPerSoma),1)*.75-.45, P9_AvgSynPerSoma,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(P14_AvgSynPerSoma),1)*14+rand(numel(P14_AvgSynPerSoma),1)*.75-.45, P14_AvgSynPerSoma,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(P28_AvgSynPerSoma),1)*28+rand(numel(P28_AvgSynPerSoma),1)*.75-.45, P28_AvgSynPerSoma,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
title('Avergae synapses per target - Soma')

%% AD AXONS

% load('Z:\Data\goura\FigureUpdates\AvgSynPerTarget\Data_31-10-2019\L4_ADAxons_AvgSynPerTarget.mat');
ToSel_7 = find(cx_data(:,1)== 7& cx_data(:,3)== 1); %use the age and seeding as filter
P7_AvgSynPerAD = cx_data(ToSel_7,7);
ToSel_8 = find(cx_data(:,1)== 9.1& cx_data(:,3)== 1); %use the age and seeding as filter
P9a_AvgSynPerAD = cx_data(ToSel_8,7);
ToSel_9 = find(cx_data(:,1)== 9.2& cx_data(:,3)== 1); %use the age and seeding as filter
P9b_AvgSynPerAD = cx_data(ToSel_9,7);
ToSel_10 = find(cx_data(:,1)== 14.1& cx_data(:,3)== 1); %use the age and seeding as filter
P14a_AvgSynPerAD = cx_data(ToSel_10,7);
ToSel_11 = find(cx_data(:,1)== 14.2& cx_data(:,3)== 1); %use the age and seeding as filter
P14b_AvgSynPerAD = cx_data(ToSel_11,7);
ToSel_12 = find(cx_data(:,1)== 28& cx_data(:,3)== 1); %use the age and seeding as filter
P28_AvgSynPerAD = cx_data(ToSel_12,7);


P9_AvgSynPerAD = cat(1,P9a_AvgSynPerAD,P9b_AvgSynPerAD);
P14_AvgSynPerAD = cat(1,P14a_AvgSynPerAD,P14b_AvgSynPerAD);

%%
Data = cat(1, P7_AvgSynPerAD, P9_AvgSynPerAD, P14_AvgSynPerAD, P28_AvgSynPerAD);
g1 = ones(size(P7_AvgSynPerAD));g2 = 2*ones(size(P9_AvgSynPerAD));g3 = 3*ones(size(P14_AvgSynPerAD));g4 = 4*ones(size(P28_AvgSynPerAD));
G = cat(1,g1,g2,g3,g4);
Mean = [mean(P7_AvgSynPerAD); mean(P9_AvgSynPerAD); mean(P14_AvgSynPerAD); mean(P28_AvgSynPerAD)];
Err_SEM = [(std(P7_AvgSynPerAD)/sqrt(numel(P7_AvgSynPerAD)));
(std(P9_AvgSynPerAD)/sqrt(numel(P9_AvgSynPerAD)));
(std(P14_AvgSynPerAD)/sqrt(numel(P14_AvgSynPerAD)));
(std(P28_AvgSynPerAD)/sqrt(numel(P28_AvgSynPerAD)))];
Age = [7 9 14 28];
 
figure
boxplot(Data,G,'Positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 5])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Avg # syn / AD')
xlabel('Postnatal Age (days)')
hold on
plot(Age,Mean,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot(Age,(Mean+Err_SEM),'--k')% lplus err line
hold on
plot(Age,(Mean-Err_SEM),'--k')% minus err line
hold on
scatter(ones(numel(P7_AvgSynPerAD),1)*7+rand(numel(P7_AvgSynPerAD),1)*.75-.45, P7_AvgSynPerAD,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(P9_AvgSynPerAD),1)*9+rand(numel(P9_AvgSynPerAD),1)*.75-.45, P9_AvgSynPerAD,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(P14_AvgSynPerAD),1)*14+rand(numel(P14_AvgSynPerAD),1)*.75-.45, P14_AvgSynPerAD,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
scatter(ones(numel(P28_AvgSynPerAD),1)*28+rand(numel(P28_AvgSynPerAD),1)*.75-.45, P28_AvgSynPerAD,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200);
hold on
title('Avergae synapses per target - AD')


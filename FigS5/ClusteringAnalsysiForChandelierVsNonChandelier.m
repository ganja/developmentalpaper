pd = pdist([x,y],'euclidean');
lnk = linkage(pd,'average');
T = cluster(lnk,'maxclust',2);
gr1 = find(T==1);
gr2 = find(T==2);

H = dendrogram(lnk, numel(x), 'labels',num2str([1:38]'));

figure; 
z = [x,y];
scatter(x,y)
hold on 
scatter(z(gr1,1),z(gr1,2),'ro','filled')
scatter(z(gr2,1),z(gr2,2),'go','filled')

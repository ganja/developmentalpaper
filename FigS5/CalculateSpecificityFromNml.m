%% SPECIFICITY FRACTIONS FROM NML
% skel = skeleton('Z:\Data\goura\Analysis\NMLfiles\P14L4(2)ADandADax_AGtraced.nml');
skel = skeleton('C:\Users\goura\Downloads\truncAxons_P9L4n2_updated.nml');
%% Finding correct axons
AxonTrees = find(cellfun(@(x)any(strfind(x, 'Sm0')==1),skel.names));
AxonCount = numel(AxonTrees);
PostSynTarget = {'AD','AIS','Soma','Spine','Shaft','Glia','Somatic fil.'};
TargetCount = length(PostSynTarget);
SynData = NaN(AxonCount,TargetCount);
TotalSyn = NaN(AxonCount,1);
ADSyn = NaN(AxonCount,1);
AISSyn = NaN(AxonCount,1);
SomaSyn = NaN(AxonCount,1);
SpineSyn = NaN(AxonCount,1);
ShaftSyn = NaN(AxonCount,1);
GliaSyn = NaN(AxonCount,1);
SomFilSyn = NaN(AxonCount,1);
for i = 1:AxonCount
    ADnodes = numel(getNodesWithComment(skel,'AD',AxonTrees(i),'partial'));
    ADSyn(i) = ADnodes(1,:);
    AISnodes = numel(getNodesWithComment(skel,'AIS',AxonTrees(i),'partial'));
    AISSyn(i) = AISnodes(1,:);
    SomaNodes = numel(getNodesWithComment(skel,'Soma',AxonTrees(i),'partial'));
    SomaSyn(i) = SomaNodes(1,:);
    SpineNodes = numel(getNodesWithComment(skel,'head spine',AxonTrees(i),'partial') + getNodesWithComment(skel,'neck spine',AxonTrees(i),'partial'));
    SpineSyn(i) = SpineNodes(1,:);
    ShaftNodes = numel(getNodesWithComment(skel,'head spine',AxonTrees(i),'partial'));
    ShaftSyn(i) = ShaftNodes(1,:);
    GliaNodes = numel(getNodesWithComment(skel,'glia',AxonTrees(i),'partial')); 
    SpineSyn(i) = GliaNodes(1,:);
    SomFilNodes = numel(getNodesWithComment(skel,'Sm.',AxonTrees(i),'partial'));
    SomFilSyn(i) = SomFilNodes(1,:);
    TotalNodes = numel(getNodesWithComment(skel,'syn',AxonTrees(i),'partial'));
    TotalSyn(i) = TotalNodes(1,:);
end
%% For names path lengths and densities
AxonNames = skel.names(AxonTrees);
AxonLengths = zeros(AxonCount,1); %All the path lengths will be stored in TreeLengths :)
for i = 1:AxonCount
 currentPathLength =(skel.pathLength(AxonTrees(i)));
 currentPathLengthMicrons = currentPathLength/1000;
 AxonLengths(i) = currentPathLengthMicrons;
end

AxonSynDensity = TotalSyn./AxonLengths;
Data = cat(2,AxonNames,TotalSyn,AxonLengths,AxonSynDensity,SomaSyn);

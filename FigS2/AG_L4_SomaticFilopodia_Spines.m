%% Analysis of somatic filopodia and spines
load('Z:\Data\goura\FigureUpdates\SomaticFilopodia_L4\MATLAB\data_03-02-2020');
%%
g1 = ones(size(P7Syn));
g2 = 2*ones(size(P9Syn));
g3 = 3*ones(size(P14Syn));
g4 = 4*ones(size(P28Syn));
G = cat(1,g1,g2,g3,g4);
%%
Data_Syn = cat(1,P7Syn,P9Syn,P14Syn,P28Syn);
Data_SomFil = cat(1,P7SomFil,P9SomFil,P14SomFil,P28SomFil);
Data_InnSomFil = cat(1,P7InnSomFil,P9InnSomFil,P14InnSomFil,P28InnSomFil);

ErrSyn = [(std(P7Syn)/sqrt(numel(P7Syn))); (std(P9Syn)/sqrt(numel(P9Syn)));
          (std(P14Syn)/sqrt(numel(P14Syn))); (std(P28Syn)/sqrt(numel(P28Syn)))];
      
ErrSomFil = [(std(P7SomFil)/sqrt(numel(P7SomFil))); (std(P9SomFil)/sqrt(numel(P9SomFil)));
          (std(P14SomFil)/sqrt(numel(P14SomFil))); (std(P28SomFil)/sqrt(numel(P28SomFil)))];

ErrInnFil = [(std(P7InnSomFil)/sqrt(numel(P7InnSomFil))); (std(P9InnSomFil)/sqrt(numel(P9InnSomFil)));
          (std(P14InnSomFil)/sqrt(numel(P14InnSomFil))); (std(P28InnSomFil)/sqrt(numel(P28InnSomFil)))];
      
Age = [7; 9; 14; 28];
%%
figure;
% yyaxis left
boxplot(Data_SomFil,G,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 30])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('# somatic filopodia OR somatic spines')
xlabel('Postnatal Age (days)')
plot(Age,MeanSomFil,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(MeanSomFil+ErrSomFil),'--g')% lplus err line
plot(Age,(MeanSomFil-ErrSomFil),'--g')% lminus err line
scatter(ones(numel(P7SomFil),1)*7+rand(numel(P7SomFil),1)*.75-.45, P7SomFil,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P9SomFil),1)*9+rand(numel(P9SomFil),1)*.75-.45, P9SomFil,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P14SomFil),1)*14+rand(numel(P14SomFil),1)*.75-.45, P14SomFil,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P28SomFil),1)*28+rand(numel(P28SomFil),1)*.75-.45, P28SomFil,'x','MarkerEdgeColor','g','SizeData',200);
yyaxis right
boxplot(Data_Syn,G,'positions',Age,'Colors','k')
set(gca,'Ylim',[0 100])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
ylabel('# soma syn (excl fil/spine syn)')
xlabel('Postnatal Age (days)')
plot(Age,MeanSyn,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(MeanSyn+ErrSyn),'--m')% lplus err line
plot(Age,(MeanSyn-ErrSyn),'--m')% lminus err line
scatter(ones(numel(P7Syn),1)*7+rand(numel(P7Syn),1)*.75-.45, P7Syn,'x','MarkerEdgeColor','m','SizeData',200);
scatter(ones(numel(P9Syn),1)*9+rand(numel(P9Syn),1)*.75-.45, P9Syn,'x','MarkerEdgeColor','m','SizeData',200);
scatter(ones(numel(P14Syn),1)*14+rand(numel(P14Syn),1)*.75-.45, P14Syn,'x','MarkerEdgeColor','m','SizeData',200);
scatter(ones(numel(P28Syn),1)*28+rand(numel(P28Syn),1)*.75-.45, P28Syn,'x','MarkerEdgeColor','m','SizeData',200);
title('L4 - Development of somatic filopodia and spines')

%% Fraction of filopodia or spines innervated
P7frac = P7InnSomFil./P7SomFil;
P9frac = P9InnSomFil./P9SomFil;
P14frac = P14InnSomFil./P14SomFil;
P28frac = P28InnSomFil./P28SomFil;
MeanFrac = [mean(P7frac); mean(P9frac); mean(P14frac); mean(P28frac)];
ErrFrac = [(std(P7frac)/sqrt(numel(P7frac))); 
(std(P9frac)/sqrt(numel(P9frac)));
(std(P14frac)/sqrt(numel(P14frac)));
(std(P28frac)/sqrt(numel(P28frac)))];
Data_Frac = cat(1,P7frac,P9frac,P14frac,P28frac);

figure;
boxplot(Data_Frac,G,'positions',Age,'Colors','k')
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
ylabel('Fraction of innervated somatic filopodia OR spines')
xlabel('Postnatal Age (days)')
hold on
plot(Age,MeanFrac,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(MeanFrac+ErrFrac),'--g')% lplus err line
plot(Age,(MeanFrac-ErrFrac),'--g')% lminus err line
scatter(ones(numel(P7frac),1)*7+rand(numel(P7frac),1)*.75-.45, P7frac,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P9frac),1)*9+rand(numel(P9frac),1)*.75-.45, P9frac,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P14frac),1)*14+rand(numel(P14frac),1)*.75-.45, P14frac,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P28frac),1)*28+rand(numel(P28frac),1)*.75-.45, P28frac,'x','MarkerEdgeColor','g','SizeData',200);
title('L4 - Fraction of innervated somatic filopdia OR spines')

%% VERSION 2 - FOR SPLIT PLOTS
MeanInnSomFil = [mean(P7InnSomFil); mean(P9InnSomFil); mean(P14InnSomFil); mean(P28InnSomFil)];

figure
boxplot(Data_SomFil,G,'positions',Age,'Colors','k')
hold on
boxplot(Data_Syn,G,'positions',Age,'Colors','k')
hold on
boxplot(Data_InnSomFil,G,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 100])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('# somatic filopodia OR Soma synapses OR Innervated som fil')
xlabel('Postnatal Age (days)')
hold on
plot(Age,MeanSomFil,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(MeanSomFil+ErrSomFil),'--g')% lplus err line
plot(Age,(MeanSomFil-ErrSomFil),'--g')% lminus err line
plot(Age,MeanInnSomFil,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(MeanInnSomFil+ErrInnFil),'--g')% lplus err line
plot(Age,(MeanInnSomFil-ErrInnFil),'--g')% lminus err line
plot(Age,MeanSyn,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(MeanSyn+ErrSyn),'--m')% lplus err line
plot(Age,(MeanSyn-ErrSyn),'--m')% lminus err line
hold on
scatter(ones(numel(P7SomFil),1)*7+rand(numel(P7SomFil),1)*.75-.45, P7SomFil,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P9SomFil),1)*9+rand(numel(P9SomFil),1)*.75-.45, P9SomFil,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P14SomFil),1)*14+rand(numel(P14SomFil),1)*.75-.45, P14SomFil,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P28SomFil),1)*28+rand(numel(P28SomFil),1)*.75-.45, P28SomFil,'x','MarkerEdgeColor','g','SizeData',200);
hold on
scatter(ones(numel(P7InnSomFil),1)*7+rand(numel(P7InnSomFil),1)*.75-.45, P7InnSomFil,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P9InnSomFil),1)*9+rand(numel(P9InnSomFil),1)*.75-.45, P9InnSomFil,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P14InnSomFil),1)*14+rand(numel(P14InnSomFil),1)*.75-.45, P14InnSomFil,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P28InnSomFil),1)*28+rand(numel(P28InnSomFil),1)*.75-.45, P28InnSomFil,'x','MarkerEdgeColor','g','SizeData',200);
hold on
scatter(ones(numel(P7Syn),1)*7+rand(numel(P7Syn),1)*.75-.45, P7Syn,'x','MarkerEdgeColor','m','SizeData',200);
scatter(ones(numel(P9Syn),1)*9+rand(numel(P9Syn),1)*.75-.45, P9Syn,'x','MarkerEdgeColor','m','SizeData',200);
scatter(ones(numel(P14Syn),1)*14+rand(numel(P14Syn),1)*.75-.45, P14Syn,'x','MarkerEdgeColor','m','SizeData',200);
scatter(ones(numel(P28Syn),1)*28+rand(numel(P28Syn),1)*.75-.45, P28Syn,'x','MarkerEdgeColor','m','SizeData',200);
title('L4 - Development of somatic filopodia and somatic synapses')

%% For contribution of som fil synapses
P7fracFil = P7InnSomFil./(P7Syn+P7InnSomFil);
P9fracFil = P9InnSomFil./(P9Syn+P9InnSomFil);
P14fracFil = P14InnSomFil./(P14Syn+P14InnSomFil);
P28fracFil = P28InnSomFil./(P28Syn+P28InnSomFil);

MeanFracFil = [mean(P7fracFil); mean(P9fracFil); mean(P14fracFil); mean(P28fracFil)];
ErrFracFil = [(std(P7fracFil)/sqrt(numel(P7fracFil))); 
(std(P9fracFil)/sqrt(numel(P9fracFil)));
(std(P14fracFil)/sqrt(numel(P14fracFil)));
(std(P28fracFil)/sqrt(numel(P28fracFil)))];

Data_fracFil = cat(1,P7fracFil,P9fracFil,P14fracFil,P28fracFil);

figure;
boxplot(Data_fracFil,G,'positions',Age,'Colors','k')
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
set(gca,'XtickLabel',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
ylabel('Fraction of innervated somatic filopodia (or spines) over total target synapses')
xlabel('Postnatal Age (days)')
hold on
plot(Age,MeanFracFil,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
plot(Age,(MeanFracFil+ErrFracFil),'--g')% lplus err line
plot(Age,(MeanFracFil-ErrFracFil),'--g')% lminus err line
scatter(ones(numel(P7fracFil),1)*7+rand(numel(P7fracFil),1)*.75-.45, P7fracFil,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P9fracFil),1)*9+rand(numel(P9fracFil),1)*.75-.45, P9fracFil,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P14fracFil),1)*14+rand(numel(P14fracFil),1)*.75-.45, P14fracFil,'x','MarkerEdgeColor','g','SizeData',200);
scatter(ones(numel(P28fracFil),1)*28+rand(numel(P28fracFil),1)*.75-.45, P28fracFil,'x','MarkerEdgeColor','g','SizeData',200);
title('L4 - Contribution of filopodial innervation to overall somatic innervation')

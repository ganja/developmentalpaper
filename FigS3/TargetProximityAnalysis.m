%TARGET PROXIMITY ANALYSIS
%To check if axons are in certain proximity of a given target class.
% outDir = 'Z:\Data\goura\Analysis\TargetProximity\P9aL4Axons\';
skelAxons = skeleton('Z:\Data\goura\FigureUpdates\NMLFiles_08-05-2019\P9_L4_n2.nml');%nml with all tracings and trees
treeIndices = find(cellfun(@(x)any(strfind(x, 'Sm')==1),skelAxons.names));

%Delete trees with one node!
TreeNodes = zeros(length(treeIndices),1);
for i = 1:length(treeIndices)
TreeNodes(i) = length(getNodes(skelAxons,treeIndices(i)));
end

Tab = cat(2,TreeNodes,treeIndices);
Tab(Tab(:,1)<4,:) = [];
treeIndices = Tab(:,2);
names = skelAxons.names(treeIndices);
AxCount = size(treeIndices);

%Correct the names for any special characters
namesCorrect = {};
for i=1:AxCount
    namesCorrect{i} = strrep(names{i},'/','Or');
%     namesCorrect{i} = strrep(names{i},'?','Q');
%     namesCorrect{i} = strrep(names{i},'!','Q');
end
namesCorrect = namesCorrect';
% getting all the axon trees as individual skeletons!
% for i=1:AxCount
%     skelAxons.write(namesCorrect{i},treeIndices(i))
% end
% skelAx = {};
%% Get somas from soma annotated nml
skelSoma = skeleton('Z:\Data\goura\FigureUpdates\NMLFiles_08-05-2019\P9_L4_n2_SomaMap.nml');
SomaCentres_wk = skelSoma.getNodes;
SomaCentres = SomaCentres_wk.*skelSoma.scale;

for i=1:AxCount
    skelAx{i}=skeleton(['Z:\Data\goura\Analysis\TargetProximity\P9L4bSomaAxons\' namesCorrect{i},'.nml']);
end
numSomas = length(SomaCentres);

%% Calculate radii from tracings
DiaIdx = find(cellfun(@(x)any(strfind(x, 'Dia_Sm')==1),skelAxons.names));
DiaNames = skelAxons.names(DiaIdx);
DiaCount = size(DiaIdx);
DiaLengths = zeros(length(DiaNames),1);
for i = 1:DiaCount
 currentPathLength =(skelAxons.pathLength(DiaIdx(i)));
 currentPathLengthMicrons = currentPathLength/1000;
 DiaLengths(i) = currentPathLengthMicrons;
end
 MeanDia = mean(DiaLengths)
 Radius = MeanDia/2;
%% Proximity analysis
% outDir = 'Z:\Data\goura\Analysis\TargetProximity\Test\';
% radiusThr = [6800; 8800; 10800]; % For P7L4 - SomaDimeter = 11.6um
radiusThr = [7500; 9500; 11500]; % For P9L4 - SomaDiameter = 12.7um (give the treshold in nm scale i.e. radius of soma + proximity, eg. fo rsoma of 11um radiusThr = 5.5
% radiusThr = [9550; 11550; 13550]; %For P7 - values from new equivalent diameter calculation 
% radiusThr = [10535; 12535; 14535];%For P9 - values from new equivalent diameter calculation 
qNodes = cellfun(@(x) x.nodes{1}(:,1:3),skelAx,'uni',0);%(node wise approach)
scale = skelAxons.scale;
pLFracIn = zeros(length(skelAx),3);
pLFrac = zeros(length(skelAx),1);
for k = 1:length(radiusThr)
radiusThr_new = radiusThr(k);
for i = 1:length(skelAx)
    tic
    skel = skelAx{i};
    nodes_wk = qNodes{i};
    nodes_nm = nodes_wk.*scale;
    toKeep = false(size(nodes_nm,1),1);
        for j = 1:length(nodes_nm)
            distances = (pdist2(nodes_nm(j,:),SomaCentres))';
            distIN = distances <= radiusThr_new;
            if any(distIN)
                toKeep(j) = true;
            end
        end
        nodesToDel = ~toKeep;
        skelOut = skel.deleteNodes(1,nodesToDel,false);
        pLFrac(i) = skelOut.pathLength/skel.pathLength;
    toc
end
pLFracIn(:,k) = pLFrac;
end
sprintf('Done')
%% Plotting
Pathlength_fraction_1 = pLFracIn(:,1);
Pathlength_fraction_2 = pLFracIn(:,2);
Pathlength_fraction_3 = pLFracIn(:,3);

PL_1 = histcounts(Pathlength_fraction_1,0:0.1:1);
PL_2 = histcounts(Pathlength_fraction_2,0:0.1:1);
PL_3 = histcounts(Pathlength_fraction_3,0:0.1:1);

Specificity = histcounts(SomaInn,0:0.1:1);

figure;
bar(0.05:.1:0.95,Specificity,1,'g')
hold on
stairs([0:0.1:1]',[PL_1';PL_1(end)],'r','LineWidth',2,'LineStyle','--');
hold on
stairs([0:0.1:1]',[PL_2';PL_2(end)],'r','LineWidth',2,'LineStyle',':');
hold on
stairs([0:0.1:1]',[PL_3';PL_3(end)],'r','LineWidth',2,'LineStyle','-');
% axis([0 1 0 10])
set(gca,'Ylim',[0 20])
set(gca,'Xlim',[0 1])
box off
set(gca,'TickDir','out');
xlabel('Fraction of innervation OR axonal path length in proximity')
ylabel('# axons')
title('P28 - SomaProximity analysis - SomaAxons')
legend('Innervation fraction','Soma proximity - 1um','Soma proximity - 3um','Soma proximity - 5um');

%% Visualizing for verification
%as plot in Matlab
figure
hold on
skel.plot(1,[0,0,1])
skelOut.plot(1,[1,0,1])
scatter3(SomaCentres(:,1),SomaCentres(:,2),SomaCentres(:,3))

%in WK
skelOutJoined = skel.deleteNodes(1,nodesToDel,true);
skelOutJoined = skelOutJoined.addNodesAsTrees(SomaCentres_wk);
skelOutJoined.write(fullfile(outDir,'skelWithSomaProximal.nml'))

%% Plotting as scatter - 08-11-2019
figure;
% scatter(P7_Inn,P7_pL(:,1),200,'g','x')
% hold on
% scatter(P7_Inn,P7_pL(:,2),200,'g','x')
% hold on
scatter(P7_Inn,P7_pL(:,3),200,'g','x')
hold on
% scatter(P9_Inn,P9_pL(:,1),200,'c','x')
% hold on
% scatter(P9_Inn,P9_pL(:,2),200,'c','d')
% hold on
scatter(P9_Inn,P9_pL(:,3),200,'c','x')
% hold on
daspect([1 1 1])
box off
set(gca,'TickDir','out')
set(gca,'Xlim',[0 1])
set(gca,'Ylim',[0 1])


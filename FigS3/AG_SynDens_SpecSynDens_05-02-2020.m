%L4 -  synapse density
data = xlsread('Z:\Data\goura\FigureUpdates\RawData_Excels\L4_RawData_24-01-2020.xlsx','Sheet1');

%% SomaAxons - overall synapse density
ToSel_1 = find(data(:,1)== 7& data(:,3)== 3); %use the age and seeding as filter
SM7_pL = data(ToSel_1,4);
SM7_totalSyn = data(ToSel_1,6);
SM7_SomaSyn = data(ToSel_1,9);

ToSel_2 = find(data(:,1)== 9& data(:,3)== 3); %use the age and seeding as filter
SM9a_pL = data(ToSel_2,4);
SM9a_totalSyn = data(ToSel_2,6);
SM9a_SomaSyn = data(ToSel_2,9);
ToSel_3 = find(data(:,1)== 9.2& data(:,3)== 3); %use the age and seeding as filter
SM9b_pL = data(ToSel_3,4);
SM9b_totalSyn = data(ToSel_3,6);
SM9b_SomaSyn = data(ToSel_3,9);

ToSel_4 = find(data(:,1)== 14.1& data(:,3)== 3); %use the age and seeding as filter
SM14a_pL = data(ToSel_4,4);
SM14a_totalSyn = data(ToSel_4,6);
SM14a_SomaSyn = data(ToSel_4,9);
ToSel_5 = find(data(:,1)== 14.2& data(:,3)== 3); %use the age and seeding as filter
SM14b_pL = data(ToSel_5,4);
SM14b_totalSyn = data(ToSel_5,6);
SM14b_SomaSyn = data(ToSel_5,9);

ToSel_6 = find(data(:,1)== 28& data(:,3)== 3); %use the age and seeding as filter
SM28_pL = data(ToSel_6,4);
SM28_totalSyn = data(ToSel_6,6);
SM28_SomaSyn = data(ToSel_6,9);

SM9_pL = cat(1,SM9a_pL,SM9b_pL); 
SM9_totalSyn = cat(1,SM9a_totalSyn,SM9b_totalSyn);
SM9_SomaSyn = cat(1,SM9a_SomaSyn,SM9b_SomaSyn);
SM14_pL = cat(1,SM14a_pL,SM14b_pL);
SM14_totalSyn = cat(1,SM14a_totalSyn,SM14b_totalSyn);
SM14_SomaSyn = cat(1,SM14a_SomaSyn,SM14b_SomaSyn);

%% Bulk density and errors

SM7dens = SM7_totalSyn./SM7_pL;    BulkSM7dens = sum(SM7_totalSyn)/sum(SM7_pL);
SM9dens = SM9_totalSyn./SM9_pL;    BulkSM9dens = sum(SM9_totalSyn)/sum(SM9_pL);
SM14dens = SM14_totalSyn./SM14_pL; BulkSM14dens = sum(SM14_totalSyn)/sum(SM14_pL);
SM28dens = SM28_totalSyn./SM28_pL; BulkSM28dens = sum(SM28_totalSyn)/sum(SM28_pL);


SM7NsDens = (SM7_totalSyn - SM7_SomaSyn)./SM7_pL;       BulkSM7NsDens = (sum(SM7_totalSyn)-sum(SM7_SomaSyn))/sum(SM7_pL);
SM9NsDens = (SM9_totalSyn - SM9_SomaSyn)./SM9_pL;       BulkSM9NsDens = (sum(SM9_totalSyn)-sum(SM9_SomaSyn))/sum(SM9_pL);
SM14NsDens = (SM14_totalSyn - SM14_SomaSyn)./SM14_pL;   BulkSM14NsDens = (sum(SM14_totalSyn)-sum(SM14_SomaSyn))/sum(SM14_pL);
SM28NsDens = (SM28_totalSyn - SM28_SomaSyn)./SM28_pL;   BulkSM28NsDens = (sum(SM28_totalSyn)-sum(SM28_SomaSyn))/sum(SM28_pL);

%%  AD axons 
ToSel_1 = find(data(:,1)== 7& data(:,3)== 1); %use the age and seeding as filter
SM7_pL = data(ToSel_1,4);
SM7_totalSyn = data(ToSel_1,6);
SM7_SomaSyn = data(ToSel_1,9);

ToSel_2 = find(data(:,1)== 9& data(:,3)== 1); %use the age and seeding as filter
SM9a_pL = data(ToSel_2,4);
SM9a_totalSyn = data(ToSel_2,6);
SM9a_SomaSyn = data(ToSel_2,9);
ToSel_3 = find(data(:,1)== 9.2& data(:,3)== 1); %use the age and seeding as filter
SM9b_pL = data(ToSel_3,4);
SM9b_totalSyn = data(ToSel_3,6);
SM9b_SomaSyn = data(ToSel_3,9);

ToSel_4 = find(data(:,1)== 14.1& data(:,3)== 1); %use the age and seeding as filter
SM14a_pL = data(ToSel_4,4);
SM14a_totalSyn = data(ToSel_4,6);
SM14a_SomaSyn = data(ToSel_4,9);
ToSel_5 = find(data(:,1)== 14.2& data(:,3)== 1); %use the age and seeding as filter
SM14b_pL = data(ToSel_5,4);
SM14b_totalSyn = data(ToSel_5,6);
SM14b_SomaSyn = data(ToSel_5,9);

ToSel_6 = find(data(:,1)== 28& data(:,3)== 1); %use the age and seeding as filter
SM28_pL = data(ToSel_6,4);
SM28_totalSyn = data(ToSel_6,6);
SM28_SomaSyn = data(ToSel_6,9);

SM9_pL = cat(1,SM9a_pL,SM9b_pL); 
SM9_totalSyn = cat(1,SM9a_totalSyn,SM9b_totalSyn);
SM9_SomaSyn = cat(1,SM9a_SomaSyn,SM9b_SomaSyn);
SM14_pL = cat(1,SM14a_pL,SM14b_pL);
SM14_totalSyn = cat(1,SM14a_totalSyn,SM14b_totalSyn);
SM14_SomaSyn = cat(1,SM14a_SomaSyn,SM14b_SomaSyn);

% Bulk density and errors

SM7dens = SM7_totalSyn./SM7_pL;    BulkSM7dens = sum(SM7_totalSyn)/sum(SM7_pL);
SM9dens = SM9_totalSyn./SM9_pL;    BulkSM9dens = sum(SM9_totalSyn)/sum(SM9_pL);
SM14dens = SM14_totalSyn./SM14_pL; BulkSM14dens = sum(SM14_totalSyn)/sum(SM14_pL);
SM28dens = SM28_totalSyn./SM28_pL; BulkSM28dens = sum(SM28_totalSyn)/sum(SM28_pL);


SM7NsDens = (SM7_totalSyn - SM7_SomaSyn)./SM7_pL;       BulkSM7NsDens = (sum(SM7_totalSyn)-sum(SM7_SomaSyn))/sum(SM7_pL);
SM9NsDens = (SM9_totalSyn - SM9_SomaSyn)./SM9_pL;       BulkSM9NsDens = (sum(SM9_totalSyn)-sum(SM9_SomaSyn))/sum(SM9_pL);
SM14NsDens = (SM14_totalSyn - SM14_SomaSyn)./SM14_pL;   BulkSM14NsDens = (sum(SM14_totalSyn)-sum(SM14_SomaSyn))/sum(SM14_pL);
SM28NsDens = (SM28_totalSyn - SM28_SomaSyn)./SM28_pL;   BulkSM28NsDens = (sum(SM28_totalSyn)-sum(SM28_SomaSyn))/sum(SM28_pL);


ErrDens = [std(SM7dens)/sqrt(numel(SM7dens));
std(SM9dens)/sqrt(numel(SM9dens));
std(SM14dens)/sqrt(numel(SM14dens));
std(SM28dens)/sqrt(numel(SM28dens))];

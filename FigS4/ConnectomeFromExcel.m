%Visualizing connectome from excel

A = xlsread('Z:\Data\goura\Analysis\P14_L23\P14_numConnectivityMatrix_04.11.2016.xlsx')
figure;
imagesc(A)
daspect([1 1 1])

colormap ()

%%




B = A;
figure;
bar(B)
ylabel('Input Syn #')
xlabel('Somata')
imagesc(A)
daspect([1 1 1])
colormap 'hsv'

%%
B = xlsread('Z:\Data\goura\Analysis\P14_L23\AISaxLengths.xlsx')
%C = flipud(B)
C = flipud(B)
figure;
bar(C)
figure;



bar(B)
h = stairs(A,B,C)



%ylim ([0 600])

%% 
A1 = xlsread('Z:\Data\goura\Analysis\P14_L4\PercentSoma.xlsx');
A2 = xlsread('Z:\Data\goura\Analysis\P14_L4\PercentAIS.xlsx');
A3 = xlsread('Z:\Data\goura\Analysis\P14_L4\PercentApical.xlsx');

figure;
h1 = stairs(A1);

hold on
h2 = stairs(A2);
hold on
h3 = stairs(A3);




%% 
x = xlsread('Z:\Data\goura\Analysis\P14_L4\AisSpecificity.xlsx');
y = xlsread('Z:\Data\goura\Analysis\P14_L4\SomaSpecificity.xlsx');
a = xlsread('Z:\Data\goura\Analysis\P14_L4\TotalSyn_AISax.xlsx');

figure;
X1 = histogram(x,5);
hold on
Y1 = histogram(y,20);




figure;
scatter(x,y,'filled');
scatter(x,a,'filled')
hold on
scatter(y,a,'r')
xlabel('# total soma syn');
ylabel('# total dend syn');
ax = gca;
xlim ([0 80])
ylim ([0 80])
set(ax,'XTick',[0:5:80]);
set(ax,'YTick',[0:5:80]);
title('P9 L4: Total Somatic Syn vs Total Dendritic Syn');
Line1 = refline(1);
set(Line1,'LineStyle',':');
Line1.Color = 'r';
% Line2 = refline(0.5);
% set(Line2,'LineStyle',':');
% Line2.Color = 'c';
% Line3 = refline(0.25);
% set(Line3,'LineStyle',':');
% Line3.Color = 'g';
% Line4 = refline(0.75);
% set(Line4,'LineStyle',':');
% Line4.Color = 'm';

%% Fractional Specificity Line plots

A = xlsread('Z:\Data\goura\Analysis\P14_L4\Fractional Specifcity\SeedingBias\FractionalSyn_seedingBias.xlsx');
A1 = A';
figure(1);
plot(A);

B = xlsread('Z:\Data\goura\Analysis\P14_L4\Fractional Specifcity\NoSeedingBias\FractionSyn_noBias.xlsx');
figure(2);
plot(B);

C = xlsread('Z:\Data\goura\Analysis\P14_L4\Fractional Specifcity\NoThresholding\FractionalSyn_bias_noThr.xlsx');
D = C';
figure(3);
plot(D);

E = xlsread('Z:\Data\goura\Analysis\P14_L4\Fractional Specifcity\NoThresholding\FractionalSyn_noBias_noThr.xlsx');
F = E';
figure(4);
plot(F);

h1 = hist(A1)
figure(5);
stairs(h1)

h2 = hist(A)
figure(6);
stairs(h2)

a1 = A(1,:);
a1p = a1';
a2 = A(2,:);
a2p = a2'
a3 = A(3,:);
a3p = a3';
a4 = A(4,:);
a5 = A(5,:);
a6 = A(6,:);
a7 = A(7,:);
a8 = A(8,:);
a9 = A(9,:);

h1 = hist(a1p);
h2 = hist(a2p);
h3 = hist(a3p);



figure(7);
stairs(h1);
hold on
stairs(h2);
hold on
stairs(h3);
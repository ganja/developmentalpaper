% Somatic and perisomatic specificity
%skel = skeleton('Z:\Data\goura\FigureUpdates\NMLFiles_08-05-2019\P14_L4_n1.nml');%nml with all tracings and trees
treeIndices = find(cellfun(@(x)any(strfind(x, 'Ax0')==1),skel.names));

%Delete trees with one node!
TreeNodes = zeros(length(treeIndices),1);
for i = 1:length(treeIndices)
TreeNodes(i) = length(getNodes(skel,treeIndices(i)));
end

Tab = cat(2,TreeNodes,treeIndices);
Tab(Tab(:,1)<4,:) = [];
treeIndices = Tab(:,2);
names = skel.names(treeIndices);
AxCount = size(treeIndices);

%Correct the names for any special characters
namesCorrect = {};
for i=1:AxCount
    namesCorrect{i} = strrep(names{i},'/','Or');
%     namesCorrect{i} = strrep(names{i},'?','Q');
%     namesCorrect{i} = strrep(names{i},'!','Q');
end
namesCorrect = namesCorrect';

%% Getting syn data
AllSyn = skel.getNodesWithComment('syn',treeIndices,'partial'); %first get all syn
SomaComments = skel.getNodesWithComment('Soma',treeIndices,'partial');%for soma axons
PeriSomaComments = skel.getNodesWithComment('proximal',treeIndices,'partial');%for soma axons
SomaSyn = {};% for soma axons
for i = 1:AxCount
SomaSyn{i}= intersect(AllSyn{i},SomaComments{i});
end
SomaSyn = SomaSyn';
PeriSomaSyn = {};
for i = 1:AxCount
PeriSomaSyn{i}= intersect(AllSyn{i},PeriSomaComments{i});
end
PeriSomaSyn = PeriSomaSyn';


%num data
SSsyn = zeros(length(namesCorrect),1);
TotalSyn = zeros(length(namesCorrect),1);
InnFractions = zeros(length(namesCorrect),1);
SpecSynDens = zeros(length(namesCorrect),1);
OtherSynDens = zeros(length(namesCorrect),1);
pL = zeros(length(namesCorrect),1);
for i = 1:AxCount
    SomaSyn_num = numel(SomaSyn{i});
    PeriSomaSyn_num = numel(PeriSomaSyn{i});
    SSsyn(i) = SomaSyn_num+PeriSomaSyn_num;
    TotalSyn(i) = numel(AllSyn{i});
    InnFractions(i) = ((SomaSyn_num+PeriSomaSyn_num)-1)/(numel(AllSyn{i})-1);
    pL(i) = skel.pathLength(treeIndices(i))/1000;
    SpecSynDens(i) = SSsyn(i) / pL(i);
    OtherSynDens(i) = (TotalSyn(i) - SSsyn(i))/pL(i);
end
SpecSynDens_Bulk = sum(SSsyn)/sum(pL);
OtherSynDens_Bulk = (sum(TotalSyn) - sum(SSsyn))/sum(pL);
PeriSomInn_Bulk = (sum(SSsyn) - length(SSsyn))/(sum(TotalSyn)-length(SSsyn));
%% Concatanate and plot
P9_OtherSynDens = cat(1,P9a_OtherSynDens,P9b_OtherSynDens);
P14_OtherSynDens = cat(1,P14a_OtherSynDens,P14b_OtherSynDens);

P9_SpecSynDens = cat(1,P9a_SpecSynDens,P9b_SpecSynDens);
P14_SpecSynDens = cat(1,P14a_SpecSynDens,P14b_SpecSynDens);

P9_PeriSomInnFractions = cat(1,P9a_PeriSomInnFractions,P9b_PeriSomInnFractions);
P14_PeriSomInnFractions = cat(1,P14a_PeriSomInnFractions,P14b_PeriSomInnFractions);

g1 = ones(size(P7_OtherSynDens));
g2 = 2*ones(size(P9_OtherSynDens));
g3 = 3*ones(size(P14_OtherSynDens));
g4 = 4*ones(size(P28_OtherSynDens));

% Errors
P7_OtherSynDens_bstrp = bootstrp(10,@mean,P7_OtherSynDens); SE_OtherSynDens_7 = std(P7_OtherSynDens_bstrp);
P9_OtherSynDens_bstrp = bootstrp(10,@mean,P9_OtherSynDens); SE_OtherSynDens_9 = std(P9_OtherSynDens_bstrp);
P14_OtherSynDens_bstrp = bootstrp(10,@mean,P14_OtherSynDens); SE_OtherSynDens_14 = std(P14_OtherSynDens_bstrp);
P28_OtherSynDens_bstrp = bootstrp(10,@mean,P28_OtherSynDens); SE_OtherSynDens_28 = std(P28_OtherSynDens_bstrp);

P7_SpecSynDens_bstrp = bootstrp(10,@mean,P7_SpecSynDens); SE_SpecSynDens_7 = std(P7_SpecSynDens_bstrp);
P9_SpecSynDens_bstrp = bootstrp(10,@mean,P9_SpecSynDens); SE_SpecSynDens_9 = std(P9_SpecSynDens_bstrp);
P14_SpecSynDens_bstrp = bootstrp(10,@mean,P14_SpecSynDens); SE_SpecSynDens_14 = std(P14_SpecSynDens_bstrp);
P28_SpecSynDens_bstrp = bootstrp(10,@mean,P28_SpecSynDens); SE_SpecSynDens_28 = std(P28_SpecSynDens_bstrp);

P7_PeriSomInn_bstrp = bootstrp(10,@mean,P7_PeriSomInnFractions); SE_PeriSomInn_7 = std(P7_PeriSomInn_bstrp);
P9_PeriSomInn_bstrp = bootstrp(10,@mean,P9_PeriSomInnFractions); SE_PeriSomInn_9 = std(P9_PeriSomInn_bstrp);
P14_PeriSomInn_bstrp = bootstrp(10,@mean,P14_PeriSomInnFractions); SE_PeriSomInn_14 = std(P14_PeriSomInn_bstrp);
P28_PeriSomInn_bstrp = bootstrp(10,@mean,P28_PeriSomInnFractions); SE_PeriSomInn_28 = std(P28_PeriSomInn_bstrp);

BootStrpData_OtherSynDens = cat(2,P7_OtherSynDens_bstrp,P9_OtherSynDens_bstrp,P14_OtherSynDens_bstrp,P28_OtherSynDens_bstrp);
BootStrpData_SpecSynDens = cat(2,P7_SpecSynDens_bstrp,P9_SpecSynDens_bstrp,P14_SpecSynDens_bstrp,P28_SpecSynDens_bstrp);
BootStrpData_PeriSomInn = cat(2,P7_PeriSomInn_bstrp,P9_PeriSomInn_bstrp,P14_PeriSomInn_bstrp,P28_PeriSomInn_bstrp);

SE_OtherSynDens = cat(1, SE_OtherSynDens_7,SE_OtherSynDens_9,SE_OtherSynDens_14,SE_OtherSynDens_28);
SE_SpecSynDens = cat(1,SE_SpecSynDens_7,SE_SpecSynDens_9,SE_SpecSynDens_14,SE_SpecSynDens_28);
SE_PeriSomInn = cat(1,SE_PeriSomInn_7,SE_PeriSomInn_9,SE_PeriSomInn_14,SE_PeriSomInn_28);

OtherSynDens_Bulk = cat(1,P7_OtherSynDens_Bulk,P9_OtherSynDens_Bulk,P14_OtherSynDens_Bulk,P28_OtherSynDens_Bulk);
SpecSynDens_Bulk = cat(1,P7_SpecSynDens_Bulk,P9_SpecSynDens_Bulk,P14_SpecSynDens_Bulk,P28_SpecSynDens_Bulk);
PeriSomInn_Bulk = cat(1,P7_PeriSomInn_Bulk,P9_PeriSomInn_Bulk,P14_PeriSomInn_Bulk,P28_PeriSomInn_Bulk);



SE_OtherSynDens_plusLine = OtherDens_Bulk + SE_OtherSynDens;
SE_SpecSynDens_plusLine = SpecSynDens_Bulk + SE_SpecSynDens;
SE_PeriSomInn_plusLine = PeriSomInn_Bulk + SE_PeriSomInn;
SE_OtherSynDens_minusLine = OtherDens_Bulk - SE_OtherSynDens;
SE_SpecSynDens_minusLine = SpecSynDens_Bulk - SE_SpecSynDens;
SE_PeriSomInn_minusLine = PeriSomInn_Bulk - SE_PeriSomInn;

% Concatanete the data
InnData =  cat(1,P7_PeriSomInnFractions,P9_PeriSomInnFractions,P14_PeriSomInnFractions,P28_PeriSomInnFractions);
OthSynData = cat(1, P7_OtherSynDens,P9_OtherSynDens,P14_OtherSynDens,P28_OtherSynDens);
SpecSynData = cat(1,P7_SpecSynDens,P9_SpecSynDens,P14_SpecSynDens,P28_SpecSynDens);
Group = cat(1,g1,g2,g3,g4);

%% Figures
Age = [7 9 14 28]
figure;
boxplot(InnData,Group,'positions',Age,'Colors','k','Widths',1.8)
hold on
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Soma and peri-somatic innervation fraction')
xlabel('Postnatal Age (days)')
hold on
% plot(Age,PeriSomInn_Bulk,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
% hold on
% plot(Age,SE_PeriSomInn_plusLine,'--k')% lplus err line
% hold on
% plot(Age,SE_PeriSomInn_minusLine,'--k')% minus err line
hold on
scatter(ones(numel(P7_PeriSomInnFractions),1)*7+rand(numel(P7_PeriSomInnFractions),1)*.75-.45, P7_PeriSomInnFractions,'x','MarkerEdgeColor','k','SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(P9_PeriSomInnFractions),1)*9+rand(numel(P9_PeriSomInnFractions),1)*.75-.45, P9_PeriSomInnFractions,'x','MarkerEdgeColor','k','SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(P14_PeriSomInnFractions),1)*14+rand(numel(P14_PeriSomInnFractions),1)*.75-.45, P14_PeriSomInnFractions,'x','MarkerEdgeColor','k','SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(P28_PeriSomInnFractions),1)*28+rand(numel(P28_PeriSomInnFractions),1)*.75-.45, P28_PeriSomInnFractions,'x','MarkerEdgeColor','k','SizeData',200, 'LineWidth',0.0005);
hold on
title('L4 Innervation fractions - Soma axons (somatic and peri-somatic)')

figure;
boxplot(OthSynData,Group,'positions',Age,'Colors','k','Widths',1.8)
hold on
boxplot(SpecSynData,Group,'positions',Age,'Colors','k','Widths',1.8)
hold on
set(gca,'Ylim',[0 0.6])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Specific and Non-specific syn density')
xlabel('Postnatal Age (days)')
hold on
% plot(Age,PeriSomInn_Bulk,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
% hold on
% plot(Age,SE_PeriSomInn_plusLine,'--k')% lplus err line
% hold on
% plot(Age,SE_PeriSomInn_minusLine,'--k')% minus err line
hold on
scatter(ones(numel(P7_SpecSynDens),1)*7+rand(numel(P7_SpecSynDens),1)*.75-.45, P7_SpecSynDens,'x','MarkerEdgeColor','r','SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(P9_SpecSynDens),1)*9+rand(numel(P9_SpecSynDens),1)*.75-.45, P9_SpecSynDens,'x','MarkerEdgeColor','r','SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(P14_SpecSynDens),1)*14+rand(numel(P14_SpecSynDens),1)*.75-.45, P14_SpecSynDens,'x','MarkerEdgeColor','r','SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(P28_SpecSynDens),1)*28+rand(numel(P28_SpecSynDens),1)*.75-.45, P28_SpecSynDens,'x','MarkerEdgeColor','r','SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(P7_OtherSynDens),1)*7+rand(numel(P7_OtherSynDens),1)*.75-.45, P7_OtherSynDens,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(P9_OtherSynDens),1)*9+rand(numel(P9_OtherSynDens),1)*.75-.45, P9_OtherSynDens,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(P14_OtherSynDens),1)*14+rand(numel(P14_OtherSynDens),1)*.75-.45, P14_OtherSynDens,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(P28_OtherSynDens),1)*28+rand(numel(P28_OtherSynDens),1)*.75-.45, P28_OtherSynDens,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200, 'LineWidth',0.0005);
hold on
title('L4 Specific and Non-specific syn density - Soma axons (somatic and peri-somatic)')
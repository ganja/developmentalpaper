%Code for axon gallery

skel = skeleton('Z:\Data\goura\FigureUpdates\NMLFiles_08-05-2019\P28_L23.nml');

treeID = find(cellfun(@(x)any(strfind(x, 'Sm0')==1),skel.names));
treeCount = numel(treeID);
treeName = skel.names(treeID);
%check if that is all the trees you need!
namesCorrect = {};
for i=1:treeCount
%     namesCorrect{i} = strrep(treeName{i},'/','Or');
    namesCorrect{i} = strrep(treeName{i},'?','Q');
%     namesCorrect{i} = strrep(names{i},'!','Q');
end

%% Get syn data for plotting
%For Soma axons
AllSyn = skel.getNodesWithComment('syn',treeID,'partial'); %first get all syn
SomaComments = skel.getNodesWithComment('Soma',treeID,'partial');%for soma axons

SomaSyn = {};% for soma axons
for i = 1:treeCount
SomaSyn{i}= intersect(AllSyn{i},SomaComments{i})
end
SomaSyn = SomaSyn'

OtherSyn = {}; % for soma syn
for i = 1:treeCount
OtherSyn{i}=setdiff(AllSyn{i},SomaSyn{i})
end
OtherSyn = OtherSyn'

SpecSyn = {};
NonSpecSyn = {};
for i = 1:treeCount
SpecSyn{i} = (skel.getNodes(treeID(i),SomaSyn(i)).*skel.scale)
NonSpecSyn{i} = (skel.getNodes(treeID(i),OtherSyn(i)).*skel.scale)
end

SpecSyn = SpecSyn';
NonSpecSyn = NonSpecSyn';

pathLengths = zeros(treeCount,1); %All the path lengths will be stored in TreeLengths :)
for i = 1:treeCount
 currentPathLength =(skel.pathLength(treeID(i)));
 currentPathLengthMicrons = currentPathLength/1000;
 pathLengths(i) = currentPathLengthMicrons;
end

TotalSyn = zeros(treeCount,1);
InnFractions = zeros(treeCount,1);
%soma Axons
for i = 1:treeCount
SomaSyn_num = numel(SomaSyn{i});
OtherSyn_num = numel(OtherSyn{i});
TotalSyn(i) = SomaSyn_num + OtherSyn_num;
InnFractions(i) = (SomaSyn_num - 1)/((SomaSyn_num -1) + OtherSyn_num);
end

SynDensity = TotalSyn./pathLengths;
%% For AD axons
AllSyn = skel.getNodesWithComment('syn',treeID,'partial'); %first get all syn
ADComments = skel.getNodesWithComment('AD',treeID,'partial'); 

ADSyn = {};
for i = 1:treeCount
ADSyn{i}= intersect(AllSyn{i},ADComments{i})
end
ADSyn = ADSyn'

OtherSyn = {};
for i = 1:treeCount
OtherSyn{i}=setdiff(AllSyn{i},ADSyn{i})
end
OtherSyn = OtherSyn'

SpecSyn = {};
NonSpecSyn = {};
for i = 1:treeCount
SpecSyn{i} = (skel.getNodes(treeID(i),ADSyn(i)).*skel.scale)
NonSpecSyn{i} = (skel.getNodes(treeID(i),OtherSyn(i)).*skel.scale)
end
SpecSyn = SpecSyn';
NonSpecSyn = NonSpecSyn';

pathLengths = zeros(treeCount,1); %All the path lengths will be stored in TreeLengths :)
for i = 1:treeCount
 currentPathLength =(skel.pathLength(treeID(i)));
 currentPathLengthMicrons = currentPathLength/1000;
 pathLengths(i) = currentPathLengthMicrons;
end

TotalSyn = zeros(treeCount,1);
InnFractions = zeros(treeCount,1);

%AD axons
for i = 1:treeCount
ADSyn_num = numel(ADSyn{i});
OtherSyn_num = numel(OtherSyn{i});
TotalSyn(i) = ADSyn_num + OtherSyn_num;
InnFractions(i) = (ADSyn_num - 1)/((ADSyn_num -1) + OtherSyn_num);
end

SynDensity = TotalSyn./pathLengths;
%% For AIS axons
AllSyn = skel.getNodesWithComment('syn',treeID,'partial'); %first get all syn
ISComments = skel.getNodesWithComment('AIS',treeID,'partial'); 

ISSyn = {};
for i = 1:treeCount
ISSyn{i}= intersect(AllSyn{i},ISComments{i})
end
ISSyn = ISSyn'

OtherSyn = {};
for i = 1:treeCount
OtherSyn{i}=setdiff(AllSyn{i},ISSyn{i})
end
OtherSyn = OtherSyn'

SpecSyn = {};
NonSpecSyn = {};
for i = 1:treeCount
SpecSyn{i} = (skel.getNodes(treeID(i),ISSyn(i)).*skel.scale)
NonSpecSyn{i} = (skel.getNodes(treeID(i),OtherSyn(i)).*skel.scale)
end
SpecSyn = SpecSyn';
NonSpecSyn = NonSpecSyn';

pathLengths = zeros(treeCount,1); %All the path lengths will be stored in TreeLengths :)
for i = 1:treeCount
 currentPathLength =(skel.pathLength(treeID(i)));
 currentPathLengthMicrons = currentPathLength/1000;
 pathLengths(i) = currentPathLengthMicrons;
end

TotalSyn = zeros(treeCount,1);
InnFractions = zeros(treeCount,1);

%AIS axons
for i = 1:treeCount
ISSyn_num = numel(ISSyn{i});
OtherSyn_num = numel(OtherSyn{i});
TotalSyn(i) = ISSyn_num + OtherSyn_num;
InnFractions(i) = (ISSyn_num - 1)/((ISSyn_num -1) + OtherSyn_num);
end

SynDensity = TotalSyn./pathLengths;
%%
% DoubleSyn = skel.getNodesWithComment('2x',treeID,'partial'); %first get all syn
% IncomingSyn = skel.getNodesWithComment('incoming',treeID,'partial'); %first get all syn
% Syn = [AllSyn DoubleSyn IncomingSyn];

% ISComments = skel.getNodesWithComment('Soma',treeID,'partial');% from the comments choose only cnodes having 'IS' in comments
% ISsyn = intersect(AllSyn,ISComments);%get an intersection to have 'IS"with syn
% OtherSyn = setdiff(AllSyn,ISsyn);
%% Figures for axon galleries
treeColor = [0 0 0] %for black axon skeletons
umScale = true;
lineWidths = 1.5;
SynSize = 36;
Col_NonSyn = [0 0 0]
Col_SomSyn = [1 0 0]
Bbox = skel.getBbox;
Bbox_X = ((Bbox(1,2) - Bbox(1,1))*11.24)/1000; %bounding box in x
Bbox_Y = ((Bbox(2,2) - Bbox(2,1))*11.24)/1000;%bounding box in y
Bbox_Z = ((Bbox(3,2) - Bbox(3,1))*30)/1000;%bounding box in z
dirOut = 'Z:\Data\goura\FigureUpdates\AxonGallery\P28L23\SomaAxons\14-06-2019\';
for i = 1:treeCount
   NonSyn = (cell2mat(NonSpecSyn(i)))./1000; 
   SomSyn = (cell2mat(SpecSyn(i)))./1000; 
   figure ('Position',get(0,'Screensize')) %saves figure in full size window
   skel.plot(treeID(i),treeColor,umScale,lineWidths)
   hold on
   scatter3(NonSyn(:,1),NonSyn(:,2),NonSyn(:,3),SynSize,Col_NonSyn,'filled')
   hold on
   scatter3(SomSyn(:,1),SomSyn(:,2),SomSyn(:,3),SynSize,Col_SomSyn,'filled')
   hold on
   daspect([1 1 1])
   set(gca,'Xlim',[0 120]); %for P7 L4, P9L4n1, P14L4n2, P28L4 - [0 80], P9L4n2 [0 120], P9L23 - [0 100]
   set(gca,'Ylim',[0 90]);%for P7 L4, P9L4n1, p14L4n2, P28L4 - [0 110], P9L4n2 [0 90], P9L23 - [0 120], P!$L2 - [0 90]
   set(gca,'zlim',[0 250]);%for P7 L4, P9L4n1, P28L4 - [0 180], P9L4n2 [0 180]
   set(gca,'TickDir','out') 
%    set(gca,'YAxisLocation','right'); %for P28L4
    set(gca,'YAxisLocation','left'); %for P28L4
   set(gca,'XAxisLocation','bottom'); %for  P14L2
   box off 
%    title(treeName(i))
   str = sprintf('Axon - %s \nSynapse density - %d syn/micron \nInnervation fraction - %d \nPath length - %d microns \nTotal synapses - %d',treeName{i},SynDensity(i),InnFractions(i),pathLengths(i),TotalSyn(i));
   title(str)
%    annotation('textbox',[0.7 0.825 0.1 0.1],'String',str) % position of textbox for P7 L4 - [0.7 0.825 0.1 0.1]
%    view([0 90])%for xy view  for P7 L4
   view([0 90])% for xy view for P9 L4 n1; P14 L4 n2; for P9L4n2 - [0 -90] 
%    camroll(10)% for xy vie - pia top, WM bottom (P9L4n2) dataset - (90); P28L4 - (-90); P14L2 - (90)
   print(fullfile(fullfile(dirOut,namesCorrect{i})),'-dpdf','-fillpage')
end
sprintf('**** done ****')

%% Figure for axon galleries - AD axons
treeColor = [0 0 0] %for black axon skeletons
umScale = true;
lineWidths = 1.5;
SynSize = 36;
Col_NonSyn = [0 0 0]
Col_SomSyn = [1 0 0]
Bbox = skel.getBbox;
Bbox_X = ((Bbox(1,2) - Bbox(1,1))*11.24)/1000; %bounding box in x
Bbox_Y = ((Bbox(2,2) - Bbox(2,1))*11.24)/1000;%bounding box in y
Bbox_Z = ((Bbox(3,2) - Bbox(3,1))*30)/1000;%bounding box in z
dirOut = 'Z:\Data\goura\FigureUpdates\AxonGallery\P14L4 - n2\ADaxons\';

for i = 1:treeCount
   NonSyn = (cell2mat(NonSpecSyn(i)))./1000; 
   SomSyn = (cell2mat(SpecSyn(i)))./1000; 
   figure ('Position',get(0,'Screensize')) %saves figure in full size window
   skel.plot(treeID(i),treeColor,umScale,lineWidths)
   hold on
   scatter3(NonSyn(:,1),NonSyn(:,2),NonSyn(:,3),SynSize,Col_NonSyn,'filled')
   hold on
   scatter3(SomSyn(:,1),SomSyn(:,2),SomSyn(:,3),SynSize,Col_SomSyn,'filled')
   hold on
   daspect([1 1 1])
   set(gca,'Xlim',[0 80]); 
   set(gca,'Ylim',[0 100]);
   set(gca,'zlim',[0 140]);
   set(gca,'TickDir','out') 
   box off 
   title(treeName(i))
%    view([0 90])%for xy view  for P7 L4
   view([0 -90])% for xy view for P9 L4 n1; P14 L4 n2
   camroll(90)% for xy vie - pia top, WM bottom (P9L4n2) dataset
   print(fullfile(fullfile(dirOut,treeName{i})),'-dpdf','-fillpage')
end
## About Developmental paper code repository:

This repository contains all the code and data used for the following manuscript:   
**Postnatal connectomic development of inhibitory axons in mouse barrel cortex**   
Anjali Gour, Kevin M. Boergens, Philip Laserstein, Yunfeng Hua, Moritz Helmstaedter   
  

This code repository was developed at the Max Planck Insitute for brain research (2015-2019)




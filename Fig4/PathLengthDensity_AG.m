%% GETTING CENTRE OF AXONS AND CALCULTING LENGTH DENSITY

skel = skeleton('Z:\Data\goura\FigureUpdates\L23\NML\AG_P14_L23.nml');% original skeleton
AxonTrees = find(cellfun(@(x)any(strfind(x, 'ChAx001')==1),skel.names));
AxonCount = numel(AxonTrees);
AxonNames = skel.names(AxonTrees);

%% Analysis parameters
MeasuringBox = [20 20 20] %give the bbox for analysis in um
voxelSize = [11.24 11.24 30].*10^-3 %dataset resolution in nm
MeasuringBox_vx = MeasuringBox./voxelSize; %analysis bbox in vx
DatasetBbox = skel.getBbox;

%% Delete empty trees or trees only with one node!

TreeNodes = zeros(length(AxonTrees),1)
for i = 1:length(AxonTrees)
TreeNodes(i) = length(getNodes(skel,AxonTrees(i)));
end

Tab = cat(2,TreeNodes,AxonTrees);
Tab(Tab(:,1)<4,:) = [];

AxonTrees_New = Tab(:,2);
AxonCount_New = numel(AxonTrees_New);
AxonNames_new = skel.names(AxonTrees_New);

%% Get path lengths and branch points
AxonLengths = zeros(AxonCount_New,1); %All the path lengths will be stored in TreeLengths :)
AxonBranchPoints = zeros(AxonCount_New,1);
for i = 1:AxonCount_New
 currentPathLength =(skel.pathLength(AxonTrees_New(i)));
 currentPathLengthMicrons = currentPathLength/1000;
 AxonLengths(i) = currentPathLengthMicrons;
 AxonBranchPoints(i) = length(getBranchpoints(skel,AxonTrees_New(i)));
end

BranchPointDensity = AxonBranchPoints./AxonLengths;

%% Calculate the approx centres of axons
%first sort nodes as per z and then extract the centre node
AxonCentres = zeros(length(AxonTrees_New),3);
AxonLengthDensity = zeros(length(AxonTrees_New),1);


for i = 1:AxonCount_New
    Nodes = getNodes(skel,AxonTrees_New(i)); % get all nodes of the axon
    centre = mean(Nodes,1);
    NodeToExtract = getClosestNode(skel,centre,AxonTrees_New(i));
    AxonCentres(i,:) = Nodes(NodeToExtract,:); % get all axon centres
end

%% Calculate Length density around the centres within specified Bbox
tic
parfor i = 1:AxonCount_New
    AnalysisBox_start = AxonCentres(i,:) - 0.5*(MeasuringBox_vx); %start point of analysis bound box
    AnalysisBox_end = AxonCentres(i,:) + 0.5*(MeasuringBox_vx);
    AnalysisBox = (cat(1,AnalysisBox_start,AnalysisBox_end))'; % analysis box in BBOX format!
    
    MinSide=max([DatasetBbox(:,1),AnalysisBox(:,1)],[],2); %intersect the analysis volume with the dataset
    MaxSide=min([DatasetBbox(:,2),AnalysisBox(:,2)],[],2); % to avoid blank space
    intersectionBbox=[MinSide,MaxSide];
    
    skelNew = restrictToBBox(skel,intersectionBbox);
    
    Tree = find(cellfun(@(x)any(strfind(x, skel.names(AxonTrees_New(i)))==1),skelNew.names));
    
    volume = prod(diff(AnalysisBox, [], 2) .* voxelSize(:));
    AxonLength = skelNew.pathLength(Tree)/1000;
    AxonLengthDensity(i) = AxonLength/volume;
end
toc

sprintf('Done with Axon Centre Approach!')

%% Calculating density over every node
AxonDensity_everyNode = zeros(length(AxonTrees_New),1);
% AxonLengthDensity_node = NaN(length(AxonTrees),max(skel.nu))
for i = 1:AxonCount_New
    Nodes = getNodes(skel,AxonTrees_New(i)); % get all nodes of the axon
    tic
        parfor j = 1:length(Nodes)
            Box_start = Nodes(j,:) - 0.5*(MeasuringBox_vx); %start point of analysis bound box
            Box_end = Nodes(j,:) + 0.5*(MeasuringBox_vx);
            Box = (cat(1,Box_start,Box_end))';
            
            MinSide=max([DatasetBbox(:,1),Box(:,1)],[],2); %intersect the analysis volume with the dataset
            MaxSide=min([DatasetBbox(:,2),Box(:,2)],[],2); % to avoid blank space
            intersectionBbox=[MinSide,MaxSide];
            
            
            skelNew_node = restrictToBBox(skel,intersectionBbox, AxonTrees_New(i));
           
            
            Tree = find(cellfun(@(x)any(strfind(x, skel.names(AxonTrees_New(i)))==1),skelNew_node.names));
    
            volume_node = prod(diff(Box, [], 2) .* voxelSize(:));
            AxonLength_node = skelNew_node.pathLength(Tree)/1000;
            AxonLengthDensity_node(j) = AxonLength_node/volume_node;
        end
     toc
    AxonDensity_everyNode(i) = mean(AxonLengthDensity_node);
end

sprintf('*******Done going through every node ******')
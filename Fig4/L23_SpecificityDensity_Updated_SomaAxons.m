%%L2/3 plots - AIS axons - Innervation and Density

cx_data = xlsread('Z:\Data\goura\Analysis\L23_Updated_01-03-2019.xlsx','Sheet1');
Age = [9 14 28]

%% For SOMA specificity 

ToSel_1 = find(cx_data(:,1)== 9& cx_data(:,3)== 3); 
SMInn_9 = cx_data(ToSel_1,19); SMSyn_9 = cx_data(ToSel_1,6); SMDens_9 = cx_data(ToSel_1,25); 
 
ToSel_2 = find(cx_data(:,1)== 14& cx_data(:,3)== 3);
SMInn_14 = cx_data(ToSel_2,19); SMSyn_14 = cx_data(ToSel_2,6); SMDens_14 = cx_data(ToSel_2,25);
 
ToSel_3 = find(cx_data(:,1)== 28& cx_data(:,3)== 3); %use the age and seeding as filter
SMInn_28 = cx_data(ToSel_3,19);  SMSyn_28 = cx_data(ToSel_3,6); SMDens_28 = cx_data(ToSel_3,25); 


%% Concatenate all the parameters and sort
SM9all = cat(2,SMSyn_9,SMInn_9,SMDens_9);
SM14all = cat(2,SMSyn_14,SMInn_14,SMDens_14);
SM28all = cat(2,SMSyn_28,SMInn_28,SMDens_28);
 
SM9sort = sortrows(SM9all,1);
SM14sort = sortrows(SM14all,1);
SM28sort = sortrows(SM28all,1);


%% Different thresholding
%thrA = <10; thrB = 10-20; thrC = > 20syn

SelA = find(SM9sort(:,1) < 10);
SelB = find(SM9sort(:,1) > 9 & SM9sort(:,1) <20);
SelC = find(SM9sort(:,1) > 19); 
SM9_thrA_Inn = SM9sort(SelA,2); SM9_thrB_Inn = SM9sort(SelB,2); SM9_thrC_Inn = SM9sort(SelC,2);
SM9_thrA_Dens = SM9sort(SelA,3); SM9_thrB_Dens = SM9sort(SelB,3); SM9_thrC_Dens = SM9sort(SelC,3);
 
SelA = find(SM14sort(:,1) < 10);
SelB = find(SM14sort(:,1) > 9 & SM14sort(:,1) <20);
SelC = find(SM14sort(:,1) > 19); 
SM14_thrA_Inn = SM14sort(SelA,2); SM14_thrB_Inn = SM14sort(SelB,2); SM14_thrC_Inn = SM14sort(SelC,2);
SM14_thrA_Dens = SM14sort(SelA,3); SM14_thrB_Dens = SM14sort(SelB,3); SM14_thrC_Dens = SM14sort(SelC,3);
 
  
SelA = find(SM28sort(:,1) < 10);
SelB = find(SM28sort(:,1) > 9 & SM28sort(:,1) <20);
SelC = find(SM28sort(:,1) > 19); 
SM28_thrA_Inn = SM28sort(SelA,2); SM28_thrB_Inn = SM28sort(SelB,2); SM28_thrC_Inn = SM28sort(SelC,2);
SM28_thrA_Dens = SM28sort(SelA,3); SM28_thrB_Dens = SM28sort(SelB,3); SM28_thrC_Dens = SM28sort(SelC,3);


%% Bulk and Cumulative data for plotting along with Bootstrapping
% getting general parameters
SM_BulkInn = []; SM_BulkDens = [];
 
% Bootstrapping to get error means
SMInn_9_bstrp = bootstrp(10,@mean,SMInn_9); SE_SMInn_9 = std(SMInn_9_bstrp); 
SMInn_14_bstrp = bootstrp(10,@mean,SMInn_14); SE_SMInn_14 = std(SMInn_14_bstrp); 
SMInn_28_bstrp = bootstrp(10,@mean,SMInn_28); SE_SMInn_28 = std(SMInn_28_bstrp);
BootStrpData_SMInn = cat(2,SMInn_9_bstrp,SMInn_14_bstrp,SMInn_28_bstrp);
 
SMDens_9_bstrp = bootstrp(10,@mean,SMDens_9); SE_SMDens_9 = std(SMDens_9_bstrp); 
SMDens_14_bstrp = bootstrp(10,@mean,SMDens_14); SE_SMDens_14 = std(SMDens_14_bstrp); 
SMDens_28_bstrp = bootstrp(10,@mean,SMDens_28); SE_SMDens_28 = std(SMDens_28_bstrp);
BootStrpData_SMDens = cat(2,SMDens_9_bstrp,SMDens_14_bstrp,SMDens_28_bstrp);
 
 
% Concatenate lines
SE_Inn = cat(1,SE_SMInn_9,SE_SMInn_14,SE_SMInn_28);
SE_Dens = cat(1,SE_SMDens_9,SE_SMDens_14,SE_SMDens_28);
 
% Concatenate data for box plots
InnData = cat(1,SMInn_9, SMInn_14, SMInn_28);
DensData = cat(1,SMDens_9, SMDens_14, SMDens_28);
 
 
% Grouping
g1 = ones(size(SMInn_9));
g2 = 2*ones(size(SMInn_14));
g3 = 3*ones(size(SMInn_28));
Group = cat(1,g1,g2,g3);


%% Plotting
%error lines
SE_Inn_plus = SM_BulkInn + SE_Inn;
SE_Inn_minus = SM_BulkInn - SE_Inn;
 
SE_Dens_plus = SM_BulkDens + SE_Dens;
SE_Dens_minus = SM_BulkDens - SE_Dens;
 
 
% Innervation fractions
 
figure;
boxplot(InnData,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 9 14 28])
ylabel('SM innervation fraction)')
xlabel('Postnatal Age')
hold on
plot(Age,SM_BulkInn,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot(Age,SE_Inn_plus,'--k')% lplus err line
hold on
plot(Age,SE_Inn_minus,'--k')% minus err line
hold on
scatter(ones(numel(SM9_thrA_Inn),1)*9+rand(numel(SM9_thrA_Inn),1)*.75-.45, SM9_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
hold on
scatter(ones(numel(SM9_thrB_Inn),1)*9+rand(numel(SM9_thrB_Inn),1)*.75-.45, SM9_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
hold on
scatter(ones(numel(SM9_thrC_Inn),1)*9+rand(numel(SM9_thrC_Inn),1)*.75-.45, SM9_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
hold on
scatter(ones(numel(SM14_thrA_Inn),1)*14+rand(numel(SM14_thrA_Inn),1)*.75-.45, SM14_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
hold on
scatter(ones(numel(SM14_thrB_Inn),1)*14+rand(numel(SM14_thrB_Inn),1)*.75-.45, SM14_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
hold on
scatter(ones(numel(SM14_thrC_Inn),1)*14+rand(numel(SM14_thrC_Inn),1)*.75-.45, SM14_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
hold on
scatter(ones(numel(SM28_thrA_Inn),1)*28+rand(numel(SM28_thrA_Inn),1)*.75-.45, SM28_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
hold on
scatter(ones(numel(SM28_thrB_Inn),1)*28+rand(numel(SM28_thrB_Inn),1)*.75-.45, SM28_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
hold on
scatter(ones(numel(SM28_thrC_Inn),1)*28+rand(numel(SM28_thrC_Inn),1)*.75-.45, SM28_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
hold on
title('L2/3 Innervation fractions - SM axons')
InnData1 = cat(1,SMInn_9,SMInn_14);
Group1 = cat(1,g1,g2);
p = anova1(InnData1,Group1);
InnData2 = cat(1,SMInn_14,SMInn_28);
Group2 = cat(1,g2,g3);
p = anova1(InnData2,Group2); 
 
 
% Syn desnities
figure;
boxplot(DensData,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 0.5])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 9 14 28])
set(gca,'Ytick',[0:0.1:0.5])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 9 14 28])
ylabel('SM Denservation fraction)')
xlabel('Postnatal Age')
hold on
plot(Age,SM_BulkDens,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot(Age,SE_Dens_plus,'--k')% lplus err line
hold on
plot(Age,SE_Dens_minus,'--k')% minus err line
hold on
scatter(ones(numel(SM9_thrA_Dens),1)*9+rand(numel(SM9_thrA_Dens),1)*.75-.45, SM9_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
hold on
scatter(ones(numel(SM9_thrB_Dens),1)*9+rand(numel(SM9_thrB_Dens),1)*.75-.45, SM9_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
hold on
scatter(ones(numel(SM9_thrC_Dens),1)*9+rand(numel(SM9_thrC_Dens),1)*.75-.45, SM9_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
hold on
scatter(ones(numel(SM14_thrA_Dens),1)*14+rand(numel(SM14_thrA_Dens),1)*.75-.45, SM14_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
hold on
scatter(ones(numel(SM14_thrB_Dens),1)*14+rand(numel(SM14_thrB_Dens),1)*.75-.45, SM14_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
hold on
scatter(ones(numel(SM14_thrC_Dens),1)*14+rand(numel(SM14_thrC_Dens),1)*.75-.45, SM14_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
hold on
scatter(ones(numel(SM28_thrA_Dens),1)*28+rand(numel(SM28_thrA_Dens),1)*.75-.45, SM28_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
hold on
scatter(ones(numel(SM28_thrB_Dens),1)*28+rand(numel(SM28_thrB_Dens),1)*.75-.45, SM28_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
hold on
scatter(ones(numel(SM28_thrC_Dens),1)*28+rand(numel(SM28_thrC_Dens),1)*.75-.45, SM28_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
hold on
title('L2/3 Synapse Density - SM axons')
DensData1 = cat(1,SMDens_9,SMDens_14);
Group1 = cat(1,g1,g2);
p = anova1(DensData1,Group1);
DensData2 = cat(1,SMDens_14,SMDens_28);
Group2 = cat(1,g2,g3);
p = anova1(DensData2,Group2); 

%% Innervation vs Density plots
p1 = polyfit(SMDens_9,SMInn_9,1);
f1 = polyval(p1,SMDens_9);
p2 = polyfit(SMDens_14,SMInn_14,1);
f2 = polyval(p2,SMDens_14);
p3 = polyfit(SMDens_28,SMInn_28,1);
f3 = polyval(p3,SMDens_28);
 
%figure
figure;
scatter(SM9_thrA_Dens,SM9_thrA_Inn,'x','MarkerEdgeColor','r','SizeData',20)
hold on
scatter(SM9_thrB_Dens,SM9_thrB_Inn,'x','MarkerEdgeColor','r','SizeData',70)
hold on
scatter(SM9_thrC_Dens,SM9_thrC_Inn,'x','MarkerEdgeColor','r','SizeData',150)
hold on
scatter(SM14_thrA_Dens,SM14_thrA_Inn,'x','MarkerEdgeColor','g','SizeData',20)
hold on
scatter(SM14_thrB_Dens,SM14_thrB_Inn,'x','MarkerEdgeColor','g','SizeData',70)
hold on
scatter(SM14_thrC_Dens,SM14_thrC_Inn,'x','MarkerEdgeColor','g','SizeData',150)
hold on
scatter(SM28_thrA_Dens,SM28_thrA_Inn,'x','MarkerEdgeColor','m','SizeData',20)
hold on
scatter(SM28_thrB_Dens,SM28_thrB_Inn,'x','MarkerEdgeColor','m','SizeData',70)
hold on
scatter(SM28_thrC_Dens,SM28_thrC_Inn,'x','MarkerEdgeColor','m','SizeData',150)
hold on
plot(SMDens_9,f1,'-','Color','r')
hold on
plot(SMDens_14,f2,'-','Color','g')
hold on
plot(SMDens_28,f3,'-','Color','m')
hold on
box off
set(gca,'TickDir','out')
daspect([1 1 1])
set(gca,'Xlim',[0 1])
set(gca,'Ylim',[0 1])
set(gca,'XTick',[0 0.2 0.4 0.6 0.8 1])
set(gca,'YTick',[0 0.2 0.4 0.6 0.8 1])
xlabel('Synapse density (syn/um axonal path length')
ylabel('SM innervation fraction')
title('L23 - Innervation fraction vs Syn density (SM Axons)')
%% 
lm = fitlm(SMDens_9,SMInn_9) 

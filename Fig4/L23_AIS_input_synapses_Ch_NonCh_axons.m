%L23_AIS input by CH and NonCh - fractions
P14Ch = [];P14NonCh = [];
P28Ch=[]; P28NonCh=[];
BulkCh = []; BulkNonCh =[];

%% 
Err_Ch = [std(P14Ch)/sqrt(numel(P14Ch)); std(P28Ch)/sqrt(numel(P28Ch))];
Err_NonCh = [std(P14NonCh)/sqrt(numel(P14NonCh)); std(P28NonCh)/sqrt(numel(P28NonCh))];

G_Ch = cat(1,ones(size(P14Ch)),(2*ones(size(P28Ch))));
G_NonCh = cat(1,ones(size(P14NonCh)),(2*ones(size(P28NonCh))));

ChData = cat(1,P14Ch,P28Ch);
NonChData = cat(1,P14NonCh,P28NonCh);

%% 
figure
boxplot(ChData,G_Ch,'Colors','k')
hold on
boxplot(NonChData,G_NonCh,'Colors','k')
hold on
plot(BulkCh,'-ok','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot((BulkCh+Err_Ch), '--m')
hold on
plot((BulkCh-Err_Ch),'--m')
hold on
plot(BulkNonCh,'-ok','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot((BulkNonCh+Err_NonCh), '--b')
hold on
plot((BulkNonCh-Err_NonCh),'--b')
hold on
scatter(ones(numel(P14Ch),1)*1+rand(numel(P14Ch),1)*.25-.125, P14Ch,'x','MarkerEdgeColor','m','SizeData',200);
hold on
scatter(ones(numel(P28Ch),1)*2+rand(numel(P28Ch),1)*.25-.125, P28Ch,'x','MarkerEdgeColor','m','SizeData',200);
hold on
scatter(ones(numel(P14NonCh),1)*1+rand(numel(P14NonCh),1)*.25-.125, P14NonCh,'x','MarkerEdgeColor','b','SizeData',200);
hold on
scatter(ones(numel(P28NonCh),1)*2+rand(numel(P28NonCh),1)*.25-.125, P28NonCh,'x','MarkerEdgeColor','b','SizeData',200);
set(gca,'TickDir','out')
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 3])
set(gca,'Xtick',[0 1 2])
box off;
set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 14 28])
ylabel('Fraction of AIS input synapses (5)')
xlabel('Postnatal age (days)')
title('L23_Fraction of AIS input synapses by Ch and NonCh axons')

%% %% L23_Scatter plot - AIS input by CH and NonCh axons and synapses
NonChSyn28 = [];NonChAx28 = [];NonChAx14 = [];NonChSyn14 = [];
ChSyn14 = [];ChSyn28 = [];ChAx14 = [];ChAx28 = [];
% 
figure
scatter(ChAx14,ChSyn14,'xm','SizeData',200)
hold on
scatter(ChAx28,ChSyn28,'xr','SizeData',200)
hold on
scatter(NonChAx14,NonChSyn14,'xc','SizeData',200)
hold on
scatter(NonChAx28,NonChSyn28,'xb','SizeData',200)
box off
set(gca,'TickDir','out')
daspect([1 1 1])
set(gca,'Xlim',[0 15])
set(gca,'Ylim',[0 25])
set(gca,'XTick',[0 5 10 15])
set(gca,'YTick',[0 5 10 15 20 25])
xlabel('# axons per AIS')
ylabel('# synapses per AIS')
title('L23 - AIS input synapses and axons')
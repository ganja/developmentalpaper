%L2/3 Comaprison of Non CH and Soma innervating axons
AISax_AIS_14 = [];%non Ch ais-innervating axons AIS inn fraction
AISax_SOMA_14 = [];%non Ch ais-innervaing axons - Soma fraction
SMax_AIS_14 = [];% soma seeded axons - AIS inn fraction
SMax_SOMA_14 = [];% soma seeded axons - SOMA inn fraction

AISax_AIS_28 = [];
AISax_SOMA_28 = [];
SMax_AIS_28 = [];
SMax_SOMA_28 = [];

%% 
figure;
scatter(AISax_SOMA_14,AISax_AIS_14,'xb','SizeData',200);
hold on
scatter(SMax_SOMA_14,SMax_AIS_14,'xk','SizeData',200);
hold on
daspect([1 1 1]);
box off
set(gca,'TickDir','out');
set(gca,'Ylim',[0 0.5])
set(gca,'Xlim',[0 1])
ylabel('AIS innervation fraction')
xlabel('Soma innervation fraction')
title('P14 L2/3 - NonCh and Soma Axons comparison');

figure;
scatter(AISax_SOMA_28,AISax_AIS_28,'xb','SizeData',200);
hold on
scatter(SMax_SOMA_28,SMax_AIS_28,'xk','SizeData',200);
hold on
daspect([1 1 1]);
box off
set(gca,'TickDir','out');
set(gca,'Ylim',[0 0.5])
set(gca,'Xlim',[0 1])
ylabel('AIS innervation fraction')
xlabel('Soma innervation fraction')
title('P28L2/3 - NonCh and Soma Axons comparison');


%% histograms
P14AISax_AIS = histcounts(AISax_AIS_14,0:0.1:0.5);
P14SMax_AIS = histcounts(SMax_AIS_14,0:0.1:0.5);

figure;
stairs([0:0.1:0.5]',[P14AISax_AIS';P14AISax_AIS(end)],'b','LineWidth',2,'LineStyle','-');
hold on
stairs([0:0.1:0.5]',[P14SMax_AIS';P14SMax_AIS(end)],'k','LineWidth',2,'LineStyle','-');
hold on
set(gca,'Xlim',[0 0.5])
set(gca,'XTick',[0 0.1 0.2 0.3 0.4 0.5])
box off
set(gca,'TickDir','out')
xlabel('AIS innervation fraction');
ylabel('# axons');
title('P14 L2/3 histogram - AIS innervation by NonCh and Soma axons')

P14AISax_SOMA = histcounts(AISax_SOMA_14,0:0.1:1);
P14SMax_SOMA = histcounts(SMax_SOMA_14,0:0.1:1);

figure;
stairs([0:0.1:1]',[P14AISax_SOMA';P14AISax_SOMA(end)],'b','LineWidth',2,'LineStyle','-');
hold on
stairs([0:0.1:1]',[P14SMax_SOMA';P14SMax_SOMA(end)],'k','LineWidth',2,'LineStyle','-');
hold on
set(gca,'Xlim',[0 1])
set(gca,'XTick',[0:0.1:1])
box off
set(gca,'TickDir','out')
xlabel('Soma innervation fraction');
ylabel('# axons');
title('P14 L2/3 histogram - Soma innervation by NonCh and Soma axons')

%%SPLIT BRANCH ANALYSIS FOR MANUALLY CORRECTED SKELETONS!
skel = skeleton('Z:\Data\goura\FigureUpdates\L23\AxonCartridgeSpecSynDensity\NML\SplitBranches_P28L23_ChAxons_11-11-2019.nml');
treeIndices_1 = find(cellfun(@(x)any(strfind(x, 'Ch')==1),skel.names));
% treeIndices_2 = find(cellfun(@(x)any(strfind(x, 'IS')==1),skel.names));
% treeIndices = cat(1,treeIndices_1,treeIndices_2);
treeIndices = treeIndices_1;
treeNames = skel.names(treeIndices);
%% Branchwise data
SplitAxons = length(treeIndices); %all branches of all axons post splitting
SomaSyn = {};
% SomFilSyn = {};
% ProxSyn = {};
%L4Proximal
% AllSyn = skel.getNodesWithComment('syn',treeIndices,'partial');
% SomaComments = skel.getNodesWithComment('Soma',treeIndices,'partial');%for soma axons
% SomFilComments = skel.getNodesWithComment('Som. fil.',treeIndices,'partial');%for soma axons
% ProximalSynComments = skel.getNodesWithComment('proximal',treeIndices,'partial');%for soma axons
AllSyn = skel.getNodesWithComment('syn',treeIndices,'partial');
SomaComments = skel.getNodesWithComment('AIS',treeIndices,'partial');%for soma axons


 for i = 1:SplitAxons
     SomaSyn{i}= intersect(AllSyn{i},SomaComments{i});
%      SomFilSyn{i}= intersect(AllSyn{i},SomFilComments{i});
%      ProximalSyn{i} = intersect(AllSyn{i},ProximalSynComments{i});
 end
SomaSyn = SomaSyn';
% SomFilSyn = SomFilSyn';
% ProximalSyn = ProximalSyn';

SSsyn = zeros(SplitAxons,1);
TotalSyn = zeros(SplitAxons,1);
Specificity = zeros(SplitAxons,1);
SpecSynDens = zeros(SplitAxons,1);
OtherSynDens = zeros(SplitAxons,1);
TotalDens = zeros(SplitAxons,1);
pL = zeros(SplitAxons,1);
% ProximalSynDens = zeros(SplitAxons,1);
% ProximalSyn_num = zeros(SplitAxons,1);
% SomaSyn_num = zeros(SplitAxons,1);
% SomFilSyn_num = zeros(SplitAxons,1);
% SomaSynDens = zeros(SplitAxons,1);
for i = 1:SplitAxons
    SomaSyn_num(i) = numel(SomaSyn{i});
%     SomFilSyn_num(i) = numel(SomFilSyn{i});
%     ProximalSyn_num(i) = numel(ProximalSyn{i});
%     SSsyn(i) = SomaSyn_num(i) + SomFilSyn_num(i) + ProximalSyn_num(i);
    SSsyn(i) = SomaSyn_num(i);
    TotalSyn(i) = numel(AllSyn{i});
%     Specificity(i) = ((SomaSyn_num(i) + SomFilSyn_num(i) + ProximalSyn_num(i))-1)/(numel(AllSyn{i})-1);
    Specificity(i) = ((SomaSyn_num(i))-1)/(numel(AllSyn{i})-1);
    pL(i) = skel.pathLength(treeIndices(i))/1000;
    SpecSynDens(i) = SSsyn(i) / pL(i);
    TotalDens(i)=TotalSyn(i)/pL(i);
    OtherSynDens(i) = (TotalSyn(i) - SSsyn(i))/pL(i);
%     ProximalSynDens(i) = (SSsyn(i)-SomaSyn_num(i))/pL(i);
%     SomaSynDens(i)= SomaSyn_num(i)/pL(i);
end
SpecSynDens_Bulk = sum(SSsyn)/sum(pL);
% ProximalSynDens_Bulk = sum(ProximalSyn_num)/sum(pL);
OtherSynDens_Bulk = (sum(TotalSyn) - sum(SSsyn))/sum(pL);
SomaDensBulk = sum(SomaSyn_num)/sum(pL);
BulkInnervation = (sum(SSsyn)-SplitAxons)/(sum(TotalSyn)-SplitAxons);
%% Plotting these as histograms
specDens_P7 = histcounts(SpecSynDens_P7SmAx,0:0.1:0.5); % divide by total number of axons for normalizing it!
specDens_P9 = histcounts(SpecSynDens_P9SmAx,0:0.1:0.5); % divide by total number of axons for normalizing it!

OtherspecDens_P7 = histcounts(OtherSynDens_P7SmAx,0:0.1:0.5)./573; % divide by total number of axons for normalizing it!
OtherspecDens_P9 = histcounts(OtherSynDens_P9SmAx,0:0.1:0.5)./1044; % divide by total number of axons for normalizing it!


specificity_P7 = histcounts(Specificity_P7SmAx,0:0.1:1);
specificity_P9 = histcounts(Specificity_P9SmAx,0:0.1:1);

%specific synapse density
figure
stairs([0:0.1:0.5]',[specDens_P7';specDens_P7(end)],'r','LineWidth',2,'LineStyle','-');
hold on
stairs([0:0.1:0.5]',[specDens_P9';specDens_P9(end)],'b','LineWidth',2,'LineStyle','-');
hold on
box off
set(gca,'TickDir','out')
set(gca,'Xlim',[0 1])
set(gca,'Ylim',[0 1])

%Non-specific synapse density
figure
stairs([0:0.1:0.5]',[OtherspecDens_P7';OtherspecDens_P7(end)],'r','LineWidth',2,'LineStyle','--');
hold on
stairs([0:0.1:0.5]',[OtherspecDens_P9';OtherspecDens_P9(end)],'b','LineWidth',2,'LineStyle','--');
hold on
box off
set(gca,'TickDir','out')
set(gca,'Xlim',[0 1])
set(gca,'Ylim',[0 1])




% specificity 
figure
stairs([0:0.1:1]',[specificity_P7';specificity_P7(end)],'r','LineWidth',2,'LineStyle','-');
hold on
stairs([0:0.1:1]',[specificity_P9a';specificity_P9a(end)],'b','LineWidth',2,'LineStyle','-');
hold on
box off
set(gca,'TickDir','out')
set(gca,'Xlim',[0 1])
set(gca,'Ylim',[0 700])


%% %% plot as boxplots for L23 Ch Axons
data_SpecSynDens = cat(1, SpecSynDens_P14ChAx,SpecSynDens_P28ChAx);
data_OtherSynDens = cat(1,OtherSynDens_P14ChAx,OtherSynDens_P28ChAx);

% bootstrapping
specDens14_bstrp = bootstrp(10,@mean,SpecSynDens_P14ChAx); SE_specDens14 = std(specDens14_bstrp);
specDens28_bstrp = bootstrp(10,@mean,SpecSynDens_P28ChAx); SE_specDens28 = std(specDens28_bstrp);
otherSynDens14_bstrp = bootstrp(10,@mean,OtherSynDens_P14ChAx); SE_otherSynDens14 = std(otherSynDens14_bstrp);
otherSynDens28_bstrp = bootstrp(10,@mean,OtherSynDens_P28ChAx); SE_otherSynDens28 = std(otherSynDens28_bstrp);

Bootstrp_specDens = cat(2,specDens14_bstrp,specDens28_bstrp);
Bootstrp_otherSynDens = cat(2,otherSynDens14_bstrp,otherSynDens28_bstrp);

SE_specDens = cat(1,SE_specDens14,SE_specDens28);
SE_otherSynDens = cat(1,SE_otherSynDens14,SE_otherSynDens28);

BulkSpecDens = cat(1,SpecSynDens_Bulk_P14ChAx,SpecSynDens_Bulk_P28ChAx);
BulkOtherSynDens = cat(1,OtherSynDens_Bulk_P14ChAx,OtherSynDens_Bulk_P28ChAx);

% grouping
g1 = ones(size(SpecSynDens_P14ChAx));
g2 = 2*ones(size(SpecSynDens_P28ChAx));
Group = cat(1,g1,g2);
% error lines
SE_specDens_plus = BulkSpecDens + SE_specDens;
SE_specDens_minus = BulkSpecDens - SE_specDens;
SE_otherSynDens_plus = BulkOtherSynDens + SE_otherSynDens;
SE_otherSynDens_minus = BulkOtherSynDens - SE_otherSynDens;

% plot as box plot
figure;
% boxplot(data_SpecSynDens,Group,'positions',[1 2],'Colors','k','Widths',1.8)
boxplot(data_SpecSynDens,Group,'positions',[1 2],'Colors','k')
hold on
% boxplot(data_OtherSynDens,Group,'positions',[14 28],'Colors','k','Widths',1.8)
hold on
set(gca,'Ylim',[0 0.5])
set(gca,'Xlim',[0 2.5])
set(gca,'Xtick',[0 1 2])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 14 28])
ylabel('Specific syn density of axon cartridges')
xlabel('Postnatal Age')
hold on
plot([1 2],BulkSpecDens,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot([1 2],SE_specDens_plus,'--m')% lplus err line
hold on
plot([1 2],SE_specDens_minus,'--m')% minus err line
hold on
% plot([14 28],BulkOtherSynDens,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
% hold on
% plot([14 28],SE_otherSynDens_plus,'--k')% lplus err line
% hold on
% plot([14 28],SE_otherSynDens_minus,'--k')% minus err line
hold on
scatter(ones(numel(SpecSynDens_P14ChAx),1)*1+rand(numel(SpecSynDens_P14ChAx),1)*.25-.125, SpecSynDens_P14ChAx,'x','MarkerEdgeColor','m','SizeData',200, 'LineWidth',0.005);
hold on
% scatter(ones(numel(OtherSynDens_P14ChAx),1)*14+rand(numel(OtherSynDens_P14ChAx),1)*.75-.45, OtherSynDens_P14ChAx,'x','MarkerEdgeColor','k','SizeData',200, 'LineWidth',0.005);
hold on
scatter(ones(numel(SpecSynDens_P28ChAx),1)*2+rand(numel(SpecSynDens_P28ChAx),1)*.25-.125, SpecSynDens_P28ChAx,'x','MarkerEdgeColor','m','SizeData',200, 'LineWidth',0.005);
hold on
% scatter(ones(numel(OtherSynDens_P28ChAx),1)*28+rand(numel(OtherSynDens_P28ChAx),1)*.75-.45, OtherSynDens_P28ChAx,'x','MarkerEdgeColor','k','SizeData',200, 'LineWidth',0.005);
hold on
title('L2/3 - Chandelier cartridge specific synapse density');

%% Plot for L4 soma axons (P7 P9)
data_SpecSynDens = cat(1, SpecSynDens_P7SmAx,SpecSynDens_P9SmAx,SpecSynDens_P14SmAx,SpecSynDens_P28SmAx);
data_OtherSynDens = cat(1,OtherSynDens_P7SmAx,OtherSynDens_P9SmAx,OtherSynDens_P14SmAx,OtherSynDens_P28SmAx);
 
% bootstrapping
specDens7_bstrp = bootstrp(10,@mean,SpecSynDens_P7SmAx); SE_specDens7 = std(specDens7_bstrp);
specDens9_bstrp = bootstrp(10,@mean,SpecSynDens_P9SmAx); SE_specDens9 = std(specDens9_bstrp);
specDens14_bstrp = bootstrp(10,@mean,SpecSynDens_P14SmAx); SE_specDens14 = std(specDens14_bstrp);
specDens28_bstrp = bootstrp(10,@mean,SpecSynDens_P28SmAx); SE_specDens28 = std(specDens28_bstrp);
otherSynDens7_bstrp = bootstrp(10,@mean,OtherSynDens_P7SmAx); SE_otherSynDens7 = std(otherSynDens7_bstrp);
otherSynDens9_bstrp = bootstrp(10,@mean,OtherSynDens_P9SmAx); SE_otherSynDens9 = std(otherSynDens9_bstrp);
otherSynDens14_bstrp = bootstrp(10,@mean,OtherSynDens_P14SmAx); SE_otherSynDens14 = std(otherSynDens14_bstrp);
otherSynDens28_bstrp = bootstrp(10,@mean,OtherSynDens_P28SmAx); SE_otherSynDens28 = std(otherSynDens28_bstrp);
 
Bootstrp_specDens = cat(2,specDens7_bstrp,specDens9_bstrp,specDens14_bstrp,specDens28_bstrp);
Bootstrp_otherSynDens = cat(2,otherSynDens7_bstrp,otherSynDens9_bstrp,otherSynDens14_bstrp,otherSynDens28_bstrp);
 
SE_specDens = cat(1,SE_specDens7,SE_specDens9,SE_specDens14,SE_specDens28);
SE_otherSynDens = cat(1,SE_otherSynDens7,SE_otherSynDens9,SE_otherSynDens14,SE_otherSynDens28);
 
BulkSpecDens = cat(1,SpecSynDens_Bulk_P7SmAx,SpecSynDens_Bulk_P9SmAx,SpecSynDens_Bulk_P14SmAx,SpecSynDens_Bulk_P28SmAx);
BulkOtherSynDens = cat(1,OtherSynDens_Bulk_P7SmAx,OtherSynDens_Bulk_P9SmAx,OtherSynDens_Bulk_P14SmAx,OtherSynDens_Bulk_P28SmAx);
 
% grouping
g1 = ones(size(SpecSynDens_P7SmAx));
g2 = 2*ones(size(SpecSynDens_P9SmAx));
g3 = 3*ones(size(SpecSynDens_P14SmAx));
g4 = 4*ones(size(SpecSynDens_P28SmAx));
Group = cat(1,g1,g2,g3,g4);
% error lines
SE_specDens_plus = BulkSpecDens + SE_specDens;
SE_specDens_minus = BulkSpecDens - SE_specDens;
SE_otherSynDens_plus = BulkOtherSynDens + SE_otherSynDens;
SE_otherSynDens_minus = BulkOtherSynDens - SE_otherSynDens;

% plot as box plot
figure;
boxplot(data_SpecSynDens,Group,'positions',[7 9 14 28],'Colors','k','Widths',1.8)
hold on
% boxplot(data_OtherSynDens,Group,'positions',[7 9 14 28],'Colors','k','Widths',1.8)
hold on
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 7 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 7 9 14 28])
ylabel('Specific syn density')
xlabel('Postnatal Age (days)')
hold on
plot([7 9 14 28],BulkSpecDens,'-ro','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot([7 9 14 28],SE_specDens_plus,'--r')% lplus err line
hold on
plot([7 9 14 28],SE_specDens_minus,'--r')% minus err line
hold on
% plot([14 28],BulkOtherSynDens,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
% hold on
% plot([14 28],SE_otherSynDens_plus,'--k')% lplus err line
% hold on
% plot([14 28],SE_otherSynDens_minus,'--k')% minus err line
hold on
scatter(ones(numel(SpecSynDens_P7SmAx),1)*7+rand(numel(SpecSynDens_P7SmAx),1)*.75-.45, SpecSynDens_P7SmAx,'x','MarkerEdgeColor','r','SizeData',200, 'LineWidth',0.005);
hold on
% scatter(ones(numel(OtherSynDens_P14ChAx),1)*14+rand(numel(OtherSynDens_P14ChAx),1)*.75-.45, OtherSynDens_P14ChAx,'x','MarkerEdgeColor','k','SizeData',200, 'LineWidth',0.005);
hold on
scatter(ones(numel(SpecSynDens_P9SmAx),1)*9+rand(numel(SpecSynDens_P9SmAx),1)*.75-.45, SpecSynDens_P9SmAx,'x','MarkerEdgeColor','r','SizeData',200, 'LineWidth',0.005);
hold on
% scatter(ones(numel(OtherSynDens_P28ChAx),1)*28+rand(numel(OtherSynDens_P28ChAx),1)*.75-.45, OtherSynDens_P28ChAx,'x','MarkerEdgeColor','k','SizeData',200, 'LineWidth',0.005);
hold on
scatter(ones(numel(SpecSynDens_P14SmAx),1)*14+rand(numel(SpecSynDens_P14SmAx),1)*.75-.45, SpecSynDens_P14SmAx,'x','MarkerEdgeColor','r','SizeData',200, 'LineWidth',0.005);
hold on
% scatter(ones(numel(OtherSynDens_P28ChAx),1)*28+rand(numel(OtherSynDens_P28ChAx),1)*.75-.45, OtherSynDens_P28ChAx,'x','MarkerEdgeColor','k','SizeData',200, 'LineWidth',0.005);
hold on
scatter(ones(numel(SpecSynDens_P28SmAx),1)*28+rand(numel(SpecSynDens_P28SmAx),1)*.75-.45, SpecSynDens_P28SmAx,'x','MarkerEdgeColor','r','SizeData',200, 'LineWidth',0.005);
hold on
title('L4 - Branchwise specific and off target syn density - Soma Axons');

%% Checking between specific bins of histograms
%bin 1 - 0 - 0.1; bin2 - 0.1-0.2; bin3 - 0.2-0.3; bin4 - 0.3-0.4; bin5 -
%0.4-0.5
P7_ToSel_bin1 = find(SpecSynDens_P7SmAx(:,1)<=0.1 & SpecSynDens_P7SmAx(:,1)>= 0);
specDens7_bin1 = SpecSynDens_P7SmAx(P7_ToSel_bin1,1);
totalSyn_bin1 = 
P7_ToSel_bin2 = find(SpecSynDens_P7SmAx(:,1)<=0.2 & SpecSynDens_P7SmAx(:,1)> 0.1);
specDens7_bin2 = SpecSynDens_P7SmAx(P7_ToSel_bin2,1);
P7_ToSel_bin3 = find(SpecSynDens_P7SmAx(:,1)<=0.3 & SpecSynDens_P7SmAx(:,1)> 0.2);
specDens7_bin3 = SpecSynDens_P7SmAx(P7_ToSel_bin3,1);
P7_ToSel_bin4 = find(SpecSynDens_P7SmAx(:,1)<=0.4 & SpecSynDens_P7SmAx(:,1)> 0.3);
specDens7_bin4 = SpecSynDens_P7SmAx(P7_ToSel_bin4,1);
P7_ToSel_bin5 = find(SpecSynDens_P7SmAx(:,1)<=0.5 & SpecSynDens_P7SmAx(:,1)> 0.4);
specDens7_bin5 = SpecSynDens_P7SmAx(P7_ToSel_bin5,1);

P9_ToSel_bin1 = find(SpecSynDens_P9SmAx(:,1)<=0.1 & SpecSynDens_P9SmAx(:,1)>= 0);
specDens9_bin1 = SpecSynDens_P9SmAx(P9_ToSel_bin1,1);
P9_ToSel_bin2 = find(SpecSynDens_P9SmAx(:,1)<=0.2 & SpecSynDens_P9SmAx(:,1)> 0.1);
specDens9_bin2 = SpecSynDens_P9SmAx(P9_ToSel_bin2,1);
P9_ToSel_bin3 = find(SpecSynDens_P9SmAx(:,1)<=0.3 & SpecSynDens_P9SmAx(:,1)> 0.2);
specDens9_bin3 = SpecSynDens_P9SmAx(P9_ToSel_bin3,1);
P9_ToSel_bin4 = find(SpecSynDens_P9SmAx(:,1)<=0.4 & SpecSynDens_P9SmAx(:,1)> 0.3);
specDens9_bin4 = SpecSynDens_P9SmAx(P9_ToSel_bin4,1);
P9_ToSel_bin5 = find(SpecSynDens_P9SmAx(:,1)<=0.5 & SpecSynDens_P9SmAx(:,1)> 0.4);
specDens9_bin5 = SpecSynDens_P9SmAx(P9_ToSel_bin5,1);

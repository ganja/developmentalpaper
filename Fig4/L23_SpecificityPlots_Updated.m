%%L2/3 plots - AIS axons - Innervation and Density

cx_data = xlsread('Z:\Data\goura\Analysis\L23_Updated_01-03-2019.xlsx','Sheet1');
Age = [9 14 28]

%% For AD specificity 

ToSel_1 = find(cx_data(:,1)== 9& cx_data(:,3)== 2); 
AISInn_9 = cx_data(ToSel_1,18); AISSyn_9 = cx_data(ToSel_1,6); AISDens_9 = cx_data(ToSel_1,25); 

ToSel_2 = find(cx_data(:,1)== 14& cx_data(:,3)== 2);
AISInn_14 = cx_data(ToSel_2,18); AISSyn_14 = cx_data(ToSel_2,6); AISDens_14 = cx_data(ToSel_2,25);

ToSel_3 = find(cx_data(:,1)== 28& cx_data(:,3)== 2); %use the age and seeding as filter
AISInn_28 = cx_data(ToSel_3,18);  AISSyn_28 = cx_data(ToSel_3,6); AISDens_28 = cx_data(ToSel_3,25); 

%% Concatenate all the parameters and sort
AIS9all = cat(2,AISSyn_9,AISInn_9,AISDens_9);
AIS14all = cat(2,AISSyn_14,AISInn_14,AISDens_14);
AIS28all = cat(2,AISSyn_28,AISInn_28,AISDens_28);

AIS9sort = sortrows(AIS9all,1);
AIS14sort = sortrows(AIS14all,1);
AIS28sort = sortrows(AIS28all,1);

%% Different thresholding
%thrA = <10; thrB = 10-20; thrC = > 20syn

SelA = find(AIS9sort(:,1) < 10);
SelB = find(AIS9sort(:,1) > 9 & AIS9sort(:,1) <20);
SelC = find(AIS9sort(:,1) > 19); 
AIS9_thrA_Inn = AIS9sort(SelA,2); AIS9_thrB_Inn = AIS9sort(SelB,2); AIS9_thrC_Inn = AIS9sort(SelC,2);
AIS9_thrA_Dens = AIS9sort(SelA,3); AIS9_thrB_Dens = AIS9sort(SelB,3); AIS9_thrC_Dens = AIS9sort(SelC,3);
 
SelA = find(AIS14sort(:,1) < 10);
SelB = find(AIS14sort(:,1) > 9 & AIS14sort(:,1) <20);
SelC = find(AIS14sort(:,1) > 19); 
AIS14_thrA_Inn = AIS14sort(SelA,2); AIS14_thrB_Inn = AIS14sort(SelB,2); AIS14_thrC_Inn = AIS14sort(SelC,2);
AIS14_thrA_Dens = AIS14sort(SelA,3); AIS14_thrB_Dens = AIS14sort(SelB,3); AIS14_thrC_Dens = AIS14sort(SelC,3);

  
SelA = find(AIS28sort(:,1) < 10);
SelB = find(AIS28sort(:,1) > 9 & AIS28sort(:,1) <20);
SelC = find(AIS28sort(:,1) > 19); 
AIS28_thrA_Inn = AIS28sort(SelA,2); AIS28_thrB_Inn = AIS28sort(SelB,2); AIS28_thrC_Inn = AIS28sort(SelC,2);
AIS28_thrA_Dens = AIS28sort(SelA,3); AIS28_thrB_Dens = AIS28sort(SelB,3); AIS28_thrC_Dens = AIS28sort(SelC,3);

%% Bulk and Cumulative data for plotting along with Bootstrapping
% getting general parameters
AIS_BulkInn = []; AIS_BulkDens = [];
 
% Bootstrapping to get error means
AISInn_9_bstrp = bootstrp(10,@mean,AISInn_9); SE_AISInn_9 = std(AISInn_9_bstrp); 
AISInn_14_bstrp = bootstrp(10,@mean,AISInn_14); SE_AISInn_14 = std(AISInn_14_bstrp); 
AISInn_28_bstrp = bootstrp(10,@mean,AISInn_28); SE_AISInn_28 = std(AISInn_28_bstrp);
BootStrpData_AISInn = cat(2,AISInn_9_bstrp,AISInn_14_bstrp,AISInn_28_bstrp);
 
AISDens_9_bstrp = bootstrp(10,@mean,AISDens_9); SE_AISDens_9 = std(AISDens_9_bstrp); 
AISDens_14_bstrp = bootstrp(10,@mean,AISDens_14); SE_AISDens_14 = std(AISDens_14_bstrp); 
AISDens_28_bstrp = bootstrp(10,@mean,AISDens_28); SE_AISDens_28 = std(AISDens_28_bstrp);
BootStrpData_AISDens = cat(2,AISDens_9_bstrp,AISDens_14_bstrp,AISDens_28_bstrp);


% Concatenate lines
SE_Inn = cat(1,SE_AISInn_9,SE_AISInn_14,SE_AISInn_28);
SE_Dens = cat(1,SE_AISDens_9,SE_AISDens_14,SE_AISDens_28);
 
% Concatenate data for box plots
InnData = cat(1,AISInn_9, AISInn_14, AISInn_28);
DensData = cat(1,AISDens_9, AISDens_14, AISDens_28);

 
% Grouping
g1 = ones(size(AISInn_9));
g2 = 2*ones(size(AISInn_14));
g3 = 3*ones(size(AISInn_28));
Group = cat(1,g1,g2,g3);

%% Plotting
%error lines
SE_Inn_plus = AIS_BulkInn + SE_Inn;
SE_Inn_minus = AIS_BulkInn - SE_Inn;
 
SE_Dens_plus = AIS_BulkDens + SE_Dens;
SE_Dens_minus = AIS_BulkDens - SE_Dens;


% Innervation fractions

figure;
boxplot(InnData,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 9 14 28])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 9 14 28])
ylabel('AIS innervation fraction)')
xlabel('Postnatal Age')
hold on
plot(Age,AIS_BulkInn,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot(Age,SE_Inn_plus,'--k')% lplus err line
hold on
plot(Age,SE_Inn_minus,'--k')% minus err line
hold on
scatter(ones(numel(AIS9_thrA_Inn),1)*9+rand(numel(AIS9_thrA_Inn),1)*.75-.45, AIS9_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
hold on
scatter(ones(numel(AIS9_thrB_Inn),1)*9+rand(numel(AIS9_thrB_Inn),1)*.75-.45, AIS9_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
hold on
scatter(ones(numel(AIS9_thrC_Inn),1)*9+rand(numel(AIS9_thrC_Inn),1)*.75-.45, AIS9_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
hold on
scatter(ones(numel(AIS14_thrA_Inn),1)*14+rand(numel(AIS14_thrA_Inn),1)*.75-.45, AIS14_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
hold on
scatter(ones(numel(AIS14_thrB_Inn),1)*14+rand(numel(AIS14_thrB_Inn),1)*.75-.45, AIS14_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
hold on
scatter(ones(numel(AIS14_thrC_Inn),1)*14+rand(numel(AIS14_thrC_Inn),1)*.75-.45, AIS14_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
hold on
scatter(ones(numel(AIS28_thrA_Inn),1)*28+rand(numel(AIS28_thrA_Inn),1)*.75-.45, AIS28_thrA_Inn,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
hold on
scatter(ones(numel(AIS28_thrB_Inn),1)*28+rand(numel(AIS28_thrB_Inn),1)*.75-.45, AIS28_thrB_Inn,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
hold on
scatter(ones(numel(AIS28_thrC_Inn),1)*28+rand(numel(AIS28_thrC_Inn),1)*.75-.45, AIS28_thrC_Inn,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
hold on
title('L2/3 Innervation fractions - AIS axons')

InnData1 = cat(1,AISInn_9,AISInn_14);
Group1 = cat(1,g1,g2);
p = anova1(InnData1,Group1);
InnData2 = cat(1,AISInn_14,AISInn_28);
Group2 = cat(1,g2,g3);
p = anova1(InnData2,Group2);
% Syn desnities
figure;
boxplot(DensData,Group,'positions',Age,'Colors','k')
hold on
set(gca,'Ylim',[0 0.5])
set(gca,'Xlim',[0 30])
set(gca,'Xtick',[0 9 14 28])
set(gca,'Ytick',[0:0.1:0.5])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 9 14 28])
ylabel('AIS Denservation fraction)')
xlabel('Postnatal Age')
hold on
plot(Age,AIS_BulkDens,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot(Age,SE_Dens_plus,'--k')% lplus err line
hold on
plot(Age,SE_Dens_minus,'--k')% minus err line
hold on
scatter(ones(numel(AIS9_thrA_Dens),1)*9+rand(numel(AIS9_thrA_Dens),1)*.75-.45, AIS9_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
hold on
scatter(ones(numel(AIS9_thrB_Dens),1)*9+rand(numel(AIS9_thrB_Dens),1)*.75-.45, AIS9_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
hold on
scatter(ones(numel(AIS9_thrC_Dens),1)*9+rand(numel(AIS9_thrC_Dens),1)*.75-.45, AIS9_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
hold on
scatter(ones(numel(AIS14_thrA_Dens),1)*14+rand(numel(AIS14_thrA_Dens),1)*.75-.45, AIS14_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
hold on
scatter(ones(numel(AIS14_thrB_Dens),1)*14+rand(numel(AIS14_thrB_Dens),1)*.75-.45, AIS14_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
hold on
scatter(ones(numel(AIS14_thrC_Dens),1)*14+rand(numel(AIS14_thrC_Dens),1)*.75-.45, AIS14_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
hold on
scatter(ones(numel(AIS28_thrA_Dens),1)*28+rand(numel(AIS28_thrA_Dens),1)*.75-.45, AIS28_thrA_Dens,'x','MarkerEdgeColor',[0.75 0.75 0.75],'SizeData',70, 'LineWidth',0.005);
hold on
scatter(ones(numel(AIS28_thrB_Dens),1)*28+rand(numel(AIS28_thrB_Dens),1)*.75-.45, AIS28_thrB_Dens,'x','MarkerEdgeColor',[0.55 0.55 0.55],'SizeData',70, 'LineWidth',0.05);
hold on
scatter(ones(numel(AIS28_thrC_Dens),1)*28+rand(numel(AIS28_thrC_Dens),1)*.75-.45, AIS28_thrC_Dens,'x','MarkerEdgeColor','k','SizeData',70, 'LineWidth',0.1);
hold on
title('L2/3 Synapse Density - AIS axons')
DensData1 = cat(1,AISDens_9,AISDens_14);
Group1 = cat(1,g1,g2);
p = anova1(DensData1,Group1);
DensData2 = cat(1,AISDens_14,AISDens_28);
Group2 = cat(1,g2,g3);
p = anova1(DensData2,Group2);


%% Innervation vs Density plots
p1 = polyfit(AISDens_9,AISInn_9,1);
f1 = polyval(p1,AISDens_9);
p2 = polyfit(AISDens_14,AISInn_14,1);
f2 = polyval(p2,AISDens_14);
p3 = polyfit(AISDens_28,AISInn_28,1);
f3 = polyval(p3,AISDens_28);

%figure
figure;
scatter(AIS9_thrA_Dens,AIS9_thrA_Inn,'x','MarkerEdgeColor','r','SizeData',20)
hold on
scatter(AIS9_thrB_Dens,AIS9_thrB_Inn,'x','MarkerEdgeColor','r','SizeData',70)
hold on
scatter(AIS9_thrC_Dens,AIS9_thrC_Inn,'x','MarkerEdgeColor','r','SizeData',150)
hold on
scatter(AIS14_thrA_Dens,AIS14_thrA_Inn,'x','MarkerEdgeColor','g','SizeData',20)
hold on
scatter(AIS14_thrB_Dens,AIS14_thrB_Inn,'x','MarkerEdgeColor','g','SizeData',70)
hold on
scatter(AIS14_thrC_Dens,AIS14_thrC_Inn,'x','MarkerEdgeColor','g','SizeData',150)
hold on
scatter(AIS28_thrA_Dens,AIS28_thrA_Inn,'x','MarkerEdgeColor','m','SizeData',20)
hold on
scatter(AIS28_thrB_Dens,AIS28_thrB_Inn,'x','MarkerEdgeColor','m','SizeData',70)
hold on
scatter(AIS28_thrC_Dens,AIS28_thrC_Inn,'x','MarkerEdgeColor','m','SizeData',150)
hold on
plot(AISDens_9,f1,'-','Color','r')
hold on
plot(AISDens_14,f2,'-','Color','g')
hold on
plot(AISDens_28,f3,'-','Color','m')
hold on
box off
set(gca,'TickDir','out')
daspect([1 1 1])
set(gca,'Xlim',[0 1])
set(gca,'Ylim',[0 1])
set(gca,'XTick',[0 0.2 0.4 0.6 0.8 1])
set(gca,'YTick',[0 0.2 0.4 0.6 0.8 1])
xlabel('Synapse density (syn/um axonal path length')
ylabel('AIS innervation fraction')
title('L23 - Innervation fraction vs Syn density (AIS Axons)')
%%
lm = fitlm(AISDens_9,AISInn_9) 

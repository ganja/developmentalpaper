%L2/3 Chandelier innervation and axon plots
%load Data from excel

%% Box plots for innnervation of AIS exclusively by ChAxons
MeanChFrac = cat(1, mean(ChFrac_P14AIS),mean(ChFrac_P28AIS));
g1 = ones(size(ChFrac_P14AIS));
g2 = 2*ones(size(ChFrac_P28AIS));
G = cat(1,g1,g2);
Data = cat(1,ChFrac_P14AIS,ChFrac_P28AIS);
 
% boot strp
ChFrac_P14btstrp = bootstrp(10,@mean,ChFrac_P14AIS); SE_14 = std(ChFrac_P14btstrp);
ChFrac_P28btstrp = bootstrp(10,@mean,ChFrac_P28AIS); SE_28 = std(ChFrac_P28btstrp);
BtstrpChFrac = cat(2, ChFrac_P14btstrp,ChFrac_P28btstrp);
SE = cat(1,SE_14,SE_28);
plus = MeanChFrac+SE;
minus = MeanChFrac-SE;

figure;
boxplot(Data,G,'Colors','k')
hold on
plot([1 2],MeanChFrac,'-ko','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',8)
hold on
plot([1 2],plus,'--k')% lplus err line
hold on
plot([1 2],minus,'--k')% lplus err line
hold on
set(gca,'Ylim',[0 1])
set(gca,'Xlim',[0 3])
set(gca,'Xtick',[0 1 2])
box off;set(gca,'TickDir','out')
set(gca,'XtickLabel',[0 14 28])
ylabel('Innervation of AIS by Ch-axons')
xlabel('Postnatal Age (days)')
hold on
scatter(ones(numel(ChFrac_P14AIS),1)*1+rand(numel(ChFrac_P14AIS),1)*.25-.125, ChFrac_P14AIS,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200, 'LineWidth',0.0005);
hold on
scatter(ones(numel(ChFrac_P28AIS),1)*2+rand(numel(ChFrac_P28AIS),1)*.25-.125, ChFrac_P28AIS,'x','MarkerEdgeColor',[0.5 0.5 0.5],'SizeData',200, 'LineWidth',0.0005);
hold on

%% Scatter plots of Ch axons and Ch syn
figure;
scatter(ChAx_P14,ChSyn_P14,'x','MarkerEdgeColor','m','SizeData',200);
hold on
scatter(ChAx_P28,ChSyn_P28,'x','MarkerEdgeColor','g','SizeData',200);
hold on
box off
set(gca,'TickDir','out')
set(gca,'Ylim',[0 25])
set(gca,'Xlim',[0 10])
daspect([1 1 1])
xlabel('#AIS-specific axons per AIS')
ylabel('#syn of AIS-specific axon per AIS')
title('L2/3 - AIS-specific axons and synapses')
